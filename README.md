# Billing engine
This project is using Spring Security and AngularJs.

Please checkout, run a clean build to download all the necessary components and libs needed for the project.

For more information you can read my wordpress post <a href="https://samerabdelkafi.wordpress.com/2016/01/25/secure-angularjs-application-with-spring-security" target="_blank" >here</a>.

<h2>Deployment</h2>
This project uses servlet 3 API. Check if your application server or your servlet container supports servlet 3 spec. 

For Tomcat you can have this information in this link: <a href="http://tomcat.apache.org/whichversion" > tomcat.apache.org/whichversion </a> 

