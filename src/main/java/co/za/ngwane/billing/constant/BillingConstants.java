package co.za.ngwane.billing.constant;

public class BillingConstants {

    public static int FIRST_DAY_OF_THE_MONTH = 1;

    public static final String EMAIL_TO_ADMIN = "mokkhutu@gmail.com";
    public static final String EMAIL_FROM_NOTIFICATION = "mokkhutu@gmail.com";
    public static final String EMAIL_NOTIFICATION_USRNAME = "mokkhutu@gmail.com";
    public static final String EMAIL_NOTIFICATION_PWD = "f@3953335";
    public static final String EMAIL_HOST = "smtp.gmail.com";
    //public static final String EMAIL_HOST = "10.200.50.5";

    public static final String EMAIL_NOTIFICATION_BCC = "bheki.lubisi@gmail.com";
    public static final String EMAIL_HELP_DESK = "info@ngwanesolutions.com";

    /*
     TAG STATS
     */
    public static final String TAG_RESET = "0";
    public static final String TAG_ADD = "1";
    public static final String TAG_UPDATE = "2";
    public static final String TAG_DELETE = "3";

    /*
     LU_STAT
     */
    public static final String LU_STAT_RESET = "00";
    public static final String LU_STAT_ACTIVE = "01";
    public static final String LU_STAT_DELETED = "02";

    /*
     LU_REMINDER_PERIOD
     */
    public static final String LU_REMINDER_DAILY = "01";

    /*
     LU_ORDER_PROCESS_STAT
     */
    public static final String LU_ORDER_PROCESS_STAT_PENDING_DIRECTORS = "01";
    public static final String LU_ORDER_PROCESS_STAT_DIRECTOR_APPROVED = "02";
    public static final String LU_ORDER_PROCESS_STAT_DIRECTOR_REJECETED = "03";
    public static final String LU_ORDER_PROCESS_STAT_DIRECTOR_DECLINED = "04";
    public static final String LU_ORDER_PROCESS_STAT_PENDING_PROC_GENERATION = "05";
    public static final String LU_ORDER_PROCESS_STAT_PROC_GENERETED = "06";

    /*
     LU_BILL_PROCESS_STAT_BE
     */
    public static final String LU_BILL_PROCESS_STAT_PENDING = "01";
    public static final String LU_BILL_PROCESS_STAT_PROCESSED = "02";
    public static final String LU_BILL_PROCESS_STAT_BEING_PROCESSED = "03";
    public static final String LU_BILL_PROCESS_STAT_PROCESSING_FAILED = "04";

    /*
     LU_CUST_TYPE
     */
    public static final String LU_CUST_TYPE_CUSTOMER = "01";
    public static final String LU_CUST_TYPE_BRILLIANTEL = "02";

    /*
     LU_REF_NO_DESC_BE
     */
    public static final String LU_REF_NO_DESC_BE_INVOICE = "01";
    public static final String LU_REF_NO_DESC_BE_INVOICE_SUPPORT_DOC = "02";


    /*
      LU_LAST_NO_TYPE_BE
     */
    public static final String LU_LAST_NO_TYPE_BE_ORDER = "01";

    /*
     BOOLEAN
     */
    public static final String BOOLEAN_FALSE = "01";
    public static final String BOOLEAN_TRUE = "02";

    public static final int COMPANY_BRILLIANTEL = 1;

    /** ***********************
    EMAIL CONSTANTS
     **************************/
    public static final String EMAIL_SEND_URL = "";

}
