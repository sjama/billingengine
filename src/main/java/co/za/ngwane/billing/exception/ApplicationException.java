package co.za.ngwane.billing.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.print.attribute.standard.Severity;

import co.za.ngwane.billing.service.dto.Message;

/**
 * <strong>Description</strong> <br>
 * like validation error
 *
 * @author Jacob
 */

public class ApplicationException extends RuntimeException {

    private List<Message> messages = new ArrayList<Message>();

    public ApplicationException(Message[] allMessages) {
        super(allMessages.length>0?allMessages[0].getText():"???Unknown???");
        this.messages = Arrays.asList(allMessages);
    }

    public ApplicationException(List<Message> messages) {
        super(messages.isEmpty()?"???Unknown???":messages.get(0).getText());
        this.messages = messages;
    }

    public ApplicationException(Message message) {
        super(message.getText());
        this.messages.add(message);
    }

    public ApplicationException(String message) {
        super(message);
        Message msg = new Message("", message, Severity.ERROR);
        this.messages.add(msg);
    }

    public List<Message> getMessages() {
        return this.messages;
    }

}
