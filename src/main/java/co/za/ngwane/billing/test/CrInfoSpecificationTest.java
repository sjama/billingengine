package co.za.ngwane.billing.test;

//package co.za.rtmc.choco.test;
//import static org.junit.Assert.fail;
//
//import java.util.Date;
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//import org.springframework.transaction.annotation.Transactional;
//
//import co.za.rtmc.choco.config.JPAConfig;
//import co.za.rtmc.choco.persist.db.CrInfo;
//import co.za.rtmc.choco.persist.repository.CrInfoRepo;
//import co.za.rtmc.choco.persist.repository.CrInfoSpecification;
//import co.za.rtmc.choco.service.CrInfoService;
//import co.za.rtmc.choco.service.dto.CrashSearchParameters;
//
//@SuppressWarnings("deprecation")
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = { JPAConfig.class })
//@Transactional
//@TransactionConfiguration
//public class CrInfoSpecificationTest {
//	@Autowired
//    private CrInfoRepo repository;
//	
//	private CrashSearchParameters crashSearchDates;
//	private CrashSearchParameters crashSearchCaseN;
//	private CrashSearchParameters crashSearchPoliceStas;
//	CrInfo filter1 = null;
//	CrInfo filter2 = null;
//	CrInfo filter3 = null;
//
//
//	
//	@Before
//    public void init() {
//		crashSearchDates = new CrashSearchParameters();
//		crashSearchDates.setDateFrom(new Date("2017/05/01"));
//		crashSearchDates.setDateTo(new Date("2017/06/31"));
//		crashSearchDates.setProvCd("GP");
//		crashSearchDates.setPoliceStation("7");
//		crashSearchDates.setReportType("0");
//		filter1 = new CrInfoService().getSerachfilter(crashSearchDates);
//		
//		crashSearchCaseN = new CrashSearchParameters();
//		crashSearchCaseN.setCasNumber("CAS2345");
//
//		filter2 = new CrInfoService().getSerachfilter(crashSearchCaseN);
//
//		
//		crashSearchPoliceStas = new CrashSearchParameters();
//		crashSearchPoliceStas.setProvCd("GP");
//		crashSearchPoliceStas.setPoliceStation("7");
//		filter3 = new CrInfoService().getSerachfilter(crashSearchPoliceStas);
//
//	}
//
//	@Test
//	public void testCrashSearchDates() {
//		CrInfoSpecification spec = new CrInfoSpecification(filter1);
//		List<CrInfo> results = repository.findAll(spec);
//		fail("Results " + results);
//	}
//	
////	@Test
////	public void testCrashSearchCaseN() {
////		CrInfoSpecification spec = new CrInfoSpecification(filter2);
////		List<CrInfo> results = repository.findAll(spec);
////		fail("Results " + results);
////	}
////	
////	@Test
////	public void testCrashSearchPoliceStas() {
////		CrInfoSpecification spec = new CrInfoSpecification(filter3);
////		List<CrInfo> results = repository.findAll(spec);
////		fail("Results " + results);
////	}
//
//}
