/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "BILL_DET_BE")
@XmlRootElement
public class BillDetBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ACCOUNT_NO")
    private String accountNo;
    @Column(name = "CALL_DATE")
    @Temporal(TemporalType.DATE)
    private Date callDate;
    @Column(name = "ORIGINATING_NO")
    private String originatingNo;
    @Column(name = "DIALLED_NO")
    private String dialledNo;
    @Column(name = "DURATION_SEC")
    private String durationSec;
    @Column(name = "DURATION_MIN")
    private String durationMin;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AMOUNT_NEO")
    private double amountNeo;

    @Column(name = "AMOUNT_COM")
    private double amountCom;

    @Column(name = "IS_NEO_AND_COM_AMNT_SAME")
    private Boolean isNeoAndComAmntSame;

    @Column(name = "AMOUNT_CLIENT")
    private BigDecimal amountClient;
    @Column(name = "CALL_TYPE")
    private String callType;
    @Column(name = "DESTINATION_COUNTRY")
    private String destinationCountry;
    @Column(name = "EXTENTION_NO")
    private String extentionNo;
    @Basic(optional = false)
    @Column(name = "LU_BILLING_TYPE_CD")
    private String luBillingTypeCd;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;

    @Column(name = "CDR_FILE_REF_NO")
    private String cdrFileRefNo;

    @Transient
    Long custId;

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    @JoinColumn(name = "CUSTOMERS_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private CustomersBe customersId;

    public BillDetBe() {
    }

    public BillDetBe(Long id) {
        this.id = id;
    }

    public BillDetBe(Long id, String luBillingTypeCd) {
        this.id = id;
        this.luBillingTypeCd = luBillingTypeCd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Date getCallDate() {
        return callDate;
    }

    public void setCallDate(Date callDate) {
        this.callDate = callDate;
    }

    public String getOriginatingNo() {
        return originatingNo;
    }

    public void setOriginatingNo(String originatingNo) {
        this.originatingNo = originatingNo;
    }

    public String getDialledNo() {
        return dialledNo;
    }

    public void setDialledNo(String dialledNo) {
        this.dialledNo = dialledNo;
    }

    public String getDurationSec() {
        return durationSec;
    }

    public void setDurationSec(String durationSec) {
        this.durationSec = durationSec;
    }

    public String getDurationMin() {
        return durationMin;
    }

    public void setDurationMin(String durationMin) {
        this.durationMin = durationMin;
    }

    public double getAmountNeo() {
        return amountNeo;
    }

    public void setAmountNeo(double amountNeo) {
        this.amountNeo = amountNeo;
    }

    public BigDecimal getAmountClient() {
        return amountClient;
    }

    public void setAmountClient(BigDecimal amountClient) {
        this.amountClient = amountClient;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(String destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public String getExtentionNo() {
        return extentionNo;
    }

    public void setExtentionNo(String extentionNo) {
        this.extentionNo = extentionNo;
    }

    public String getLuBillingTypeCd() {
        return luBillingTypeCd;
    }

    public void setLuBillingTypeCd(String luBillingTypeCd) {
        this.luBillingTypeCd = luBillingTypeCd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public CustomersBe getCustomersId() {
        return customersId;
    }

    public void setCustomersId(CustomersBe customersId) {
        this.customersId = customersId;
    }

    public String getCdrFileRefNo() {
        return cdrFileRefNo;
    }

    public void setCdrFileRefNo(String cdrFileRefNo) {
        this.cdrFileRefNo = cdrFileRefNo;
    }

    public double getAmountCom() {
        return amountCom;
    }

    public void setAmountCom(double amountCom) {
        this.amountCom = amountCom;
    }

    public Boolean getNeoAndComAmntSame() {
        return isNeoAndComAmntSame;
    }

    public void setNeoAndComAmntSame(Boolean neoAndComAmntSame) {
        isNeoAndComAmntSame = neoAndComAmntSame;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BillDetBe)) {
            return false;
        }
        BillDetBe other = (BillDetBe) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.BillDetBe[ id=" + id + " ]";
    }

}
