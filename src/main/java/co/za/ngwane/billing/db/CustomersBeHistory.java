/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "CUSTOMERS_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomersBeHistory.findAll", query = "SELECT c FROM CustomersBeHistory c")
    , @NamedQuery(name = "CustomersBeHistory.findById", query = "SELECT c FROM CustomersBeHistory c WHERE c.customersBeHistoryPK.id = :id")
    , @NamedQuery(name = "CustomersBeHistory.findByRev", query = "SELECT c FROM CustomersBeHistory c WHERE c.customersBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "CustomersBeHistory.findByRevtype", query = "SELECT c FROM CustomersBeHistory c WHERE c.revtype = :revtype")
    , @NamedQuery(name = "CustomersBeHistory.findByAccountNo", query = "SELECT c FROM CustomersBeHistory c WHERE c.accountNo = :accountNo")
    , @NamedQuery(name = "CustomersBeHistory.findByAgreedRate", query = "SELECT c FROM CustomersBeHistory c WHERE c.agreedRate = :agreedRate")
    , @NamedQuery(name = "CustomersBeHistory.findByCustVatNo", query = "SELECT c FROM CustomersBeHistory c WHERE c.custVatNo = :custVatNo")
    , @NamedQuery(name = "CustomersBeHistory.findByLogoRefn", query = "SELECT c FROM CustomersBeHistory c WHERE c.logoRefn = :logoRefn")
    , @NamedQuery(name = "CustomersBeHistory.findByName", query = "SELECT c FROM CustomersBeHistory c WHERE c.name = :name")
    , @NamedQuery(name = "CustomersBeHistory.findByOriginatingNo", query = "SELECT c FROM CustomersBeHistory c WHERE c.originatingNo = :originatingNo")
    , @NamedQuery(name = "CustomersBeHistory.findByPhyAddrCity", query = "SELECT c FROM CustomersBeHistory c WHERE c.phyAddrCity = :phyAddrCity")
    , @NamedQuery(name = "CustomersBeHistory.findByPhyAddrCode", query = "SELECT c FROM CustomersBeHistory c WHERE c.phyAddrCode = :phyAddrCode")
    , @NamedQuery(name = "CustomersBeHistory.findByPhyAddrLine1", query = "SELECT c FROM CustomersBeHistory c WHERE c.phyAddrLine1 = :phyAddrLine1")
    , @NamedQuery(name = "CustomersBeHistory.findByPhyAddrLine2", query = "SELECT c FROM CustomersBeHistory c WHERE c.phyAddrLine2 = :phyAddrLine2")
    , @NamedQuery(name = "CustomersBeHistory.findByPhyAddrLine3", query = "SELECT c FROM CustomersBeHistory c WHERE c.phyAddrLine3 = :phyAddrLine3")
    , @NamedQuery(name = "CustomersBeHistory.findByPhyAddrSuburb", query = "SELECT c FROM CustomersBeHistory c WHERE c.phyAddrSuburb = :phyAddrSuburb")
    , @NamedQuery(name = "CustomersBeHistory.findByPosAddrCity", query = "SELECT c FROM CustomersBeHistory c WHERE c.posAddrCity = :posAddrCity")
    , @NamedQuery(name = "CustomersBeHistory.findByPosAddrCode", query = "SELECT c FROM CustomersBeHistory c WHERE c.posAddrCode = :posAddrCode")
    , @NamedQuery(name = "CustomersBeHistory.findByPosAddrLine1", query = "SELECT c FROM CustomersBeHistory c WHERE c.posAddrLine1 = :posAddrLine1")
    , @NamedQuery(name = "CustomersBeHistory.findByPosAddrLine2", query = "SELECT c FROM CustomersBeHistory c WHERE c.posAddrLine2 = :posAddrLine2")
    , @NamedQuery(name = "CustomersBeHistory.findByPosAddrLine3", query = "SELECT c FROM CustomersBeHistory c WHERE c.posAddrLine3 = :posAddrLine3")
    , @NamedQuery(name = "CustomersBeHistory.findByPosAddrSuburb", query = "SELECT c FROM CustomersBeHistory c WHERE c.posAddrSuburb = :posAddrSuburb")
    , @NamedQuery(name = "CustomersBeHistory.findByStatChgD", query = "SELECT c FROM CustomersBeHistory c WHERE c.statChgD = :statChgD")
    , @NamedQuery(name = "CustomersBeHistory.findByStatus", query = "SELECT c FROM CustomersBeHistory c WHERE c.status = :status")
    , @NamedQuery(name = "CustomersBeHistory.findByLuCustTypeCd", query = "SELECT c FROM CustomersBeHistory c WHERE c.luCustTypeCd = :luCustTypeCd")})
public class CustomersBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CustomersBeHistoryPK customersBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "ACCOUNT_NO")
    private String accountNo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AGREED_RATE")
    private Float agreedRate;
    @Column(name = "CUST_VAT_NO")
    private String custVatNo;
    @Column(name = "LOGO_REFN")
    private String logoRefn;
    @Column(name = "NAME")
    private String name;
    @Column(name = "ORIGINATING_NO")
    private String originatingNo;
    @Column(name = "PHY_ADDR_CITY")
    private String phyAddrCity;
    @Column(name = "PHY_ADDR_CODE")
    private String phyAddrCode;
    @Column(name = "PHY_ADDR_LINE1")
    private String phyAddrLine1;
    @Column(name = "PHY_ADDR_LINE2")
    private String phyAddrLine2;
    @Column(name = "PHY_ADDR_LINE3")
    private String phyAddrLine3;
    @Column(name = "PHY_ADDR_SUBURB")
    private String phyAddrSuburb;
    @Column(name = "POS_ADDR_CITY")
    private String posAddrCity;
    @Column(name = "POS_ADDR_CODE")
    private String posAddrCode;
    @Column(name = "POS_ADDR_LINE1")
    private String posAddrLine1;
    @Column(name = "POS_ADDR_LINE2")
    private String posAddrLine2;
    @Column(name = "POS_ADDR_LINE3")
    private String posAddrLine3;
    @Column(name = "POS_ADDR_SUBURB")
    private String posAddrSuburb;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "LU_CUST_TYPE_CD")
    private String luCustTypeCd;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public CustomersBeHistory() {
    }

    public CustomersBeHistory(CustomersBeHistoryPK customersBeHistoryPK) {
        this.customersBeHistoryPK = customersBeHistoryPK;
    }

    public CustomersBeHistory(long id, int rev) {
        this.customersBeHistoryPK = new CustomersBeHistoryPK(id, rev);
    }

    public CustomersBeHistoryPK getCustomersBeHistoryPK() {
        return customersBeHistoryPK;
    }

    public void setCustomersBeHistoryPK(CustomersBeHistoryPK customersBeHistoryPK) {
        this.customersBeHistoryPK = customersBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Float getAgreedRate() {
        return agreedRate;
    }

    public void setAgreedRate(Float agreedRate) {
        this.agreedRate = agreedRate;
    }

    public String getCustVatNo() {
        return custVatNo;
    }

    public void setCustVatNo(String custVatNo) {
        this.custVatNo = custVatNo;
    }

    public String getLogoRefn() {
        return logoRefn;
    }

    public void setLogoRefn(String logoRefn) {
        this.logoRefn = logoRefn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginatingNo() {
        return originatingNo;
    }

    public void setOriginatingNo(String originatingNo) {
        this.originatingNo = originatingNo;
    }

    public String getPhyAddrCity() {
        return phyAddrCity;
    }

    public void setPhyAddrCity(String phyAddrCity) {
        this.phyAddrCity = phyAddrCity;
    }

    public String getPhyAddrCode() {
        return phyAddrCode;
    }

    public void setPhyAddrCode(String phyAddrCode) {
        this.phyAddrCode = phyAddrCode;
    }

    public String getPhyAddrLine1() {
        return phyAddrLine1;
    }

    public void setPhyAddrLine1(String phyAddrLine1) {
        this.phyAddrLine1 = phyAddrLine1;
    }

    public String getPhyAddrLine2() {
        return phyAddrLine2;
    }

    public void setPhyAddrLine2(String phyAddrLine2) {
        this.phyAddrLine2 = phyAddrLine2;
    }

    public String getPhyAddrLine3() {
        return phyAddrLine3;
    }

    public void setPhyAddrLine3(String phyAddrLine3) {
        this.phyAddrLine3 = phyAddrLine3;
    }

    public String getPhyAddrSuburb() {
        return phyAddrSuburb;
    }

    public void setPhyAddrSuburb(String phyAddrSuburb) {
        this.phyAddrSuburb = phyAddrSuburb;
    }

    public String getPosAddrCity() {
        return posAddrCity;
    }

    public void setPosAddrCity(String posAddrCity) {
        this.posAddrCity = posAddrCity;
    }

    public String getPosAddrCode() {
        return posAddrCode;
    }

    public void setPosAddrCode(String posAddrCode) {
        this.posAddrCode = posAddrCode;
    }

    public String getPosAddrLine1() {
        return posAddrLine1;
    }

    public void setPosAddrLine1(String posAddrLine1) {
        this.posAddrLine1 = posAddrLine1;
    }

    public String getPosAddrLine2() {
        return posAddrLine2;
    }

    public void setPosAddrLine2(String posAddrLine2) {
        this.posAddrLine2 = posAddrLine2;
    }

    public String getPosAddrLine3() {
        return posAddrLine3;
    }

    public void setPosAddrLine3(String posAddrLine3) {
        this.posAddrLine3 = posAddrLine3;
    }

    public String getPosAddrSuburb() {
        return posAddrSuburb;
    }

    public void setPosAddrSuburb(String posAddrSuburb) {
        this.posAddrSuburb = posAddrSuburb;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLuCustTypeCd() {
        return luCustTypeCd;
    }

    public void setLuCustTypeCd(String luCustTypeCd) {
        this.luCustTypeCd = luCustTypeCd;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customersBeHistoryPK != null ? customersBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomersBeHistory)) {
            return false;
        }
        CustomersBeHistory other = (CustomersBeHistory) object;
        if ((this.customersBeHistoryPK == null && other.customersBeHistoryPK != null) || (this.customersBeHistoryPK != null && !this.customersBeHistoryPK.equals(other.customersBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.CustomersBeHistory[ customersBeHistoryPK=" + customersBeHistoryPK + " ]";
    }
    
}
