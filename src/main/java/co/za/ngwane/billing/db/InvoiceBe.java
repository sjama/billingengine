/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author bheki.lubisi
 */
@Entity
@Table(name = "INVOICE_BE")
@XmlRootElement
public class InvoiceBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "INV_NO")
    private String invNo;
    @Column(name = "INV_DATE")
    @Temporal(TemporalType.DATE)
    private Date invDate;
    @Column(name = "INV_START_DATE")
    @Temporal(TemporalType.DATE)
    private Date invStartDate;
    @Column(name = "INV_END_DATE")
    @Temporal(TemporalType.DATE)
    private Date invEndDate;
    @Column(name = "REF_NO")
    private String refNo;
    @JoinColumn(name = "CUSTOMERS_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private CustomersBe customersId;
    @Column(name = "INV_AMOUNT")
    private BigDecimal invAmout;
    @Column(name = "INV_TOT_MIN")
    private BigDecimal invTotMin;

    @Column(name = "CDR_FILE_REF_NO")
    private String cdrFileRefNo;

    @Transient
    private BigDecimal totInvAmout;

    public BigDecimal getInvAmout() {
        return invAmout;
    }

    public void setInvAmout(BigDecimal invAmout) {
        this.invAmout = invAmout;
    }

    public BigDecimal getInvTotMin() {
        return invTotMin;
    }

    public void setInvTotMin(BigDecimal invTotMin) {
        this.invTotMin = invTotMin;
    }

    public InvoiceBe() {
    }

    public InvoiceBe(String invNo) {
        this.invNo = invNo;
    }

    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    public Date getInvDate() {
        return invDate;
    }

    public void setInvDate(Date invDate) {
        this.invDate = invDate;
    }

    public Date getInvStartDate() {
        return invStartDate;
    }

    public void setInvStartDate(Date invStartDate) {
        this.invStartDate = invStartDate;
    }

    public Date getInvEndDate() {
        return invEndDate;
    }

    public void setInvEndDate(Date invEndDate) {
        this.invEndDate = invEndDate;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public CustomersBe getCustomersId() {
        return customersId;
    }

    public void setCustomersId(CustomersBe customersId) {
        this.customersId = customersId;
    }

    public String getCdrFileRefNo() {
        return cdrFileRefNo;
    }

    public void setCdrFileRefNo(String cdrFileRefNo) {
        this.cdrFileRefNo = cdrFileRefNo;
    }

    public BigDecimal getTotInvAmout() {
        return totInvAmout;
    }

    public void setTotInvAmout(BigDecimal totInvAmout) {
        this.totInvAmout = totInvAmout;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invNo != null ? invNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvoiceBe)) {
            return false;
        }
        InvoiceBe other = (InvoiceBe) object;
        return (this.invNo != null || other.invNo == null) && (this.invNo == null || this.invNo.equals(other.invNo));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.InvoiceBe[ invNo=" + invNo + " ]";
    }


}
