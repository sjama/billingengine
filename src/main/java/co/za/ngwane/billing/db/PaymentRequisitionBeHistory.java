/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "PAYMENT_REQUISITION_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentRequisitionBeHistory.findAll", query = "SELECT p FROM PaymentRequisitionBeHistory p")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findById", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.paymentRequisitionBeHistoryPK.id = :id")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByRev", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.paymentRequisitionBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByRevtype", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.revtype = :revtype")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByAmountRequested", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.amountRequested = :amountRequested")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByCreatedDate", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.createdDate = :createdDate")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByLuPayRequisitionProcessStatCd", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.luPayRequisitionProcessStatCd = :luPayRequisitionProcessStatCd")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByRefNo", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.refNo = :refNo")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByServiceDesc", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.serviceDesc = :serviceDesc")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByStatChgDate", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.statChgDate = :statChgDate")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByStatus", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.status = :status")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByCreatedByUserId", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.createdByUserId = :createdByUserId")
    , @NamedQuery(name = "PaymentRequisitionBeHistory.findByPurchaseOrderId", query = "SELECT p FROM PaymentRequisitionBeHistory p WHERE p.purchaseOrderId = :purchaseOrderId")})
public class PaymentRequisitionBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PaymentRequisitionBeHistoryPK paymentRequisitionBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AMOUNT_REQUESTED")
    private Float amountRequested;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "LU_PAY_REQUISITION_PROCESS_STAT_CD")
    private String luPayRequisitionProcessStatCd;
    @Column(name = "REF_NO")
    private String refNo;
    @Column(name = "SERVICE_DESC")
    private String serviceDesc;
    @Column(name = "STAT_CHG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "CREATED_BY_USER_ID")
    private Integer createdByUserId;
    @Column(name = "PURCHASE_ORDER_ID")
    private BigInteger purchaseOrderId;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public PaymentRequisitionBeHistory() {
    }

    public PaymentRequisitionBeHistory(PaymentRequisitionBeHistoryPK paymentRequisitionBeHistoryPK) {
        this.paymentRequisitionBeHistoryPK = paymentRequisitionBeHistoryPK;
    }

    public PaymentRequisitionBeHistory(long id, int rev) {
        this.paymentRequisitionBeHistoryPK = new PaymentRequisitionBeHistoryPK(id, rev);
    }

    public PaymentRequisitionBeHistoryPK getPaymentRequisitionBeHistoryPK() {
        return paymentRequisitionBeHistoryPK;
    }

    public void setPaymentRequisitionBeHistoryPK(PaymentRequisitionBeHistoryPK paymentRequisitionBeHistoryPK) {
        this.paymentRequisitionBeHistoryPK = paymentRequisitionBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Float getAmountRequested() {
        return amountRequested;
    }

    public void setAmountRequested(Float amountRequested) {
        this.amountRequested = amountRequested;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLuPayRequisitionProcessStatCd() {
        return luPayRequisitionProcessStatCd;
    }

    public void setLuPayRequisitionProcessStatCd(String luPayRequisitionProcessStatCd) {
        this.luPayRequisitionProcessStatCd = luPayRequisitionProcessStatCd;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public Date getStatChgDate() {
        return statChgDate;
    }

    public void setStatChgDate(Date statChgDate) {
        this.statChgDate = statChgDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(Integer createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public BigInteger getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(BigInteger purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paymentRequisitionBeHistoryPK != null ? paymentRequisitionBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentRequisitionBeHistory)) {
            return false;
        }
        PaymentRequisitionBeHistory other = (PaymentRequisitionBeHistory) object;
        if ((this.paymentRequisitionBeHistoryPK == null && other.paymentRequisitionBeHistoryPK != null) || (this.paymentRequisitionBeHistoryPK != null && !this.paymentRequisitionBeHistoryPK.equals(other.paymentRequisitionBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.PaymentRequisitionBeHistory[ paymentRequisitionBeHistoryPK=" + paymentRequisitionBeHistoryPK + " ]";
    }
    
}
