/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author bheki.lubisi
 */
@Embeddable
public class PurchaseOrderBeHistoryPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "ID")
    private long id;
    @Basic(optional = false)
    @Column(name = "REV")
    private int rev;

    public PurchaseOrderBeHistoryPK() {
    }

    public PurchaseOrderBeHistoryPK(long id, int rev) {
        this.id = id;
        this.rev = rev;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRev() {
        return rev;
    }

    public void setRev(int rev) {
        this.rev = rev;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) rev;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PurchaseOrderBeHistoryPK)) {
            return false;
        }
        PurchaseOrderBeHistoryPK other = (PurchaseOrderBeHistoryPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.rev != other.rev) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.PurchaseOrderBeHistoryPK[ id=" + id + ", rev=" + rev + " ]";
    }
    
}
