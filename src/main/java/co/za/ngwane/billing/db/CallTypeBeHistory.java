/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "CALL_TYPE_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CallTypeBeHistory.findAll", query = "SELECT c FROM CallTypeBeHistory c")
    , @NamedQuery(name = "CallTypeBeHistory.findByCode", query = "SELECT c FROM CallTypeBeHistory c WHERE c.callTypeBeHistoryPK.code = :code")
    , @NamedQuery(name = "CallTypeBeHistory.findByRev", query = "SELECT c FROM CallTypeBeHistory c WHERE c.callTypeBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "CallTypeBeHistory.findByRevtype", query = "SELECT c FROM CallTypeBeHistory c WHERE c.revtype = :revtype")
    , @NamedQuery(name = "CallTypeBeHistory.findByDescription", query = "SELECT c FROM CallTypeBeHistory c WHERE c.description = :description")})
public class CallTypeBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CallTypeBeHistoryPK callTypeBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "DESCRIPTION")
    private String description;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public CallTypeBeHistory() {
    }

    public CallTypeBeHistory(CallTypeBeHistoryPK callTypeBeHistoryPK) {
        this.callTypeBeHistoryPK = callTypeBeHistoryPK;
    }

    public CallTypeBeHistory(String code, int rev) {
        this.callTypeBeHistoryPK = new CallTypeBeHistoryPK(code, rev);
    }

    public CallTypeBeHistoryPK getCallTypeBeHistoryPK() {
        return callTypeBeHistoryPK;
    }

    public void setCallTypeBeHistoryPK(CallTypeBeHistoryPK callTypeBeHistoryPK) {
        this.callTypeBeHistoryPK = callTypeBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (callTypeBeHistoryPK != null ? callTypeBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CallTypeBeHistory)) {
            return false;
        }
        CallTypeBeHistory other = (CallTypeBeHistory) object;
        if ((this.callTypeBeHistoryPK == null && other.callTypeBeHistoryPK != null) || (this.callTypeBeHistoryPK != null && !this.callTypeBeHistoryPK.equals(other.callTypeBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.CallTypeBeHistory[ callTypeBeHistoryPK=" + callTypeBeHistoryPK + " ]";
    }
    
}
