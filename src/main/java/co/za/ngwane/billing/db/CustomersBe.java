/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "CUSTOMERS_BE")
@XmlRootElement
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
public class CustomersBe implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "NAME")
    private String name;
    @Column(name = "ACCOUNT_NO")
    private String accountNo;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Basic(optional = false)
    @Column(name = "ORIGINATING_NO")
    private String originatingNo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AGREED_RATE")
    private Float agreedRate;
    @JoinColumn(name = "LU_CUST_TYPE_CD", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private LuCustTypeBe luCustTypeBeCd;
    
    @Column(name = "CUST_VAT_NO")
    private String custVatNo;
    @Column(name = "PHY_ADDR_LINE1")
    private String phyAddrLine1;
    @Column(name = "PHY_ADDR_LINE2")
    private String phyAddrLine2;
    @Column(name = "PHY_ADDR_LINE3")
    private String phyAddrLine3;
    @Column(name = "PHY_ADDR_SUBURB")
    private String phyAddrSuburb;
    @Column(name = "PHY_ADDR_CITY")
    private String phyAddrCity;
    @Column(name = "PHY_ADDR_CODE")
    private String phyAddrCode;
    @Column(name = "POS_ADDR_LINE1")
    private String posAddrLine1;
    @Column(name = "POS_ADDR_LINE2")
    private String posAddrLine2;
    @Column(name = "POS_ADDR_LINE3")
    private String posAddrLine3;
    @Column(name = "POS_ADDR_SUBURB")
    private String posAddrSuburb;
    @Column(name = "POS_ADDR_CITY")
    private String posAddrCity;
    @Column(name = "POS_ADDR_CODE")
    private String posAddrCode;
    @Column(name = "LOGO_REFN")
    private String logoRefn;
    
    @Transient
    private String tagName;
    @Transient
    private String tagFlag;
    @Transient
    private String tagMsg;

    public CustomersBe() {
    }

    public CustomersBe(Long id) {
        this.id = id;
    }

    public CustomersBe(Long id, int addedByEmployeesId) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }


    public String getOriginatingNo() {
        return originatingNo;
    }

    public void setOriginatingNo(String originatingNo) {
        this.originatingNo = originatingNo;
    }

    public Float getAgreedRate() {
        return agreedRate;
    }

    public void setAgreedRate(Float agreedRate) {
        this.agreedRate = agreedRate;
    }

    public LuCustTypeBe getLuCustTypeBeCd() {
        return luCustTypeBeCd;
    }

    public void setLuCustTypeBeCd(LuCustTypeBe luCustTypeBeCd) {
        this.luCustTypeBeCd = luCustTypeBeCd;
    }

    public String getCustVatNo() {
        return custVatNo;
    }

    public void setCustVatNo(String custVatNo) {
        this.custVatNo = custVatNo;
    }

    public String getPhyAddrLine1() {
        return phyAddrLine1;
    }

    public void setPhyAddrLine1(String phyAddrLine1) {
        this.phyAddrLine1 = phyAddrLine1;
    }

    public String getPhyAddrLine2() {
        return phyAddrLine2;
    }

    public void setPhyAddrLine2(String phyAddrLine2) {
        this.phyAddrLine2 = phyAddrLine2;
    }

    public String getPhyAddrLine3() {
        return phyAddrLine3;
    }

    public void setPhyAddrLine3(String phyAddrLine3) {
        this.phyAddrLine3 = phyAddrLine3;
    }

    public String getPhyAddrSuburb() {
        return phyAddrSuburb;
    }

    public void setPhyAddrSuburb(String phyAddrSuburb) {
        this.phyAddrSuburb = phyAddrSuburb;
    }

    public String getPhyAddrCity() {
        return phyAddrCity;
    }

    public void setPhyAddrCity(String phyAddrCity) {
        this.phyAddrCity = phyAddrCity;
    }

    public String getPhyAddrCode() {
        return phyAddrCode;
    }

    public void setPhyAddrCode(String phyAddrCode) {
        this.phyAddrCode = phyAddrCode;
    }

    public String getPosAddrLine1() {
        return posAddrLine1;
    }

    public void setPosAddrLine1(String posAddrLine1) {
        this.posAddrLine1 = posAddrLine1;
    }

    public String getPosAddrLine2() {
        return posAddrLine2;
    }

    public void setPosAddrLine2(String posAddrLine2) {
        this.posAddrLine2 = posAddrLine2;
    }

    public String getPosAddrLine3() {
        return posAddrLine3;
    }

    public void setPosAddrLine3(String posAddrLine3) {
        this.posAddrLine3 = posAddrLine3;
    }

    public String getPosAddrSuburb() {
        return posAddrSuburb;
    }

    public void setPosAddrSuburb(String posAddrSuburb) {
        this.posAddrSuburb = posAddrSuburb;
    }

    public String getPosAddrCity() {
        return posAddrCity;
    }

    public void setPosAddrCity(String posAddrCity) {
        this.posAddrCity = posAddrCity;
    }

    public String getPosAddrCode() {
        return posAddrCode;
    }

    public void setPosAddrCode(String posAddrCode) {
        this.posAddrCode = posAddrCode;
    }

    public String getLogoRefn() {
        return logoRefn;
    }

    public void setLogoRefn(String logoRefn) {
        this.logoRefn = logoRefn;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagFlag() {
        return tagFlag;
    }

    public void setTagFlag(String tagFlag) {
        this.tagFlag = tagFlag;
    }

    public String getTagMsg() {
        return tagMsg;
    }

    public void setTagMsg(String tagMsg) {
        this.tagMsg = tagMsg;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomersBe)) {
            return false;
        }
        CustomersBe other = (CustomersBe) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.CustomersBe[ id=" + id + " ]";
    }
    
}
