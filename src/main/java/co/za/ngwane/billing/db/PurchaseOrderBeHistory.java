/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "PURCHASE_ORDER_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PurchaseOrderBeHistory.findAll", query = "SELECT p FROM PurchaseOrderBeHistory p")
    , @NamedQuery(name = "PurchaseOrderBeHistory.findById", query = "SELECT p FROM PurchaseOrderBeHistory p WHERE p.purchaseOrderBeHistoryPK.id = :id")
    , @NamedQuery(name = "PurchaseOrderBeHistory.findByRev", query = "SELECT p FROM PurchaseOrderBeHistory p WHERE p.purchaseOrderBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "PurchaseOrderBeHistory.findByRevtype", query = "SELECT p FROM PurchaseOrderBeHistory p WHERE p.revtype = :revtype")
    , @NamedQuery(name = "PurchaseOrderBeHistory.findByPoNo", query = "SELECT p FROM PurchaseOrderBeHistory p WHERE p.poNo = :poNo")
    , @NamedQuery(name = "PurchaseOrderBeHistory.findByRefNo", query = "SELECT p FROM PurchaseOrderBeHistory p WHERE p.refNo = :refNo")
    , @NamedQuery(name = "PurchaseOrderBeHistory.findByStatChgDate", query = "SELECT p FROM PurchaseOrderBeHistory p WHERE p.statChgDate = :statChgDate")
    , @NamedQuery(name = "PurchaseOrderBeHistory.findByStatus", query = "SELECT p FROM PurchaseOrderBeHistory p WHERE p.status = :status")
    , @NamedQuery(name = "PurchaseOrderBeHistory.findByGeneratedByUserId", query = "SELECT p FROM PurchaseOrderBeHistory p WHERE p.generatedByUserId = :generatedByUserId")
    , @NamedQuery(name = "PurchaseOrderBeHistory.findByProcurementId", query = "SELECT p FROM PurchaseOrderBeHistory p WHERE p.procurementId = :procurementId")})
public class PurchaseOrderBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PurchaseOrderBeHistoryPK purchaseOrderBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "PO_NO")
    private String poNo;
    @Column(name = "REF_NO")
    private String refNo;
    @Column(name = "STAT_CHG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "GENERATED_BY_USER_ID")
    private Integer generatedByUserId;
    @Column(name = "PROCUREMENT_ID")
    private BigInteger procurementId;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public PurchaseOrderBeHistory() {
    }

    public PurchaseOrderBeHistory(PurchaseOrderBeHistoryPK purchaseOrderBeHistoryPK) {
        this.purchaseOrderBeHistoryPK = purchaseOrderBeHistoryPK;
    }

    public PurchaseOrderBeHistory(long id, int rev) {
        this.purchaseOrderBeHistoryPK = new PurchaseOrderBeHistoryPK(id, rev);
    }

    public PurchaseOrderBeHistoryPK getPurchaseOrderBeHistoryPK() {
        return purchaseOrderBeHistoryPK;
    }

    public void setPurchaseOrderBeHistoryPK(PurchaseOrderBeHistoryPK purchaseOrderBeHistoryPK) {
        this.purchaseOrderBeHistoryPK = purchaseOrderBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getPoNo() {
        return poNo;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Date getStatChgDate() {
        return statChgDate;
    }

    public void setStatChgDate(Date statChgDate) {
        this.statChgDate = statChgDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getGeneratedByUserId() {
        return generatedByUserId;
    }

    public void setGeneratedByUserId(Integer generatedByUserId) {
        this.generatedByUserId = generatedByUserId;
    }

    public BigInteger getProcurementId() {
        return procurementId;
    }

    public void setProcurementId(BigInteger procurementId) {
        this.procurementId = procurementId;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (purchaseOrderBeHistoryPK != null ? purchaseOrderBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PurchaseOrderBeHistory)) {
            return false;
        }
        PurchaseOrderBeHistory other = (PurchaseOrderBeHistory) object;
        if ((this.purchaseOrderBeHistoryPK == null && other.purchaseOrderBeHistoryPK != null) || (this.purchaseOrderBeHistoryPK != null && !this.purchaseOrderBeHistoryPK.equals(other.purchaseOrderBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.PurchaseOrderBeHistory[ purchaseOrderBeHistoryPK=" + purchaseOrderBeHistoryPK + " ]";
    }
    
}
