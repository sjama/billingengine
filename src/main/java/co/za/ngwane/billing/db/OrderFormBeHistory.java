/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "ORDER_FORM_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderFormBeHistory.findAll", query = "SELECT o FROM OrderFormBeHistory o")
    , @NamedQuery(name = "OrderFormBeHistory.findById", query = "SELECT o FROM OrderFormBeHistory o WHERE o.orderFormBeHistoryPK.id = :id")
    , @NamedQuery(name = "OrderFormBeHistory.findByRev", query = "SELECT o FROM OrderFormBeHistory o WHERE o.orderFormBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "OrderFormBeHistory.findByRevtype", query = "SELECT o FROM OrderFormBeHistory o WHERE o.revtype = :revtype")
    , @NamedQuery(name = "OrderFormBeHistory.findByContactPersonId", query = "SELECT o FROM OrderFormBeHistory o WHERE o.contactPersonId = :contactPersonId")
    , @NamedQuery(name = "OrderFormBeHistory.findByCreatedDate", query = "SELECT o FROM OrderFormBeHistory o WHERE o.createdDate = :createdDate")
    , @NamedQuery(name = "OrderFormBeHistory.findByDeliveryDate", query = "SELECT o FROM OrderFormBeHistory o WHERE o.deliveryDate = :deliveryDate")
    , @NamedQuery(name = "OrderFormBeHistory.findByNoDaysLeft", query = "SELECT o FROM OrderFormBeHistory o WHERE o.noDaysLeft = :noDaysLeft")
    , @NamedQuery(name = "OrderFormBeHistory.findByOrderDesc", query = "SELECT o FROM OrderFormBeHistory o WHERE o.orderDesc = :orderDesc")
    , @NamedQuery(name = "OrderFormBeHistory.findByOrderNo", query = "SELECT o FROM OrderFormBeHistory o WHERE o.orderNo = :orderNo")
    , @NamedQuery(name = "OrderFormBeHistory.findByProjId", query = "SELECT o FROM OrderFormBeHistory o WHERE o.projId = :projId")
    , @NamedQuery(name = "OrderFormBeHistory.findByProjName", query = "SELECT o FROM OrderFormBeHistory o WHERE o.projName = :projName")
    , @NamedQuery(name = "OrderFormBeHistory.findByProjNo", query = "SELECT o FROM OrderFormBeHistory o WHERE o.projNo = :projNo")
    , @NamedQuery(name = "OrderFormBeHistory.findByRefNo", query = "SELECT o FROM OrderFormBeHistory o WHERE o.refNo = :refNo")
    , @NamedQuery(name = "OrderFormBeHistory.findByRequestDate", query = "SELECT o FROM OrderFormBeHistory o WHERE o.requestDate = :requestDate")
    , @NamedQuery(name = "OrderFormBeHistory.findByStatChgDate", query = "SELECT o FROM OrderFormBeHistory o WHERE o.statChgDate = :statChgDate")
    , @NamedQuery(name = "OrderFormBeHistory.findByStatus", query = "SELECT o FROM OrderFormBeHistory o WHERE o.status = :status")
    , @NamedQuery(name = "OrderFormBeHistory.findByCapturedByUsersId", query = "SELECT o FROM OrderFormBeHistory o WHERE o.capturedByUsersId = :capturedByUsersId")
    , @NamedQuery(name = "OrderFormBeHistory.findByCustomersId", query = "SELECT o FROM OrderFormBeHistory o WHERE o.customersId = :customersId")
    , @NamedQuery(name = "OrderFormBeHistory.findByLuReminderPeriodCd", query = "SELECT o FROM OrderFormBeHistory o WHERE o.luReminderPeriodCd = :luReminderPeriodCd")
    , @NamedQuery(name = "OrderFormBeHistory.findByRequestedByUsersId", query = "SELECT o FROM OrderFormBeHistory o WHERE o.requestedByUsersId = :requestedByUsersId")})
public class OrderFormBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrderFormBeHistoryPK orderFormBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "CONTACT_PERSON_ID")
    private BigInteger contactPersonId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "DELIVERY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryDate;
    @Column(name = "NO_DAYS_LEFT")
    private Integer noDaysLeft;
    @Column(name = "ORDER_DESC")
    private String orderDesc;
    @Column(name = "ORDER_NO")
    private String orderNo;
    @Column(name = "PROJ_ID")
    private Integer projId;
    @Column(name = "PROJ_NAME")
    private String projName;
    @Column(name = "PROJ_NO")
    private String projNo;
    @Column(name = "REF_NO")
    private String refNo;
    @Column(name = "REQUEST_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;
    @Column(name = "STAT_CHG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "CAPTURED_BY_USERS_ID")
    private Integer capturedByUsersId;
    @Column(name = "CUSTOMERS_ID")
    private BigInteger customersId;
    @Column(name = "LU_REMINDER_PERIOD_CD")
    private String luReminderPeriodCd;
    @Column(name = "REQUESTED_BY_USERS_ID")
    private Integer requestedByUsersId;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public OrderFormBeHistory() {
    }

    public OrderFormBeHistory(OrderFormBeHistoryPK orderFormBeHistoryPK) {
        this.orderFormBeHistoryPK = orderFormBeHistoryPK;
    }

    public OrderFormBeHistory(long id, int rev) {
        this.orderFormBeHistoryPK = new OrderFormBeHistoryPK(id, rev);
    }

    public OrderFormBeHistoryPK getOrderFormBeHistoryPK() {
        return orderFormBeHistoryPK;
    }

    public void setOrderFormBeHistoryPK(OrderFormBeHistoryPK orderFormBeHistoryPK) {
        this.orderFormBeHistoryPK = orderFormBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public BigInteger getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(BigInteger contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getNoDaysLeft() {
        return noDaysLeft;
    }

    public void setNoDaysLeft(Integer noDaysLeft) {
        this.noDaysLeft = noDaysLeft;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getProjId() {
        return projId;
    }

    public void setProjId(Integer projId) {
        this.projId = projId;
    }

    public String getProjName() {
        return projName;
    }

    public void setProjName(String projName) {
        this.projName = projName;
    }

    public String getProjNo() {
        return projNo;
    }

    public void setProjNo(String projNo) {
        this.projNo = projNo;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getStatChgDate() {
        return statChgDate;
    }

    public void setStatChgDate(Date statChgDate) {
        this.statChgDate = statChgDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCapturedByUsersId() {
        return capturedByUsersId;
    }

    public void setCapturedByUsersId(Integer capturedByUsersId) {
        this.capturedByUsersId = capturedByUsersId;
    }

    public BigInteger getCustomersId() {
        return customersId;
    }

    public void setCustomersId(BigInteger customersId) {
        this.customersId = customersId;
    }

    public String getLuReminderPeriodCd() {
        return luReminderPeriodCd;
    }

    public void setLuReminderPeriodCd(String luReminderPeriodCd) {
        this.luReminderPeriodCd = luReminderPeriodCd;
    }

    public Integer getRequestedByUsersId() {
        return requestedByUsersId;
    }

    public void setRequestedByUsersId(Integer requestedByUsersId) {
        this.requestedByUsersId = requestedByUsersId;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderFormBeHistoryPK != null ? orderFormBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderFormBeHistory)) {
            return false;
        }
        OrderFormBeHistory other = (OrderFormBeHistory) object;
        if ((this.orderFormBeHistoryPK == null && other.orderFormBeHistoryPK != null) || (this.orderFormBeHistoryPK != null && !this.orderFormBeHistoryPK.equals(other.orderFormBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.OrderFormBeHistory[ orderFormBeHistoryPK=" + orderFormBeHistoryPK + " ]";
    }
    
}
