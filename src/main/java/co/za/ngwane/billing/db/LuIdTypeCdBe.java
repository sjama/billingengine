/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "LU_ID_TYPE_CD_BE")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "LuIdTypeCd.findAll", query = "SELECT l FROM LuIdTypeCd l")
//    , @NamedQuery(name = "LuIdTypeCd.findByCode", query = "SELECT l FROM LuIdTypeCd l WHERE l.code = :code")
//    , @NamedQuery(name = "LuIdTypeCd.findByDescription", query = "SELECT l FROM LuIdTypeCd l WHERE l.description = :description")})
public class LuIdTypeCdBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODE")
    private String code;
    @Column(name = "DESCRIPTION")
    private String description;

    public LuIdTypeCdBe() {
    }

    public LuIdTypeCdBe(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LuIdTypeCdBe)) {
            return false;
        }
        LuIdTypeCdBe other = (LuIdTypeCdBe) object;
        return (this.code != null || other.code == null) && (this.code == null || this.code.equals(other.code));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.LuIdTypeCdBe[ code=" + code + " ]";
    }
    
}
