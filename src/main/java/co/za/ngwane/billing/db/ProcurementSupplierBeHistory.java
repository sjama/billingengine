/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "PROCUREMENT_SUPPLIER_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProcurementSupplierBeHistory.findAll", query = "SELECT p FROM ProcurementSupplierBeHistory p")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findById", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.procurementSupplierBeHistoryPK.id = :id")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findByRev", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.procurementSupplierBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findByRevtype", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.revtype = :revtype")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findByCreatedDate", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.createdDate = :createdDate")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findByNoOfDays", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.noOfDays = :noOfDays")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findByRefNo", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.refNo = :refNo")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findByStatChgDate", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.statChgDate = :statChgDate")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findByStatus", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.status = :status")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findBySupplierAmount", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.supplierAmount = :supplierAmount")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findByProcurementId", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.procurementId = :procurementId")
    , @NamedQuery(name = "ProcurementSupplierBeHistory.findBySuppliersId", query = "SELECT p FROM ProcurementSupplierBeHistory p WHERE p.suppliersId = :suppliersId")})
public class ProcurementSupplierBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProcurementSupplierBeHistoryPK procurementSupplierBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "NO_OF_DAYS")
    private Integer noOfDays;
    @Column(name = "REF_NO")
    private String refNo;
    @Column(name = "STAT_CHG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgDate;
    @Column(name = "STATUS")
    private String status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SUPPLIER_AMOUNT")
    private Float supplierAmount;
    @Column(name = "PROCUREMENT_ID")
    private BigInteger procurementId;
    @Column(name = "SUPPLIERS_ID")
    private BigInteger suppliersId;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public ProcurementSupplierBeHistory() {
    }

    public ProcurementSupplierBeHistory(ProcurementSupplierBeHistoryPK procurementSupplierBeHistoryPK) {
        this.procurementSupplierBeHistoryPK = procurementSupplierBeHistoryPK;
    }

    public ProcurementSupplierBeHistory(long id, int rev) {
        this.procurementSupplierBeHistoryPK = new ProcurementSupplierBeHistoryPK(id, rev);
    }

    public ProcurementSupplierBeHistoryPK getProcurementSupplierBeHistoryPK() {
        return procurementSupplierBeHistoryPK;
    }

    public void setProcurementSupplierBeHistoryPK(ProcurementSupplierBeHistoryPK procurementSupplierBeHistoryPK) {
        this.procurementSupplierBeHistoryPK = procurementSupplierBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(Integer noOfDays) {
        this.noOfDays = noOfDays;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Date getStatChgDate() {
        return statChgDate;
    }

    public void setStatChgDate(Date statChgDate) {
        this.statChgDate = statChgDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getSupplierAmount() {
        return supplierAmount;
    }

    public void setSupplierAmount(Float supplierAmount) {
        this.supplierAmount = supplierAmount;
    }

    public BigInteger getProcurementId() {
        return procurementId;
    }

    public void setProcurementId(BigInteger procurementId) {
        this.procurementId = procurementId;
    }

    public BigInteger getSuppliersId() {
        return suppliersId;
    }

    public void setSuppliersId(BigInteger suppliersId) {
        this.suppliersId = suppliersId;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (procurementSupplierBeHistoryPK != null ? procurementSupplierBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcurementSupplierBeHistory)) {
            return false;
        }
        ProcurementSupplierBeHistory other = (ProcurementSupplierBeHistory) object;
        if ((this.procurementSupplierBeHistoryPK == null && other.procurementSupplierBeHistoryPK != null) || (this.procurementSupplierBeHistoryPK != null && !this.procurementSupplierBeHistoryPK.equals(other.procurementSupplierBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.ProcurementSupplierBeHistory[ procurementSupplierBeHistoryPK=" + procurementSupplierBeHistoryPK + " ]";
    }
    
}
