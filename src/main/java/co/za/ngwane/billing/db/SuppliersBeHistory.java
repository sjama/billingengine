/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "SUPPLIERS_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SuppliersBeHistory.findAll", query = "SELECT s FROM SuppliersBeHistory s")
    , @NamedQuery(name = "SuppliersBeHistory.findById", query = "SELECT s FROM SuppliersBeHistory s WHERE s.suppliersBeHistoryPK.id = :id")
    , @NamedQuery(name = "SuppliersBeHistory.findByRev", query = "SELECT s FROM SuppliersBeHistory s WHERE s.suppliersBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "SuppliersBeHistory.findByRevtype", query = "SELECT s FROM SuppliersBeHistory s WHERE s.revtype = :revtype")
    , @NamedQuery(name = "SuppliersBeHistory.findByAddressId", query = "SELECT s FROM SuppliersBeHistory s WHERE s.addressId = :addressId")
    , @NamedQuery(name = "SuppliersBeHistory.findByBankDetId", query = "SELECT s FROM SuppliersBeHistory s WHERE s.bankDetId = :bankDetId")
    , @NamedQuery(name = "SuppliersBeHistory.findByComName", query = "SELECT s FROM SuppliersBeHistory s WHERE s.comName = :comName")
    , @NamedQuery(name = "SuppliersBeHistory.findByContactPersonId", query = "SELECT s FROM SuppliersBeHistory s WHERE s.contactPersonId = :contactPersonId")
    , @NamedQuery(name = "SuppliersBeHistory.findByRefNo", query = "SELECT s FROM SuppliersBeHistory s WHERE s.refNo = :refNo")
    , @NamedQuery(name = "SuppliersBeHistory.findByRegNo", query = "SELECT s FROM SuppliersBeHistory s WHERE s.regNo = :regNo")
    , @NamedQuery(name = "SuppliersBeHistory.findByStatChgD", query = "SELECT s FROM SuppliersBeHistory s WHERE s.statChgD = :statChgD")
    , @NamedQuery(name = "SuppliersBeHistory.findByStatus", query = "SELECT s FROM SuppliersBeHistory s WHERE s.status = :status")
    , @NamedQuery(name = "SuppliersBeHistory.findBySupplierNo", query = "SELECT s FROM SuppliersBeHistory s WHERE s.supplierNo = :supplierNo")
    , @NamedQuery(name = "SuppliersBeHistory.findByTradeName", query = "SELECT s FROM SuppliersBeHistory s WHERE s.tradeName = :tradeName")
    , @NamedQuery(name = "SuppliersBeHistory.findByVatRegNo", query = "SELECT s FROM SuppliersBeHistory s WHERE s.vatRegNo = :vatRegNo")})
public class SuppliersBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SuppliersBeHistoryPK suppliersBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "ADDRESS_ID")
    private BigInteger addressId;
    @Column(name = "BANK_DET_ID")
    private Integer bankDetId;
    @Column(name = "COM_NAME")
    private String comName;
    @Column(name = "CONTACT_PERSON_ID")
    private BigInteger contactPersonId;
    @Column(name = "REF_NO")
    private String refNo;
    @Column(name = "REG_NO")
    private String regNo;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "SUPPLIER_NO")
    private String supplierNo;
    @Column(name = "TRADE_NAME")
    private String tradeName;
    @Column(name = "VAT_REG_NO")
    private String vatRegNo;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public SuppliersBeHistory() {
    }

    public SuppliersBeHistory(SuppliersBeHistoryPK suppliersBeHistoryPK) {
        this.suppliersBeHistoryPK = suppliersBeHistoryPK;
    }

    public SuppliersBeHistory(long id, int rev) {
        this.suppliersBeHistoryPK = new SuppliersBeHistoryPK(id, rev);
    }

    public SuppliersBeHistoryPK getSuppliersBeHistoryPK() {
        return suppliersBeHistoryPK;
    }

    public void setSuppliersBeHistoryPK(SuppliersBeHistoryPK suppliersBeHistoryPK) {
        this.suppliersBeHistoryPK = suppliersBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public BigInteger getAddressId() {
        return addressId;
    }

    public void setAddressId(BigInteger addressId) {
        this.addressId = addressId;
    }

    public Integer getBankDetId() {
        return bankDetId;
    }

    public void setBankDetId(Integer bankDetId) {
        this.bankDetId = bankDetId;
    }

    public String getComName() {
        return comName;
    }

    public void setComName(String comName) {
        this.comName = comName;
    }

    public BigInteger getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(BigInteger contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSupplierNo() {
        return supplierNo;
    }

    public void setSupplierNo(String supplierNo) {
        this.supplierNo = supplierNo;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getVatRegNo() {
        return vatRegNo;
    }

    public void setVatRegNo(String vatRegNo) {
        this.vatRegNo = vatRegNo;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (suppliersBeHistoryPK != null ? suppliersBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuppliersBeHistory)) {
            return false;
        }
        SuppliersBeHistory other = (SuppliersBeHistory) object;
        if ((this.suppliersBeHistoryPK == null && other.suppliersBeHistoryPK != null) || (this.suppliersBeHistoryPK != null && !this.suppliersBeHistoryPK.equals(other.suppliersBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.SuppliersBeHistory[ suppliersBeHistoryPK=" + suppliersBeHistoryPK + " ]";
    }
    
}
