/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "DELIVERY_NOTE_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DeliveryNoteBeHistory.findAll", query = "SELECT d FROM DeliveryNoteBeHistory d")
    , @NamedQuery(name = "DeliveryNoteBeHistory.findById", query = "SELECT d FROM DeliveryNoteBeHistory d WHERE d.deliveryNoteBeHistoryPK.id = :id")
    , @NamedQuery(name = "DeliveryNoteBeHistory.findByRev", query = "SELECT d FROM DeliveryNoteBeHistory d WHERE d.deliveryNoteBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "DeliveryNoteBeHistory.findByRevtype", query = "SELECT d FROM DeliveryNoteBeHistory d WHERE d.revtype = :revtype")
    , @NamedQuery(name = "DeliveryNoteBeHistory.findByCreatedDate", query = "SELECT d FROM DeliveryNoteBeHistory d WHERE d.createdDate = :createdDate")
    , @NamedQuery(name = "DeliveryNoteBeHistory.findByRefNo", query = "SELECT d FROM DeliveryNoteBeHistory d WHERE d.refNo = :refNo")
    , @NamedQuery(name = "DeliveryNoteBeHistory.findByCreatedByUserId", query = "SELECT d FROM DeliveryNoteBeHistory d WHERE d.createdByUserId = :createdByUserId")
    , @NamedQuery(name = "DeliveryNoteBeHistory.findByPurchaseOrderId", query = "SELECT d FROM DeliveryNoteBeHistory d WHERE d.purchaseOrderId = :purchaseOrderId")})
public class DeliveryNoteBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DeliveryNoteBeHistoryPK deliveryNoteBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "REF_NO")
    private String refNo;
    @Column(name = "CREATED_BY_USER_ID")
    private Integer createdByUserId;
    @Column(name = "PURCHASE_ORDER_ID")
    private BigInteger purchaseOrderId;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public DeliveryNoteBeHistory() {
    }

    public DeliveryNoteBeHistory(DeliveryNoteBeHistoryPK deliveryNoteBeHistoryPK) {
        this.deliveryNoteBeHistoryPK = deliveryNoteBeHistoryPK;
    }

    public DeliveryNoteBeHistory(long id, int rev) {
        this.deliveryNoteBeHistoryPK = new DeliveryNoteBeHistoryPK(id, rev);
    }

    public DeliveryNoteBeHistoryPK getDeliveryNoteBeHistoryPK() {
        return deliveryNoteBeHistoryPK;
    }

    public void setDeliveryNoteBeHistoryPK(DeliveryNoteBeHistoryPK deliveryNoteBeHistoryPK) {
        this.deliveryNoteBeHistoryPK = deliveryNoteBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Integer getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(Integer createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public BigInteger getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(BigInteger purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (deliveryNoteBeHistoryPK != null ? deliveryNoteBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DeliveryNoteBeHistory)) {
            return false;
        }
        DeliveryNoteBeHistory other = (DeliveryNoteBeHistory) object;
        if ((this.deliveryNoteBeHistoryPK == null && other.deliveryNoteBeHistoryPK != null) || (this.deliveryNoteBeHistoryPK != null && !this.deliveryNoteBeHistoryPK.equals(other.deliveryNoteBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.DeliveryNoteBeHistory[ deliveryNoteBeHistoryPK=" + deliveryNoteBeHistoryPK + " ]";
    }
    
}
