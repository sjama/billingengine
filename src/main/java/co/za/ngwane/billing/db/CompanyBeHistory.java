/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "COMPANY_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompanyBeHistory.findAll", query = "SELECT c FROM CompanyBeHistory c")
    , @NamedQuery(name = "CompanyBeHistory.findById", query = "SELECT c FROM CompanyBeHistory c WHERE c.companyBeHistoryPK.id = :id")
    , @NamedQuery(name = "CompanyBeHistory.findByRev", query = "SELECT c FROM CompanyBeHistory c WHERE c.companyBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "CompanyBeHistory.findByRevtype", query = "SELECT c FROM CompanyBeHistory c WHERE c.revtype = :revtype")
    , @NamedQuery(name = "CompanyBeHistory.findByBankAccNo", query = "SELECT c FROM CompanyBeHistory c WHERE c.bankAccNo = :bankAccNo")
    , @NamedQuery(name = "CompanyBeHistory.findByBankBranch", query = "SELECT c FROM CompanyBeHistory c WHERE c.bankBranch = :bankBranch")
    , @NamedQuery(name = "CompanyBeHistory.findByBankBranchCd", query = "SELECT c FROM CompanyBeHistory c WHERE c.bankBranchCd = :bankBranchCd")
    , @NamedQuery(name = "CompanyBeHistory.findByBankName", query = "SELECT c FROM CompanyBeHistory c WHERE c.bankName = :bankName")
    , @NamedQuery(name = "CompanyBeHistory.findByEmail", query = "SELECT c FROM CompanyBeHistory c WHERE c.email = :email")
    , @NamedQuery(name = "CompanyBeHistory.findByFaxNo", query = "SELECT c FROM CompanyBeHistory c WHERE c.faxNo = :faxNo")
    , @NamedQuery(name = "CompanyBeHistory.findByLogoRefn", query = "SELECT c FROM CompanyBeHistory c WHERE c.logoRefn = :logoRefn")
    , @NamedQuery(name = "CompanyBeHistory.findByName", query = "SELECT c FROM CompanyBeHistory c WHERE c.name = :name")
    , @NamedQuery(name = "CompanyBeHistory.findByPhyAddrCity", query = "SELECT c FROM CompanyBeHistory c WHERE c.phyAddrCity = :phyAddrCity")
    , @NamedQuery(name = "CompanyBeHistory.findByPhyAddrCode", query = "SELECT c FROM CompanyBeHistory c WHERE c.phyAddrCode = :phyAddrCode")
    , @NamedQuery(name = "CompanyBeHistory.findByPhyAddrLine1", query = "SELECT c FROM CompanyBeHistory c WHERE c.phyAddrLine1 = :phyAddrLine1")
    , @NamedQuery(name = "CompanyBeHistory.findByPhyAddrLine2", query = "SELECT c FROM CompanyBeHistory c WHERE c.phyAddrLine2 = :phyAddrLine2")
    , @NamedQuery(name = "CompanyBeHistory.findByPhyAddrLine3", query = "SELECT c FROM CompanyBeHistory c WHERE c.phyAddrLine3 = :phyAddrLine3")
    , @NamedQuery(name = "CompanyBeHistory.findByPhyAddrSuburb", query = "SELECT c FROM CompanyBeHistory c WHERE c.phyAddrSuburb = :phyAddrSuburb")
    , @NamedQuery(name = "CompanyBeHistory.findByPosAddrCity", query = "SELECT c FROM CompanyBeHistory c WHERE c.posAddrCity = :posAddrCity")
    , @NamedQuery(name = "CompanyBeHistory.findByPosAddrCode", query = "SELECT c FROM CompanyBeHistory c WHERE c.posAddrCode = :posAddrCode")
    , @NamedQuery(name = "CompanyBeHistory.findByPosAddrLine1", query = "SELECT c FROM CompanyBeHistory c WHERE c.posAddrLine1 = :posAddrLine1")
    , @NamedQuery(name = "CompanyBeHistory.findByPosAddrLine2", query = "SELECT c FROM CompanyBeHistory c WHERE c.posAddrLine2 = :posAddrLine2")
    , @NamedQuery(name = "CompanyBeHistory.findByPosAddrLine3", query = "SELECT c FROM CompanyBeHistory c WHERE c.posAddrLine3 = :posAddrLine3")
    , @NamedQuery(name = "CompanyBeHistory.findByPosAddrSuburb", query = "SELECT c FROM CompanyBeHistory c WHERE c.posAddrSuburb = :posAddrSuburb")
    , @NamedQuery(name = "CompanyBeHistory.findByRegNo", query = "SELECT c FROM CompanyBeHistory c WHERE c.regNo = :regNo")
    , @NamedQuery(name = "CompanyBeHistory.findByStat", query = "SELECT c FROM CompanyBeHistory c WHERE c.stat = :stat")
    , @NamedQuery(name = "CompanyBeHistory.findByStatChgD", query = "SELECT c FROM CompanyBeHistory c WHERE c.statChgD = :statChgD")
    , @NamedQuery(name = "CompanyBeHistory.findByTelNo", query = "SELECT c FROM CompanyBeHistory c WHERE c.telNo = :telNo")
    , @NamedQuery(name = "CompanyBeHistory.findByVatNo", query = "SELECT c FROM CompanyBeHistory c WHERE c.vatNo = :vatNo")
    , @NamedQuery(name = "CompanyBeHistory.findByWebUrl", query = "SELECT c FROM CompanyBeHistory c WHERE c.webUrl = :webUrl")})
public class CompanyBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CompanyBeHistoryPK companyBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "BANK_ACC_NO")
    private String bankAccNo;
    @Column(name = "BANK_BRANCH")
    private String bankBranch;
    @Column(name = "BANK_BRANCH_CD")
    private String bankBranchCd;
    @Column(name = "BANK_NAME")
    private String bankName;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "FAX_NO")
    private String faxNo;
    @Column(name = "LOGO_REFN")
    private String logoRefn;
    @Column(name = "NAME")
    private String name;
    @Column(name = "PHY_ADDR_CITY")
    private String phyAddrCity;
    @Column(name = "PHY_ADDR_CODE")
    private String phyAddrCode;
    @Column(name = "PHY_ADDR_LINE1")
    private String phyAddrLine1;
    @Column(name = "PHY_ADDR_LINE2")
    private String phyAddrLine2;
    @Column(name = "PHY_ADDR_LINE3")
    private String phyAddrLine3;
    @Column(name = "PHY_ADDR_SUBURB")
    private String phyAddrSuburb;
    @Column(name = "POS_ADDR_CITY")
    private String posAddrCity;
    @Column(name = "POS_ADDR_CODE")
    private String posAddrCode;
    @Column(name = "POS_ADDR_LINE1")
    private String posAddrLine1;
    @Column(name = "POS_ADDR_LINE2")
    private String posAddrLine2;
    @Column(name = "POS_ADDR_LINE3")
    private String posAddrLine3;
    @Column(name = "POS_ADDR_SUBURB")
    private String posAddrSuburb;
    @Column(name = "REG_NO")
    private String regNo;
    @Column(name = "STAT")
    private String stat;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Column(name = "TEL_NO")
    private String telNo;
    @Column(name = "VAT_NO")
    private String vatNo;
    @Column(name = "WEB_URL")
    private String webUrl;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public CompanyBeHistory() {
    }

    public CompanyBeHistory(CompanyBeHistoryPK companyBeHistoryPK) {
        this.companyBeHistoryPK = companyBeHistoryPK;
    }

    public CompanyBeHistory(int id, int rev) {
        this.companyBeHistoryPK = new CompanyBeHistoryPK(id, rev);
    }

    public CompanyBeHistoryPK getCompanyBeHistoryPK() {
        return companyBeHistoryPK;
    }

    public void setCompanyBeHistoryPK(CompanyBeHistoryPK companyBeHistoryPK) {
        this.companyBeHistoryPK = companyBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankBranchCd() {
        return bankBranchCd;
    }

    public void setBankBranchCd(String bankBranchCd) {
        this.bankBranchCd = bankBranchCd;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getLogoRefn() {
        return logoRefn;
    }

    public void setLogoRefn(String logoRefn) {
        this.logoRefn = logoRefn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhyAddrCity() {
        return phyAddrCity;
    }

    public void setPhyAddrCity(String phyAddrCity) {
        this.phyAddrCity = phyAddrCity;
    }

    public String getPhyAddrCode() {
        return phyAddrCode;
    }

    public void setPhyAddrCode(String phyAddrCode) {
        this.phyAddrCode = phyAddrCode;
    }

    public String getPhyAddrLine1() {
        return phyAddrLine1;
    }

    public void setPhyAddrLine1(String phyAddrLine1) {
        this.phyAddrLine1 = phyAddrLine1;
    }

    public String getPhyAddrLine2() {
        return phyAddrLine2;
    }

    public void setPhyAddrLine2(String phyAddrLine2) {
        this.phyAddrLine2 = phyAddrLine2;
    }

    public String getPhyAddrLine3() {
        return phyAddrLine3;
    }

    public void setPhyAddrLine3(String phyAddrLine3) {
        this.phyAddrLine3 = phyAddrLine3;
    }

    public String getPhyAddrSuburb() {
        return phyAddrSuburb;
    }

    public void setPhyAddrSuburb(String phyAddrSuburb) {
        this.phyAddrSuburb = phyAddrSuburb;
    }

    public String getPosAddrCity() {
        return posAddrCity;
    }

    public void setPosAddrCity(String posAddrCity) {
        this.posAddrCity = posAddrCity;
    }

    public String getPosAddrCode() {
        return posAddrCode;
    }

    public void setPosAddrCode(String posAddrCode) {
        this.posAddrCode = posAddrCode;
    }

    public String getPosAddrLine1() {
        return posAddrLine1;
    }

    public void setPosAddrLine1(String posAddrLine1) {
        this.posAddrLine1 = posAddrLine1;
    }

    public String getPosAddrLine2() {
        return posAddrLine2;
    }

    public void setPosAddrLine2(String posAddrLine2) {
        this.posAddrLine2 = posAddrLine2;
    }

    public String getPosAddrLine3() {
        return posAddrLine3;
    }

    public void setPosAddrLine3(String posAddrLine3) {
        this.posAddrLine3 = posAddrLine3;
    }

    public String getPosAddrSuburb() {
        return posAddrSuburb;
    }

    public void setPosAddrSuburb(String posAddrSuburb) {
        this.posAddrSuburb = posAddrSuburb;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getVatNo() {
        return vatNo;
    }

    public void setVatNo(String vatNo) {
        this.vatNo = vatNo;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (companyBeHistoryPK != null ? companyBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompanyBeHistory)) {
            return false;
        }
        CompanyBeHistory other = (CompanyBeHistory) object;
        if ((this.companyBeHistoryPK == null && other.companyBeHistoryPK != null) || (this.companyBeHistoryPK != null && !this.companyBeHistoryPK.equals(other.companyBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.CompanyBeHistory[ companyBeHistoryPK=" + companyBeHistoryPK + " ]";
    }
    
}
