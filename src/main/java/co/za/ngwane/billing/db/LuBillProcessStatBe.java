/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "LU_BILL_PROCESS_STAT_BE")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "LuStatBe.findAll", query = "SELECT l FROM LuStatBe l")
//    , @NamedQuery(name = "LuStatBe.findByCode", query = "SELECT l FROM LuStatBe l WHERE l.code = :code")
//    , @NamedQuery(name = "LuStatBe.findByDescription", query = "SELECT l FROM LuStatBe l WHERE l.description = :description")})
public class LuBillProcessStatBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODE")
    private String code;
    @Column(name = "DESCRIPTION")
    private String description;

    public LuBillProcessStatBe() {
    }

    public LuBillProcessStatBe(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LuBillProcessStatBe)) {
            return false;
        }
        LuBillProcessStatBe other = (LuBillProcessStatBe) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.LuBillProcessStatBe[ code=" + code + " ]";
    }
    
}
