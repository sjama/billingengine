/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "USERS_BE")
@XmlRootElement
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
//@NamedQueries({
//    @NamedQuery(name = "UsersBe.findAll", query = "SELECT u FROM UsersBe u")
//    , @NamedQuery(name = "UsersBe.findById", query = "SELECT u FROM UsersBe u WHERE u.id = :id")
//    , @NamedQuery(name = "UsersBe.findByName", query = "SELECT u FROM UsersBe u WHERE u.name = :name")
//    , @NamedQuery(name = "UsersBe.findByMiddlename", query = "SELECT u FROM UsersBe u WHERE u.middlename = :middlename")
//    , @NamedQuery(name = "UsersBe.findBySurname", query = "SELECT u FROM UsersBe u WHERE u.surname = :surname")
//    , @NamedQuery(name = "UsersBe.findByUsername", query = "SELECT u FROM UsersBe u WHERE u.username = :username")
//    , @NamedQuery(name = "UsersBe.findByPassword", query = "SELECT u FROM UsersBe u WHERE u.password = :password")
//    , @NamedQuery(name = "UsersBe.findByEmail", query = "SELECT u FROM UsersBe u WHERE u.email = :email")
//    , @NamedQuery(name = "UsersBe.findByStatus", query = "SELECT u FROM UsersBe u WHERE u.status = :status")
//    , @NamedQuery(name = "UsersBe.findByStatChgD", query = "SELECT u FROM UsersBe u WHERE u.statChgD = :statChgD")})
public class UsersBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @JoinColumn(name = "CUSTOMERS_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private CustomersBe customersId;
    @JoinColumn(name = "LU_ROLES_CD", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private LuRolesBe luRolesCd;

    @Column(name = "USER_ID")
    private Integer userId;

    @Transient
    private String tagName;
    @Transient
    private boolean tagFlag;
    @Transient
    private String tagMessage;

    @Transient
    private String name;
    @Transient
    private String surname;
    @Transient
    private String username;
    @Transient
    private String password;
    @Transient
    private String email;

    public UsersBe() {
    }

    public UsersBe(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public CustomersBe getCustomersId() {
        return customersId;
    }

    public void setCustomersId(CustomersBe customersId) {
        this.customersId = customersId;
    }

    public LuRolesBe getLuRolesCd() {
        return luRolesCd;
    }

    public void setLuRolesCd(LuRolesBe luRolesCd) {
        this.luRolesCd = luRolesCd;
    }


    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public boolean isTagFlag() {
        return tagFlag;
    }

    public void setTagFlag(boolean tagFlag) {
        this.tagFlag = tagFlag;
    }

    public String getTagMessage() {
        return tagMessage;
    }

    public void setTagMessage(String tagMessage) {
        this.tagMessage = tagMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersBe)) {
            return false;
        }
        UsersBe other = (UsersBe) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db[ id=" + id + " ]";
    }
    
}
