/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "SERVICE_PROVIDER_PRODUCTS_BE")
@XmlRootElement
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
//@NamedQueries({
//    @NamedQuery(name = "ServiceProviderProducts.findAll", query = "SELECT s FROM ServiceProviderProducts s")
//    , @NamedQuery(name = "ServiceProviderProducts.findByCode", query = "SELECT s FROM ServiceProviderProducts s WHERE s.code = :code")
//    , @NamedQuery(name = "ServiceProviderProducts.findByDescription", query = "SELECT s FROM ServiceProviderProducts s WHERE s.description = :description")
//    , @NamedQuery(name = "ServiceProviderProducts.findByPriceAmount", query = "SELECT s FROM ServiceProviderProducts s WHERE s.priceAmount = :priceAmount")})
public class ServiceProviderProductsBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CODE")
    private Integer code;
    @Column(name = "DESCRIPTION")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRICE_AMOUNT")
    private Float priceAmount;
    @JoinColumn(name = "LU_SERVICE_PROVIDER_BECODE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private LuServiceProviderBe luServiceProviderBecode;

    public ServiceProviderProductsBe() {
    }

    public ServiceProviderProductsBe(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPriceAmount() {
        return priceAmount;
    }

    public void setPriceAmount(Float priceAmount) {
        this.priceAmount = priceAmount;
    }

    public LuServiceProviderBe getLuServiceProviderBecode() {
        return luServiceProviderBecode;
    }

    public void setLuServiceProviderBecode(LuServiceProviderBe luServiceProviderBecode) {
        this.luServiceProviderBecode = luServiceProviderBecode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServiceProviderProductsBe)) {
            return false;
        }
        ServiceProviderProductsBe other = (ServiceProviderProductsBe) object;
        return (this.code != null || other.code == null) && (this.code == null || this.code.equals(other.code));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.ServiceProviderProductsBe[ code=" + code + " ]";
    }
    
}
