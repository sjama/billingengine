/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "CDR_FILE_HIST_BE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CdrFileHistBe.findAll", query = "SELECT c FROM CdrFileHistBe c")
    , @NamedQuery(name = "CdrFileHistBe.findById", query = "SELECT c FROM CdrFileHistBe c WHERE c.id = :id")
    , @NamedQuery(name = "CdrFileHistBe.findByCreatedDate", query = "SELECT c FROM CdrFileHistBe c WHERE c.createdDate = :createdDate")
    , @NamedQuery(name = "CdrFileHistBe.findByFileName", query = "SELECT c FROM CdrFileHistBe c WHERE c.fileName = :fileName")
    , @NamedQuery(name = "CdrFileHistBe.findByFileLoc", query = "SELECT c FROM CdrFileHistBe c WHERE c.fileLoc = :fileLoc")})
public class CdrFileHistBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "CREATED_DATE")
    private Integer createdDate;
    @Lob
    @Column(name = "FILE_DATA")
    private byte[] fileData;
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "FILE_LOC")
    private String fileLoc;

    public CdrFileHistBe() {
    }

    public CdrFileHistBe(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Integer createdDate) {
        this.createdDate = createdDate;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLoc() {
        return fileLoc;
    }

    public void setFileLoc(String fileLoc) {
        this.fileLoc = fileLoc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CdrFileHistBe)) {
            return false;
        }
        CdrFileHistBe other = (CdrFileHistBe) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.CdrFileHistBe[ id=" + id + " ]";
    }

}
