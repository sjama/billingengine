/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "CONTRACT_TYPE_BE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContractTypeBe.findAll", query = "SELECT c FROM ContractTypeBe c")
    , @NamedQuery(name = "ContractTypeBe.findByCode", query = "SELECT c FROM ContractTypeBe c WHERE c.code = :code")
    , @NamedQuery(name = "ContractTypeBe.findByDescription", query = "SELECT c FROM ContractTypeBe c WHERE c.description = :description")
    , @NamedQuery(name = "ContractTypeBe.findByRatePerMin", query = "SELECT c FROM ContractTypeBe c WHERE c.ratePerMin = :ratePerMin")
    , @NamedQuery(name = "ContractTypeBe.findByNoOfMin", query = "SELECT c FROM ContractTypeBe c WHERE c.noOfMin = :noOfMin")
    , @NamedQuery(name = "ContractTypeBe.findByStatus", query = "SELECT c FROM ContractTypeBe c WHERE c.status = :status")
    , @NamedQuery(name = "ContractTypeBe.findByStatChgD", query = "SELECT c FROM ContractTypeBe c WHERE c.statChgD = :statChgD")})
public class ContractTypeBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CODE")
    private Long code;
    @Column(name = "DESCRIPTION")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "RATE_PER_MIN")
    private Float ratePerMin;
    @Column(name = "NO_OF_MIN")
    private Float noOfMin;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;

    public ContractTypeBe() {
    }

    public ContractTypeBe(Long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getRatePerMin() {
        return ratePerMin;
    }

    public void setRatePerMin(Float ratePerMin) {
        this.ratePerMin = ratePerMin;
    }

    public Float getNoOfMin() {
        return noOfMin;
    }

    public void setNoOfMin(Float noOfMin) {
        this.noOfMin = noOfMin;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContractTypeBe)) {
            return false;
        }
        ContractTypeBe other = (ContractTypeBe) object;
        return (this.code != null || other.code == null) && (this.code == null || this.code.equals(other.code));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.ContractTypeBe[ code=" + code + " ]";
    }
    
}
