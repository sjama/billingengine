/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "LU_LAST_NO_TYPE_BE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LuLastNoTypeBe.findAll", query = "SELECT l FROM LuLastNoTypeBe l")
    , @NamedQuery(name = "LuLastNoTypeBe.findByCode", query = "SELECT l FROM LuLastNoTypeBe l WHERE l.code = :code")
    , @NamedQuery(name = "LuLastNoTypeBe.findByDescription", query = "SELECT l FROM LuLastNoTypeBe l WHERE l.description = :description")})
public class LuLastNoTypeBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODE")
    private String code;
    @Column(name = "DESCRIPTION")
    private String description;

    public LuLastNoTypeBe() {
    }

    public LuLastNoTypeBe(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LuLastNoTypeBe)) {
            return false;
        }
        LuLastNoTypeBe other = (LuLastNoTypeBe) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.LuLastNoTypeBe[ code=" + code + " ]";
    }
    
}
