/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "PROCUREMENT_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProcurementBeHistory.findAll", query = "SELECT p FROM ProcurementBeHistory p")
    , @NamedQuery(name = "ProcurementBeHistory.findById", query = "SELECT p FROM ProcurementBeHistory p WHERE p.procurementBeHistoryPK.id = :id")
    , @NamedQuery(name = "ProcurementBeHistory.findByRev", query = "SELECT p FROM ProcurementBeHistory p WHERE p.procurementBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "ProcurementBeHistory.findByRevtype", query = "SELECT p FROM ProcurementBeHistory p WHERE p.revtype = :revtype")
    , @NamedQuery(name = "ProcurementBeHistory.findByActualDeliveryDate", query = "SELECT p FROM ProcurementBeHistory p WHERE p.actualDeliveryDate = :actualDeliveryDate")
    , @NamedQuery(name = "ProcurementBeHistory.findByBudget", query = "SELECT p FROM ProcurementBeHistory p WHERE p.budget = :budget")
    , @NamedQuery(name = "ProcurementBeHistory.findByCodSupplier", query = "SELECT p FROM ProcurementBeHistory p WHERE p.codSupplier = :codSupplier")
    , @NamedQuery(name = "ProcurementBeHistory.findByCreatedDate", query = "SELECT p FROM ProcurementBeHistory p WHERE p.createdDate = :createdDate")
    , @NamedQuery(name = "ProcurementBeHistory.findByCreditAvail", query = "SELECT p FROM ProcurementBeHistory p WHERE p.creditAvail = :creditAvail")
    , @NamedQuery(name = "ProcurementBeHistory.findByCreditLimit", query = "SELECT p FROM ProcurementBeHistory p WHERE p.creditLimit = :creditLimit")
    , @NamedQuery(name = "ProcurementBeHistory.findByExpectedDeliveryDate", query = "SELECT p FROM ProcurementBeHistory p WHERE p.expectedDeliveryDate = :expectedDeliveryDate")
    , @NamedQuery(name = "ProcurementBeHistory.findByExpectedTurnAroundTime", query = "SELECT p FROM ProcurementBeHistory p WHERE p.expectedTurnAroundTime = :expectedTurnAroundTime")
    , @NamedQuery(name = "ProcurementBeHistory.findByRefNo", query = "SELECT p FROM ProcurementBeHistory p WHERE p.refNo = :refNo")
    , @NamedQuery(name = "ProcurementBeHistory.findByStatChgDate", query = "SELECT p FROM ProcurementBeHistory p WHERE p.statChgDate = :statChgDate")
    , @NamedQuery(name = "ProcurementBeHistory.findByStatus", query = "SELECT p FROM ProcurementBeHistory p WHERE p.status = :status")
    , @NamedQuery(name = "ProcurementBeHistory.findByOrderId", query = "SELECT p FROM ProcurementBeHistory p WHERE p.orderId = :orderId")})
public class ProcurementBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProcurementBeHistoryPK procurementBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "ACTUAL_DELIVERY_DATE")
    private Integer actualDeliveryDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BUDGET")
    private Float budget;
    @Column(name = "COD_SUPPLIER")
    private String codSupplier;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "CREDIT_AVAIL")
    private Float creditAvail;
    @Column(name = "CREDIT_LIMIT")
    private Float creditLimit;
    @Column(name = "EXPECTED_DELIVERY_DATE")
    @Temporal(TemporalType.DATE)
    private Date expectedDeliveryDate;
    @Column(name = "EXPECTED_TURN_AROUND_TIME")
    private String expectedTurnAroundTime;
    @Column(name = "REF_NO")
    private String refNo;
    @Column(name = "STAT_CHG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "ORDER_ID")
    private BigInteger orderId;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public ProcurementBeHistory() {
    }

    public ProcurementBeHistory(ProcurementBeHistoryPK procurementBeHistoryPK) {
        this.procurementBeHistoryPK = procurementBeHistoryPK;
    }

    public ProcurementBeHistory(long id, int rev) {
        this.procurementBeHistoryPK = new ProcurementBeHistoryPK(id, rev);
    }

    public ProcurementBeHistoryPK getProcurementBeHistoryPK() {
        return procurementBeHistoryPK;
    }

    public void setProcurementBeHistoryPK(ProcurementBeHistoryPK procurementBeHistoryPK) {
        this.procurementBeHistoryPK = procurementBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Integer getActualDeliveryDate() {
        return actualDeliveryDate;
    }

    public void setActualDeliveryDate(Integer actualDeliveryDate) {
        this.actualDeliveryDate = actualDeliveryDate;
    }

    public Float getBudget() {
        return budget;
    }

    public void setBudget(Float budget) {
        this.budget = budget;
    }

    public String getCodSupplier() {
        return codSupplier;
    }

    public void setCodSupplier(String codSupplier) {
        this.codSupplier = codSupplier;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Float getCreditAvail() {
        return creditAvail;
    }

    public void setCreditAvail(Float creditAvail) {
        this.creditAvail = creditAvail;
    }

    public Float getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(Float creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public String getExpectedTurnAroundTime() {
        return expectedTurnAroundTime;
    }

    public void setExpectedTurnAroundTime(String expectedTurnAroundTime) {
        this.expectedTurnAroundTime = expectedTurnAroundTime;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Date getStatChgDate() {
        return statChgDate;
    }

    public void setStatChgDate(Date statChgDate) {
        this.statChgDate = statChgDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigInteger getOrderId() {
        return orderId;
    }

    public void setOrderId(BigInteger orderId) {
        this.orderId = orderId;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (procurementBeHistoryPK != null ? procurementBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcurementBeHistory)) {
            return false;
        }
        ProcurementBeHistory other = (ProcurementBeHistory) object;
        if ((this.procurementBeHistoryPK == null && other.procurementBeHistoryPK != null) || (this.procurementBeHistoryPK != null && !this.procurementBeHistoryPK.equals(other.procurementBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.ProcurementBeHistory[ procurementBeHistoryPK=" + procurementBeHistoryPK + " ]";
    }
    
}
