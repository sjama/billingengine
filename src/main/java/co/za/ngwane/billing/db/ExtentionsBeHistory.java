/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "EXTENTIONS_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExtentionsBeHistory.findAll", query = "SELECT e FROM ExtentionsBeHistory e")
    , @NamedQuery(name = "ExtentionsBeHistory.findById", query = "SELECT e FROM ExtentionsBeHistory e WHERE e.extentionsBeHistoryPK.id = :id")
    , @NamedQuery(name = "ExtentionsBeHistory.findByRev", query = "SELECT e FROM ExtentionsBeHistory e WHERE e.extentionsBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "ExtentionsBeHistory.findByRevtype", query = "SELECT e FROM ExtentionsBeHistory e WHERE e.revtype = :revtype")
    , @NamedQuery(name = "ExtentionsBeHistory.findByStatChgD", query = "SELECT e FROM ExtentionsBeHistory e WHERE e.statChgD = :statChgD")
    , @NamedQuery(name = "ExtentionsBeHistory.findByStatus", query = "SELECT e FROM ExtentionsBeHistory e WHERE e.status = :status")
    , @NamedQuery(name = "ExtentionsBeHistory.findByBranchCd", query = "SELECT e FROM ExtentionsBeHistory e WHERE e.branchCd = :branchCd")})
public class ExtentionsBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ExtentionsBeHistoryPK extentionsBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "BRANCH_CD")
    private String branchCd;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public ExtentionsBeHistory() {
    }

    public ExtentionsBeHistory(ExtentionsBeHistoryPK extentionsBeHistoryPK) {
        this.extentionsBeHistoryPK = extentionsBeHistoryPK;
    }

    public ExtentionsBeHistory(long id, int rev) {
        this.extentionsBeHistoryPK = new ExtentionsBeHistoryPK(id, rev);
    }

    public ExtentionsBeHistoryPK getExtentionsBeHistoryPK() {
        return extentionsBeHistoryPK;
    }

    public void setExtentionsBeHistoryPK(ExtentionsBeHistoryPK extentionsBeHistoryPK) {
        this.extentionsBeHistoryPK = extentionsBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBranchCd() {
        return branchCd;
    }

    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (extentionsBeHistoryPK != null ? extentionsBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExtentionsBeHistory)) {
            return false;
        }
        ExtentionsBeHistory other = (ExtentionsBeHistory) object;
        if ((this.extentionsBeHistoryPK == null && other.extentionsBeHistoryPK != null) || (this.extentionsBeHistoryPK != null && !this.extentionsBeHistoryPK.equals(other.extentionsBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.ExtentionsBeHistory[ extentionsBeHistoryPK=" + extentionsBeHistoryPK + " ]";
    }
    
}
