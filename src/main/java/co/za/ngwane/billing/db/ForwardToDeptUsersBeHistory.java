/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "FORWARD_TO_DEPT_USERS_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ForwardToDeptUsersBeHistory.findAll", query = "SELECT f FROM ForwardToDeptUsersBeHistory f")
    , @NamedQuery(name = "ForwardToDeptUsersBeHistory.findById", query = "SELECT f FROM ForwardToDeptUsersBeHistory f WHERE f.forwardToDeptUsersBeHistoryPK.id = :id")
    , @NamedQuery(name = "ForwardToDeptUsersBeHistory.findByRev", query = "SELECT f FROM ForwardToDeptUsersBeHistory f WHERE f.forwardToDeptUsersBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "ForwardToDeptUsersBeHistory.findByRevtype", query = "SELECT f FROM ForwardToDeptUsersBeHistory f WHERE f.revtype = :revtype")
    , @NamedQuery(name = "ForwardToDeptUsersBeHistory.findByStatChgDate", query = "SELECT f FROM ForwardToDeptUsersBeHistory f WHERE f.statChgDate = :statChgDate")
    , @NamedQuery(name = "ForwardToDeptUsersBeHistory.findByStatus", query = "SELECT f FROM ForwardToDeptUsersBeHistory f WHERE f.status = :status")
    , @NamedQuery(name = "ForwardToDeptUsersBeHistory.findByLuForwardToDeptCd", query = "SELECT f FROM ForwardToDeptUsersBeHistory f WHERE f.luForwardToDeptCd = :luForwardToDeptCd")
    , @NamedQuery(name = "ForwardToDeptUsersBeHistory.findByUsersId", query = "SELECT f FROM ForwardToDeptUsersBeHistory f WHERE f.usersId = :usersId")})
public class ForwardToDeptUsersBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ForwardToDeptUsersBeHistoryPK forwardToDeptUsersBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "STAT_CHG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "LU_FORWARD_TO_DEPT_CD")
    private String luForwardToDeptCd;
    @Column(name = "USERS_ID")
    private Integer usersId;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public ForwardToDeptUsersBeHistory() {
    }

    public ForwardToDeptUsersBeHistory(ForwardToDeptUsersBeHistoryPK forwardToDeptUsersBeHistoryPK) {
        this.forwardToDeptUsersBeHistoryPK = forwardToDeptUsersBeHistoryPK;
    }

    public ForwardToDeptUsersBeHistory(long id, int rev) {
        this.forwardToDeptUsersBeHistoryPK = new ForwardToDeptUsersBeHistoryPK(id, rev);
    }

    public ForwardToDeptUsersBeHistoryPK getForwardToDeptUsersBeHistoryPK() {
        return forwardToDeptUsersBeHistoryPK;
    }

    public void setForwardToDeptUsersBeHistoryPK(ForwardToDeptUsersBeHistoryPK forwardToDeptUsersBeHistoryPK) {
        this.forwardToDeptUsersBeHistoryPK = forwardToDeptUsersBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Date getStatChgDate() {
        return statChgDate;
    }

    public void setStatChgDate(Date statChgDate) {
        this.statChgDate = statChgDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLuForwardToDeptCd() {
        return luForwardToDeptCd;
    }

    public void setLuForwardToDeptCd(String luForwardToDeptCd) {
        this.luForwardToDeptCd = luForwardToDeptCd;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (forwardToDeptUsersBeHistoryPK != null ? forwardToDeptUsersBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ForwardToDeptUsersBeHistory)) {
            return false;
        }
        ForwardToDeptUsersBeHistory other = (ForwardToDeptUsersBeHistory) object;
        if ((this.forwardToDeptUsersBeHistoryPK == null && other.forwardToDeptUsersBeHistoryPK != null) || (this.forwardToDeptUsersBeHistoryPK != null && !this.forwardToDeptUsersBeHistoryPK.equals(other.forwardToDeptUsersBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.ForwardToDeptUsersBeHistory[ forwardToDeptUsersBeHistoryPK=" + forwardToDeptUsersBeHistoryPK + " ]";
    }
    
}
