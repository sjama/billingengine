/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "CONTACT_PERSON_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContactPersonBeHistory.findAll", query = "SELECT c FROM ContactPersonBeHistory c")
    , @NamedQuery(name = "ContactPersonBeHistory.findById", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.contactPersonBeHistoryPK.id = :id")
    , @NamedQuery(name = "ContactPersonBeHistory.findByRev", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.contactPersonBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "ContactPersonBeHistory.findByRevtype", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.revtype = :revtype")
    , @NamedQuery(name = "ContactPersonBeHistory.findByCellNo", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.cellNo = :cellNo")
    , @NamedQuery(name = "ContactPersonBeHistory.findByEmail", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.email = :email")
    , @NamedQuery(name = "ContactPersonBeHistory.findByName", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.name = :name")
    , @NamedQuery(name = "ContactPersonBeHistory.findByPhoneNo", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.phoneNo = :phoneNo")
    , @NamedQuery(name = "ContactPersonBeHistory.findByStatChgDate", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.statChgDate = :statChgDate")
    , @NamedQuery(name = "ContactPersonBeHistory.findByStatus", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.status = :status")
    , @NamedQuery(name = "ContactPersonBeHistory.findBySurname", query = "SELECT c FROM ContactPersonBeHistory c WHERE c.surname = :surname")})
public class ContactPersonBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContactPersonBeHistoryPK contactPersonBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "CELL_NO")
    private String cellNo;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "NAME")
    private String name;
    @Column(name = "PHONE_NO")
    private String phoneNo;
    @Column(name = "STAT_CHG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "SURNAME")
    private String surname;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public ContactPersonBeHistory() {
    }

    public ContactPersonBeHistory(ContactPersonBeHistoryPK contactPersonBeHistoryPK) {
        this.contactPersonBeHistoryPK = contactPersonBeHistoryPK;
    }

    public ContactPersonBeHistory(long id, int rev) {
        this.contactPersonBeHistoryPK = new ContactPersonBeHistoryPK(id, rev);
    }

    public ContactPersonBeHistoryPK getContactPersonBeHistoryPK() {
        return contactPersonBeHistoryPK;
    }

    public void setContactPersonBeHistoryPK(ContactPersonBeHistoryPK contactPersonBeHistoryPK) {
        this.contactPersonBeHistoryPK = contactPersonBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getCellNo() {
        return cellNo;
    }

    public void setCellNo(String cellNo) {
        this.cellNo = cellNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Date getStatChgDate() {
        return statChgDate;
    }

    public void setStatChgDate(Date statChgDate) {
        this.statChgDate = statChgDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contactPersonBeHistoryPK != null ? contactPersonBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactPersonBeHistory)) {
            return false;
        }
        ContactPersonBeHistory other = (ContactPersonBeHistory) object;
        if ((this.contactPersonBeHistoryPK == null && other.contactPersonBeHistoryPK != null) || (this.contactPersonBeHistoryPK != null && !this.contactPersonBeHistoryPK.equals(other.contactPersonBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.ContactPersonBeHistory[ contactPersonBeHistoryPK=" + contactPersonBeHistoryPK + " ]";
    }
    
}
