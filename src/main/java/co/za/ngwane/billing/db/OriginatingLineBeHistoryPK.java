/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author bheki.lubisi
 */
@Embeddable
public class OriginatingLineBeHistoryPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "LINE")
    private String line;
    @Basic(optional = false)
    @Column(name = "REV")
    private int rev;

    public OriginatingLineBeHistoryPK() {
    }

    public OriginatingLineBeHistoryPK(String line, int rev) {
        this.line = line;
        this.rev = rev;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public int getRev() {
        return rev;
    }

    public void setRev(int rev) {
        this.rev = rev;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (line != null ? line.hashCode() : 0);
        hash += (int) rev;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OriginatingLineBeHistoryPK)) {
            return false;
        }
        OriginatingLineBeHistoryPK other = (OriginatingLineBeHistoryPK) object;
        if ((this.line == null && other.line != null) || (this.line != null && !this.line.equals(other.line))) {
            return false;
        }
        if (this.rev != other.rev) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.OriginatingLineBeHistoryPK[ line=" + line + ", rev=" + rev + " ]";
    }
    
}
