/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import org.hibernate.envers.Audited;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "AGREED_RATES_BE")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "AgreedRates.findAll", query = "SELECT a FROM AgreedRates a")
//    , @NamedQuery(name = "AgreedRates.findByCustomersId", query = "SELECT a FROM AgreedRates a WHERE a.agreedRatesPK.customersId = :customersId")
//    , @NamedQuery(name = "AgreedRates.findByCallTypeCd", query = "SELECT a FROM AgreedRates a WHERE a.agreedRatesPK.callTypeCd = :callTypeCd")
//    , @NamedQuery(name = "AgreedRates.findByDestCountryCd", query = "SELECT a FROM AgreedRates a WHERE a.agreedRatesPK.destCountryCd = :destCountryCd")
//    , @NamedQuery(name = "AgreedRates.findByLuBillingTypeCd", query = "SELECT a FROM AgreedRates a WHERE a.agreedRatesPK.luBillingTypeCd = :luBillingTypeCd")
//    , @NamedQuery(name = "AgreedRates.findByAmount", query = "SELECT a FROM AgreedRates a WHERE a.amount = :amount")
//    , @NamedQuery(name = "AgreedRates.findByStatus", query = "SELECT a FROM AgreedRates a WHERE a.status = :status")
//    , @NamedQuery(name = "AgreedRates.findByStatChgD", query = "SELECT a FROM AgreedRates a WHERE a.statChgD = :statChgD")})
public class AgreedRatesBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AgreedRatesPK agreedRatesPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "COMPANY_RATE")
    private Float comRate;

    @Column(name = "SERVICE_PROVIDER_RATE")
    private Float serviceProviderRate;

    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgD;
    @JoinColumn(name = "CUSTOMERS_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CustomersBe customersBe;
    @JoinColumn(name = "DEST_COUNTRY_CD", referencedColumnName = "CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private DestCountryBe destCountryBe;
    @JoinColumn(name = "LU_BILLING_TYPE_CD", referencedColumnName = "CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private LuBillingTypeBe luBillingType;
    @JoinColumn(name = "CALL_TYPE_CD", referencedColumnName = "CODE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CallTypeBe callType;

    @Transient
    private String tagName;
    @Transient
    private String tagFlag;
    @Transient
    private String tagMsg;

    @Transient
    private AgreedRatesPK oldAgreedRatesPK;
    @Transient
    private Boolean pkChanged;

    public AgreedRatesBe() {
    }

    public AgreedRatesBe(AgreedRatesPK agreedRatesPK) {
        this.agreedRatesPK = agreedRatesPK;
    }

    public AgreedRatesBe(long customersId, String callTypeCd, String destCountryCd, String luBillingTypeCd) {
        this.agreedRatesPK = new AgreedRatesPK(customersId, callTypeCd, destCountryCd, luBillingTypeCd);
    }

    public AgreedRatesPK getAgreedRatesPK() {
        return agreedRatesPK;
    }

    public void setAgreedRatesPK(AgreedRatesPK agreedRatesPK) {
        this.agreedRatesPK = agreedRatesPK;
    }


    public Float getComRate() {
        return comRate;
    }

    public void setComRate(Float comRate) {
        this.comRate = comRate;
    }

    public Float getServiceProviderRate() {
        return serviceProviderRate;
    }

    public void setServiceProviderRate(Float serviceProviderRate) {
        this.serviceProviderRate = serviceProviderRate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public CustomersBe getCustomersBe() {
        return customersBe;
    }

    public void setCustomersBe(CustomersBe customersBe) {
        this.customersBe = customersBe;
    }

    public DestCountryBe getDestCountryBe() {
        return destCountryBe;
    }

    public void setDestCountryBe(DestCountryBe destCountryBe) {
        this.destCountryBe = destCountryBe;
    }

    public LuBillingTypeBe getLuBillingType() {
        return luBillingType;
    }

    public void setLuBillingType(LuBillingTypeBe luBillingType) {
        this.luBillingType = luBillingType;
    }

    public CallTypeBe getCallType() {
        return callType;
    }

    public void setCallType(CallTypeBe callType) {
        this.callType = callType;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagFlag() {
        return tagFlag;
    }

    public void setTagFlag(String tagFlag) {
        this.tagFlag = tagFlag;
    }

    public String getTagMsg() {
        return tagMsg;
    }

    public void setTagMsg(String tagMsg) {
        this.tagMsg = tagMsg;
    }

    public AgreedRatesPK getOldAgreedRatesPK() {
        return oldAgreedRatesPK;
    }

    public void setOldAgreedRatesPK(AgreedRatesPK oldAgreedRatesPK) {
        this.oldAgreedRatesPK = oldAgreedRatesPK;
    }

    public Boolean getPkChanged() {
        return pkChanged;
    }

    public void setPkChanged(Boolean pkChanged) {
        this.pkChanged = pkChanged;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agreedRatesPK != null ? agreedRatesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgreedRatesBe)) {
            return false;
        }
        AgreedRatesBe other = (AgreedRatesBe) object;
        if ((this.agreedRatesPK == null && other.agreedRatesPK != null) || (this.agreedRatesPK != null && !this.agreedRatesPK.equals(other.agreedRatesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.AgreedRatesBe[ agreedRatesPK=" + agreedRatesPK + " ]";
    }
    
}
