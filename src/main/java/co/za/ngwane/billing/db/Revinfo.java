/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "REVINFO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Revinfo.findAll", query = "SELECT r FROM Revinfo r")
    , @NamedQuery(name = "Revinfo.findByRev", query = "SELECT r FROM Revinfo r WHERE r.rev = :rev")
    , @NamedQuery(name = "Revinfo.findByRevtstmp", query = "SELECT r FROM Revinfo r WHERE r.revtstmp = :revtstmp")})
public class Revinfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "REV")
    private Integer rev;
    @Column(name = "REVTSTMP")
    private BigInteger revtstmp;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<ProcurementSupplierBeHistory> procurementSupplierBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<OrderFormBeHistory> orderFormBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<AddressBeHistory> addressBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<ForwardToDeptUsersBeHistory> forwardToDeptUsersBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<IcasaRatesBeHistory> icasaRatesBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<DeliveryNoteBeHistory> deliveryNoteBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<ProcurementBeHistory> procurementBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<OriginatingLineBeHistory> originatingLineBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<PurchaseOrderBeHistory> purchaseOrderBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<BankDetBeHistory> bankDetBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<CustomersBeHistory> customersBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<UsersBeHistory> usersBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<ExtentionsBeHistory> extentionsBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<CallTypeBeHistory> callTypeBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<SuppliersBeHistory> suppliersBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<ChosenSupplierBeHistory> chosenSupplierBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<CompanyBeHistory> companyBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<ContactPersonBeHistory> contactPersonBeHistoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revinfo")
    private Collection<ServiceProviderProductsBeHistory> serviceProviderProductsBeHistoryCollection;

    public Revinfo() {
    }

    public Revinfo(Integer rev) {
        this.rev = rev;
    }

    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public BigInteger getRevtstmp() {
        return revtstmp;
    }

    public void setRevtstmp(BigInteger revtstmp) {
        this.revtstmp = revtstmp;
    }

    @XmlTransient
    public Collection<ProcurementSupplierBeHistory> getProcurementSupplierBeHistoryCollection() {
        return procurementSupplierBeHistoryCollection;
    }

    public void setProcurementSupplierBeHistoryCollection(Collection<ProcurementSupplierBeHistory> procurementSupplierBeHistoryCollection) {
        this.procurementSupplierBeHistoryCollection = procurementSupplierBeHistoryCollection;
    }

    @XmlTransient
    public Collection<OrderFormBeHistory> getOrderFormBeHistoryCollection() {
        return orderFormBeHistoryCollection;
    }

    public void setOrderFormBeHistoryCollection(Collection<OrderFormBeHistory> orderFormBeHistoryCollection) {
        this.orderFormBeHistoryCollection = orderFormBeHistoryCollection;
    }

    @XmlTransient
    public Collection<AddressBeHistory> getAddressBeHistoryCollection() {
        return addressBeHistoryCollection;
    }

    public void setAddressBeHistoryCollection(Collection<AddressBeHistory> addressBeHistoryCollection) {
        this.addressBeHistoryCollection = addressBeHistoryCollection;
    }

    @XmlTransient
    public Collection<ForwardToDeptUsersBeHistory> getForwardToDeptUsersBeHistoryCollection() {
        return forwardToDeptUsersBeHistoryCollection;
    }

    public void setForwardToDeptUsersBeHistoryCollection(Collection<ForwardToDeptUsersBeHistory> forwardToDeptUsersBeHistoryCollection) {
        this.forwardToDeptUsersBeHistoryCollection = forwardToDeptUsersBeHistoryCollection;
    }

    @XmlTransient
    public Collection<IcasaRatesBeHistory> getIcasaRatesBeHistoryCollection() {
        return icasaRatesBeHistoryCollection;
    }

    public void setIcasaRatesBeHistoryCollection(Collection<IcasaRatesBeHistory> icasaRatesBeHistoryCollection) {
        this.icasaRatesBeHistoryCollection = icasaRatesBeHistoryCollection;
    }

    @XmlTransient
    public Collection<DeliveryNoteBeHistory> getDeliveryNoteBeHistoryCollection() {
        return deliveryNoteBeHistoryCollection;
    }

    public void setDeliveryNoteBeHistoryCollection(Collection<DeliveryNoteBeHistory> deliveryNoteBeHistoryCollection) {
        this.deliveryNoteBeHistoryCollection = deliveryNoteBeHistoryCollection;
    }

    @XmlTransient
    public Collection<PaymentRequisitionBeHistory> getPaymentRequisitionBeHistoryCollection() {
        return paymentRequisitionBeHistoryCollection;
    }

    public void setPaymentRequisitionBeHistoryCollection(Collection<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryCollection) {
        this.paymentRequisitionBeHistoryCollection = paymentRequisitionBeHistoryCollection;
    }

    @XmlTransient
    public Collection<ProcurementBeHistory> getProcurementBeHistoryCollection() {
        return procurementBeHistoryCollection;
    }

    public void setProcurementBeHistoryCollection(Collection<ProcurementBeHistory> procurementBeHistoryCollection) {
        this.procurementBeHistoryCollection = procurementBeHistoryCollection;
    }

    @XmlTransient
    public Collection<OriginatingLineBeHistory> getOriginatingLineBeHistoryCollection() {
        return originatingLineBeHistoryCollection;
    }

    public void setOriginatingLineBeHistoryCollection(Collection<OriginatingLineBeHistory> originatingLineBeHistoryCollection) {
        this.originatingLineBeHistoryCollection = originatingLineBeHistoryCollection;
    }

    @XmlTransient
    public Collection<PurchaseOrderBeHistory> getPurchaseOrderBeHistoryCollection() {
        return purchaseOrderBeHistoryCollection;
    }

    public void setPurchaseOrderBeHistoryCollection(Collection<PurchaseOrderBeHistory> purchaseOrderBeHistoryCollection) {
        this.purchaseOrderBeHistoryCollection = purchaseOrderBeHistoryCollection;
    }

    @XmlTransient
    public Collection<BankDetBeHistory> getBankDetBeHistoryCollection() {
        return bankDetBeHistoryCollection;
    }

    public void setBankDetBeHistoryCollection(Collection<BankDetBeHistory> bankDetBeHistoryCollection) {
        this.bankDetBeHistoryCollection = bankDetBeHistoryCollection;
    }

    @XmlTransient
    public Collection<CustomersBeHistory> getCustomersBeHistoryCollection() {
        return customersBeHistoryCollection;
    }

    public void setCustomersBeHistoryCollection(Collection<CustomersBeHistory> customersBeHistoryCollection) {
        this.customersBeHistoryCollection = customersBeHistoryCollection;
    }

    @XmlTransient
    public Collection<UsersBeHistory> getUsersBeHistoryCollection() {
        return usersBeHistoryCollection;
    }

    public void setUsersBeHistoryCollection(Collection<UsersBeHistory> usersBeHistoryCollection) {
        this.usersBeHistoryCollection = usersBeHistoryCollection;
    }

    @XmlTransient
    public Collection<ExtentionsBeHistory> getExtentionsBeHistoryCollection() {
        return extentionsBeHistoryCollection;
    }

    public void setExtentionsBeHistoryCollection(Collection<ExtentionsBeHistory> extentionsBeHistoryCollection) {
        this.extentionsBeHistoryCollection = extentionsBeHistoryCollection;
    }

    @XmlTransient
    public Collection<CallTypeBeHistory> getCallTypeBeHistoryCollection() {
        return callTypeBeHistoryCollection;
    }

    public void setCallTypeBeHistoryCollection(Collection<CallTypeBeHistory> callTypeBeHistoryCollection) {
        this.callTypeBeHistoryCollection = callTypeBeHistoryCollection;
    }

    @XmlTransient
    public Collection<SuppliersBeHistory> getSuppliersBeHistoryCollection() {
        return suppliersBeHistoryCollection;
    }

    public void setSuppliersBeHistoryCollection(Collection<SuppliersBeHistory> suppliersBeHistoryCollection) {
        this.suppliersBeHistoryCollection = suppliersBeHistoryCollection;
    }

    @XmlTransient
    public Collection<ChosenSupplierBeHistory> getChosenSupplierBeHistoryCollection() {
        return chosenSupplierBeHistoryCollection;
    }

    public void setChosenSupplierBeHistoryCollection(Collection<ChosenSupplierBeHistory> chosenSupplierBeHistoryCollection) {
        this.chosenSupplierBeHistoryCollection = chosenSupplierBeHistoryCollection;
    }

    @XmlTransient
    public Collection<CompanyBeHistory> getCompanyBeHistoryCollection() {
        return companyBeHistoryCollection;
    }

    public void setCompanyBeHistoryCollection(Collection<CompanyBeHistory> companyBeHistoryCollection) {
        this.companyBeHistoryCollection = companyBeHistoryCollection;
    }

    @XmlTransient
    public Collection<ContactPersonBeHistory> getContactPersonBeHistoryCollection() {
        return contactPersonBeHistoryCollection;
    }

    public void setContactPersonBeHistoryCollection(Collection<ContactPersonBeHistory> contactPersonBeHistoryCollection) {
        this.contactPersonBeHistoryCollection = contactPersonBeHistoryCollection;
    }

    @XmlTransient
    public Collection<ServiceProviderProductsBeHistory> getServiceProviderProductsBeHistoryCollection() {
        return serviceProviderProductsBeHistoryCollection;
    }

    public void setServiceProviderProductsBeHistoryCollection(Collection<ServiceProviderProductsBeHistory> serviceProviderProductsBeHistoryCollection) {
        this.serviceProviderProductsBeHistoryCollection = serviceProviderProductsBeHistoryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rev != null ? rev.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Revinfo)) {
            return false;
        }
        Revinfo other = (Revinfo) object;
        if ((this.rev == null && other.rev != null) || (this.rev != null && !this.rev.equals(other.rev))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.Revinfo[ rev=" + rev + " ]";
    }
    
}
