/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "USERS_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsersBeHistory.findAll", query = "SELECT u FROM UsersBeHistory u")
    , @NamedQuery(name = "UsersBeHistory.findById", query = "SELECT u FROM UsersBeHistory u WHERE u.usersBeHistoryPK.id = :id")
    , @NamedQuery(name = "UsersBeHistory.findByRev", query = "SELECT u FROM UsersBeHistory u WHERE u.usersBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "UsersBeHistory.findByRevtype", query = "SELECT u FROM UsersBeHistory u WHERE u.revtype = :revtype")
    , @NamedQuery(name = "UsersBeHistory.findByStatChgD", query = "SELECT u FROM UsersBeHistory u WHERE u.statChgD = :statChgD")
    , @NamedQuery(name = "UsersBeHistory.findByStatus", query = "SELECT u FROM UsersBeHistory u WHERE u.status = :status")
    , @NamedQuery(name = "UsersBeHistory.findByUserId", query = "SELECT u FROM UsersBeHistory u WHERE u.userId = :userId")
    , @NamedQuery(name = "UsersBeHistory.findByCustomersId", query = "SELECT u FROM UsersBeHistory u WHERE u.customersId = :customersId")
    , @NamedQuery(name = "UsersBeHistory.findByLuRolesCd", query = "SELECT u FROM UsersBeHistory u WHERE u.luRolesCd = :luRolesCd")})
public class UsersBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsersBeHistoryPK usersBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "USER_ID")
    private Integer userId;
    @Column(name = "CUSTOMERS_ID")
    private BigInteger customersId;
    @Column(name = "LU_ROLES_CD")
    private String luRolesCd;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public UsersBeHistory() {
    }

    public UsersBeHistory(UsersBeHistoryPK usersBeHistoryPK) {
        this.usersBeHistoryPK = usersBeHistoryPK;
    }

    public UsersBeHistory(int id, int rev) {
        this.usersBeHistoryPK = new UsersBeHistoryPK(id, rev);
    }

    public UsersBeHistoryPK getUsersBeHistoryPK() {
        return usersBeHistoryPK;
    }

    public void setUsersBeHistoryPK(UsersBeHistoryPK usersBeHistoryPK) {
        this.usersBeHistoryPK = usersBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BigInteger getCustomersId() {
        return customersId;
    }

    public void setCustomersId(BigInteger customersId) {
        this.customersId = customersId;
    }

    public String getLuRolesCd() {
        return luRolesCd;
    }

    public void setLuRolesCd(String luRolesCd) {
        this.luRolesCd = luRolesCd;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usersBeHistoryPK != null ? usersBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersBeHistory)) {
            return false;
        }
        UsersBeHistory other = (UsersBeHistory) object;
        if ((this.usersBeHistoryPK == null && other.usersBeHistoryPK != null) || (this.usersBeHistoryPK != null && !this.usersBeHistoryPK.equals(other.usersBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.UsersBeHistory[ usersBeHistoryPK=" + usersBeHistoryPK + " ]";
    }
    
}
