/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "CALL_TYPE_BE")
@XmlRootElement
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
//@NamedQueries({
//    @NamedQuery(name = "CallType.findAll", query = "SELECT c FROM CallType c")
//    , @NamedQuery(name = "CallType.findByCode", query = "SELECT c FROM CallType c WHERE c.code = :code")
//    , @NamedQuery(name = "CallType.findByDescription", query = "SELECT c FROM CallType c WHERE c.description = :description")})
public class CallTypeBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODE")
    private String code;
    @Column(name = "DESCRIPTION")
    private String description;

    public CallTypeBe() {
    }

    public CallTypeBe(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CallTypeBe)) {
            return false;
        }
        CallTypeBe other = (CallTypeBe) object;
        return (this.code != null || other.code == null) && (this.code == null || this.code.equals(other.code));
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.CallTypeBe[ code=" + code + " ]";
    }
    
}
