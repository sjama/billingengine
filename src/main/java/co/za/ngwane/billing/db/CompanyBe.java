/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "COMPANY_BE")
@XmlRootElement
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
//@NamedQueries({
//        @NamedQuery(name = "CompanyBe.findAll", query = "SELECT c FROM CompanyBe c")
//        , @NamedQuery(name = "CompanyBe.findById", query = "SELECT c FROM CompanyBe c WHERE c.id = :id")
//        , @NamedQuery(name = "CompanyBe.findByName", query = "SELECT c FROM CompanyBe c WHERE c.name = :name")
//        , @NamedQuery(name = "CompanyBe.findByPhyAddrLine1", query = "SELECT c FROM CompanyBe c WHERE c.phyAddrLine1 = :phyAddrLine1")
//        , @NamedQuery(name = "CompanyBe.findByPhyAddrLine2", query = "SELECT c FROM CompanyBe c WHERE c.phyAddrLine2 = :phyAddrLine2")
//        , @NamedQuery(name = "CompanyBe.findByPhyAddrLine3", query = "SELECT c FROM CompanyBe c WHERE c.phyAddrLine3 = :phyAddrLine3")
//        , @NamedQuery(name = "CompanyBe.findByPhyAddrSuburb", query = "SELECT c FROM CompanyBe c WHERE c.phyAddrSuburb = :phyAddrSuburb")
//        , @NamedQuery(name = "CompanyBe.findByPhyAddrCity", query = "SELECT c FROM CompanyBe c WHERE c.phyAddrCity = :phyAddrCity")
//        , @NamedQuery(name = "CompanyBe.findByPhyAddrCode", query = "SELECT c FROM CompanyBe c WHERE c.phyAddrCode = :phyAddrCode")
//        , @NamedQuery(name = "CompanyBe.findByPosAddrLine1", query = "SELECT c FROM CompanyBe c WHERE c.posAddrLine1 = :posAddrLine1")
//        , @NamedQuery(name = "CompanyBe.findByPosAddrLine2", query = "SELECT c FROM CompanyBe c WHERE c.posAddrLine2 = :posAddrLine2")
//        , @NamedQuery(name = "CompanyBe.findByPosAddrLine3", query = "SELECT c FROM CompanyBe c WHERE c.posAddrLine3 = :posAddrLine3")
//        , @NamedQuery(name = "CompanyBe.findByPosAddrSuburb", query = "SELECT c FROM CompanyBe c WHERE c.posAddrSuburb = :posAddrSuburb")
//        , @NamedQuery(name = "CompanyBe.findByPosAddrCity", query = "SELECT c FROM CompanyBe c WHERE c.posAddrCity = :posAddrCity")
//        , @NamedQuery(name = "CompanyBe.findByPosAddrCode", query = "SELECT c FROM CompanyBe c WHERE c.posAddrCode = :posAddrCode")
//        , @NamedQuery(name = "CompanyBe.findByTelNo", query = "SELECT c FROM CompanyBe c WHERE c.telNo = :telNo")
//        , @NamedQuery(name = "CompanyBe.findByEmail", query = "SELECT c FROM CompanyBe c WHERE c.email = :email")
//        , @NamedQuery(name = "CompanyBe.findByLogoRefn", query = "SELECT c FROM CompanyBe c WHERE c.logoRefn = :logoRefn")
//        , @NamedQuery(name = "CompanyBe.findByWebUrl", query = "SELECT c FROM CompanyBe c WHERE c.webUrl = :webUrl")
//        , @NamedQuery(name = "CompanyBe.findByRegNo", query = "SELECT c FROM CompanyBe c WHERE c.regNo = :regNo")
//        , @NamedQuery(name = "CompanyBe.findByFaxNo", query = "SELECT c FROM CompanyBe c WHERE c.faxNo = :faxNo")
//        , @NamedQuery(name = "CompanyBe.findByVatNo", query = "SELECT c FROM CompanyBe c WHERE c.vatNo = :vatNo")
//        , @NamedQuery(name = "CompanyBe.findByBankAccNo", query = "SELECT c FROM CompanyBe c WHERE c.bankAccNo = :bankAccNo")
//        , @NamedQuery(name = "CompanyBe.findByBankName", query = "SELECT c FROM CompanyBe c WHERE c.bankName = :bankName")
//        , @NamedQuery(name = "CompanyBe.findByBankBranch", query = "SELECT c FROM CompanyBe c WHERE c.bankBranch = :bankBranch")
//        , @NamedQuery(name = "CompanyBe.findByBankBranchCd", query = "SELECT c FROM CompanyBe c WHERE c.bankBranchCd = :bankBranchCd")
//        , @NamedQuery(name = "CompanyBe.findByStat", query = "SELECT c FROM CompanyBe c WHERE c.stat = :stat")
//        , @NamedQuery(name = "CompanyBe.findByStatChgD", query = "SELECT c FROM CompanyBe c WHERE c.statChgD = :statChgD")})
public class CompanyBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "PHY_ADDR_LINE1")
    private String phyAddrLine1;
    @Column(name = "PHY_ADDR_LINE2")
    private String phyAddrLine2;
    @Column(name = "PHY_ADDR_LINE3")
    private String phyAddrLine3;
    @Column(name = "PHY_ADDR_SUBURB")
    private String phyAddrSuburb;
    @Column(name = "PHY_ADDR_CITY")
    private String phyAddrCity;
    @Column(name = "PHY_ADDR_CODE")
    private String phyAddrCode;
    @Column(name = "POS_ADDR_LINE1")
    private String posAddrLine1;
    @Column(name = "POS_ADDR_LINE2")
    private String posAddrLine2;
    @Column(name = "POS_ADDR_LINE3")
    private String posAddrLine3;
    @Column(name = "POS_ADDR_SUBURB")
    private String posAddrSuburb;
    @Column(name = "POS_ADDR_CITY")
    private String posAddrCity;
    @Column(name = "POS_ADDR_CODE")
    private String posAddrCode;
    @Column(name = "TEL_NO")
    private String telNo;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "LOGO_REFN")
    private String logoRefn;
    @Column(name = "WEB_URL")
    private String webUrl;
    @Column(name = "REG_NO")
    private String regNo;
    @Column(name = "FAX_NO")
    private String faxNo;
    @Column(name = "VAT_NO")
    private String vatNo;
    @Column(name = "BANK_ACC_NO")
    private String bankAccNo;
    @Column(name = "BANK_NAME")
    private String bankName;
    @Column(name = "BANK_BRANCH")
    private String bankBranch;
    @Column(name = "BANK_BRANCH_CD")
    private String bankBranchCd;
    @Column(name = "STAT")
    private String stat;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;

    public CompanyBe() {
    }

    public CompanyBe(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhyAddrLine1() {
        return phyAddrLine1;
    }

    public void setPhyAddrLine1(String phyAddrLine1) {
        this.phyAddrLine1 = phyAddrLine1;
    }

    public String getPhyAddrLine2() {
        return phyAddrLine2;
    }

    public void setPhyAddrLine2(String phyAddrLine2) {
        this.phyAddrLine2 = phyAddrLine2;
    }

    public String getPhyAddrLine3() {
        return phyAddrLine3;
    }

    public void setPhyAddrLine3(String phyAddrLine3) {
        this.phyAddrLine3 = phyAddrLine3;
    }

    public String getPhyAddrSuburb() {
        return phyAddrSuburb;
    }

    public void setPhyAddrSuburb(String phyAddrSuburb) {
        this.phyAddrSuburb = phyAddrSuburb;
    }

    public String getPhyAddrCity() {
        return phyAddrCity;
    }

    public void setPhyAddrCity(String phyAddrCity) {
        this.phyAddrCity = phyAddrCity;
    }

    public String getPhyAddrCode() {
        return phyAddrCode;
    }

    public void setPhyAddrCode(String phyAddrCode) {
        this.phyAddrCode = phyAddrCode;
    }

    public String getPosAddrLine1() {
        return posAddrLine1;
    }

    public void setPosAddrLine1(String posAddrLine1) {
        this.posAddrLine1 = posAddrLine1;
    }

    public String getPosAddrLine2() {
        return posAddrLine2;
    }

    public void setPosAddrLine2(String posAddrLine2) {
        this.posAddrLine2 = posAddrLine2;
    }

    public String getPosAddrLine3() {
        return posAddrLine3;
    }

    public void setPosAddrLine3(String posAddrLine3) {
        this.posAddrLine3 = posAddrLine3;
    }

    public String getPosAddrSuburb() {
        return posAddrSuburb;
    }

    public void setPosAddrSuburb(String posAddrSuburb) {
        this.posAddrSuburb = posAddrSuburb;
    }

    public String getPosAddrCity() {
        return posAddrCity;
    }

    public void setPosAddrCity(String posAddrCity) {
        this.posAddrCity = posAddrCity;
    }

    public String getPosAddrCode() {
        return posAddrCode;
    }

    public void setPosAddrCode(String posAddrCode) {
        this.posAddrCode = posAddrCode;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogoRefn() {
        return logoRefn;
    }

    public void setLogoRefn(String logoRefn) {
        this.logoRefn = logoRefn;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getVatNo() {
        return vatNo;
    }

    public void setVatNo(String vatNo) {
        this.vatNo = vatNo;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankBranchCd() {
        return bankBranchCd;
    }

    public void setBankBranchCd(String bankBranchCd) {
        this.bankBranchCd = bankBranchCd;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompanyBe)) {
            return false;
        }
        CompanyBe other = (CompanyBe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.CompanyBe[ id=" + id + " ]";
    }

}