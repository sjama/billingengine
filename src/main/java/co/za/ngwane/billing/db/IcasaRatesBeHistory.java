/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "ICASA_RATES_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IcasaRatesBeHistory.findAll", query = "SELECT i FROM IcasaRatesBeHistory i")
    , @NamedQuery(name = "IcasaRatesBeHistory.findByCode", query = "SELECT i FROM IcasaRatesBeHistory i WHERE i.icasaRatesBeHistoryPK.code = :code")
    , @NamedQuery(name = "IcasaRatesBeHistory.findByRev", query = "SELECT i FROM IcasaRatesBeHistory i WHERE i.icasaRatesBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "IcasaRatesBeHistory.findByRevtype", query = "SELECT i FROM IcasaRatesBeHistory i WHERE i.revtype = :revtype")
    , @NamedQuery(name = "IcasaRatesBeHistory.findByBillRates", query = "SELECT i FROM IcasaRatesBeHistory i WHERE i.billRates = :billRates")
    , @NamedQuery(name = "IcasaRatesBeHistory.findByDescription", query = "SELECT i FROM IcasaRatesBeHistory i WHERE i.description = :description")
    , @NamedQuery(name = "IcasaRatesBeHistory.findByNeoRates", query = "SELECT i FROM IcasaRatesBeHistory i WHERE i.neoRates = :neoRates")
    , @NamedQuery(name = "IcasaRatesBeHistory.findByStatChgD", query = "SELECT i FROM IcasaRatesBeHistory i WHERE i.statChgD = :statChgD")
    , @NamedQuery(name = "IcasaRatesBeHistory.findByStatus", query = "SELECT i FROM IcasaRatesBeHistory i WHERE i.status = :status")})
public class IcasaRatesBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected IcasaRatesBeHistoryPK icasaRatesBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BILL_RATES")
    private Float billRates;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "NEO_RATES")
    private Float neoRates;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Column(name = "STATUS")
    private String status;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public IcasaRatesBeHistory() {
    }

    public IcasaRatesBeHistory(IcasaRatesBeHistoryPK icasaRatesBeHistoryPK) {
        this.icasaRatesBeHistoryPK = icasaRatesBeHistoryPK;
    }

    public IcasaRatesBeHistory(long code, int rev) {
        this.icasaRatesBeHistoryPK = new IcasaRatesBeHistoryPK(code, rev);
    }

    public IcasaRatesBeHistoryPK getIcasaRatesBeHistoryPK() {
        return icasaRatesBeHistoryPK;
    }

    public void setIcasaRatesBeHistoryPK(IcasaRatesBeHistoryPK icasaRatesBeHistoryPK) {
        this.icasaRatesBeHistoryPK = icasaRatesBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Float getBillRates() {
        return billRates;
    }

    public void setBillRates(Float billRates) {
        this.billRates = billRates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getNeoRates() {
        return neoRates;
    }

    public void setNeoRates(Float neoRates) {
        this.neoRates = neoRates;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (icasaRatesBeHistoryPK != null ? icasaRatesBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IcasaRatesBeHistory)) {
            return false;
        }
        IcasaRatesBeHistory other = (IcasaRatesBeHistory) object;
        if ((this.icasaRatesBeHistoryPK == null && other.icasaRatesBeHistoryPK != null) || (this.icasaRatesBeHistoryPK != null && !this.icasaRatesBeHistoryPK.equals(other.icasaRatesBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.IcasaRatesBeHistory[ icasaRatesBeHistoryPK=" + icasaRatesBeHistoryPK + " ]";
    }
    
}
