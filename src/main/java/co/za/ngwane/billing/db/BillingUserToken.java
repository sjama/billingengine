/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JamaS
 */
@Entity
@Table(name = "BILLING_USER_TOKEN")
@XmlRootElement
public class BillingUserToken implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "SERIES")
    private String series;
    @Column(name = "VALUE")
    private String value;
    @Column(name = "TOKEN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tokenDate;
    @Column(name = "IP_ADDRESS")
    private String ipAddress;
    @Column(name = "USER_AGENT")
    private String userAgent;
    @Basic(optional = false)
    @Column(name = "USER_LOGIN")
    private String userLogin;

    public BillingUserToken() {
    }

    public BillingUserToken(String series) {
        this.series = series;
    }

    public BillingUserToken(String series, String userLogin) {
        this.series = series;
        this.userLogin = userLogin;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getTokenDate() {
        return tokenDate;
    }

    public void setTokenDate(Date tokenDate) {
        this.tokenDate = tokenDate;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (series != null ? series.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BillingUserToken)) {
            return false;
        }
        BillingUserToken other = (BillingUserToken) object;
        return (this.series != null || other.series == null) && (this.series == null || this.series.equals(other.series));
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.BillingUserToken[ series=" + series + " ]";
    }

}
