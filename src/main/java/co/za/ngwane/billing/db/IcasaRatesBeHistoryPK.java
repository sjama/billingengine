/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author bheki.lubisi
 */
@Embeddable
public class IcasaRatesBeHistoryPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "CODE")
    private long code;
    @Basic(optional = false)
    @Column(name = "REV")
    private int rev;

    public IcasaRatesBeHistoryPK() {
    }

    public IcasaRatesBeHistoryPK(long code, int rev) {
        this.code = code;
        this.rev = rev;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public int getRev() {
        return rev;
    }

    public void setRev(int rev) {
        this.rev = rev;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) code;
        hash += (int) rev;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IcasaRatesBeHistoryPK)) {
            return false;
        }
        IcasaRatesBeHistoryPK other = (IcasaRatesBeHistoryPK) object;
        if (this.code != other.code) {
            return false;
        }
        if (this.rev != other.rev) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.IcasaRatesBeHistoryPK[ code=" + code + ", rev=" + rev + " ]";
    }
    
}
