/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "BANK_DET_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BankDetBeHistory.findAll", query = "SELECT b FROM BankDetBeHistory b")
    , @NamedQuery(name = "BankDetBeHistory.findById", query = "SELECT b FROM BankDetBeHistory b WHERE b.bankDetBeHistoryPK.id = :id")
    , @NamedQuery(name = "BankDetBeHistory.findByRev", query = "SELECT b FROM BankDetBeHistory b WHERE b.bankDetBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "BankDetBeHistory.findByRevtype", query = "SELECT b FROM BankDetBeHistory b WHERE b.revtype = :revtype")
    , @NamedQuery(name = "BankDetBeHistory.findByAccountName", query = "SELECT b FROM BankDetBeHistory b WHERE b.accountName = :accountName")
    , @NamedQuery(name = "BankDetBeHistory.findByAccountNo", query = "SELECT b FROM BankDetBeHistory b WHERE b.accountNo = :accountNo")
    , @NamedQuery(name = "BankDetBeHistory.findByBranchCd", query = "SELECT b FROM BankDetBeHistory b WHERE b.branchCd = :branchCd")
    , @NamedQuery(name = "BankDetBeHistory.findByLuAccountTypeCd", query = "SELECT b FROM BankDetBeHistory b WHERE b.luAccountTypeCd = :luAccountTypeCd")
    , @NamedQuery(name = "BankDetBeHistory.findByName", query = "SELECT b FROM BankDetBeHistory b WHERE b.name = :name")
    , @NamedQuery(name = "BankDetBeHistory.findByStatChgD", query = "SELECT b FROM BankDetBeHistory b WHERE b.statChgD = :statChgD")
    , @NamedQuery(name = "BankDetBeHistory.findByStatus", query = "SELECT b FROM BankDetBeHistory b WHERE b.status = :status")})
public class BankDetBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BankDetBeHistoryPK bankDetBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "ACCOUNT_NAME")
    private String accountName;
    @Column(name = "ACCOUNT_NO")
    private String accountNo;
    @Column(name = "BRANCH_CD")
    private String branchCd;
    @Column(name = "LU_ACCOUNT_TYPE_CD")
    private String luAccountTypeCd;
    @Column(name = "NAME")
    private String name;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Column(name = "STATUS")
    private String status;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public BankDetBeHistory() {
    }

    public BankDetBeHistory(BankDetBeHistoryPK bankDetBeHistoryPK) {
        this.bankDetBeHistoryPK = bankDetBeHistoryPK;
    }

    public BankDetBeHistory(int id, int rev) {
        this.bankDetBeHistoryPK = new BankDetBeHistoryPK(id, rev);
    }

    public BankDetBeHistoryPK getBankDetBeHistoryPK() {
        return bankDetBeHistoryPK;
    }

    public void setBankDetBeHistoryPK(BankDetBeHistoryPK bankDetBeHistoryPK) {
        this.bankDetBeHistoryPK = bankDetBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBranchCd() {
        return branchCd;
    }

    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    public String getLuAccountTypeCd() {
        return luAccountTypeCd;
    }

    public void setLuAccountTypeCd(String luAccountTypeCd) {
        this.luAccountTypeCd = luAccountTypeCd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bankDetBeHistoryPK != null ? bankDetBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankDetBeHistory)) {
            return false;
        }
        BankDetBeHistory other = (BankDetBeHistory) object;
        if ((this.bankDetBeHistoryPK == null && other.bankDetBeHistoryPK != null) || (this.bankDetBeHistoryPK != null && !this.bankDetBeHistoryPK.equals(other.bankDetBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.BankDetBeHistory[ bankDetBeHistoryPK=" + bankDetBeHistoryPK + " ]";
    }
    
}
