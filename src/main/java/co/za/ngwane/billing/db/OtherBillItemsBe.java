/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "OTHER_BILL_ITEMS_BE")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "OtherBillItems.findAll", query = "SELECT o FROM OtherBillItems o")
//    , @NamedQuery(name = "OtherBillItems.findById", query = "SELECT o FROM OtherBillItems o WHERE o.id = :id")
//    , @NamedQuery(name = "OtherBillItems.findByQuantity", query = "SELECT o FROM OtherBillItems o WHERE o.quantity = :quantity")
//    , @NamedQuery(name = "OtherBillItems.findByStartDate", query = "SELECT o FROM OtherBillItems o WHERE o.startDate = :startDate")
//    , @NamedQuery(name = "OtherBillItems.findByEndDate", query = "SELECT o FROM OtherBillItems o WHERE o.endDate = :endDate")
//    , @NamedQuery(name = "OtherBillItems.findByStatus", query = "SELECT o FROM OtherBillItems o WHERE o.status = :status")
//    , @NamedQuery(name = "OtherBillItems.findByStatChgD", query = "SELECT o FROM OtherBillItems o WHERE o.statChgD = :statChgD")})
public class OtherBillItemsBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "QUANTITY")
    private Float quantity;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @JoinColumn(name = "CUSTOMERS_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private CustomersBe customersId;
    @JoinColumn(name = "LU_BILLABLE_ITEM_CD", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private LuBillableItemsBe luBillableItemCd;

    @Column(name = "AMOUNT")
    private Float amount;

    @Transient
    private String tagName;
    @Transient
    private String tagFlag;
    @Transient
    private String tagMsg;

    public OtherBillItemsBe() {

    }

    public OtherBillItemsBe(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public CustomersBe getCustomersId() {
        return customersId;
    }

    public void setCustomersId(CustomersBe customersId) {
        this.customersId = customersId;
    }

    public LuBillableItemsBe getLuBillableItemCd() {
        return luBillableItemCd;
    }

    public void setLuBillableItemCd(LuBillableItemsBe luBillableItemCd) {
        this.luBillableItemCd = luBillableItemCd;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagFlag() {
        return tagFlag;
    }

    public void setTagFlag(String tagFlag) {
        this.tagFlag = tagFlag;
    }

    public String getTagMsg() {
        return tagMsg;
    }

    public void setTagMsg(String tagMsg) {
        this.tagMsg = tagMsg;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OtherBillItemsBe)) {
            return false;
        }
        OtherBillItemsBe other = (OtherBillItemsBe) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.OtherBillItemsBe[ id=" + id + " ]";
    }
    
}
