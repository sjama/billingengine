/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "ORIGINATING_LINE_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OriginatingLineBeHistory.findAll", query = "SELECT o FROM OriginatingLineBeHistory o")
    , @NamedQuery(name = "OriginatingLineBeHistory.findByLine", query = "SELECT o FROM OriginatingLineBeHistory o WHERE o.originatingLineBeHistoryPK.line = :line")
    , @NamedQuery(name = "OriginatingLineBeHistory.findByRev", query = "SELECT o FROM OriginatingLineBeHistory o WHERE o.originatingLineBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "OriginatingLineBeHistory.findByRevtype", query = "SELECT o FROM OriginatingLineBeHistory o WHERE o.revtype = :revtype")
    , @NamedQuery(name = "OriginatingLineBeHistory.findByStatChgD", query = "SELECT o FROM OriginatingLineBeHistory o WHERE o.statChgD = :statChgD")
    , @NamedQuery(name = "OriginatingLineBeHistory.findByStatus", query = "SELECT o FROM OriginatingLineBeHistory o WHERE o.status = :status")
    , @NamedQuery(name = "OriginatingLineBeHistory.findByCustomersId", query = "SELECT o FROM OriginatingLineBeHistory o WHERE o.customersId = :customersId")})
public class OriginatingLineBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OriginatingLineBeHistoryPK originatingLineBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "CUSTOMERS_ID")
    private BigInteger customersId;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public OriginatingLineBeHistory() {
    }

    public OriginatingLineBeHistory(OriginatingLineBeHistoryPK originatingLineBeHistoryPK) {
        this.originatingLineBeHistoryPK = originatingLineBeHistoryPK;
    }

    public OriginatingLineBeHistory(String line, int rev) {
        this.originatingLineBeHistoryPK = new OriginatingLineBeHistoryPK(line, rev);
    }

    public OriginatingLineBeHistoryPK getOriginatingLineBeHistoryPK() {
        return originatingLineBeHistoryPK;
    }

    public void setOriginatingLineBeHistoryPK(OriginatingLineBeHistoryPK originatingLineBeHistoryPK) {
        this.originatingLineBeHistoryPK = originatingLineBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigInteger getCustomersId() {
        return customersId;
    }

    public void setCustomersId(BigInteger customersId) {
        this.customersId = customersId;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (originatingLineBeHistoryPK != null ? originatingLineBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OriginatingLineBeHistory)) {
            return false;
        }
        OriginatingLineBeHistory other = (OriginatingLineBeHistory) object;
        if ((this.originatingLineBeHistoryPK == null && other.originatingLineBeHistoryPK != null) || (this.originatingLineBeHistoryPK != null && !this.originatingLineBeHistoryPK.equals(other.originatingLineBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.OriginatingLineBeHistory[ originatingLineBeHistoryPK=" + originatingLineBeHistoryPK + " ]";
    }
    
}
