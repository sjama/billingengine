/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "LAST_NO_BE")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "LastNoBe.findAll", query = "SELECT l FROM LastNoBe l")
        , @NamedQuery(name = "LastNoBe.findById", query = "SELECT l FROM LastNoBe l WHERE l.id = :id")
        , @NamedQuery(name = "LastNoBe.findByNo", query = "SELECT l FROM LastNoBe l WHERE l.no = :no")
        , @NamedQuery(name = "LastNoBe.findByStatus", query = "SELECT l FROM LastNoBe l WHERE l.status = :status")
        , @NamedQuery(name = "LastNoBe.findByStatChgD", query = "SELECT l FROM LastNoBe l WHERE l.statChgD = :statChgD")})
public class LastNoBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "NO")
    private long no;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @JoinColumn(name = "LAST_NO_TYPE_CD", referencedColumnName = "CODE")
    @OneToOne(optional = false)
    private LuLastNoTypeBe lastNoTypeCd;

    @Transient
    private String tagName;
    @Transient
    private String tagFlag;
    @Transient
    private String tagMsg;

    public LastNoBe() {
    }

    public LastNoBe(Integer id) {
        this.id = id;
    }

    public LastNoBe(Integer id, long no) {
        this.id = id;
        this.no = no;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public long getNo() {
        return no;
    }

    public void setNo(long no) {
        this.no = no;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public LuLastNoTypeBe getLastNoTypeCd() {
        return lastNoTypeCd;
    }

    public void setLastNoTypeCd(LuLastNoTypeBe lastNoTypeCd) {
        this.lastNoTypeCd = lastNoTypeCd;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagFlag() {
        return tagFlag;
    }

    public void setTagFlag(String tagFlag) {
        this.tagFlag = tagFlag;
    }

    public String getTagMsg() {
        return tagMsg;
    }

    public void setTagMsg(String tagMsg) {
        this.tagMsg = tagMsg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LastNoBe)) {
            return false;
        }
        LastNoBe other = (LastNoBe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.LastNoBe[ id=" + id + " ]";
    }

}
