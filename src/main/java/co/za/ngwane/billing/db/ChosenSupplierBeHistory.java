/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "CHOSEN_SUPPLIER_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ChosenSupplierBeHistory.findAll", query = "SELECT c FROM ChosenSupplierBeHistory c")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findById", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.chosenSupplierBeHistoryPK.id = :id")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findByRev", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.chosenSupplierBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findByRevtype", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.revtype = :revtype")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findByApprovedDate", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.approvedDate = :approvedDate")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findByRefNo", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.refNo = :refNo")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findByStatChgDate", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.statChgDate = :statChgDate")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findByStatus", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.status = :status")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findByApprovedByUserId", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.approvedByUserId = :approvedByUserId")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findByProcurementId", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.procurementId = :procurementId")
    , @NamedQuery(name = "ChosenSupplierBeHistory.findByProcurementSupplierId", query = "SELECT c FROM ChosenSupplierBeHistory c WHERE c.procurementSupplierId = :procurementSupplierId")})
public class ChosenSupplierBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ChosenSupplierBeHistoryPK chosenSupplierBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "APPROVED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvedDate;
    @Column(name = "REF_NO")
    private String refNo;
    @Column(name = "STAT_CHG_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "APPROVED_BY_USER_ID")
    private Integer approvedByUserId;
    @Column(name = "PROCUREMENT_ID")
    private BigInteger procurementId;
    @Column(name = "PROCUREMENT_SUPPLIER_ID")
    private BigInteger procurementSupplierId;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public ChosenSupplierBeHistory() {
    }

    public ChosenSupplierBeHistory(ChosenSupplierBeHistoryPK chosenSupplierBeHistoryPK) {
        this.chosenSupplierBeHistoryPK = chosenSupplierBeHistoryPK;
    }

    public ChosenSupplierBeHistory(long id, int rev) {
        this.chosenSupplierBeHistoryPK = new ChosenSupplierBeHistoryPK(id, rev);
    }

    public ChosenSupplierBeHistoryPK getChosenSupplierBeHistoryPK() {
        return chosenSupplierBeHistoryPK;
    }

    public void setChosenSupplierBeHistoryPK(ChosenSupplierBeHistoryPK chosenSupplierBeHistoryPK) {
        this.chosenSupplierBeHistoryPK = chosenSupplierBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Date getStatChgDate() {
        return statChgDate;
    }

    public void setStatChgDate(Date statChgDate) {
        this.statChgDate = statChgDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getApprovedByUserId() {
        return approvedByUserId;
    }

    public void setApprovedByUserId(Integer approvedByUserId) {
        this.approvedByUserId = approvedByUserId;
    }

    public BigInteger getProcurementId() {
        return procurementId;
    }

    public void setProcurementId(BigInteger procurementId) {
        this.procurementId = procurementId;
    }

    public BigInteger getProcurementSupplierId() {
        return procurementSupplierId;
    }

    public void setProcurementSupplierId(BigInteger procurementSupplierId) {
        this.procurementSupplierId = procurementSupplierId;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (chosenSupplierBeHistoryPK != null ? chosenSupplierBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChosenSupplierBeHistory)) {
            return false;
        }
        ChosenSupplierBeHistory other = (ChosenSupplierBeHistory) object;
        if ((this.chosenSupplierBeHistoryPK == null && other.chosenSupplierBeHistoryPK != null) || (this.chosenSupplierBeHistoryPK != null && !this.chosenSupplierBeHistoryPK.equals(other.chosenSupplierBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.ChosenSupplierBeHistory[ chosenSupplierBeHistoryPK=" + chosenSupplierBeHistoryPK + " ]";
    }
    
}
