/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "BRANCH_BE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BranchBe.findAll", query = "SELECT b FROM BranchBe b")
    , @NamedQuery(name = "BranchBe.findByCode", query = "SELECT b FROM BranchBe b WHERE b.code = :code")
    , @NamedQuery(name = "BranchBe.findByDescription", query = "SELECT b FROM BranchBe b WHERE b.description = :description")
    , @NamedQuery(name = "BranchBe.findByExtIdentifier", query = "SELECT b FROM BranchBe b WHERE b.extIdentifier = :extIdentifier")})
public class BranchBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODE")
    private String code;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "EXT_IDENTIFIER")
    private String extIdentifier;
    @JoinColumn(name = "CUSTOMERS_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private CustomersBe customersId;

    public BranchBe() {
    }

    public BranchBe(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExtIdentifier() {
        return extIdentifier;
    }

    public void setExtIdentifier(String extIdentifier) {
        this.extIdentifier = extIdentifier;
    }

    public CustomersBe getCustomersId() {
        return customersId;
    }

    public void setCustomersId(CustomersBe customersId) {
        this.customersId = customersId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BranchBe)) {
            return false;
        }
        BranchBe other = (BranchBe) object;
        return (this.code != null || other.code == null) && (this.code == null || this.code.equals(other.code));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.BranchBe[ code=" + code + " ]";
    }
    
}
