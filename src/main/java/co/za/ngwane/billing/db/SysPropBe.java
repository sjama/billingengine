/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "SYS_PROP_BE")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "SysPropBe.findAll", query = "SELECT s FROM SysPropBe s")
//    , @NamedQuery(name = "SysPropBe.findByPropKey", query = "SELECT s FROM SysPropBe s WHERE s.propKey = :propKey")
//    , @NamedQuery(name = "SysPropBe.findByPropValue", query = "SELECT s FROM SysPropBe s WHERE s.propValue = :propValue")})
public class SysPropBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROP_KEY")
    private String propKey;
    @Column(name = "PROP_VALUE")
    private String propValue;

    public SysPropBe() {
    }

    public SysPropBe(String propKey) {
        this.propKey = propKey;
    }

    public String getPropKey() {
        return propKey;
    }

    public void setPropKey(String propKey) {
        this.propKey = propKey;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (propKey != null ? propKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SysPropBe)) {
            return false;
        }
        SysPropBe other = (SysPropBe) object;
        return (this.propKey != null || other.propKey == null) && (this.propKey == null || this.propKey.equals(other.propKey));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.SysPropBe[ propKey=" + propKey + " ]";
    }
    
}
