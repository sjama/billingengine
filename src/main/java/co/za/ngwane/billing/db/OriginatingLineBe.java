/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "ORIGINATING_LINE_BE")
@XmlRootElement
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
public class OriginatingLineBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LINE")
    private String line;
    @JoinColumn(name = "CUSTOMERS_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private CustomersBe customersId;
    
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    
    @Transient
    private String tagName;
    @Transient
    private String tagFlag;
    @Transient
    private String tagMsg;

    public OriginatingLineBe() {
    }

    public OriginatingLineBe(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public CustomersBe getCustomersId() {
        return customersId;
    }

    public void setCustomersId(CustomersBe customersId) {
        this.customersId = customersId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagFlag() {
        return tagFlag;
    }

    public void setTagFlag(String tagFlag) {
        this.tagFlag = tagFlag;
    }

    public String getTagMsg() {
        return tagMsg;
    }

    public void setTagMsg(String tagMsg) {
        this.tagMsg = tagMsg;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (line != null ? line.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OriginatingLineBe)) {
            return false;
        }
        OriginatingLineBe other = (OriginatingLineBe) object;
        return (this.line != null || other.line == null) && (this.line == null || this.line.equals(other.line));
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.OriginatingLineBe[ line=" + line + " ]";
    }
    
}
