/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "ICASA_RATES_BE")
@XmlRootElement
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@NamedQueries({
    @NamedQuery(name = "IcasaRatesBe.findAll", query = "SELECT i FROM IcasaRatesBe i")
    , @NamedQuery(name = "IcasaRatesBe.findByCode", query = "SELECT i FROM IcasaRatesBe i WHERE i.code = :code")
    , @NamedQuery(name = "IcasaRatesBe.findByDescription", query = "SELECT i FROM IcasaRatesBe i WHERE i.description = :description")
    , @NamedQuery(name = "IcasaRatesBe.findByBillRates", query = "SELECT i FROM IcasaRatesBe i WHERE i.billRates = :billRates")
    , @NamedQuery(name = "IcasaRatesBe.findByNeoRates", query = "SELECT i FROM IcasaRatesBe i WHERE i.neoRates = :neoRates")
    , @NamedQuery(name = "IcasaRatesBe.findByStatus", query = "SELECT i FROM IcasaRatesBe i WHERE i.status = :status")
    , @NamedQuery(name = "IcasaRatesBe.findByStatChgD", query = "SELECT i FROM IcasaRatesBe i WHERE i.statChgD = :statChgD")})
public class IcasaRatesBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CODE")
    private Long code;
    @Column(name = "DESCRIPTION")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BILL_RATES")
    private Float billRates;
    @Column(name = "NEO_RATES")
    private Float neoRates;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;

    public IcasaRatesBe() {
    }

    public IcasaRatesBe(Long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getBillRates() {
        return billRates;
    }

    public void setBillRates(Float billRates) {
        this.billRates = billRates;
    }

    public Float getNeoRates() {
        return neoRates;
    }

    public void setNeoRates(Float neoRates) {
        this.neoRates = neoRates;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IcasaRatesBe)) {
            return false;
        }
        IcasaRatesBe other = (IcasaRatesBe) object;
        return (this.code != null || other.code == null) && (this.code == null || this.code.equals(other.code));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.IcasaRatesBe[ code=" + code + " ]";
    }
    
}
