/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "ADDRESS_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AddressBeHistory.findAll", query = "SELECT a FROM AddressBeHistory a")
    , @NamedQuery(name = "AddressBeHistory.findById", query = "SELECT a FROM AddressBeHistory a WHERE a.addressBeHistoryPK.id = :id")
    , @NamedQuery(name = "AddressBeHistory.findByRev", query = "SELECT a FROM AddressBeHistory a WHERE a.addressBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "AddressBeHistory.findByRevtype", query = "SELECT a FROM AddressBeHistory a WHERE a.revtype = :revtype")
    , @NamedQuery(name = "AddressBeHistory.findByPhyAddrCity", query = "SELECT a FROM AddressBeHistory a WHERE a.phyAddrCity = :phyAddrCity")
    , @NamedQuery(name = "AddressBeHistory.findByPhyAddrCode", query = "SELECT a FROM AddressBeHistory a WHERE a.phyAddrCode = :phyAddrCode")
    , @NamedQuery(name = "AddressBeHistory.findByPhyAddrLine1", query = "SELECT a FROM AddressBeHistory a WHERE a.phyAddrLine1 = :phyAddrLine1")
    , @NamedQuery(name = "AddressBeHistory.findByPhyAddrLine2", query = "SELECT a FROM AddressBeHistory a WHERE a.phyAddrLine2 = :phyAddrLine2")
    , @NamedQuery(name = "AddressBeHistory.findByPhyAddrLine3", query = "SELECT a FROM AddressBeHistory a WHERE a.phyAddrLine3 = :phyAddrLine3")
    , @NamedQuery(name = "AddressBeHistory.findByPhyAddrSuburb", query = "SELECT a FROM AddressBeHistory a WHERE a.phyAddrSuburb = :phyAddrSuburb")
    , @NamedQuery(name = "AddressBeHistory.findByPosAddrCity", query = "SELECT a FROM AddressBeHistory a WHERE a.posAddrCity = :posAddrCity")
    , @NamedQuery(name = "AddressBeHistory.findByPosAddrCode", query = "SELECT a FROM AddressBeHistory a WHERE a.posAddrCode = :posAddrCode")
    , @NamedQuery(name = "AddressBeHistory.findByPosAddrLine1", query = "SELECT a FROM AddressBeHistory a WHERE a.posAddrLine1 = :posAddrLine1")
    , @NamedQuery(name = "AddressBeHistory.findByPosAddrLine2", query = "SELECT a FROM AddressBeHistory a WHERE a.posAddrLine2 = :posAddrLine2")
    , @NamedQuery(name = "AddressBeHistory.findByPosAddrLine3", query = "SELECT a FROM AddressBeHistory a WHERE a.posAddrLine3 = :posAddrLine3")
    , @NamedQuery(name = "AddressBeHistory.findByPosAddrSuburb", query = "SELECT a FROM AddressBeHistory a WHERE a.posAddrSuburb = :posAddrSuburb")
    , @NamedQuery(name = "AddressBeHistory.findByStatChgD", query = "SELECT a FROM AddressBeHistory a WHERE a.statChgD = :statChgD")
    , @NamedQuery(name = "AddressBeHistory.findByStatus", query = "SELECT a FROM AddressBeHistory a WHERE a.status = :status")
    , @NamedQuery(name = "AddressBeHistory.findByLuAddressTypeCd", query = "SELECT a FROM AddressBeHistory a WHERE a.luAddressTypeCd = :luAddressTypeCd")})
public class AddressBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AddressBeHistoryPK addressBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "PHY_ADDR_CITY")
    private String phyAddrCity;
    @Column(name = "PHY_ADDR_CODE")
    private String phyAddrCode;
    @Column(name = "PHY_ADDR_LINE1")
    private String phyAddrLine1;
    @Column(name = "PHY_ADDR_LINE2")
    private String phyAddrLine2;
    @Column(name = "PHY_ADDR_LINE3")
    private String phyAddrLine3;
    @Column(name = "PHY_ADDR_SUBURB")
    private String phyAddrSuburb;
    @Column(name = "POS_ADDR_CITY")
    private String posAddrCity;
    @Column(name = "POS_ADDR_CODE")
    private String posAddrCode;
    @Column(name = "POS_ADDR_LINE1")
    private String posAddrLine1;
    @Column(name = "POS_ADDR_LINE2")
    private String posAddrLine2;
    @Column(name = "POS_ADDR_LINE3")
    private String posAddrLine3;
    @Column(name = "POS_ADDR_SUBURB")
    private String posAddrSuburb;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "LU_ADDRESS_TYPE_CD")
    private String luAddressTypeCd;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public AddressBeHistory() {
    }

    public AddressBeHistory(AddressBeHistoryPK addressBeHistoryPK) {
        this.addressBeHistoryPK = addressBeHistoryPK;
    }

    public AddressBeHistory(long id, int rev) {
        this.addressBeHistoryPK = new AddressBeHistoryPK(id, rev);
    }

    public AddressBeHistoryPK getAddressBeHistoryPK() {
        return addressBeHistoryPK;
    }

    public void setAddressBeHistoryPK(AddressBeHistoryPK addressBeHistoryPK) {
        this.addressBeHistoryPK = addressBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getPhyAddrCity() {
        return phyAddrCity;
    }

    public void setPhyAddrCity(String phyAddrCity) {
        this.phyAddrCity = phyAddrCity;
    }

    public String getPhyAddrCode() {
        return phyAddrCode;
    }

    public void setPhyAddrCode(String phyAddrCode) {
        this.phyAddrCode = phyAddrCode;
    }

    public String getPhyAddrLine1() {
        return phyAddrLine1;
    }

    public void setPhyAddrLine1(String phyAddrLine1) {
        this.phyAddrLine1 = phyAddrLine1;
    }

    public String getPhyAddrLine2() {
        return phyAddrLine2;
    }

    public void setPhyAddrLine2(String phyAddrLine2) {
        this.phyAddrLine2 = phyAddrLine2;
    }

    public String getPhyAddrLine3() {
        return phyAddrLine3;
    }

    public void setPhyAddrLine3(String phyAddrLine3) {
        this.phyAddrLine3 = phyAddrLine3;
    }

    public String getPhyAddrSuburb() {
        return phyAddrSuburb;
    }

    public void setPhyAddrSuburb(String phyAddrSuburb) {
        this.phyAddrSuburb = phyAddrSuburb;
    }

    public String getPosAddrCity() {
        return posAddrCity;
    }

    public void setPosAddrCity(String posAddrCity) {
        this.posAddrCity = posAddrCity;
    }

    public String getPosAddrCode() {
        return posAddrCode;
    }

    public void setPosAddrCode(String posAddrCode) {
        this.posAddrCode = posAddrCode;
    }

    public String getPosAddrLine1() {
        return posAddrLine1;
    }

    public void setPosAddrLine1(String posAddrLine1) {
        this.posAddrLine1 = posAddrLine1;
    }

    public String getPosAddrLine2() {
        return posAddrLine2;
    }

    public void setPosAddrLine2(String posAddrLine2) {
        this.posAddrLine2 = posAddrLine2;
    }

    public String getPosAddrLine3() {
        return posAddrLine3;
    }

    public void setPosAddrLine3(String posAddrLine3) {
        this.posAddrLine3 = posAddrLine3;
    }

    public String getPosAddrSuburb() {
        return posAddrSuburb;
    }

    public void setPosAddrSuburb(String posAddrSuburb) {
        this.posAddrSuburb = posAddrSuburb;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLuAddressTypeCd() {
        return luAddressTypeCd;
    }

    public void setLuAddressTypeCd(String luAddressTypeCd) {
        this.luAddressTypeCd = luAddressTypeCd;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addressBeHistoryPK != null ? addressBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AddressBeHistory)) {
            return false;
        }
        AddressBeHistory other = (AddressBeHistory) object;
        if ((this.addressBeHistoryPK == null && other.addressBeHistoryPK != null) || (this.addressBeHistoryPK != null && !this.addressBeHistoryPK.equals(other.addressBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.AddressBeHistory[ addressBeHistoryPK=" + addressBeHistoryPK + " ]";
    }
    
}
