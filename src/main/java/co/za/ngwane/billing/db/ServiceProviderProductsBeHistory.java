/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "SERVICE_PROVIDER_PRODUCTS_BE_HISTORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServiceProviderProductsBeHistory.findAll", query = "SELECT s FROM ServiceProviderProductsBeHistory s")
    , @NamedQuery(name = "ServiceProviderProductsBeHistory.findByCode", query = "SELECT s FROM ServiceProviderProductsBeHistory s WHERE s.serviceProviderProductsBeHistoryPK.code = :code")
    , @NamedQuery(name = "ServiceProviderProductsBeHistory.findByRev", query = "SELECT s FROM ServiceProviderProductsBeHistory s WHERE s.serviceProviderProductsBeHistoryPK.rev = :rev")
    , @NamedQuery(name = "ServiceProviderProductsBeHistory.findByRevtype", query = "SELECT s FROM ServiceProviderProductsBeHistory s WHERE s.revtype = :revtype")
    , @NamedQuery(name = "ServiceProviderProductsBeHistory.findByDescription", query = "SELECT s FROM ServiceProviderProductsBeHistory s WHERE s.description = :description")
    , @NamedQuery(name = "ServiceProviderProductsBeHistory.findByPriceAmount", query = "SELECT s FROM ServiceProviderProductsBeHistory s WHERE s.priceAmount = :priceAmount")
    , @NamedQuery(name = "ServiceProviderProductsBeHistory.findByLuServiceProviderBecode", query = "SELECT s FROM ServiceProviderProductsBeHistory s WHERE s.luServiceProviderBecode = :luServiceProviderBecode")})
public class ServiceProviderProductsBeHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ServiceProviderProductsBeHistoryPK serviceProviderProductsBeHistoryPK;
    @Column(name = "REVTYPE")
    private Short revtype;
    @Column(name = "DESCRIPTION")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRICE_AMOUNT")
    private Float priceAmount;
    @Column(name = "LU_SERVICE_PROVIDER_BECODE")
    private String luServiceProviderBecode;
    @JoinColumn(name = "REV", referencedColumnName = "REV", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Revinfo revinfo;

    public ServiceProviderProductsBeHistory() {
    }

    public ServiceProviderProductsBeHistory(ServiceProviderProductsBeHistoryPK serviceProviderProductsBeHistoryPK) {
        this.serviceProviderProductsBeHistoryPK = serviceProviderProductsBeHistoryPK;
    }

    public ServiceProviderProductsBeHistory(int code, int rev) {
        this.serviceProviderProductsBeHistoryPK = new ServiceProviderProductsBeHistoryPK(code, rev);
    }

    public ServiceProviderProductsBeHistoryPK getServiceProviderProductsBeHistoryPK() {
        return serviceProviderProductsBeHistoryPK;
    }

    public void setServiceProviderProductsBeHistoryPK(ServiceProviderProductsBeHistoryPK serviceProviderProductsBeHistoryPK) {
        this.serviceProviderProductsBeHistoryPK = serviceProviderProductsBeHistoryPK;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPriceAmount() {
        return priceAmount;
    }

    public void setPriceAmount(Float priceAmount) {
        this.priceAmount = priceAmount;
    }

    public String getLuServiceProviderBecode() {
        return luServiceProviderBecode;
    }

    public void setLuServiceProviderBecode(String luServiceProviderBecode) {
        this.luServiceProviderBecode = luServiceProviderBecode;
    }

    public Revinfo getRevinfo() {
        return revinfo;
    }

    public void setRevinfo(Revinfo revinfo) {
        this.revinfo = revinfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serviceProviderProductsBeHistoryPK != null ? serviceProviderProductsBeHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServiceProviderProductsBeHistory)) {
            return false;
        }
        ServiceProviderProductsBeHistory other = (ServiceProviderProductsBeHistory) object;
        if ((this.serviceProviderProductsBeHistoryPK == null && other.serviceProviderProductsBeHistoryPK != null) || (this.serviceProviderProductsBeHistoryPK != null && !this.serviceProviderProductsBeHistoryPK.equals(other.serviceProviderProductsBeHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.ServiceProviderProductsBeHistory[ serviceProviderProductsBeHistoryPK=" + serviceProviderProductsBeHistoryPK + " ]";
    }
    
}
