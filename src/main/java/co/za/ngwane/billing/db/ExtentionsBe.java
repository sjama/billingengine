/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "EXTENTIONS_BE")
@XmlRootElement
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@NamedQueries({
    @NamedQuery(name = "ExtentionsBe.findAll", query = "SELECT e FROM ExtentionsBe e")
    , @NamedQuery(name = "ExtentionsBe.findById", query = "SELECT e FROM ExtentionsBe e WHERE e.id = :id")
    , @NamedQuery(name = "ExtentionsBe.findByStatus", query = "SELECT e FROM ExtentionsBe e WHERE e.status = :status")
    , @NamedQuery(name = "ExtentionsBe.findByStatChgD", query = "SELECT e FROM ExtentionsBe e WHERE e.statChgD = :statChgD")})
public class ExtentionsBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date statChgD;
    @JoinColumn(name = "BRANCH_CD", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private BranchBe branchCd;

    public ExtentionsBe() {
    }

    public ExtentionsBe(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public BranchBe getBranchCd() {
        return branchCd;
    }

    public void setBranchCd(BranchBe branchCd) {
        this.branchCd = branchCd;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExtentionsBe)) {
            return false;
        }
        ExtentionsBe other = (ExtentionsBe) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.ExtentionsBe[ id=" + id + " ]";
    }
    
}
