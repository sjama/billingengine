/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "LU_BILLABLE_ITEMS_BE")
@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "LuBillableItems.findAll", query = "SELECT l FROM LuBillableItems l")
//    , @NamedQuery(name = "LuBillableItems.findByCode", query = "SELECT l FROM LuBillableItems l WHERE l.code = :code")
//    , @NamedQuery(name = "LuBillableItems.findByDescription", query = "SELECT l FROM LuBillableItems l WHERE l.description = :description")
//    , @NamedQuery(name = "LuBillableItems.findByAmount", query = "SELECT l FROM LuBillableItems l WHERE l.amount = :amount")})
public class LuBillableItemsBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODE")
    private String code;
    @Column(name = "DESCRIPTION")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AMOUNT")
    private Float amount;

    public LuBillableItemsBe() {
    }

    public LuBillableItemsBe(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LuBillableItemsBe)) {
            return false;
        }
        LuBillableItemsBe other = (LuBillableItemsBe) object;
        return (this.code != null || other.code == null) && (this.code == null || this.code.equals(other.code));
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.LuBillableItemsBe[ code=" + code + " ]";
    }
    
}
