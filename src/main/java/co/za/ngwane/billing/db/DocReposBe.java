/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "DOC_REPOS_BE")
@XmlRootElement
public class DocReposBe implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "REF_NO")
    private String refNo;
    
    @Lob
    @Column(name = "FILE_DATA")
    private byte[] fileData;
    @Column(name = "FILE_NAME")
    private String fileName;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.DATE)
    private Date createdDate;
    
    @JoinColumn(name = "LU_REF_NO_DESC_CD", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private LuRefNoDescBe luRefNoDescCd;

    public DocReposBe() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public LuRefNoDescBe getLuRefNoDescCd() {
        return luRefNoDescCd;
    }

    public void setLuRefNoDescCd(LuRefNoDescBe luRefNoDescCd) {
        this.luRefNoDescCd = luRefNoDescCd;
    }

    public DocReposBe(String refNo) {
        this.refNo = refNo;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refNo != null ? refNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocReposBe)) {
            return false;
        }
        DocReposBe other = (DocReposBe) object;
        return (this.refNo != null || other.refNo == null) && (this.refNo == null || this.refNo.equals(other.refNo));
    }

    @Override
    public String toString() {
        return "za.co.jbjTech.brilliant.be.db.DocReposBe[ refNo=" + refNo + " ]";
    }
    
}
