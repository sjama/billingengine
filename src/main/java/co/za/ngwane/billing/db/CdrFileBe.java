/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.db;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bheki.lubisi
 */
@Entity
@Table(name = "CDR_FILE_BE")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CdrFileBe.findAll", query = "SELECT c FROM CdrFileBe c")
        , @NamedQuery(name = "CdrFileBe.findById", query = "SELECT c FROM CdrFileBe c WHERE c.id = :id")
        , @NamedQuery(name = "CdrFileBe.findByFilePeriod", query = "SELECT c FROM CdrFileBe c WHERE c.filePeriod = :filePeriod")
        , @NamedQuery(name = "CdrFileBe.findByCdrFileRefNo", query = "SELECT c FROM CdrFileBe c WHERE c.cdrFileRefNo = :cdrFileRefNo")
        , @NamedQuery(name = "CdrFileBe.findByCdrFilePath", query = "SELECT c FROM CdrFileBe c WHERE c.cdrFilePath = :cdrFilePath")
        , @NamedQuery(name = "CdrFileBe.findByCdrFileName", query = "SELECT c FROM CdrFileBe c WHERE c.cdrFileName = :cdrFileName")
        , @NamedQuery(name = "CdrFileBe.findByDateReceived", query = "SELECT c FROM CdrFileBe c WHERE c.dateReceived = :dateReceived")
        , @NamedQuery(name = "CdrFileBe.findByStatus", query = "SELECT c FROM CdrFileBe c WHERE c.status = :status")
        , @NamedQuery(name = "CdrFileBe.findByStatChgD", query = "SELECT c FROM CdrFileBe c WHERE c.statChgD = :statChgD")})
public class CdrFileBe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "FILE_PERIOD")
    @Temporal(TemporalType.DATE)
    private Date filePeriod;
    @Column(name = "CDR_FILE_REF_NO")
    private String cdrFileRefNo;
    @Column(name = "CDR_FILE_PATH")
    private String cdrFilePath;
    @Column(name = "CDR_FILE_NAME")
    private String cdrFileName;
    @Column(name = "DATE_RECEIVED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReceived;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "STAT_CHG_D")
    @Temporal(TemporalType.TIMESTAMP)
    private Date statChgD;

    @JoinColumn(name = "LU_BILL_PROCESS_STAT_CD", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private LuBillProcessStatBe luBillProStatus;

    @Column(name = "PRO_STAT_CHG_D")
    @Temporal(TemporalType.DATE)
    private Date proStatChgD;

    @Transient
    private String tagName;
    @Transient
    private String tagFlag;
    @Transient
    private String tagMsg;

    public CdrFileBe() {
    }

    public CdrFileBe(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFilePeriod() {
        return filePeriod;
    }

    public void setFilePeriod(Date filePeriod) {
        this.filePeriod = filePeriod;
    }

    public String getCdrFileRefNo() {
        return cdrFileRefNo;
    }

    public void setCdrFileRefNo(String cdrFileRefNo) {
        this.cdrFileRefNo = cdrFileRefNo;
    }

    public String getCdrFilePath() {
        return cdrFilePath;
    }

    public void setCdrFilePath(String cdrFilePath) {
        this.cdrFilePath = cdrFilePath;
    }

    public String getCdrFileName() {
        return cdrFileName;
    }

    public void setCdrFileName(String cdrFileName) {
        this.cdrFileName = cdrFileName;
    }

    public Date getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatChgD() {
        return statChgD;
    }

    public void setStatChgD(Date statChgD) {
        this.statChgD = statChgD;
    }

    public LuBillProcessStatBe getLuBillProStatus() {
        return luBillProStatus;
    }

    public void setLuBillProStatus(LuBillProcessStatBe luBillProStatus) {
        this.luBillProStatus = luBillProStatus;
    }

    public Date getProStatChgD() {
        return proStatChgD;
    }

    public void setProStatChgD(Date proStatChgD) {
        this.proStatChgD = proStatChgD;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagFlag() {
        return tagFlag;
    }

    public void setTagFlag(String tagFlag) {
        this.tagFlag = tagFlag;
    }

    public String getTagMsg() {
        return tagMsg;
    }

    public void setTagMsg(String tagMsg) {
        this.tagMsg = tagMsg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CdrFileBe)) {
            return false;
        }
        CdrFileBe other = (CdrFileBe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.ngwane.billing.db.CdrFileBe[ id=" + id + " ]";
    }

}
