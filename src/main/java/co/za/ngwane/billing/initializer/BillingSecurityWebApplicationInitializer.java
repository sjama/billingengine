package co.za.ngwane.billing.initializer;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class BillingSecurityWebApplicationInitializer
        extends AbstractSecurityWebApplicationInitializer {

}