package co.za.ngwane.billing.certificates.util;

import java.lang.annotation.*;

/**
 * Specifies that a field should be used when PDF or certrificate String gets generated
 *
 * @author Jacob Marola
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface TemplateField {

    /**
     * Value of form field in template
     */
    String value() default "";

    /**
     * Page on which field is located 
     */
    int page() default 1;

    /**
     * The XML element (if applicable) 
     */
    String xmlElement() default "";

    /**
     * The instance of the form field in the template
     * (in cases where value is used multiple times in document)  
     */
    int instance() default 0;

    /**
     * Order of  the certificate field string
     * <p >0 = to ignore field when building a string certificate</p>
     */
    int[] order() default 0;

    /**
     * Format pattern to be used for:
     * <p>a Joda date/time - {@link org.joda.time.format.DateTimeFormat}</p>
     * <p>a Number - {@link java.text.DecimalFormat}</p>
     * <p>example: <code >#.00</code> for a currency value</p>
     * <p>example: <code >HH:mm:ss</code> for a time</p>
     */
    String format() default "yyyy-MM-dd";

}
