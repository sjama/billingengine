package co.za.ngwane.billing.validator.impl.validators.support;

//package co.za.rtmc.choco.validator.impl.validators.support;
//
//import org.apache.commons.collections.CollectionUtils;
//import org.apache.commons.collections.Predicate;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.binding.message.Message;
//import org.springframework.binding.message.Severity;
//import org.springframework.stereotype.Component;
//import tasima.accidents.dao.AccidentDao;
//import tasima.accidents.domain.AccidentEntity;
//import tasima.accidents.domain.AccidentPersonEntity;
//import tasima.accidents.domain.enums.AccidentCaptureStatus;
//import tasima.accidents.domain.enums.AccidentPersonType;
//import tasima.accidents.domain.enums.InjurySeverity;
//import tasima.accidents.validators.AccidentCompletenessChecker;
//
//import java.util.List;
//
///**
// * <strong>Description</strong> <br>
// *
// * @author Jacob
// */
//@Component
//public class AccidentCompletenessCheckerImpl implements AccidentCompletenessChecker {
//
//    private static final Logger LOG = Logger.getLogger(AccidentCompletenessCheckerImpl.class);
//    @Autowired private AccidentDao accidentDao;
//
//    @Override
//    public boolean checkAccidentDuplicate(AccidentEntity accident, List<Message> messages) {
//
//        if(accident.getVehicles().size() < 1) {
//            return true;
//        }
//
//        List<AccidentEntity> duplicates = accidentDao.getAccidentDuplicates(accident);
//        StringBuilder sb = new StringBuilder();
//        for (AccidentEntity duplicate : duplicates) {
//            if(sb.length()>0){
//                sb.append(", ");
//            }
//            sb.append(duplicate.getTarn());
//        }
//
//        LOG.debug("Duplicate Accident Report(s) for [" + accident.getTarn() + "] is [" + sb.toString() + "]");
//        if (duplicates.size() > 0) {
//            Message message = new Message("tarn", "Duplicate Accident Report(s) found: " + sb.toString(), Severity.INFO);
//            messages.add(message);
//        }
//
//        return messages.isEmpty();
//    }
//
//
//
//    @Override
//    public boolean checkAccidentComplete(AccidentEntity accident, List<Message> messages) {
//        return validateComplete(accident, messages);
//    }
//
//    @Override
//    public void updateDuplicates(AccidentCaptureStatus previousCaptureStatus, AccidentEntity accident) {
//
//        if(accident.getVehicles().size() < 1) {
//            return;
//        }
//
//        List<AccidentEntity> duplicates = accidentDao.getAccidentDuplicates(accident);
//        if(duplicates.size() == 0) {
//            return;
//        }
//
//        if(accident.getAccidentCaptureStatus() == AccidentCaptureStatus.COMPLETE) {
//            // this one is complete, make all other duplicates
//            for (AccidentEntity duplicate : duplicates) {
//                if(duplicate.getAccidentCaptureStatus() == AccidentCaptureStatus.COMPLETE) {
//                    duplicate.setCaptureStatus(AccidentCaptureStatus.DUPLICATE.getCode());
//                }
//            }
//        } else if(previousCaptureStatus == AccidentCaptureStatus.COMPLETE &&
//                accident.getAccidentCaptureStatus() == AccidentCaptureStatus.INCOMPLETE) {
//            // if this one was COMPLETE, but now incomplete. make one of the other duplicates COMPLETE
//            for (AccidentEntity duplicate : duplicates) {
//                if(duplicate.getAccidentCaptureStatus() == AccidentCaptureStatus.DUPLICATE) {
//                    duplicate.setCaptureStatus(AccidentCaptureStatus.COMPLETE.getCode());
//                    break;
//                }
//            }
//        }
//    }
//
//
//    private boolean validateComplete(AccidentEntity accident, List<Message> messages) {
//
//        if(accident.getNumberOfVehicles() > accident.getVehicles().size()) {
//            Message message = new Message("numVehicles", "Not all vehicles were captured", Severity.WARNING);
//            messages.add(message);
//        }
//
//        int killed = CollectionUtils.countMatches(accident.getPersons(), new Predicate() {
//            public boolean evaluate(Object object) {
//                AccidentPersonEntity accidentPerson = (AccidentPersonEntity) object;
//                return InjurySeverity.KILLED.getCode().equals(accidentPerson.getInjurySeverity());
//            }
//        });
//        int serious = CollectionUtils.countMatches(accident.getPersons(), new Predicate() {
//            public boolean evaluate(Object object) {
//                AccidentPersonEntity accidentPerson = (AccidentPersonEntity) object;
//                return InjurySeverity.SERIOUS.getCode().equals(accidentPerson.getInjurySeverity());
//            }
//        });
//        int slight = CollectionUtils.countMatches(accident.getPersons(), new Predicate() {
//            public boolean evaluate(Object object) {
//                AccidentPersonEntity accidentPerson = (AccidentPersonEntity) object;
//                return InjurySeverity.SLIGHT.getCode().equals(accidentPerson.getInjurySeverity());
//            }
//        });
//        int notInjured = CollectionUtils.countMatches(accident.getPersons(), new Predicate() {
//            public boolean evaluate(Object object) {
//                AccidentPersonEntity accidentPerson = (AccidentPersonEntity) object;
//                return InjurySeverity.NO_INJURY.getCode().equals(accidentPerson.getInjurySeverity());
//            }
//        });
//
//        if(accident.getNumberPersonsKilled()!=null && accident.getNumberPersonsKilled() > killed) {
//            Message message = new Message("numKilled", "Not all killed persons were captured", Severity.WARNING);
//            messages.add(message);
//        } else if(accident.getNumberPersonsKilled()!=null && accident.getNumberPersonsKilled() < killed) {
//            Message message = new Message("numKilled", "Too many killed persons were captured", Severity.WARNING);
//            messages.add(message);
//        }
//
//        if(accident.getNumberPersonsSeriouslyInjured()!=null && accident.getNumberPersonsSeriouslyInjured() > serious) {
//            Message message = new Message("numSeriousInjured", "Not all seriously injured persons were captured", Severity.WARNING);
//            messages.add(message);
//        } else if(accident.getNumberPersonsSeriouslyInjured()!=null && accident.getNumberPersonsSeriouslyInjured() < serious) {
//            Message message = new Message("numSeriousInjured", "Too many seriously injured persons were captured", Severity.WARNING);
//            messages.add(message);
//        }
//
//        if(accident.getNumberPersonsSlightlyInjured()!=null && accident.getNumberPersonsSlightlyInjured() > slight) {
//            Message message = new Message("numSlightlyInjured", "Not all slightly injured persons were captured", Severity.WARNING);
//            messages.add(message);
//        } else if(accident.getNumberPersonsSlightlyInjured()!=null && accident.getNumberPersonsSlightlyInjured() < slight) {
//            Message message = new Message("numSlightlyInjured", "Too many slightly injured persons were captured", Severity.WARNING);
//            messages.add(message);
//        }
//
//        if(accident.getNumberPersonsNotInjured()!=null && accident.getNumberPersonsNotInjured() > notInjured) {
//            Message message = new Message("numSlightlyInjured", "Not all uninjured persons were captured", Severity.WARNING);
//            messages.add(message);
//        } else if(accident.getNumberPersonsNotInjured()!=null && accident.getNumberPersonsNotInjured() < notInjured) {
//            Message message = new Message("numSlightlyInjured", "Not all uninjured persons were captured", Severity.WARNING);
//            messages.add(message);
//        }
//
//        validateDriversAssignedToVehicle(accident.getPersons(), messages);
//
//        return messages.isEmpty();
//    }
//
//    private void validateDriversAssignedToVehicle(List<AccidentPersonEntity> persons, List<Message> messages) {
//
//        // no driver assigned to vehicle
//        for (AccidentPersonEntity person : persons) {
//            if(AccidentPersonType.DRIVER.getCode().equals(person.getPersonType())) {
//                if(person.getVehicle() == null) {
//                    Message message = new Message("driverId", "driver "+person.buildPersonLabel()+" not assigned to a vehicle", Severity.WARNING);
//                    messages.add(message);
//                }
//            }
//        }
//    }
//}
