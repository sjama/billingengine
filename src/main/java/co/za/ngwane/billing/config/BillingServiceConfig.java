package co.za.ngwane.billing.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("co.za.ngwane.billing.service")
public class BillingServiceConfig {
   
     

}
