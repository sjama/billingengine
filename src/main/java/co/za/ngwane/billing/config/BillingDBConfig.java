package co.za.ngwane.billing.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.dozer.DozerBeanMapper;
import org.hibernate.dialect.Oracle10gDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@EnableTransactionManagement
@ComponentScan("co.za.ngwane.billing")
@EnableJpaRepositories("co.za.ngwane.billing.repository")
public class BillingDBConfig {

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("co.za.ngwane.billing", "co.za.ngwane.billing.validator.intf");
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());
        return em;
    }

    @Bean
    public Map<String, Object> jpaProperties() {
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("hibernate.dialect", Oracle10gDialect.class.getName());
        props.put("hibernate.hbm2ddl.auto", "none"); /*update, create, delete*/
//        props.put("hibernate.show_sql", "true");
//        props.put("hibernate.format_sql", "true");
        props.put("hibernate.connection.charSet", "UTF-8");
        props.put("hibernate.cache.use_second_level_cache", "false");
        props.put("hibernate.cache.use_query_cache", "false");
        props.put("hibernate.cache.provider_class", "org.hibernate.cache.HashtableCacheProvider");
        props.put("hibernate.enable_lazy_load_no_trans", "true");
        props.put("hibernate.connection.release_mode", "after_transaction");
        //  props.put("hibernate.generate_statistics", "true");
        props.put("org.hibernate.envers.audit_table_suffix", "_HISTORY");


        return props;
    }

    @Bean
    public javax.validation.Validator localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public DataSource dataSource() {

        // JndiDataSourceLookup lookup = new JndiDataSourceLookup();
        // return lookup.getDataSource("crashDS");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");

        /*INT properties*/
        dataSource.setUrl("jdbc:mysql://197.242.156.83:3306/BE_Dev?user_reg?useSSL=false");
        dataSource.setUsername("root");
        //dataSource.setPassword("root");
        dataSource.setPassword("Ngwane@123$");

        /*DEV properties*/
//        dataSource.setUrl("jdbc:mysql://localhost:3306/BE_Dev");
//        dataSource.setUsername("root");
//        dataSource.setPassword("root");

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    final Properties additionalProperties() {

        final Properties hibernateProperties = new Properties();

//        hibernateProperties.setProperty("hibernate.hbm2ddl.auto","none" );
//        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect" );
//        hibernateProperties.setProperty("hibernate.show_sql", "true");
//        hibernateProperties.setProperty("hibernate.enable_lazy_load_no_trans", "true");

        /*hibernateProperties.setProperty("eclipselink.weaving", "false");
        hibernateProperties.setProperty("eclipselink.cache.shared.default", "false");
        hibernateProperties.setProperty("shared-cache-mode", "none");
        hibernateProperties.setProperty("eclipselink.query-results-cache", "false");
        hibernateProperties.setProperty("eclipselink.refresh", "true");

        hibernateProperties.put("org.hibernate.envers.audit_table_suffix", "_HISTORY");*/

        /*********************************/
        hibernateProperties.setProperty("hibernate.hbm2ddl.auto","update" );
        //hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect" );
        hibernateProperties.setProperty("hibernate.show_sql", "true");
        hibernateProperties.put("org.hibernate.envers.audit_table_suffix", "_HISTORY");

        return hibernateProperties;
    }

    @Bean
    public DozerBeanMapper getMapper() {
        return new DozerBeanMapper();
    }

}
