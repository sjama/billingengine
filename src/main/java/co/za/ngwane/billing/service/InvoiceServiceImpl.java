package co.za.ngwane.billing.service;

import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.*;
import co.za.ngwane.billing.repository.*;
import co.za.ngwane.billing.utils.DateUtils;
import co.za.ngwane.billing.utils.UUIDGenerator;
import co.za.ngwane.billing.vo.EmailVo;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceServiceImpl {
    private static final String TEMPLATE_FILE = "/invoice.pdf";
    private File invoiceTemplate = null;
    private static ArrayList<BillDetBe> billDetails = null;
    static double totalDuration = 0;
    static BigDecimal totalAmount = BigDecimal.ZERO;

    private int item_N_CLIET = 10;

    @Autowired
    public InvoiceBuilderServiceImpl invoiceServiceImpl;

    @Autowired
    private EmailsService emailsService;

    @Autowired
    BillDetBeRepo billDetBeRepo;

    @Autowired
    CustomerRepo customerRepo;

    @Autowired
    OriginatingLineRepo lineRepo;


    @Autowired
    InvoiceBeRepo invoiceRepo;

    @Autowired
    private CompanyRepo companyRepo;


    public void processInvoice(int month, int year, String cdrFileRefNo, LuRefNoDescBe luRefNoDescCd) {

        // We first need to get all customers
        List<CustomersBe> customersBes = customerRepo.findAll();
        for (CustomersBe be : customersBes) {
            totalDuration = 0;
            totalAmount = BigDecimal.ZERO;
            generateInvoice(be, month, year, cdrFileRefNo, luRefNoDescCd);
        }

        createExcelForNotifications();
        sendNotification();
    }

    private void generateInvoice(CustomersBe customer, int month, int year, String cdrFileRefNo, LuRefNoDescBe luRefNoDescCd) {

        // for each originating number, lets now get the itemized billing
        Date fromDate = DateUtils.constructDate(BillingConstants.FIRST_DAY_OF_THE_MONTH, month, year);

        LocalDate lastDayofMonthGivenDate = LocalDate.of(year, month, fromDate.getDate()).with(TemporalAdjusters.lastDayOfMonth());
        System.out.println("Last day of month is" + lastDayofMonthGivenDate.getDayOfWeek() + "," + lastDayofMonthGivenDate);
        Date toDate = DateUtils.constructDate(lastDayofMonthGivenDate.getDayOfMonth(), month, year);

        billDetails = new ArrayList<>();

        billDetails.addAll(billDetBeRepo.findByCustomersIdAndCallDateAfterAndCallDateBefore(customer, fromDate, toDate));

        if (!billDetails.isEmpty()) {
            billDetails.forEach((billEntry) -> {
                totalDuration = totalDuration + Double.parseDouble(billEntry.getDurationMin());
                totalAmount = totalAmount.add(billEntry.getAmountClient());

            });

            createInvoice(customer, fromDate, toDate, cdrFileRefNo, luRefNoDescCd);
        }
    }

    private void createInvoice(CustomersBe be,
                               Date fromDate,
                               Date toDate,
                               String cdrFileRefNo,
                               LuRefNoDescBe luRefNoDescCd) {
        // Lets build the InvoiceDto values
        InvoiceDto dto = new InvoiceDto();
        CompanyBe brilliantel = companyRepo.findOne(BillingConstants.COMPANY_BRILLIANTEL);

        String invNumber = "INV" + "_" + (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        Date currentDate = new Date();
        LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.plusMonths(1);
        Date currentDatePlusOneMonth = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

        dto.setAmount("ZAR " + String.valueOf(totalAmount.setScale(2, RoundingMode.CEILING)));
        dto.setBe(be);
        dto.setClientOrderNo("ITEM_" + item_N_CLIET);

        dto.setCustomerName(be.getName());

        //Invoice to
        dto.setDeliveredToLine1(be.getPhyAddrLine1());
        dto.setDeliveredToLine2(be.getPhyAddrLine2());
        dto.setDeliveredToLine3(be.getPhyAddrCity());
        dto.setDeliveredToLine4(be.getPhyAddrCode());

        dto.setInvoiceD(DateUtils.convertDateToString(new Date()));
        dto.setInvoiceDueD(DateUtils.convertDateToString(currentDatePlusOneMonth));
        dto.setInvoiceN(invNumber);
        dto.setInvoiceNumber(invNumber);
        //Delivered to
        dto.setInvToLine1(be.getPhyAddrLine1());
        dto.setInvToLine2(be.getPhyAddrLine2());
        dto.setInvToLine3(be.getPhyAddrCity());
        dto.setInvToLine4(be.getPhyAddrCode());

        dto.setItem("SIP");
        dto.setDescription("Airtime Supplied From " + DateUtils.convertDateToString(fromDate) + " to " + DateUtils.convertDateToString(toDate));
        dto.setQuantity("1");

        dto.setTotalAmount("ZAR " + String.valueOf(totalAmount.setScale(2, RoundingMode.CEILING)));
        dto.setTotalDuration(String.valueOf(totalDuration));
        dto.setUnitPrice("ZAR " + String.valueOf(totalAmount.setScale(2, RoundingMode.CEILING)));
        //dto.setVatN(String.valueOf(totalAmount * 0.14)); // m
        dto.setVatN(brilliantel.getVatNo());

        dto.setEmail(brilliantel.getEmail());
        dto.setFaxN(brilliantel.getFaxNo());
        dto.setClientVATNo(brilliantel.getVatNo());
        dto.setRegN(brilliantel.getRegNo());
        dto.setTelN(brilliantel.getTelNo());
        dto.setAccountHolder(brilliantel.getName());
        dto.setAccountN(brilliantel.getBankAccNo());
        dto.setBank(brilliantel.getBankName());
        dto.setBranch(brilliantel.getBankBranch());
        dto.setBranchCd(brilliantel.getBankBranchCd());

        dto.setVatTotal("ZAR " + String.valueOf((totalAmount.multiply(new BigDecimal(0.14))).setScale(2, RoundingMode.CEILING)));
        dto.setVatPercent("15 %");
        dto.setGrandTotal("ZAR " + totalAmount.add(totalAmount.multiply(new BigDecimal(0.14))).setScale(2, RoundingMode.CEILING));
        dto.setSubTotal("ZAR " + String.valueOf(totalAmount.setScale(2, RoundingMode.CEILING)));

        item_N_CLIET++;
        String invKey = UUIDGenerator.getKey();
        byte[] invoice = invoiceServiceImpl.createInvoice(dto, invNumber, invKey, luRefNoDescCd);

        //writeBytesToFileClassic(invoice, "/Users/bheki.lubisi/Desktop/invoice_" + be.getName().replace("/", "") + "_.pdf");

        saveToRepository(be, invNumber, fromDate, toDate, invKey, cdrFileRefNo);
    }

    private static void writeBytesToFileClassic(byte[] bFile, String fileDest) {

        FileOutputStream fileOuputStream = null;

        try {
            fileOuputStream = new FileOutputStream(fileDest);
            fileOuputStream.write(bFile);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOuputStream != null) {
                try {
                    fileOuputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void saveToRepository(CustomersBe be,
                                  String invNumber,
                                  Date fromDate,
                                  Date toDate,
                                  String invKey,
                                  String cdrFileRefNo) {
        InvoiceBe entity = new InvoiceBe();
        entity.setRefNo(invKey);
        entity.setCustomersId(be);
        entity.setInvDate(new Date());
        entity.setInvEndDate(toDate);
        entity.setInvNo(invNumber);
        entity.setInvStartDate(fromDate);
        entity.setInvAmout(totalAmount);
        entity.setInvTotMin(new BigDecimal(totalDuration));
        entity.setCdrFileRefNo(cdrFileRefNo);
        try {
            invoiceRepo.save(entity);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private static void createExcelForNotifications() {

    }

    private void sendNotification() {
        List<InvoiceBe> list = invoiceRepo.findAll();


        if (!list.isEmpty()) {
            String subject = "Invoice (s) generated";
            String msg = "Dear Finance" + "\n\n"
                    + " The latest invoices have been successfully generated and are ready for review, " +
                    "please log on to the Billing system "
                    + "to view the generated invoices."
                    + "\n\n"
                    + "Kind Regards,"
                    + "\nBilling System";

            EmailVo emailVo = new EmailVo();
            emailVo.setMsg(msg);
            emailVo.setSubject(subject);
            emailVo.setTo("mokkhutu@gmail.com");
            emailVo.setBcc(BillingConstants.EMAIL_NOTIFICATION_BCC);

            EmailsService.mail(emailVo);
        }

    }

}
