package co.za.ngwane.billing.service;

import co.za.ngwane.billing.db.LuRefNoDescBe;

public interface InvoiceService {
	/**
     * Create Invoice document 
     */
    byte[] createInvoice(Object invoice,  String fileName, String invKey, LuRefNoDescBe luRefNoDescCd);
    
    /**
     * Saves a document to the repository
     */
    void saveDocumentToRepository(byte[] document,
                                  Object CertificateDto,
                                  String fileName,
                                  String invKey,
                                  LuRefNoDescBe luRefNoDescCd);

}
