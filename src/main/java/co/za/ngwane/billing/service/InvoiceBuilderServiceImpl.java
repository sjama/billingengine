package co.za.ngwane.billing.service;

import java.util.Date;

import co.za.ngwane.billing.db.DocReposBe;
import co.za.ngwane.billing.db.LuRefNoDescBe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.za.ngwane.billing.repository.DocRepoRepo;


@Service
public class InvoiceBuilderServiceImpl implements InvoiceService{
    @Autowired 
    private PdfBuilder documentBuilder;
    
    @Autowired
    private DocRepoRepo docRepo;
    
	@Override
	public byte[] createInvoice(Object invoice, String fileName, String invKey, LuRefNoDescBe luRefNoDescCd) {
		return createInvoiceAndSave(invoice, fileName, invKey, luRefNoDescCd);
	}

	/***************************************************************************************/
    private byte[] createInvoiceAndSave(Object CertificateDto, String fileName, String invKey, LuRefNoDescBe luRefNoDescCd) {

        byte[] document = documentBuilder.buildDocument(CertificateDto, null);
        
        saveDocumentToRepository(document, CertificateDto, fileName, invKey, luRefNoDescCd);
        return document;
    }

   

	@Override
	public void saveDocumentToRepository(byte[] document, Object CertificateDto, String fileName, String invKey, LuRefNoDescBe luRefNoDescCd) {
		
		DocReposBe docReposBe = new DocReposBe();
		docReposBe.setCreatedDate(new Date());
		docReposBe.setFileData(document);
		docReposBe.setFileName(fileName);
		docReposBe.setRefNo(invKey);
		docReposBe.setLuRefNoDescCd(luRefNoDescCd);
		
		docRepo.save(docReposBe);

	}

}
