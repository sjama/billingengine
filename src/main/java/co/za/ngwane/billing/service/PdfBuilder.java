package co.za.ngwane.billing.service;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.dom4j.DocumentException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PushbuttonField;

import co.za.ngwane.billing.certificates.util.CertificateTemplate;
import co.za.ngwane.billing.certificates.util.FieldKeyType;
import co.za.ngwane.billing.certificates.util.ImageField;
import co.za.ngwane.billing.certificates.util.TemplateField;
import co.za.ngwane.billing.certificates.util.TemplateFieldWithValue;


/**
 * Builds a PDF Document
 *
 * use PdfBuilderTest.showFields to print fields of a template pdf
 *
 * @author Jacob Marola
 */

@Component
public class PdfBuilder implements DocumentBuilder {


    private static final String TEMPLATES_DIR = "templates/";

    private static final String IMAGES_DIR = "images/";

    private boolean flattenForm = true;

    private boolean displayWatermark = false;

    private static final String WATERMARK_IMAGE_LOCATION = IMAGES_DIR + "watermark.jpeg";

    // For testing purposes

    void setFlattenForm(boolean flattenForm) {
        this.flattenForm = flattenForm;
    }

    boolean isFlatterForm() {
        return flattenForm;
    }

    public void setDisplayWatermark(boolean displayWatermark) {
        this.displayWatermark = displayWatermark;
    }

    public byte[] buildDocument(Object certificate, byte[] templatex) {
        CertificateTemplate certificateTemplate = getCertificateTemplate(certificate);
        List<TemplateFieldWithValue> fields = getFields(certificate);

        byte[] template = templatex;
        if(template==null) {
            // if not specified, then read from disk (jar file)
            template = getTemplateBytesFromDisk(certificateTemplate);
        }

        try {
			return completePdfForm(certificateTemplate, fields, template);
		} catch (com.itextpdf.text.DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return template;
    }
    public byte[] buildDocument(Object certificate) {
        return buildDocument(certificate, null);
    }

    @Override
    public byte[] getDocument(Object certificate) {
        CertificateTemplate certificateTemplate = getCertificateTemplate(certificate);
        return getTemplateBytesFromDisk(certificateTemplate);
    }

    byte[] builDocumentFromXmlBindings(Object certificate, Serializable source, byte[] template) {
        CertificateTemplate certificateTemplate = getCertificateTemplate(certificate);
        if (certificateTemplate.xmlRootElement().isEmpty()) {
            throw new DocumentBuilderException("xmlRootElement in @CertificateTemplate must be specified");
        }

        XmlRootElement xmlRootElement = AnnotationUtils.findAnnotation(source.getClass(), XmlRootElement.class);
        if (xmlRootElement == null || !xmlRootElement.name().equals(certificateTemplate.xmlRootElement())) {
            throw new DocumentBuilderException("Invalid source provided to build document");
        }

        List<TemplateFieldWithValue> fields = getFields(certificate);
        populateFieldsWithValues(source, fields);
        try {
			return completePdfForm(certificateTemplate, fields, template);
		} catch (com.itextpdf.text.DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return template;
    }

    private void populateFieldsWithValues(Serializable source, List<TemplateFieldWithValue> fields) {
        for (TemplateFieldWithValue field : fields) {
            if (!field.getTemplateField().xmlElement().isEmpty()) {
                try {
                    if (field.getValue() == null) { // If already populated externally don't populate from source 
                        Object property = PropertyUtils.getProperty(source, field.getTemplateField().xmlElement());
                        field.setValue(property);
                    }
                } catch (IllegalAccessException e) {
                } catch (InvocationTargetException e) {
                } catch (NoSuchMethodException e) {
                } catch (NestedNullException e) {
                    // Will happen in case of A.B where A is null, continue
                }
            }
        }
    }

    private CertificateTemplate getCertificateTemplate(Object certificate) {
        CertificateTemplate certificateTemplate = AnnotationUtils.findAnnotation(certificate.getClass(), CertificateTemplate.class);
        if (certificateTemplate == null) {
            throw new DocumentBuilderException("@CertificateTemplate must be specified");
        } else {
            return certificateTemplate;
        }
    }

    private List<TemplateFieldWithValue> getFields(final Object certificate) {
        final List<TemplateFieldWithValue> fields = new LinkedList<TemplateFieldWithValue>();

        ReflectionUtils.doWithFields(certificate.getClass(), new ReflectionUtils.FieldCallback() {
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                TemplateField templateField = field.getAnnotation(TemplateField.class);
                if (templateField != null) {
                    field.setAccessible(true);
                    fields.add(new TemplateFieldWithValue(templateField, field.get(certificate)));
                }
            }
        });
        return fields;
    }

    private byte[] completePdfForm(CertificateTemplate certificateTemplate, List<TemplateFieldWithValue> fields, byte[] pdfTemplate) throws com.itextpdf.text.DocumentException {
        ByteArrayOutputStream outputStream;
        try {
            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfReader.unethicalreading = true;
            pdfReader.selectPages(certificateTemplate.pageRange());
            outputStream = new ByteArrayOutputStream();
            PdfStamper pdfStamper = new PdfStamper(pdfReader, outputStream);

            if (displayWatermark) {
                addWatermark(pdfStamper);
            }

            fillInFields(pdfStamper.getAcroFields(), fields, certificateTemplate.fieldKeyType());
            pdfStamper.setFormFlattening(flattenForm);
            pdfStamper.close();
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage(), e);
        } catch (DocumentException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
        return outputStream.toByteArray();
    }

    private void addWatermark(PdfStamper pdfStamper) throws IOException, BadElementException {
        PdfContentByte contentByte = pdfStamper.getOverContent(1);
        Image img = Image.getInstance(getClass().getClassLoader().getResource(WATERMARK_IMAGE_LOCATION));

        img.setAbsolutePosition(
                (PageSize.A4.getWidth() - img.getScaledWidth()) / 2,
                (PageSize.A4.getHeight() - img.getScaledHeight()) / 2);
        try {
			contentByte.addImage(img);
		} catch (com.itextpdf.text.DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public InputStream getWaterMark() throws IOException {
        Resource r = new ClassPathResource(WATERMARK_IMAGE_LOCATION);
        return r.getInputStream();
    }

    private byte[] getTemplateBytesFromDisk(CertificateTemplate certificateTemplate) {

        String fileName = TEMPLATES_DIR + "InvoiceTemplate.pdf"; //certificateTemplate.templateFile();
        try {

            ClassLoader contextClassLoader = Thread.currentThread( ).getContextClassLoader();
           InputStream inputStream = contextClassLoader.getResourceAsStream(fileName);
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to find certificate template file ["+fileName+"]");
        }
    }


    private void fillInFields(AcroFields form, List<TemplateFieldWithValue> fields, FieldKeyType fieldKeyType) throws IOException, DocumentException, com.itextpdf.text.DocumentException {
        for (TemplateFieldWithValue field : fields) {
            String fieldName = fieldKeyType.getKey(field.getTemplateField());
            int fieldType = form.getFieldType(fieldName);
            if (fieldType == AcroFields.FIELD_TYPE_CHECKBOX) {
                // The values here are dependant on the valid appearance states for a field. 1 and 0 worked in my test case
                // If the templates we received have different states this will need to change
                form.setField(fieldName, Boolean.valueOf(field.getValueAsString()) ? "1" : "0");
            } else if (fieldType == AcroFields.FIELD_TYPE_PUSHBUTTON) {    // we use pusbuttons to display images
                PushbuttonField bt = form.getNewPushbuttonFromField(fieldName);
                bt.setLayout(PushbuttonField.LAYOUT_ICON_ONLY);

                Image image = getImageForField(field);
                if (image != null) {
                    bt.setProportionalIcon(false);
                    bt.setIconFitToBounds(true);
                    bt.setImage(image); // image will only be displayed on pdf is form is flattened
                } else { // hides button if no image
                    bt.setLayout(PushbuttonField.LAYOUT_LABEL_ONLY);
                    bt.setVisibility(PushbuttonField.HIDDEN);
                }
                form.replacePushbuttonField(fieldName, bt.getField());
            } else {
                form.setField(fieldName, field.getValueAsString());
            }
        }
    }

    // returns Image object to be populated
    private Image getImageForField(TemplateFieldWithValue field) throws IOException, BadElementException {
        Object fieldValue = field.getValue();

        if(fieldValue == null) {
            return null;
        }

        if (field.getValue() instanceof byte[]) {
            // its an image if:
            // defined as a byte[] in the dto and field type AcroFields.FIELD_TYPE_PUSHBUTTON in pdf
            return Image.getInstance((byte[]) field.getValue());
        } else if (field.getValue() instanceof ImageField) {
            ImageField imageField = (ImageField) field.getValue();
            URL url = getClass().getClassLoader().getResource(IMAGES_DIR + imageField.getFileName());
            if (url != null) {
                return Image.getInstance(url);
            }
        }
        return null;
    }


}
