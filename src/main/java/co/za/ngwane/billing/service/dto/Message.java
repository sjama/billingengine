package co.za.ngwane.billing.service.dto;

import javax.print.attribute.standard.Severity;
import javax.validation.Path;

public class Message {
	
	private  Severity severity;
	private  Object source;
	private  String text;
	
	
	public Message(Path propertyPath, String message, Severity error) {
		this.severity = error;
		this.text = message;
		this.source = propertyPath;
	}
	public Message(String string, String message, Severity error) {
		this.severity = error;
		this.text = message;
		this.source = string;
	}
	/**
	 * @return the severity
	 */
	public Severity getSeverity() {
		return severity;
	}
	/**
	 * @param severity the severity to set
	 */
	public void setSeverity(Severity severity) {
		this.severity = severity;
	}
	/**
	 * @return the source
	 */
	public Object getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(Object source) {
		this.source = source;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

}
