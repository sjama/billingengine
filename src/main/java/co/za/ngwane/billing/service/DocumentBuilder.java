package co.za.ngwane.billing.service;

import org.springframework.stereotype.Service;

/**
 * Interface for different document builders (at the moment it is just PDF)
 *
 * @author Jacob Marola
 */
public interface DocumentBuilder {
    /**
     *
     * @param certificate
     * @param template if null, we will use template file on disk for the specific certificate
     * @return
     */
    byte[] buildDocument(Object certificate, byte[] template);

    /**
     * same as buildDocument(certificate, null)
     */
    byte[] buildDocument(Object certificate);

    /**
     * This one only retrieves the document, does not try to fill it.
     * Used by eservices - where forms looses headeings and text because of the filling process...
     */
    byte[] getDocument(Object certificate);

}
