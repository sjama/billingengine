package co.za.ngwane.billing.service;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import co.za.ngwane.billing.certificates.util.CertificateTemplate;
import co.za.ngwane.billing.certificates.util.TemplateField;
import co.za.ngwane.billing.certificates.util.TemplateFieldWithValue;



/**
 * Implementation of {CertificateStringBuilder} that builds certificate String
 * Uses % as delimeter and orders String according to TemplateField's order 
 *
 * @author Jacob Marola
 */
public class CertificateStringBuilderImpl implements CertificateStringBuilder {

    private static final String DELIMETER = "%";

    public String buildCertificateString(Object certificateDto) {

        Map<Integer, TemplateFieldWithValue> orderedMapOfFields = getOrderedDtoAnnotations(certificateDto);
        StringBuilder certificateStringBuilder = new StringBuilder();

        CertificateTemplate certificateTemplate = AnnotationUtils.findAnnotation(certificateDto.getClass(), CertificateTemplate.class);
        if (certificateTemplate != null) {
            certificateStringBuilder.append(certificateTemplate.certificateCode()).append(DELIMETER);
        }

        Iterator<TemplateFieldWithValue> iterator = orderedMapOfFields.values().iterator();

        while (iterator.hasNext()) {
            TemplateFieldWithValue templateFieldWithValue = iterator.next();
            String stringValue = templateFieldWithValue.getValueAsString().replaceAll(DELIMETER, "");
            certificateStringBuilder.append(stringValue);
            if (iterator.hasNext()) {
                certificateStringBuilder.append(DELIMETER);
            }
        }

        return certificateStringBuilder.toString();
    }

    private Map<Integer, TemplateFieldWithValue> getOrderedDtoAnnotations(final Object certificateDto) {

        final SortedMap<Integer,TemplateFieldWithValue> orderedMap = new TreeMap<Integer, TemplateFieldWithValue>();

        ReflectionUtils.doWithMethods(certificateDto.getClass(), new ReflectionUtils.MethodCallback() {
            public void doWith(Method method) throws IllegalArgumentException, IllegalAccessException {
                TemplateField templateField = method.getAnnotation(TemplateField.class);
                if (templateField != null) {
                    for (int order : templateField.order()) {
                        if (order > 0) {
                            try {
                                // get value with getter method
                                Object assignedValue = method.invoke(certificateDto);
                                orderedMap.put(order, new TemplateFieldWithValue(templateField, assignedValue));
                            } catch (InvocationTargetException e) {
                                orderedMap.put(order, new TemplateFieldWithValue(templateField, null));
                            }
                        }
                    }
                }
            }
        });

        ReflectionUtils.doWithFields(certificateDto.getClass(), new ReflectionUtils.FieldCallback() {
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                TemplateField templateField = field.getAnnotation(TemplateField.class);
                if (templateField != null) {
                    for (int order : templateField.order()) {
                        if (order > 0) {
                            field.setAccessible(true);
                            // get value from field
                            Object assignedValue = field.get(certificateDto);
                            orderedMap.put(order, new TemplateFieldWithValue(templateField, assignedValue));
                        }
                    }
                }
            }
        });

        return orderedMap;
    }

}
