/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.R
 */
package co.za.ngwane.billing.service;

import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.vo.EmailVo;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author bheki.lubisi
 */
@Service
public class EmailsService {

    private static final Logger logger = Logger.getLogger(EmailsService.class);

    public void sendMail(EmailVo emailVo) {
        try {
            Email email = new SimpleEmail();

            email.setHostName(BillingConstants.EMAIL_HOST);

            //this are gmail setting
            //email.setSmtpPort(465);
            //email.setStartTLSEnabled(true);
            //email.setSSLOnConnect(true);
            //RTMC exchange settings
            email.setSmtpPort(587);
            email.setAuthenticator(new DefaultAuthenticator(BillingConstants.EMAIL_NOTIFICATION_USRNAME,
                    BillingConstants.EMAIL_NOTIFICATION_PWD));

            email.setSSLOnConnect(false);
            email.setFrom(BillingConstants.EMAIL_FROM_NOTIFICATION);

            email.setSubject(emailVo.getSubject());
            email.setMsg(emailVo.getMsg());
            email.addTo(emailVo.getTo());

            if (emailVo.getBcc() != null) {
                email.addBcc(emailVo.getBcc());
            }

            if (emailVo.getCc() != null) {
                email.addCc(emailVo.getCc());
            }

            email.send();

            logger.info(
                    "===============================================================\n "
                    + "Sending email to [" + emailVo.getTo() + "] at [" + new Date() + "]"
                    + "\n=============================================================== ");

        } catch (EmailException e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }

    }

    public void sendMailList(List<EmailVo> emailVoList) {
        try {
            for (EmailVo emailVo : emailVoList) {
                Email email = new SimpleEmail();

                email.setHostName(BillingConstants.EMAIL_HOST);
                email.setSmtpPort(587);
                email.setAuthenticator(new DefaultAuthenticator(BillingConstants.EMAIL_NOTIFICATION_USRNAME,
                        BillingConstants.EMAIL_NOTIFICATION_PWD));
                email.setSSLOnConnect(false);
                email.setFrom(BillingConstants.EMAIL_FROM_NOTIFICATION);

                email.setSubject(emailVo.getSubject());
                email.setMsg(emailVo.getMsg());
                email.addTo(emailVo.getTo());

                if (emailVo.getBcc() != null) {
                    email.addBcc(emailVo.getBcc());
                }

                email.send();

                logger.info(
                        "===============================================================\n "
                        + "Sending email to [" + emailVo.getTo() + "] at [" + new Date() + "]"
                        + "\n=============================================================== ");
            }
        } catch (EmailException e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }

    }

    public void sendEmailClient(){

    }

    public static void mail(EmailVo emailVo) {

        final String username = "mokkhutu@gmail.com";
        final String password = "f@3953335";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("from-email@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("bheki.lubisi@gmail.com"));
            message.setRecipients(Message.RecipientType.BCC,
                    InternetAddress.parse("mokkhutu@gmail.com"));
            message.setSubject(emailVo.getSubject());
            message.setText(emailVo.getMsg());

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        EmailVo emailVo = new EmailVo();
        emailVo.setFrom("efdnotification@rtmc.co.za");
        emailVo.setMsg("Test");
        emailVo.setSubject("Dlala Bhekzin");
        emailVo.setTo("bheki.lubisi@gmail.com");

        EmailsService emailsService = new EmailsService();
        emailsService.sendMail(emailVo);
    }

}
