package co.za.ngwane.billing.service;

import co.za.ngwane.billing.certificates.util.GUIDKeyGenerator;
import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.*;
import co.za.ngwane.billing.repository.CdrFileRepo;
import co.za.ngwane.billing.repository.LuBillProcessStatRepo;
import co.za.ngwane.billing.repository.service.OriginatingLinesRepoService;
import co.za.ngwane.billing.repository.BillDetBeRepo;
import co.za.ngwane.billing.utils.DateUtils;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

/**
 * @author jamasithole
 */
@Service
public class BillingService {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BillingService.class);

    @Autowired
    private BillDetBeRepo billDetBeRepo;

    @Autowired
    private CdrFileRepo cdrFileRepo;
    @Autowired
    private LuBillProcessStatRepo luBillProcessStatRepo;

    private HashMap linesMap = new HashMap();

    @Autowired
    private OriginatingLinesRepoService linesService;
    private static final String excelFilePath = "/Users/jamasithole/Documents/CDR.xlsx";
    private static final String SAMPLE_CSV_FILE_PATH = "/Users/jamasithole/Downloads/E000009046DECEMBER-2017.csv";
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final BigDecimal MINUTE = new BigDecimal("60");

    public void doBilling() throws Exception {
        GUIDKeyGenerator myGUID = new GUIDKeyGenerator(true);
        FileInputStream billfile = readFile();
        getAllLines();

        String cdrFileRefNo = myGUID.generate('A', 10);

        CdrFileBe cdrFileBe = new CdrFileBe();
        cdrFileBe.setCdrFileName(cdrFileRefNo);
        cdrFileBe.setCdrFilePath("");       // Kudzingeka tibuko laaaa
        cdrFileBe.setCdrFileRefNo(cdrFileRefNo);
        cdrFileBe.setFilePeriod(new Date()); // Kudzingeka tibuko laaaa
        cdrFileBe.setDateReceived(new Date());
        LuBillProcessStatBe luBillProcessStatBe = luBillProcessStatRepo.findOne(BillingConstants.LU_BILL_PROCESS_STAT_PENDING);
        cdrFileBe.setLuBillProStatus(luBillProcessStatBe);
        cdrFileBe.setProStatChgD(new Date());
        cdrFileBe.setStatus(BillingConstants.LU_STAT_ACTIVE);

        cdrFileBe = cdrFileRepo.save(cdrFileBe);

        logger.info(
                "\n===============================================================\n "
                        + "DONE SAVING CDR FILE TO DB " + "AT [" + new Date() + "]"
                        + "\n=============================================================== ");

        if (billfile != null) {
            List<BillDetBe> billsToSave;

            billsToSave = dataBatch();

            logger.info(
                    "\n===============================================================\n "
                            + "ABOUT TO SAVE BILLS " + "AT [" + new Date() + "] NUMBER OF BILLS [" + billsToSave.size() + "]"
                            + "\n=============================================================== ");

            for (BillDetBe entry : billsToSave) {
                entry.setCdrFileRefNo(cdrFileBe.getCdrFileRefNo());
                save(entry);
            }

            //To Do : We need to add other billable items - depending other customer

            logger.info(
                    "\n===============================================================\n "
                            + "DONE SAVING BILLS " + "AT [" + new Date() + "]"
                            + "\n=============================================================== ");
        }
    }


    private void getAllLines() {

        for (OriginatingLineBe line : linesService.getAllLines()) {
            try {
                linesMap.put(line.getLine(), line.getCustomersId());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    //CSV file header
    //private static final String FILE_HEADER = "REGN,VIN,DATE,TARE";

    public List<BillDetBe> dataBatch() throws Exception {
        List<BillDetBe> bills = new ArrayList<>();

        try (
                Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                        .withHeader("ACCOUNT_NO", "CALL DATE", "ORGINATING NUMBER", "DIALED NUMBER", "DURATION", "AMOUNT", "CALL TYPE", "DESTINATION COUNTRY", "EXTN NUMBER")
                        .withIgnoreHeaderCase()
                        .withTrim())) {

            FileWriter fileWriter = null;
            try {

                //Write the CSV file header
                //fileWriter.append(FILE_HEADER.toString());
                int i = 0;
                for (CSVRecord csvRecord : csvParser.getRecords()) {
                    // Accessing values by the names assigned to each column

                    if (csvRecord.getRecordNumber() == 1) {
                        continue;

                    }

                    BillDetBe billRecord = new BillDetBe();
                    String ACCOUNT_NO = csvRecord.get("ACCOUNT_NO");
                    String CALL_DATE = csvRecord.get("CALL DATE");
                    String ORGINATING_NUMBER = csvRecord.get("ORGINATING NUMBER");
                    String DIALED_NUMBER = csvRecord.get("DIALED NUMBER");
                    String DURATION = csvRecord.get("DURATION");
                    String AMOUNT = csvRecord.get("AMOUNT");
                    String CALL_TYPE = csvRecord.get("CALL TYPE");
                    String DESTINATION_COUNTRY = csvRecord.get("DESTINATION COUNTRY");
                    String EXTN_NUMBER = csvRecord.get("EXTN NUMBER");

                    System.out.println("Record No - " + csvRecord.getRecordNumber());
                    System.out.println("---------------");
                    System.out.println("ACCOUNT_NO : " + ACCOUNT_NO + " CALL_DATE " + CALL_DATE + " ORGINATING_NUMBER " + ORGINATING_NUMBER);
                    System.out.println("DURATION : " + DURATION + " AMOUNTE " + AMOUNT + " CALL_TYPE " + CALL_TYPE);
                    System.out.println("EXTN_NUMBER : " + EXTN_NUMBER + " DESTINATION_COUNTRY " + DESTINATION_COUNTRY);
                    String dateString = CALL_DATE;
                    String[] splitStr = dateString.trim().split("\\s+");
                    System.out.println("DATE SPLIT : " + splitStr[0]);
                    billRecord.setCallDate(DateUtils.convertStringToDate(splitStr[0]));
                    System.out.println("CONVERTED DATE : " + billRecord.getCallDate());

                    billRecord.setAccountNo(ACCOUNT_NO);
                    billRecord.setOriginatingNo(ORGINATING_NUMBER.replaceAll("'", ""));
                    billRecord.setDialledNo(DIALED_NUMBER);
                    int value = Integer.valueOf(DURATION);
                    billRecord.setDurationSec(String.valueOf(value));
                    billRecord.setDurationMin(String.valueOf(convertSecondMinutestring(Integer.valueOf(billRecord.getDurationSec()))));
                    CustomersBe customer = (CustomersBe) linesMap.get(billRecord.getOriginatingNo());
                    if (customer == null) {
                        System.out.println("NO CUSTOMER FOUND FOR ORIGINATING NUMBER : " + billRecord.getOriginatingNo());
                        continue;
                    }
                    billRecord.setAmountClient(calculateCustomerAmmount(customer,
                            new BigDecimal(billRecord.getDurationMin())));
                    billRecord.setCustomersId(customer);
                    billRecord.setAmountNeo(Double.valueOf(AMOUNT));
                    billRecord.setCallType(CALL_TYPE);
                    billRecord.setDestinationCountry(DESTINATION_COUNTRY);
                    billRecord.setExtentionNo(EXTN_NUMBER);

                    bills.add(billRecord);

                    //System.out.println("ACCOUNT_NO : " + ACCOUNT_NO);
                }

                System.out.println("CSV file was created successfully !!!");
            } catch (Exception e) {
                System.out.println("Error in CsvFileWriter !!!");
                e.printStackTrace();
            } finally {


            }
        }

        return bills;
    }

    /**
     * ***** Read File From SFTP Server
     *
     * @return
     */
    public FileInputStream readFile() {
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(new File(excelFilePath));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BillingService.class.getName()).log(Level.SEVERE, null, ex);

        }
        return inputStream;

    }

    /**
     * *******************
     */
    /* *
     * @param billDetBe
     * Download file and store in a local temp directory
     * @return success or failure
     */
    public boolean dowloadfileFromServer() {
        return true;
    }

    private Workbook getWorkbook(FileInputStream inputStream, String excelFilePath)
            throws IOException {
        Workbook workbook = null;

        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    /**
     * @param excelFilePath
     * @param inputStream
     * @return
     * @throws java.io.IOException
     */
    public List<BillDetBe> readBillsFromExcelFile(FileInputStream inputStream, String excelFilePath) throws IOException {
        List<BillDetBe> billList = new ArrayList<>();

        Workbook workbook = getWorkbook(inputStream, excelFilePath);
        Sheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();

        int i = 0;
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            BillDetBe billRecord = new BillDetBe();
            i++;
            if (i == 1) {
                continue;
            }

            while (cellIterator.hasNext()) {
                Cell nextCell = cellIterator.next();

                int columnIndex = nextCell.getColumnIndex();

                switch (columnIndex) {
                    case 0:
                        billRecord.setAccountNo((String) getCellValue(nextCell));
                        System.err.println("WAccount :" + billRecord.getAccountNo());
                        break;
                    case 1:
                        String dateString = (String) getCellValue(nextCell);
                        String[] splitStr = dateString.trim().split("\\s+");
                        billRecord.setCallDate(DateUtils.convertStringToDate(splitStr[0]));
                        break;
                    case 2:
                        String str = (String) getCellValue(nextCell);
                        billRecord.setOriginatingNo(str.replaceAll("'", ""));
                        break;
                    case 3:
                        billRecord.setDialledNo((String) getCellValue(nextCell));
                        break;
                    case 4:
                        Double value = nextCell.getNumericCellValue();
                        billRecord.setDurationSec(String.valueOf(value.intValue()));
                        billRecord.setDurationMin(String.valueOf(convertSecondMinutestring(Integer.valueOf(billRecord.getDurationSec()))));
                        //    CustomersBe customer = linesService.getCustomerByLine(billRecord.getOriginatingNo());
//                        billRecord.setAmountClient(calculateCustomerAmmount(customer,
//                                new BigDecimal(billRecord.getDurationMin())));
//                        billRecord.setCustomersId(customer);
                        break;
                    case 5:
                        String neoAmount = nextCell.getStringCellValue();
                        billRecord.setAmountNeo(Double.valueOf(neoAmount));
                        break;
                    case 6:
                        billRecord.setCallType((String) getCellValue(nextCell));
                        break;
                    case 7:
                        billRecord.setDestinationCountry((String) getCellValue(nextCell));
                        break;
                    case 8:
                        billRecord.setExtentionNo((String) getCellValue(nextCell));
                        break;
                    default:
                        System.err.println("Wrong Column Number");
                        break;
                }
                System.err.println("Number : " + billRecord.getDurationSec());

                System.err.println("Number : " + billRecord.getDurationSec());

            }
            billList.add(billRecord);
        }

        workbook.close();
        inputStream.close();

        return billList;
    }

    private Object getCellValue(Cell cell) {
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();

            case Cell.CELL_TYPE_BOOLEAN:
                return cell.getBooleanCellValue();

            case Cell.CELL_TYPE_NUMERIC:
                return cell.getNumericCellValue();
        }

        return null;
    }

    public BillDetBe save(BillDetBe bills) {

        return billDetBeRepo.save(bills);
    }

    private BigDecimal convertSecondMinutestring(int nSecondTime) {
        BigDecimal seconds = new BigDecimal(nSecondTime);
        BigDecimal minutes = seconds.divide(MINUTE, 2, BigDecimal.ROUND_HALF_UP);
        return minutes;

    }

    private BigDecimal calculateCustomerAmmount(CustomersBe be, BigDecimal durationInMinutes) {
        System.out.println("CUSTOMER : " + be.getName());
        System.out.println("AGREED RATE : " + be.getAgreedRate() + " DURATION MINUTES " + durationInMinutes);
        BigDecimal rate = new BigDecimal(be.getAgreedRate());
        BigDecimal finalAmount = durationInMinutes.multiply(rate);
        return finalAmount.setScale(2);
    }

    public <T> List<T> loadObjectList(Class<T> type, String fileName) {
        try {
            CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
            CsvMapper mapper = new CsvMapper();
            File file = new ClassPathResource(fileName).getFile();
            MappingIterator<T> readValues
                    = mapper.reader(type).with(bootstrapSchema).readValues(file);
            return readValues.readAll();
        } catch (Exception e) {
            //  logger.error("Error occurred while loading object list from file " + fileName, e);
            return Collections.emptyList();
        }
    }

}
