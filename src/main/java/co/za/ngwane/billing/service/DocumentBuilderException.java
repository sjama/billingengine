package co.za.ngwane.billing.service;

/**
 * Exception encountered during building of a document
 *
 * @author Jacob Marola
 */
public class DocumentBuilderException extends RuntimeException {

     public DocumentBuilderException(String message) {
         super(message);
     }

    public DocumentBuilderException(String message, Throwable cause) {
        super(message, cause);
    }
}
