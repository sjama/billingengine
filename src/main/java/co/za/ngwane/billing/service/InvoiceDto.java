package co.za.ngwane.billing.service;

import co.za.ngwane.billing.certificates.util.CertificateTemplate;
import co.za.ngwane.billing.certificates.util.FieldKeyType;
import co.za.ngwane.billing.certificates.util.TemplateField;
import co.za.ngwane.billing.db.CustomersBe;

@CertificateTemplate(templateFile = "invoice.pdf", fieldKeyType = FieldKeyType.BASIC)
public class InvoiceDto {
	private CustomersBe be;
	
	@TemplateField(value = "cntryOfIss")
	private String invoiceNumber;
	@TemplateField(value = "cntryOfIss")
	private String totalDuration;
	@TemplateField(value = "cntryOfIss")
	private String totalAmount;
	@TemplateField(value = "cntryOfIss")
	private String customerName;
	
	@TemplateField(value = "grandTotal")
	private String grandTotal;
	@TemplateField(value = "vatTotal")
	private String vatTotal;
	@TemplateField(value = "vatPercent")
	private String vatPercent;
	@TemplateField(value = "subTotal")
	private String subTotal;
	@TemplateField(value = "branchCd")
	private String branchCd;
	@TemplateField(value = "branch")
	private String branch;
	@TemplateField(value = "accountN")
	private String accountN;
	@TemplateField(value = "bank")
	private String bank;
	@TemplateField(value = "accountHolder")
	private String accountHolder;
	
	@TemplateField(value = "comRegN")
	private String regN;
	
	@TemplateField(value = "telN")
	private String telN;
	
	@TemplateField(value = "faxN")
	private String faxN;
	
	@TemplateField(value = "emailAddr")
	private String email;
	
	@TemplateField(value = "vatN")
	private String vatN;
	
	@TemplateField(value = "invNo")
	private String invoiceN;
	
	@TemplateField(value = "invDate")
	private String invoiceD;
	@TemplateField(value = "invDueDate")
	private String invoiceDueD;
	@TemplateField(value = "clientOrderN")
	private String clientOrderNo;
	@TemplateField(value = "clientVatN")
	private String clientVATNo;
	
	@TemplateField(value = "invToLine1")
	private String invToLine1;
	@TemplateField(value = "invToLine2")
	private String invToLine2;
	@TemplateField(value = "invToLine3")
	private String invToLine3;
	@TemplateField(value = "invToLine4")
	private String invToLine4;
	
	@TemplateField(value = "deliveredToLine1")
	private String deliveredToLine1;
	@TemplateField(value = "deliveredToLine2")
	private String deliveredToLine2;
	@TemplateField(value = "deliveredToLine3")
	private String deliveredToLine3;
	@TemplateField(value = "deliveredToLine4")
	private String deliveredToLine4;
	
	@TemplateField(value = "item")
	private String item;
	@TemplateField(value = "description")
	private String description;
	@TemplateField(value = "quantity")
	private String quantity;
	@TemplateField(value = "unitPrice")
	private String unitPrice;
	
	@TemplateField(value = "amount")
	private String amount;
	
	public CustomersBe getBe() {
		return be;
	}
	public void setBe(CustomersBe be) {
		this.be = be;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getTotalDuration() {
		return totalDuration;
	}
	public void setTotalDuration(String totalDuration) {
		this.totalDuration = totalDuration;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getRegN() {
		return regN;
	}
	public void setRegN(String regN) {
		this.regN = regN;
	}
	public String getTelN() {
		return telN;
	}
	public void setTelN(String telN) {
		this.telN = telN;
	}
	public String getFaxN() {
		return faxN;
	}
	public void setFaxN(String faxN) {
		this.faxN = faxN;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVatN() {
		return vatN;
	}
	public void setVatN(String vatN) {
		this.vatN = vatN;
	}
	public String getInvoiceN() {
		return invoiceN;
	}
	public void setInvoiceN(String invoiceN) {
		this.invoiceN = invoiceN;
	}
	public String getInvoiceD() {
		return invoiceD;
	}
	public void setInvoiceD(String invoiceD) {
		this.invoiceD = invoiceD;
	}
	public String getInvoiceDueD() {
		return invoiceDueD;
	}
	public void setInvoiceDueD(String invoiceDueD) {
		this.invoiceDueD = invoiceDueD;
	}
	public String getClientOrderNo() {
		return clientOrderNo;
	}
	public void setClientOrderNo(String clientOrderNo) {
		this.clientOrderNo = clientOrderNo;
	}
	public String getClientVATNo() {
		return clientVATNo;
	}
	public void setClientVATNo(String clientVATNo) {
		this.clientVATNo = clientVATNo;
	}
	public String getInvToLine1() {
		return invToLine1;
	}
	public void setInvToLine1(String invToLine1) {
		this.invToLine1 = invToLine1;
	}
	public String getInvToLine2() {
		return invToLine2;
	}
	public void setInvToLine2(String invToLine2) {
		this.invToLine2 = invToLine2;
	}
	public String getInvToLine3() {
		return invToLine3;
	}
	public void setInvToLine3(String invToLine3) {
		this.invToLine3 = invToLine3;
	}
	public String getInvToLine4() {
		return invToLine4;
	}
	public void setInvToLine4(String invToLine4) {
		this.invToLine4 = invToLine4;
	}
	public String getDeliveredToLine1() {
		return deliveredToLine1;
	}
	public void setDeliveredToLine1(String deliveredToLine1) {
		this.deliveredToLine1 = deliveredToLine1;
	}
	public String getDeliveredToLine2() {
		return deliveredToLine2;
	}
	public void setDeliveredToLine2(String deliveredToLine2) {
		this.deliveredToLine2 = deliveredToLine2;
	}
	public String getDeliveredToLine3() {
		return deliveredToLine3;
	}
	public void setDeliveredToLine3(String deliveredToLine3) {
		this.deliveredToLine3 = deliveredToLine3;
	}
	public String getDeliveredToLine4() {
		return deliveredToLine4;
	}
	public void setDeliveredToLine4(String deliveredToLine4) {
		this.deliveredToLine4 = deliveredToLine4;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getVatTotal() {
		return vatTotal;
	}
	public void setVatTotal(String vatTotal) {
		this.vatTotal = vatTotal;
	}
	public String getVatPercent() {
		return vatPercent;
	}
	public void setVatPercent(String vatPercent) {
		this.vatPercent = vatPercent;
	}
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getBranchCd() {
		return branchCd;
	}
	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAccountN() {
		return accountN;
	}
	public void setAccountN(String accountN) {
		this.accountN = accountN;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	

}
