/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.service;

import co.za.ngwane.billing.db.BillDetBe;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import org.springframework.batch.item.ItemProcessor;

/**
 *
 * @author jamasithole
 */
public class BillItemProcessor implements ItemProcessor<BillDetBe, BillDetBe> {
    
    private static final BigDecimal MINUTE = new BigDecimal("60");
    HashMap<String, BigDecimal> linesMap = new HashMap();

    @Override
    public BillDetBe process(BillDetBe record) {
        prePareRates();
        System.out.println("Record No - ");
        System.out.println("---------------");
        System.out.println("ACCOUNT_NO : " + record.getAccountNo() + " CALL_DATE " + record.getCallDate() + " ORGINATING_NUMBER " + record.getOriginatingNo());
        System.out.println("DURATION : " + record.getDurationSec() + " AMOUNTE " + record.getAmountNeo() + " CALL_TYPE " + record.getCallType());
        System.out.println("EXTN_NUMBER : " + record.getExtentionNo() + " DESTINATION_COUNTRY " + record.getDestinationCountry());
        record.setDurationMin(convertSecondMinutestring(Integer.valueOf(record.getDurationSec())).toString());
        record.setOriginatingNo(record.getOriginatingNo().replaceAll("'", ""));
        record.setCustId(Long.valueOf("1"));
        try {
             BigDecimal rate = linesMap.get(record.getOriginatingNo()).setScale(3, RoundingMode.HALF_UP);
             record.setAmountClient(calculateCustomerAmmount(rate,new BigDecimal(record.getDurationMin())));
        } catch (Exception e) {
            record.setAmountClient(new BigDecimal(0.00));
        }
       

        return record;
    }

    public void prePareRates() {
       
       
        linesMap.put("0115897200", new BigDecimal(0.0));
        linesMap.put("0110123456", new BigDecimal(0.0));
        linesMap.put("0318301800", new BigDecimal(0.0));
        
        linesMap.put("0110101500", new BigDecimal(2.234));

        linesMap.put("0110185200", new BigDecimal(2.234));
        linesMap.put("0110188000", new BigDecimal(2.234));
        linesMap.put("0110871200", new BigDecimal(2.234));

        linesMap.put("0338142000", new BigDecimal(3.331));
        linesMap.put("0338142200", new BigDecimal(3.331));
        linesMap.put("0338142600", new BigDecimal(3.331));

        linesMap.put("0338142800", new BigDecimal(3.331));
        linesMap.put("0338142200", new BigDecimal(2.234));
        linesMap.put("0110687000", new BigDecimal(2.234));

        linesMap.put("0158174100", new BigDecimal(2.234));
        linesMap.put("0158174300", new BigDecimal(2.234));
        linesMap.put("0158174700", new BigDecimal(2.234));

        linesMap.put("0168156100", new BigDecimal(2.234));
        linesMap.put("0168156300", new BigDecimal(2.234));


    }
    
    private BigDecimal convertSecondMinutestring(int nSecondTime) {
        BigDecimal seconds = new BigDecimal(nSecondTime);
        BigDecimal minutes = seconds.divide(MINUTE, 2, BigDecimal.ROUND_HALF_UP);
        return minutes;

    }

    private BigDecimal calculateCustomerAmmount(BigDecimal agreedRate, BigDecimal durationInMinutes) {
        System.out.println("AGREED RATE : " + agreedRate + " DURATION MINUTES " + durationInMinutes);
        BigDecimal finalAmount = durationInMinutes.multiply(agreedRate);
        return finalAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

}
