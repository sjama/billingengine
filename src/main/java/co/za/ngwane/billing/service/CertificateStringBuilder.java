package co.za.ngwane.billing.service;

/**
 * Interface for creation of Certificate Strings
 *
 * @author Jacob Marola
 */
public interface CertificateStringBuilder {

    String buildCertificateString(Object certificate);
}
