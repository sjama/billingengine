//package co.za.ngwane.billing.service;
//
////~--- JDK imports ------------------------------------------------------------
//
//import java.awt.image.BufferedImage;
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.imageio.ImageIO;
//import javax.xml.bind.JAXBException;
//
//import org.apache.log4j.Logger;
//import org.apache.poi.sl.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.CellStyle;
//import org.apache.poi.ss.usermodel.ClientAnchor;
//import org.apache.poi.ss.usermodel.CreationHelper;
//import org.apache.poi.ss.usermodel.Drawing;
//import org.apache.poi.ss.usermodel.Font;
//import org.apache.poi.ss.usermodel.IndexedColors;
//import org.apache.poi.ss.usermodel.Picture;
//import org.apache.poi.ss.usermodel.PrintSetup;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.ss.util.CellRangeAddress;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//
//import co.za.ngwane.billing.utils.DateUtils;
//
//public class ANPRWriteExcel {
//    private static Logger         logger = Logger.getLogger(ANPRWriteExcel.class);
//    private static final String[] titles = {
//        "Customer Name", "Invoice Number", "Invoice Number", "Call Duration", "Total Amount"
//    };
//
//    /**
//     * Create a library of cell styles
//     */
//    public static Map<String, CellStyle> createStyles(Workbook wb) {
//        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
//        CellStyle              style;
//        Font                   titleFont = wb.createFont();
//
//        titleFont.setFontHeightInPoints((short) 18);
//        //titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
//        style = wb.createCellStyle();
//        //style.setAlignment(CellStyle.classALIGN_CENTER);
//        //style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//        style.setFont(titleFont);
//        styles.put("title", style);
//
//        Font monthFont = wb.createFont();
//
//        monthFont.setFontHeightInPoints((short) 11);
//        monthFont.setColor(IndexedColors.WHITE.getIndex());
//        style = wb.createCellStyle();
//        style.setAlignment(CellStyle.ALIGN_CENTER);
//        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//        style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
//        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
//        style.setFont(monthFont);
//        style.setWrapText(true);
//        styles.put("header", style);
//        style = wb.createCellStyle();
//        style.setAlignment(CellStyle.ALIGN_CENTER);
//        style.setWrapText(true);
//        style.setBorderRight(CellStyle.BORDER_THIN);
//        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
//        style.setBorderLeft(CellStyle.BORDER_THIN);
//        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
//        style.setBorderTop(CellStyle.BORDER_THIN);
//        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
//        style.setBorderBottom(CellStyle.BORDER_THIN);
//        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
//        styles.put("cell", style);
//        style = wb.createCellStyle();
//        style.setAlignment(CellStyle.ALIGN_CENTER);
//        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
//        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
//        style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
//        styles.put("formula", style);
//        style = wb.createCellStyle();
//        style.setAlignment(CellStyle.ALIGN_CENTER);
//        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
//        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
//        style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
//        styles.put("formula_2", style);
//        
//		style = wb.createCellStyle();
//		style.setAlignment(CellStyle.ALIGN_CENTER);
//		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//		style.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
//		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
////		style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
//		styles.put("footer", style);
//
//
//        return styles;
//    }
//
//    private static Picture getpic(Workbook wb, Sheet sheet, int i, int k, int pos, byte[] image) throws JAXBException {
//
//        /* Add Picture to workbook and get a index for the picture */
//        int pictureIdx = wb.addPicture(image, Workbook.PICTURE_TYPE_JPEG);
//
//        // Returns an object that handles instantiating concrete classes
//        CreationHelper helper = wb.getCreationHelper();
//
//        // Creates the top-level drawing patriarch.
//        Drawing drawing = sheet.createDrawingPatriarch();
//
//        // Create an anchor that is attached to the worksheet
//        ClientAnchor anchor = helper.createClientAnchor();
//
//        // set top-left corner for the image
//        anchor.setCol1(pos);
//        anchor.setRow1(k);
//
//        // Creates a picture
//        Picture pict = drawing.createPicture(anchor, pictureIdx);
//
//        // Reset the image to the original size
//        pict.resize(1.02, 1.0);
//
//        return pict;
//    }
//
//    public static ByteArrayOutputStream produceExcel(Anusrsrchist anusrsrchist) throws Exception {
//        Workbook               wb                = new XSSFWorkbook();
//        ByteArrayOutputStream  arrayOutputStream = new ByteArrayOutputStream();
//        Map<String, CellStyle> styles            = createStyles(wb);
//        Sheet                  sheet             = wb.createSheet("Sighting History");
//        PrintSetup             printSetup        = sheet.getPrintSetup();
//
//        printSetup.setLandscape(true);
//        sheet.setFitToPage(true);
//        sheet.setHorizontallyCenter(true);
//
//        // title row
//        Row titleRow = sheet.createRow(0);
//
//        titleRow.setHeightInPoints(45);
//
//        Cell titleCell = titleRow.createCell(0);
//
//        titleCell.setCellValue("Sighting History Report");
//        titleCell.setCellStyle(styles.get("title"));
//        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$I$1"));
//
//        // header row
//        Row headerRow = sheet.createRow(1);
//
//        headerRow.setHeightInPoints(40);
//
//        Cell headerCell;
//
//        for (int i = 0; i < titles.length; i++) {
//            headerCell = headerRow.createCell(i);
//            headerCell.setCellValue(titles[i]);
//            headerCell.setCellStyle(styles.get("header"));
//        }
//
//        // Create a key value pair for camera and Its sightings
//        Map<String, ArrayList<Hit>> dataMap   = new HashMap<String, ArrayList<Hit>>();
//        ArrayList<Hit>              hits      = new ArrayList<Hit>();
//        List<Hit>                   arrayList = new HitListOperations().getBySearchParms(anusrsrchist);;
//
//        logger.info("=======================================================");
//        logger.info(" Trying to build the map for Excel : " + arrayList.size());
//        logger.info("=======================================================");
//
//        for (Hit hit : arrayList) {
//            String key = hit.getCameraid().getCameraid();
//
//            hits = dataMap.get(key);
//
//            if (hits == null) {
//                hits = new ArrayList<Hit>();
//                hits.add(hit);
//            } else {
//                hits.add(hit);
//            }
//
//            dataMap.put(hit.getCameraid().getCameraid(), hits);
//        }
//
//        Row row = null;
//        int j   = 3;
//
//        for (String key : dataMap.keySet()) {
//            row = sheet.createRow(j);
//
//            Camera camera = new CameraServiceImpl().getByCameraId(key);
//
//            camera.setGpsxcoordinates(new GPSCoordinates().coordinatesToFrontEnd(camera.getGpsxcoordinates()));
//            camera.setGpsycoordinates(new GPSCoordinates().coordinatesToFrontEnd(camera.getGpsycoordinates()));
//
//            Cell localCell = row.createCell(0);
//
//            localCell.setCellValue(camera.getProv().getName());
//            localCell.setCellStyle(styles.get("cell"));
//
//            ArrayList<Hit> myArray = dataMap.get(key);
//
//            if (null != row) {
//                for (int i = 0; i < myArray.size(); i++) {
//                    Hit    hit         = myArray.get(i);
//                    int    m           = 0;
//                    int    local       = m + 1;
//                    String sessionName = WeekDaysEnum.getDescByCode(hit.getCameratime().getDay()) + " "
//                                         + DateUtils.getFormattedDate(hit.getCameratime(), "ddMMyyyy") + " "
//                                         + camera.getLocation();
//
//                    localCell = row.createCell(local);
//                    localCell.setCellValue(camera.getLocation());
//                    localCell.setCellStyle(styles.get("cell"));
//                    localCell = row.createCell(++local);
//                    localCell.setCellValue(sessionName);
//                    localCell.setCellStyle(styles.get("cell"));
//                    localCell = row.createCell(++local);
//                    localCell.setCellValue(DateUtils.getFormattedDate(hit.getCameratime(), "dd-MM-yyyy hh:mm:ss"));
//                    localCell.setCellStyle(styles.get("cell"));
//                    localCell = row.createCell(++local);
//                    localCell.setCellValue(hit.getMvreg());
//                    localCell.setCellStyle(styles.get("cell"));
//                    localCell = row.createCell(++local);
//                    localCell.setCellValue(hit.getCameraid().getCitytown() + " " + hit.getCameraid().getLocation());
//                    localCell.setCellStyle(styles.get("cell"));
//                    localCell = row.createCell(++local);
//                    localCell.setCellValue(camera.getGpsxcoordinates() + "," + camera.getGpsycoordinates());
//                    localCell.setCellStyle(styles.get("cell"));
//                    getpic(wb, sheet, ++local, j, 7, hit.getVregimage());
//
//                    if (hit.getVehimage() != null) {
//                        getpic(wb, sheet, ++local, j, 8, hit.getVehimage());
//                    }
//
//                    ++j;
//                    row = sheet.createRow(j);
//                }
//
//                j++;
//            }
//        }
//
//        // finally set column widths, the width is measured in units of 1/256th of a character width
//        for (int i = 0; i < 9; i++) {
//            sheet.setColumnWidth(i, 30 * 256);    // 6 characters wide
//        }
//
//        sheet.setColumnWidth(10, 10 * 256);    // 10 characters wide
//        wb.write(arrayOutputStream);
//        arrayOutputStream.close();
//
//        return arrayOutputStream;
//    }
//
//    public static byte[] convertImg(byte[] imageString) {
//        byte[] imageInByte = null;
//
//        try {
//            InputStream           is            = new ByteArrayInputStream(imageString);
//            BufferedImage         originalImage = ImageIO.read(is);
//            ByteArrayOutputStream baos          = new ByteArrayOutputStream();
//
//            ImageIO.write(originalImage, "png", baos);
//            baos.flush();
//            imageInByte = baos.toByteArray();
//            baos.close();
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//        }
//
//        return imageInByte;
//    }
//
//    public static byte[] convertImg() {
//        byte[] imageInByte = null;
//
//        try {
//            BufferedImage         originalImage = ImageIO.read(new File("C:/cool.jpg"));
//            ByteArrayOutputStream baos          = new ByteArrayOutputStream();
//
//            ImageIO.write(originalImage, "png", baos);
//            baos.flush();
//            imageInByte = baos.toByteArray();
//            baos.close();
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//        }
//
//        return imageInByte;
//    }
//}
//
//
////~ Formatted by Jindent --- http://www.jindent.com
