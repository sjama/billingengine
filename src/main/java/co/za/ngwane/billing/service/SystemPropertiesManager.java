package co.za.ngwane.billing.service;

import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;

import co.za.ngwane.billing.db.SysPropBe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.za.ngwane.billing.repository.BoSysPropRepo;

@Component
public class SystemPropertiesManager {
    Properties properties = new Properties();
    
    @Autowired
    private BoSysPropRepo boSysPropRepo;

    @PostConstruct
    protected void initialize() {
        updateConfiguration();
    }

    private void updateConfiguration() {

        List<SysPropBe> configurations = boSysPropRepo.findAll();
        
        for (SysPropBe configuration : configurations) {
            properties.setProperty(configuration.getPropKey(), configuration.getPropValue());
        }

    }
    
    public Object getValue(String key) {
        return properties.get(key);
    }


}
