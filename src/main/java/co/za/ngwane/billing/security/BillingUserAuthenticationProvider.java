/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package co.za.ngwane.billing.security;

import co.za.ngwane.billing.db.UsersBe;
import co.za.ngwane.billing.repository.TokenRepo;
import co.za.ngwane.billing.repository.service.UserBeRepoService;
import co.za.ngwane.billing.service.SystemPropertiesManager;
import co.za.ngwane.billing.service.dto.Role;
import co.za.ngwane.billing.service.dto.User;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JamaS
 */
@Component("billingUserAuthenticationProvider")
public class BillingUserAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    TokenRepo tokenRepo;

    @Autowired
    UserBeRepoService userBeRepoService;

    @Autowired
    private SystemPropertiesManager propertiesManager;

    private static String AUTH_URL = "";
    private static String NREP_U = "NREPU";
    private static String NREP_S = "NREPS";

    @Override
    @Transactional
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

        User user = loadUserByUsername(username, password);

        if (user == null || !user.getUsername().equalsIgnoreCase(username)) {
            throw new BadCredentialsException("User not found.");
        }

        if (!password.equals(user.getPassword())) {
            throw new BadCredentialsException("Wrong password.");
        }

        return new UsernamePasswordAuthenticationToken(username, password, user.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private static HttpHeaders createHttpHeaders(String user, String password) {
        String notEncoded = "Basic " + user + ":" + password;
        String encodedAuth = Base64.getEncoder().encodeToString(notEncoded.getBytes());
        System.out.println("encodedAuth = " + encodedAuth);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        // MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        //  headers.setContentType(mediaType);
        headers.add("Authorization", encodedAuth);
        headers.add("realm", "/za/rtmc/ad");
        return headers;
    }

    public User loadUserByUsername(final String username, String password) {

        User user = new User();
        List<Role> grantedAuthorities = new ArrayList<Role>();
        Role role = new Role();

        UsersBe usersBe = new UsersBe();
                //UsersBe usersBe = userBeRepoService.findByUsername(username);

        if (usersBe == null) {
            user = null;
        } else {
            user.setUsername(usersBe.getUsername());
            user.setPassword(usersBe.getPassword());
            user.setLastName(usersBe.getSurname());
            user.setFirstName(usersBe.getName());

            role.setName(usersBe.getLuRolesCd().getCode());
            grantedAuthorities.add(role);
            user.setAuthorities(grantedAuthorities);
        }
        return user;
    }
}
