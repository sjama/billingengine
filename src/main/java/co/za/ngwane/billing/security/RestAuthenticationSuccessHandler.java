package co.za.ngwane.billing.security;

import co.za.ngwane.billing.service.dto.Role;
import co.za.ngwane.billing.service.dto.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

/**
 * Spring Security success handler, specialized for Ajax requests.
 */
@Component
public class RestAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication)
            throws IOException {
        User user = new User();
        user.setUsername(authentication.getName());
        user.setPassword("");
        //user.setLastName("woza");
        //user.setFirstName("ngceshe"); 
        
        List<Role> authList =  new ArrayList<Role>();
        for(GrantedAuthority obj : authentication.getAuthorities()){
            Role authority = new Role();
            authority.setName(obj.getAuthority());
            authList.add(authority);
        }
        
        user.setAuthorities(authList);
        
        SecurityUtils.sendResponse(response, HttpServletResponse.SC_OK, user);
    }
}
