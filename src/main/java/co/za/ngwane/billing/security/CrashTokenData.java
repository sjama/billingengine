/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.security;

/**
 *
 * @author JamaS
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"aud",
"user_name",
"scope",
"exp",
"authorities",
"jti",
"client_id"
})
public class CrashTokenData {

@JsonProperty("aud")
private List<String> aud = null;
@JsonProperty("user_name")
private String userName;
@JsonProperty("scope")
private List<String> scope = null;
@JsonProperty("exp")
private Integer exp;
@JsonProperty("authorities")
private List<String> authorities = null;
@JsonProperty("jti")
private String jti;
@JsonProperty("client_id")
private String clientId;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("aud")
public List<String> getAud() {
return aud;
}

@JsonProperty("aud")
public void setAud(List<String> aud) {
this.aud = aud;
}

@JsonProperty("user_name")
public String getUserName() {
return userName;
}

@JsonProperty("user_name")
public void setUserName(String userName) {
this.userName = userName;
}

@JsonProperty("scope")
public List<String> getScope() {
return scope;
}

@JsonProperty("scope")
public void setScope(List<String> scope) {
this.scope = scope;
}

@JsonProperty("exp")
public Integer getExp() {
return exp;
}

@JsonProperty("exp")
public void setExp(Integer exp) {
this.exp = exp;
}

@JsonProperty("authorities")
public List<String> getAuthorities() {
return authorities;
}

@JsonProperty("authorities")
public void setAuthorities(List<String> authorities) {
this.authorities = authorities;
}

@JsonProperty("jti")
public String getJti() {
return jti;
}

@JsonProperty("jti")
public void setJti(String jti) {
this.jti = jti;
}

@JsonProperty("client_id")
public String getClientId() {
return clientId;
}

@JsonProperty("client_id")
public void setClientId(String clientId) {
this.clientId = clientId;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
