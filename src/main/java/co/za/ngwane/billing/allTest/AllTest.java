/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.allTest;

import co.za.ngwane.billing.db.DocReposBe;
import co.za.ngwane.billing.db.SysPropBe;
import co.za.ngwane.billing.repository.DocRepoRepo;
import co.za.ngwane.billing.repository.service.SysPropRepoService;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bheki.lubisi
 */
@Service
public class AllTest {

    @Autowired
    public DocRepoRepo docRepoRepo;

    @Autowired
    public SysPropRepoService boSysPropRepo;

    public void addUser() {
        try {
            DocReposBe docReposBe = new DocReposBe();
            docReposBe.setCreatedDate(new Date());
            docReposBe.setFileData(null);
            docReposBe.setFileName("DlalaBhekzin");
            docReposBe.setRefNo("DlalaBhekzin");

            docRepoRepo.save(docReposBe);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addSysProp() {
        try {
            SysPropBe prop = new SysPropBe();
            prop.setPropKey("Bheki");
            prop.setPropValue("Lubisi");

            boSysPropRepo.save(prop);

            System.out.println("co.za.ngwane.billing.allTest.AllTest.addSysProp()");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        AllTest allTest = new AllTest();
        try {
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            String year = calendar.get(Calendar.YEAR) + "";
            String day = calendar.get(Calendar.DATE) + "";
            String month = calendar.get(Calendar.MONTH) + 1 + "";

            System.out.println(" Lets See : O-" + day + month + year.substring(2, 4));

//            https://www.mkyong.com/java/how-to-get-current-timestamps-in-java/
            //method 1
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            System.out.println("Thy Timestamp is : "+timestamp);

        } catch (Exception e) {
        }

    }

}
