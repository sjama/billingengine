package co.za.ngwane.billing.utils;

import java.util.UUID;

public class UUIDGenerator {
	
	public static String getKey() {
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
		return randomUUIDString;
	}

}
