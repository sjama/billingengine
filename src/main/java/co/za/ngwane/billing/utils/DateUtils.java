package co.za.ngwane.billing.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

	public static String getDayFromDate(Date date) {
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
		return simpleDateformat.format(date);

	}

	public static String getTimeFromDate(Date date) {

		DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
		String time=dateFormat.format(date);
		System.out.println(time);
		return time;

	}
	
	public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

	/**
	Make an int Month from a date
	 */
	public static int getMonthInt(Date date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
		return Integer.parseInt(dateFormat.format(date));
	}

	public static int getYearInt(Date date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		return Integer.parseInt(dateFormat.format(date));
	}

	public static String convertDateToString(Date dateToConvert){

		/*
		 * To convert java.util.Date to String, use SimpleDateFormat class.
		 */

		/*
		 * crate new SimpleDateFormat instance with desired date format.
		 * We are going to use yyyy-mm-dd hh:mm:ss here.
		 */
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		//to convert Date to String, use format method of SimpleDateFormat class.
		String strDate = dateFormat.format(dateToConvert);

		//         System.out.println("Date converted to String: " + strDate);
		return strDate;

	}

	// We can move the dateFormat to be a method param so the method can be reused.
	public static Date convertStringToDate(String dateToConvert){

		Date date = null;
		try {
			date = new SimpleDateFormat("MM/dd/yyyy").parse(dateToConvert);
		} catch (ParseException e) {
			e.printStackTrace();
		}  

		return date;

	}
	
	// We can move the dateFormat to be a method param so the method can be reused.
		public static Date dateUtils(String dateToConvert, String pattern){

			Date date = null;
			try {
				date = new SimpleDateFormat(pattern).parse(dateToConvert);
			} catch (ParseException e) {
				e.printStackTrace();
			}  

			return date;

		}

	// We can move the dateFormat to be a method param so the method can be reused.
	public static Date convertStringToNiceDate(String dateToConvert){

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("MMM yyyy", Locale.ENGLISH);
		LocalDate ld = LocalDate.parse(dateToConvert, dtf);
		String month_name = dtf2.format(ld);
		System.out.println(month_name);
		return null;
	}

	public static void main(String [] args) {
		DateUtils.convertStringToNiceDate("2017/06/01");

	}

	public static Date constructDate(int day, int month, int year) {

		String date = year + "/" + month + "/" + day;
		Date utilDate = null;

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			utilDate = formatter.parse(date);
			System.out.println("utilDate:" + utilDate);
		} catch (ParseException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		
		return utilDate;
	}


}
