package co.za.ngwane.billing.utils;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestFacade {


    public RestFacade() {
        super();
    }

    public static String doRestGetCall(String URL) {
        System.out.println(URL);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON,MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> result = restTemplate.exchange(URL, HttpMethod.GET, entity, String.class);

        System.out.println(result);
        
//        ClientResponse response = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_XML).type(MediaType.APPLICATION_XML).get(ClientResponse.class);
//
//        if (response.getStatus() != 200) {
//            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
//        }
//
//        String output = response.getEntity(String.class);
//
//        logger.fine(" My Output is : " + output);s
        return result.getBody();
    }
    
    
    
}
