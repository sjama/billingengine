/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.utils;

/**
 *
 * @author jamasithole
 */
import co.za.ngwane.billing.service.BillingService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;

public class JobCompletionNotificationListener extends JobExecutionListenerSupport {
    @Autowired BillingService billingService;

    @Override
    public void afterJob(JobExecution jobExecution) {
        if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
            try {
                //billingService.dataBatch();
            } catch (Exception ex) {
                Logger.getLogger(JobCompletionNotificationListener.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("BATCH JOB FINISHED SUCCESSFULLY");
        }
    }
}
