/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;


import co.za.ngwane.billing.db.AddressBe;
import co.za.ngwane.billing.db.LuAddressTypeBe;
import co.za.ngwane.billing.repository.AddressBeRepo;
import co.za.ngwane.billing.repository.LuAddressTypeBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/addressBe")
public class AddressBeController {
private static final Logger logger = Logger.getLogger(AddressBeController.class);

@Autowired
private AddressBeRepo addressBeRepo;
@Autowired
private LuAddressTypeBeRepo luAddressTypeBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<AddressBe> save(
            @RequestBody AddressBe addressBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            addressBe = addressBeRepo.save(addressBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<AddressBe> update(
            @RequestBody AddressBe addressBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            addressBe = addressBeRepo.save(addressBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<AddressBe> delete(
            @RequestBody AddressBe addressBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            addressBe = addressBeRepo.save(addressBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "addressBe", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAllAddressBe() {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findAll();
            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe List FOUND Query returned [" + addressBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "AddressBe List FOUND Query returned [" + addressBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPhyAddrLine1/{phyAddrLine1}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPhyAddrLine1(@PathVariable String phyAddrLine1) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPhyAddrLine1(phyAddrLine1);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for phyAddrLine1 [ " + phyAddrLine1 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for phyAddrLine1 [ " + phyAddrLine1 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPhyAddrLine2/{phyAddrLine2}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPhyAddrLine2(@PathVariable String phyAddrLine2) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPhyAddrLine2(phyAddrLine2);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for phyAddrLine2 [ " + phyAddrLine2 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for phyAddrLine2 [ " + phyAddrLine2 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPhyAddrLine3/{phyAddrLine3}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPhyAddrLine3(@PathVariable String phyAddrLine3) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPhyAddrLine3(phyAddrLine3);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for phyAddrLine3 [ " + phyAddrLine3 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for phyAddrLine3 [ " + phyAddrLine3 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPhyAddrSuburb/{phyAddrSuburb}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPhyAddrSuburb(@PathVariable String phyAddrSuburb) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPhyAddrSuburb(phyAddrSuburb);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for phyAddrSuburb [ " + phyAddrSuburb + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for phyAddrSuburb [ " + phyAddrSuburb + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPhyAddrCity/{phyAddrCity}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPhyAddrCity(@PathVariable String phyAddrCity) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPhyAddrCity(phyAddrCity);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for phyAddrCity [ " + phyAddrCity + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for phyAddrCity [ " + phyAddrCity + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPhyAddrCode/{phyAddrCode}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPhyAddrCode(@PathVariable String phyAddrCode) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPhyAddrCode(phyAddrCode);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for phyAddrCode [ " + phyAddrCode + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for phyAddrCode [ " + phyAddrCode + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPosAddrLine1/{posAddrLine1}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPosAddrLine1(@PathVariable String posAddrLine1) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPosAddrLine1(posAddrLine1);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for posAddrLine1 [ " + posAddrLine1 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for posAddrLine1 [ " + posAddrLine1 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPosAddrLine2/{posAddrLine2}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPosAddrLine2(@PathVariable String posAddrLine2) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPosAddrLine2(posAddrLine2);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for posAddrLine2 [ " + posAddrLine2 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for posAddrLine2 [ " + posAddrLine2 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPosAddrLine3/{posAddrLine3}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPosAddrLine3(@PathVariable String posAddrLine3) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPosAddrLine3(posAddrLine3);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for posAddrLine3 [ " + posAddrLine3 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for posAddrLine3 [ " + posAddrLine3 + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPosAddrSuburb/{posAddrSuburb}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPosAddrSuburb(@PathVariable String posAddrSuburb) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPosAddrSuburb(posAddrSuburb);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for posAddrSuburb [ " + posAddrSuburb + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for posAddrSuburb [ " + posAddrSuburb + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPosAddrCity/{posAddrCity}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPosAddrCity(@PathVariable String posAddrCity) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPosAddrCity(posAddrCity);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for posAddrCity [ " + posAddrCity + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for posAddrCity [ " + posAddrCity + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByPosAddrCode/{posAddrCode}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByPosAddrCode(@PathVariable String posAddrCode) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByPosAddrCode(posAddrCode);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for posAddrCode [ " + posAddrCode + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for posAddrCode [ " + posAddrCode + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByStatus(@PathVariable String status) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByStatus(status);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for status [ " + status + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for status [ " + status + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByStatChgD(@PathVariable Date statChgD) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            addressBeList = addressBeRepo.findByStatChgD(statChgD);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for statChgD [ " + statChgD + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for statChgD [ " + statChgD + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeByLuAddressTypeCd/{luAddressTypeCd}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBe>> getAddressBeByLuAddressTypeCd(@PathVariable String luAddressTypeCd) {
        List<AddressBe> addressBeList = new ArrayList<>();
        try {
            LuAddressTypeBe luAddressTypeBe = luAddressTypeBeRepo.findOne(new String(luAddressTypeCd));
            addressBeList = addressBeRepo.findByLuAddressTypeCd(luAddressTypeBe);

            if (addressBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBe FOUND for luAddressTypeCd [ " + luAddressTypeCd + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBe FOUND for luAddressTypeCd [ " + luAddressTypeCd + " ] query returned [" + addressBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeList,
                HttpStatus.OK);
    }



}