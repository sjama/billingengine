/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.LuAddressTypeBe;
import co.za.ngwane.billing.repository.LuAddressTypeBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/luAddressTypeBe")
public class LuAddressTypeBeController {
private static final Logger logger = Logger.getLogger(LuAddressTypeBeController.class);

@Autowired
private LuAddressTypeBeRepo luAddressTypeBeRepo;


@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<LuAddressTypeBe> save(
            @RequestBody LuAddressTypeBe luAddressTypeBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luAddressTypeBe = luAddressTypeBeRepo.save(luAddressTypeBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luAddressTypeBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<LuAddressTypeBe> update(
            @RequestBody LuAddressTypeBe luAddressTypeBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luAddressTypeBe = luAddressTypeBeRepo.save(luAddressTypeBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luAddressTypeBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<LuAddressTypeBe> delete(
            @RequestBody LuAddressTypeBe luAddressTypeBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luAddressTypeBe = luAddressTypeBeRepo.save(luAddressTypeBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luAddressTypeBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "luAddressTypeBe", method = RequestMethod.GET)
    public ResponseEntity<List<LuAddressTypeBe>> getAllLuAddressTypeBe() {
        List<LuAddressTypeBe> luAddressTypeBeList = new ArrayList<>();
        try {
            luAddressTypeBeList = luAddressTypeBeRepo.findAll();
            if (luAddressTypeBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuAddressTypeBe List FOUND Query returned [" + luAddressTypeBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "LuAddressTypeBe List FOUND Query returned [" + luAddressTypeBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luAddressTypeBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "luAddressTypeBeByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<LuAddressTypeBe>> getLuAddressTypeBeByDescription(@PathVariable String description) {
        List<LuAddressTypeBe> luAddressTypeBeList = new ArrayList<>();
        try {
            luAddressTypeBeList = luAddressTypeBeRepo.findByDescription(description);

            if (luAddressTypeBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuAddressTypeBe FOUND for description [ " + description + " ] query returned [" + luAddressTypeBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LuAddressTypeBe FOUND for description [ " + description + " ] query returned [" + luAddressTypeBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luAddressTypeBeList,
                HttpStatus.OK);
    }

}