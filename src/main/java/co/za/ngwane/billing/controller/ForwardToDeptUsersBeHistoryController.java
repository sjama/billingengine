/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ForwardToDeptUsersBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.ForwardToDeptUsersBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/forwardToDeptUsersBeHistory")
public class ForwardToDeptUsersBeHistoryController {
private static final Logger logger = Logger.getLogger(ForwardToDeptUsersBeHistoryController.class);

@Autowired
private ForwardToDeptUsersBeHistoryRepo forwardToDeptUsersBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ForwardToDeptUsersBeHistory> save(
            @RequestBody ForwardToDeptUsersBeHistory forwardToDeptUsersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            forwardToDeptUsersBeHistory = forwardToDeptUsersBeHistoryRepo.save(forwardToDeptUsersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ForwardToDeptUsersBeHistory> update(
            @RequestBody ForwardToDeptUsersBeHistory forwardToDeptUsersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            forwardToDeptUsersBeHistory = forwardToDeptUsersBeHistoryRepo.save(forwardToDeptUsersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ForwardToDeptUsersBeHistory> delete(
            @RequestBody ForwardToDeptUsersBeHistory forwardToDeptUsersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            forwardToDeptUsersBeHistory = forwardToDeptUsersBeHistoryRepo.save(forwardToDeptUsersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "forwardToDeptUsersBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBeHistory>> getAllForwardToDeptUsersBeHistory() {
        List<ForwardToDeptUsersBeHistory> forwardToDeptUsersBeHistoryList = new ArrayList<>();
        try {
            forwardToDeptUsersBeHistoryList = forwardToDeptUsersBeHistoryRepo.findAll();
            if (forwardToDeptUsersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBeHistory List FOUND Query returned [" + forwardToDeptUsersBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ForwardToDeptUsersBeHistory List FOUND Query returned [" + forwardToDeptUsersBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBeHistory>> getForwardToDeptUsersBeHistoryByRevtype(@PathVariable Short revtype) {
        List<ForwardToDeptUsersBeHistory> forwardToDeptUsersBeHistoryList = new ArrayList<>();
        try {
            forwardToDeptUsersBeHistoryList = forwardToDeptUsersBeHistoryRepo.findByRevtype(revtype);

            if (forwardToDeptUsersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeHistoryByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBeHistory>> getForwardToDeptUsersBeHistoryByStatChgDate(@PathVariable Date statChgDate) {
        List<ForwardToDeptUsersBeHistory> forwardToDeptUsersBeHistoryList = new ArrayList<>();
        try {
            forwardToDeptUsersBeHistoryList = forwardToDeptUsersBeHistoryRepo.findByStatChgDate(statChgDate);

            if (forwardToDeptUsersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBeHistory>> getForwardToDeptUsersBeHistoryByStatus(@PathVariable String status) {
        List<ForwardToDeptUsersBeHistory> forwardToDeptUsersBeHistoryList = new ArrayList<>();
        try {
            forwardToDeptUsersBeHistoryList = forwardToDeptUsersBeHistoryRepo.findByStatus(status);

            if (forwardToDeptUsersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBeHistory FOUND for status [ " + status + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBeHistory FOUND for status [ " + status + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeHistoryByLuForwardToDeptCd/{luForwardToDeptCd}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBeHistory>> getForwardToDeptUsersBeHistoryByLuForwardToDeptCd(@PathVariable String luForwardToDeptCd) {
        List<ForwardToDeptUsersBeHistory> forwardToDeptUsersBeHistoryList = new ArrayList<>();
        try {
            forwardToDeptUsersBeHistoryList = forwardToDeptUsersBeHistoryRepo.findByLuForwardToDeptCd(luForwardToDeptCd);

            if (forwardToDeptUsersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBeHistory FOUND for luForwardToDeptCd [ " + luForwardToDeptCd + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBeHistory FOUND for luForwardToDeptCd [ " + luForwardToDeptCd + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeHistoryByUsersId/{usersId}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBeHistory>> getForwardToDeptUsersBeHistoryByUsersId(@PathVariable Integer usersId) {
        List<ForwardToDeptUsersBeHistory> forwardToDeptUsersBeHistoryList = new ArrayList<>();
        try {
            forwardToDeptUsersBeHistoryList = forwardToDeptUsersBeHistoryRepo.findByUsersId(usersId);

            if (forwardToDeptUsersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBeHistory FOUND for usersId [ " + usersId + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBeHistory FOUND for usersId [ " + usersId + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBeHistory>> getForwardToDeptUsersBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<ForwardToDeptUsersBeHistory> forwardToDeptUsersBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            forwardToDeptUsersBeHistoryList = forwardToDeptUsersBeHistoryRepo.findByRevinfo(revinfo);

            if (forwardToDeptUsersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + forwardToDeptUsersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeHistoryList,
                HttpStatus.OK);
    }



}