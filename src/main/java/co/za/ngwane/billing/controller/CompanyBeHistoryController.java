/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.CompanyBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.CompanyBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/companyBeHistory")
public class CompanyBeHistoryController {
private static final Logger logger = Logger.getLogger(CompanyBeHistoryController.class);

@Autowired
private CompanyBeHistoryRepo companyBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<CompanyBeHistory> save(
            @RequestBody CompanyBeHistory companyBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            companyBeHistory = companyBeHistoryRepo.save(companyBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<CompanyBeHistory> update(
            @RequestBody CompanyBeHistory companyBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            companyBeHistory = companyBeHistoryRepo.save(companyBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<CompanyBeHistory> delete(
            @RequestBody CompanyBeHistory companyBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            companyBeHistory = companyBeHistoryRepo.save(companyBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "companyBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getAllCompanyBeHistory() {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findAll();
            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory List FOUND Query returned [" + companyBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "CompanyBeHistory List FOUND Query returned [" + companyBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByRevtype(@PathVariable Short revtype) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByRevtype(revtype);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByBankAccNo/{bankAccNo}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByBankAccNo(@PathVariable String bankAccNo) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByBankAccNo(bankAccNo);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for bankAccNo [ " + bankAccNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for bankAccNo [ " + bankAccNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByBankBranch/{bankBranch}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByBankBranch(@PathVariable String bankBranch) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByBankBranch(bankBranch);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for bankBranch [ " + bankBranch + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for bankBranch [ " + bankBranch + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByBankBranchCd/{bankBranchCd}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByBankBranchCd(@PathVariable String bankBranchCd) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByBankBranchCd(bankBranchCd);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for bankBranchCd [ " + bankBranchCd + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for bankBranchCd [ " + bankBranchCd + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByBankName/{bankName}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByBankName(@PathVariable String bankName) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByBankName(bankName);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for bankName [ " + bankName + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for bankName [ " + bankName + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByEmail/{email}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByEmail(@PathVariable String email) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByEmail(email);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for email [ " + email + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for email [ " + email + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByFaxNo/{faxNo}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByFaxNo(@PathVariable String faxNo) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByFaxNo(faxNo);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for faxNo [ " + faxNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for faxNo [ " + faxNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByLogoRefn/{logoRefn}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByLogoRefn(@PathVariable String logoRefn) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByLogoRefn(logoRefn);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for logoRefn [ " + logoRefn + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for logoRefn [ " + logoRefn + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByName/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByName(@PathVariable String name) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByName(name);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for name [ " + name + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for name [ " + name + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPhyAddrCity/{phyAddrCity}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPhyAddrCity(@PathVariable String phyAddrCity) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPhyAddrCity(phyAddrCity);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for phyAddrCity [ " + phyAddrCity + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for phyAddrCity [ " + phyAddrCity + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPhyAddrCode/{phyAddrCode}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPhyAddrCode(@PathVariable String phyAddrCode) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPhyAddrCode(phyAddrCode);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for phyAddrCode [ " + phyAddrCode + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for phyAddrCode [ " + phyAddrCode + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPhyAddrLine1/{phyAddrLine1}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPhyAddrLine1(@PathVariable String phyAddrLine1) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPhyAddrLine1(phyAddrLine1);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for phyAddrLine1 [ " + phyAddrLine1 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for phyAddrLine1 [ " + phyAddrLine1 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPhyAddrLine2/{phyAddrLine2}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPhyAddrLine2(@PathVariable String phyAddrLine2) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPhyAddrLine2(phyAddrLine2);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for phyAddrLine2 [ " + phyAddrLine2 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for phyAddrLine2 [ " + phyAddrLine2 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPhyAddrLine3/{phyAddrLine3}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPhyAddrLine3(@PathVariable String phyAddrLine3) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPhyAddrLine3(phyAddrLine3);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for phyAddrLine3 [ " + phyAddrLine3 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for phyAddrLine3 [ " + phyAddrLine3 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPhyAddrSuburb/{phyAddrSuburb}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPhyAddrSuburb(@PathVariable String phyAddrSuburb) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPhyAddrSuburb(phyAddrSuburb);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for phyAddrSuburb [ " + phyAddrSuburb + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for phyAddrSuburb [ " + phyAddrSuburb + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPosAddrCity/{posAddrCity}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPosAddrCity(@PathVariable String posAddrCity) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPosAddrCity(posAddrCity);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for posAddrCity [ " + posAddrCity + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for posAddrCity [ " + posAddrCity + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPosAddrCode/{posAddrCode}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPosAddrCode(@PathVariable String posAddrCode) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPosAddrCode(posAddrCode);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for posAddrCode [ " + posAddrCode + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for posAddrCode [ " + posAddrCode + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPosAddrLine1/{posAddrLine1}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPosAddrLine1(@PathVariable String posAddrLine1) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPosAddrLine1(posAddrLine1);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for posAddrLine1 [ " + posAddrLine1 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for posAddrLine1 [ " + posAddrLine1 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPosAddrLine2/{posAddrLine2}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPosAddrLine2(@PathVariable String posAddrLine2) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPosAddrLine2(posAddrLine2);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for posAddrLine2 [ " + posAddrLine2 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for posAddrLine2 [ " + posAddrLine2 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPosAddrLine3/{posAddrLine3}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPosAddrLine3(@PathVariable String posAddrLine3) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPosAddrLine3(posAddrLine3);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for posAddrLine3 [ " + posAddrLine3 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for posAddrLine3 [ " + posAddrLine3 + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByPosAddrSuburb/{posAddrSuburb}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByPosAddrSuburb(@PathVariable String posAddrSuburb) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByPosAddrSuburb(posAddrSuburb);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for posAddrSuburb [ " + posAddrSuburb + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for posAddrSuburb [ " + posAddrSuburb + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByRegNo/{regNo}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByRegNo(@PathVariable String regNo) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByRegNo(regNo);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for regNo [ " + regNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for regNo [ " + regNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByStat/{stat}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByStat(@PathVariable String stat) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByStat(stat);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for stat [ " + stat + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for stat [ " + stat + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByStatChgD(@PathVariable Date statChgD) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByStatChgD(statChgD);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByTelNo/{telNo}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByTelNo(@PathVariable String telNo) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByTelNo(telNo);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for telNo [ " + telNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for telNo [ " + telNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByVatNo/{vatNo}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByVatNo(@PathVariable String vatNo) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByVatNo(vatNo);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for vatNo [ " + vatNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for vatNo [ " + vatNo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByWebUrl/{webUrl}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByWebUrl(@PathVariable String webUrl) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            companyBeHistoryList = companyBeHistoryRepo.findByWebUrl(webUrl);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for webUrl [ " + webUrl + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for webUrl [ " + webUrl + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "companyBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<CompanyBeHistory>> getCompanyBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<CompanyBeHistory> companyBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            companyBeHistoryList = companyBeHistoryRepo.findByRevinfo(revinfo);

            if (companyBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CompanyBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CompanyBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + companyBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(companyBeHistoryList,
                HttpStatus.OK);
    }



}