/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.LuDeliveryNoteStatBe;
import co.za.ngwane.billing.repository.LuDeliveryNoteStatBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/luDeliveryNoteStatBe")
public class LuDeliveryNoteStatBeController {
private static final Logger logger = Logger.getLogger(LuDeliveryNoteStatBeController.class);

@Autowired
private LuDeliveryNoteStatBeRepo luDeliveryNoteStatBeRepo;


@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<LuDeliveryNoteStatBe> save(
            @RequestBody LuDeliveryNoteStatBe luDeliveryNoteStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luDeliveryNoteStatBe = luDeliveryNoteStatBeRepo.save(luDeliveryNoteStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luDeliveryNoteStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<LuDeliveryNoteStatBe> update(
            @RequestBody LuDeliveryNoteStatBe luDeliveryNoteStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luDeliveryNoteStatBe = luDeliveryNoteStatBeRepo.save(luDeliveryNoteStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luDeliveryNoteStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<LuDeliveryNoteStatBe> delete(
            @RequestBody LuDeliveryNoteStatBe luDeliveryNoteStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luDeliveryNoteStatBe = luDeliveryNoteStatBeRepo.save(luDeliveryNoteStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luDeliveryNoteStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "luDeliveryNoteStatBe", method = RequestMethod.GET)
    public ResponseEntity<List<LuDeliveryNoteStatBe>> getAllLuDeliveryNoteStatBe() {
        List<LuDeliveryNoteStatBe> luDeliveryNoteStatBeList = new ArrayList<>();
        try {
            luDeliveryNoteStatBeList = luDeliveryNoteStatBeRepo.findAll();
            if (luDeliveryNoteStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuDeliveryNoteStatBe List FOUND Query returned [" + luDeliveryNoteStatBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "LuDeliveryNoteStatBe List FOUND Query returned [" + luDeliveryNoteStatBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luDeliveryNoteStatBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "luDeliveryNoteStatBeByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<LuDeliveryNoteStatBe>> getLuDeliveryNoteStatBeByDescription(@PathVariable String description) {
        List<LuDeliveryNoteStatBe> luDeliveryNoteStatBeList = new ArrayList<>();
        try {
            luDeliveryNoteStatBeList = luDeliveryNoteStatBeRepo.findByDescription(description);

            if (luDeliveryNoteStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuDeliveryNoteStatBe FOUND for description [ " + description + " ] query returned [" + luDeliveryNoteStatBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LuDeliveryNoteStatBe FOUND for description [ " + description + " ] query returned [" + luDeliveryNoteStatBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luDeliveryNoteStatBeList,
                HttpStatus.OK);
    }


}