/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.RevinfoRepo;

/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/revinfo")
public class RevinfoController {
private static final Logger logger = Logger.getLogger(RevinfoController.class);

@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<Revinfo> save(
            @RequestBody Revinfo revinfo ,
            UriComponentsBuilder ucBuilder) {
        try {
            revinfo = revinfoRepo.save(revinfo);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(revinfo, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<Revinfo> update(
            @RequestBody Revinfo revinfo ,
            UriComponentsBuilder ucBuilder) {
        try {
            revinfo = revinfoRepo.save(revinfo);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(revinfo, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<Revinfo> delete(
            @RequestBody Revinfo revinfo ,
            UriComponentsBuilder ucBuilder) {
        try {
            revinfo = revinfoRepo.save(revinfo);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(revinfo, HttpStatus.CREATED);
    }

@RequestMapping(value = "revinfo", method = RequestMethod.GET)
    public ResponseEntity<List<Revinfo>> getAllRevinfo() {
        List<Revinfo> revinfoList = new ArrayList<>();
        try {
            revinfoList = revinfoRepo.findAll();
            if (revinfoList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO Revinfo List FOUND Query returned [" + revinfoList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "Revinfo List FOUND Query returned [" + revinfoList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(revinfoList,
                HttpStatus.OK);
    }

@RequestMapping(value = "revinfoByRevtstmp/{revtstmp}", method = RequestMethod.GET)
    public ResponseEntity<List<Revinfo>> getRevinfoByRevtstmp(@PathVariable BigInteger revtstmp) {
        List<Revinfo> revinfoList = new ArrayList<>();
        try {
            revinfoList = revinfoRepo.findByRevtstmp(revtstmp);

            if (revinfoList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO Revinfo FOUND for revtstmp [ " + revtstmp + " ] query returned [" + revinfoList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "Revinfo FOUND for revtstmp [ " + revtstmp + " ] query returned [" + revinfoList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(revinfoList,
                HttpStatus.OK);
    }

}