/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.OriginatingLineBe;
import co.za.ngwane.billing.repository.OriginatingLineRepo;
import co.za.ngwane.billing.repository.service.CustomerRepoService;
import co.za.ngwane.billing.repository.service.OriginatingLinesRepoService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api/originatingLineController/")
public class OriginatingNumberController {

    private static final Logger logger = Logger.getLogger(OriginatingNumberController.class);

    @Autowired
    public OriginatingLinesRepoService linesRepoService;

    @Autowired
    public OriginatingLineRepo lineRepo;

    @Autowired
    public CustomerRepoService customerRepoService;

    @RequestMapping(value = "lines/{customerId}", method = RequestMethod.GET)
    public ResponseEntity<List<OriginatingLineBe>> getByCustomer(@PathVariable int customerId) {
        List<OriginatingLineBe> customerList = new ArrayList<>();
        try {
            customerList = linesRepoService.findByCustomersId(customerId);
            if (customerList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                        + "NO LINES FOUND Get All Customers returned [" + customerList.size() + "] ORIGNATING LINES.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + "Customers ORIGNATING LINES  FOUND  [" + customerList.size() + "] ORIGINATING LINES.\n"
                        + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customerList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "allLines", method = RequestMethod.GET)
    public ResponseEntity<List<OriginatingLineBe>> getAll() {
        List<OriginatingLineBe> customerList = new ArrayList<>();
        try {
            customerList = linesRepoService.getAllLines();
            if (customerList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                        + "NO LINES FOUND  returned [" + customerList.size() + "] ORIGNATING LINES.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + " ORIGNATING LINES  FOUND  [" + customerList.size() + "] ORIGINATING LINES.\n"
                        + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customerList,
                HttpStatus.OK);
    }
    
    @RequestMapping(value = "lines", method = RequestMethod.GET)
    public ResponseEntity<List<OriginatingLineBe>> getAllActive() {
        List<OriginatingLineBe> customerList = new ArrayList<>();
        try {
            customerList = linesRepoService.getAllActive(BillingConstants.LU_STAT_DELETED);
            if (customerList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                        + "NO LINES FOUND  returned [" + customerList.size() + "] ORIGNATING LINES.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + " ORIGNATING LINES  FOUND  [" + customerList.size() + "] ORIGINATING LINES.\n"
                        + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customerList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<OriginatingLineBe> add(
            @RequestBody OriginatingLineBe originatingLine,
            UriComponentsBuilder ucBuilder) {
        try {

            logger.info(
                    "===============================================================\n"
                    + "Originating Line we are in TAG [" + originatingLine.getTagName() + "] AT [" + new Date() + "].\n"
                    + "\n===============================================================");

            switch (originatingLine.getTagName()) {
                       case BillingConstants.TAG_ADD:
                    originatingLine.setStatus(BillingConstants.LU_STAT_ACTIVE);
                    originatingLine.setStatChgD(new Date());
                    break;

                case BillingConstants.TAG_UPDATE:
                    break;

                case BillingConstants.TAG_DELETE:
                    originatingLine.setStatus(BillingConstants.LU_STAT_DELETED);
                    originatingLine.setStatChgD(new Date());
                    break;

                default:

            }

            lineRepo.save(originatingLine);

            logger.info(
                    "===============================================================\n"
                    + "Originating Line with number [" + originatingLine.getLine() + "] with "
                            + "TAG [" + originatingLine.getTagName() + "] saved successful.\n"
                    + "\n===============================================================");

        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }

        return new ResponseEntity<>(originatingLine, HttpStatus.CREATED);
    }
}
