/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.AgreedRatesBe;
import co.za.ngwane.billing.db.AgreedRatesPK;
import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.repository.AgreedRatesRepo;
import co.za.ngwane.billing.repository.service.AgreedRatesRepoService;
import co.za.ngwane.billing.repository.service.CustomerRepoService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api/agreedRateController/")
public class AgreedRateController {

    private static final Logger logger = Logger.getLogger(AgreedRateController.class);

    @Autowired
    public AgreedRatesRepoService agreedRatesRepoService;

    @Autowired
    public AgreedRatesRepo agreedRatesRepo;

    @Autowired
    public CustomerRepoService customerRepoService;

    @RequestMapping(value = "rates/{customerId}", method = RequestMethod.GET)
    public ResponseEntity<List<AgreedRatesBe>> getByCustomer(@PathVariable int customerId) {
        List<AgreedRatesBe> agreedRatesList = new ArrayList<>();
        try {
            CustomersBe customersBe = customerRepoService.getById(new Long(customerId));
            agreedRatesList = agreedRatesRepoService.findByCustomersBeAndStatusNot(customersBe, BillingConstants.LU_STAT_DELETED);
            if (agreedRatesList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO Rates found Query returned [" + agreedRatesList.size() + "] ORIGNATING LINES.\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + "Rates FOUND Query returned [" + agreedRatesList.size() + "] ORIGINATING LINES.\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(agreedRatesList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "allRates", method = RequestMethod.GET)
    public ResponseEntity<List<AgreedRatesBe>> getAll() {
        List<AgreedRatesBe> agreedRatesList = new ArrayList<>();
        try {
            agreedRatesList = agreedRatesRepoService.getAllRates();
            if (agreedRatesList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO Rates found Query returned [" + agreedRatesList.size() + "] ORIGNATING LINES.\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + "Rates FOUND Query returned [" + agreedRatesList.size() + "] ORIGINATING LINES.\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(agreedRatesList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "rates", method = RequestMethod.GET)
    public ResponseEntity<List<AgreedRatesBe>> getAllActive() {
        List<AgreedRatesBe> agreedRatesList = new ArrayList<>();
        try {
            agreedRatesList = agreedRatesRepoService.getAllActive(BillingConstants.LU_STAT_DELETED);
            if (agreedRatesList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO Rates found Query returned [" + agreedRatesList.size() + "] ORIGNATING LINES.\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + "Rates FOUND Query returned [" + agreedRatesList.size() + "] ORIGINATING LINES.\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(agreedRatesList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<AgreedRatesBe> add(
            @RequestBody AgreedRatesBe agreedRates,
            UriComponentsBuilder ucBuilder) {
        try {

            logger.info(
                    "===============================================================\n"
                            + "Agreed Rate we are in TAG [" + agreedRates.getTagName() + "] AT [" + new Date() + "].\n"
                            + "\n===============================================================");

            //AgreedRatesBe checkExist = agreedRatesRepo.findOne(agreedRates.getAgreedRatesPK());

            AgreedRatesBe existAlready = checkExist(agreedRates.getAgreedRatesPK());

            switch (agreedRates.getTagName()) {
                case BillingConstants.TAG_ADD:

                    if (existAlready != null) {
                        //check if is it not deleted
                        if (existAlready.getStatus().equals(BillingConstants.LU_STAT_DELETED)) {
                            agreedRates.setStatus(BillingConstants.LU_STAT_ACTIVE);
                            agreedRates.setStatChgD(new Date());

                            agreedRatesRepoService.save(agreedRates);

                            agreedRates.setTagFlag(BillingConstants.BOOLEAN_TRUE);
                            agreedRates.setTagMsg("Rate saved successful.");
                        } else {
                            agreedRates.setTagFlag(BillingConstants.BOOLEAN_FALSE);
                            agreedRates.setTagMsg("A rate with this parameters , Call Type, Destination" +
                                    " and Billing Type already exist for this customer.");

                            logger.info(
                                    "===============================================================\n"
                                            + " [" + agreedRates.getTagMsg() + "] AT [" + new Date() + "].\n"
                                            + "\n===============================================================");
                        }

                    } else {
                        agreedRates.setStatus(BillingConstants.LU_STAT_ACTIVE);
                        agreedRates.setStatChgD(new Date());

                        agreedRatesRepoService.save(agreedRates);

                        agreedRates.setTagFlag(BillingConstants.BOOLEAN_TRUE);
                        agreedRates.setTagMsg("Rate saved successful.");

                        logger.info(
                                "===============================================================\n"
                                        + " [" + agreedRates.getTagMsg() + "] AT [" + new Date() + "].\n"
                                        + "\n===============================================================");
                    }

                    break;

                case BillingConstants.TAG_UPDATE:

                    if (agreedRates.getPkChanged()) {
                        if (existAlready != null &&
                                !(agreedRates.getAgreedRatesPK().equals(agreedRates.getOldAgreedRatesPK()))) {

                            //We cant save since this will add an already existing rate
                            agreedRates.setTagFlag(BillingConstants.BOOLEAN_FALSE);
                            agreedRates.setTagMsg("A rate with this parameters, Call Type, Destination and Billing Type already exist for this " +
                                    "customer.");

                            logger.info(
                                    "===============================================================\n"
                                            + " [" + agreedRates.getTagMsg() + "] AT [" + new Date() + "].\n"
                                            + "\n===============================================================");

                        } else {

                            agreedRatesRepoService.save(agreedRates);

                            agreedRates.setTagFlag(BillingConstants.BOOLEAN_TRUE);
                            agreedRates.setTagMsg("Rate saved successful.");

                            logger.info(
                                    "===============================================================\n"
                                            + " [" + agreedRates.getTagMsg() + "] AT [" + new Date() + "].\n"
                                            + "\n===============================================================");
                        }

                    }
                    break;

                case BillingConstants.TAG_DELETE:
                    agreedRates.setStatus(BillingConstants.LU_STAT_DELETED);
                    agreedRates.setStatChgD(new Date());

                    agreedRatesRepoService.save(agreedRates);

                    agreedRates.setTagFlag(BillingConstants.BOOLEAN_TRUE);
                    agreedRates.setTagMsg("Rate deleted successfully.");

                    logger.info(
                            "===============================================================\n"
                                    + " [" + agreedRates.getTagMsg() + "] AT [" + new Date() + "].\n"
                                    + "\n===============================================================");

                    break;

                default:

            }

        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }

        return new ResponseEntity<>(agreedRates, HttpStatus.CREATED);
    }


    public AgreedRatesBe checkExist(AgreedRatesPK agreedRatesPK) {
        AgreedRatesBe existAlready;

        AgreedRatesBe agreedRates = agreedRatesRepo.findOne(agreedRatesPK);
//        if (agreedRates != null) {
//            existAlready = true;
//        } else {
//            existAlready = false;
//        }

        return agreedRates;
    }
}
