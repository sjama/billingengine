/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ContactPersonBe;
import co.za.ngwane.billing.repository.ContactPersonBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/contactPersonBe")
public class ContactPersonBeController {
private static final Logger logger = Logger.getLogger(ContactPersonBeController.class);

@Autowired
private ContactPersonBeRepo contactPersonBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ContactPersonBe> save(
            @RequestBody ContactPersonBe contactPersonBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            contactPersonBe = contactPersonBeRepo.save(contactPersonBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ContactPersonBe> update(
            @RequestBody ContactPersonBe contactPersonBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            contactPersonBe = contactPersonBeRepo.save(contactPersonBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ContactPersonBe> delete(
            @RequestBody ContactPersonBe contactPersonBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            contactPersonBe = contactPersonBeRepo.save(contactPersonBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "contactPersonBe", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBe>> getAllContactPersonBe() {
        List<ContactPersonBe> contactPersonBeList = new ArrayList<>();
        try {
            contactPersonBeList = contactPersonBeRepo.findAll();
            if (contactPersonBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBe List FOUND Query returned [" + contactPersonBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ContactPersonBe List FOUND Query returned [" + contactPersonBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeByName/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBe>> getContactPersonBeByName(@PathVariable String name) {
        List<ContactPersonBe> contactPersonBeList = new ArrayList<>();
        try {
            contactPersonBeList = contactPersonBeRepo.findByName(name);

            if (contactPersonBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBe FOUND for name [ " + name + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBe FOUND for name [ " + name + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeBySurname/{surname}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBe>> getContactPersonBeBySurname(@PathVariable String surname) {
        List<ContactPersonBe> contactPersonBeList = new ArrayList<>();
        try {
            contactPersonBeList = contactPersonBeRepo.findBySurname(surname);

            if (contactPersonBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBe FOUND for surname [ " + surname + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBe FOUND for surname [ " + surname + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeByCellNo/{cellNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBe>> getContactPersonBeByCellNo(@PathVariable String cellNo) {
        List<ContactPersonBe> contactPersonBeList = new ArrayList<>();
        try {
            contactPersonBeList = contactPersonBeRepo.findByCellNo(cellNo);

            if (contactPersonBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBe FOUND for cellNo [ " + cellNo + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBe FOUND for cellNo [ " + cellNo + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeByPhoneNo/{phoneNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBe>> getContactPersonBeByPhoneNo(@PathVariable String phoneNo) {
        List<ContactPersonBe> contactPersonBeList = new ArrayList<>();
        try {
            contactPersonBeList = contactPersonBeRepo.findByPhoneNo(phoneNo);

            if (contactPersonBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBe FOUND for phoneNo [ " + phoneNo + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBe FOUND for phoneNo [ " + phoneNo + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeByEmail/{email}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBe>> getContactPersonBeByEmail(@PathVariable String email) {
        List<ContactPersonBe> contactPersonBeList = new ArrayList<>();
        try {
            contactPersonBeList = contactPersonBeRepo.findByEmail(email);

            if (contactPersonBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBe FOUND for email [ " + email + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBe FOUND for email [ " + email + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBe>> getContactPersonBeByStatus(@PathVariable String status) {
        List<ContactPersonBe> contactPersonBeList = new ArrayList<>();
        try {
            contactPersonBeList = contactPersonBeRepo.findByStatus(status);

            if (contactPersonBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBe FOUND for status [ " + status + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBe FOUND for status [ " + status + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBe>> getContactPersonBeByStatChgDate(@PathVariable Date statChgDate) {
        List<ContactPersonBe> contactPersonBeList = new ArrayList<>();
        try {
            contactPersonBeList = contactPersonBeRepo.findByStatChgDate(statChgDate);

            if (contactPersonBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + contactPersonBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeList,
                HttpStatus.OK);
    }



}