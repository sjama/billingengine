/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.BankDetBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.BankDetBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/bankDetBeHistory")
public class BankDetBeHistoryController {
private static final Logger logger = Logger.getLogger(BankDetBeHistoryController.class);

@Autowired
private BankDetBeHistoryRepo bankDetBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<BankDetBeHistory> save(
            @RequestBody BankDetBeHistory bankDetBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            bankDetBeHistory = bankDetBeHistoryRepo.save(bankDetBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<BankDetBeHistory> update(
            @RequestBody BankDetBeHistory bankDetBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            bankDetBeHistory = bankDetBeHistoryRepo.save(bankDetBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<BankDetBeHistory> delete(
            @RequestBody BankDetBeHistory bankDetBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            bankDetBeHistory = bankDetBeHistoryRepo.save(bankDetBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "bankDetBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getAllBankDetBeHistory() {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            bankDetBeHistoryList = bankDetBeHistoryRepo.findAll();
            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory List FOUND Query returned [" + bankDetBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "BankDetBeHistory List FOUND Query returned [" + bankDetBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getBankDetBeHistoryByRevtype(@PathVariable Short revtype) {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            bankDetBeHistoryList = bankDetBeHistoryRepo.findByRevtype(revtype);

            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeHistoryByAccountName/{accountName}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getBankDetBeHistoryByAccountName(@PathVariable String accountName) {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            bankDetBeHistoryList = bankDetBeHistoryRepo.findByAccountName(accountName);

            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory FOUND for accountName [ " + accountName + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBeHistory FOUND for accountName [ " + accountName + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeHistoryByAccountNo/{accountNo}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getBankDetBeHistoryByAccountNo(@PathVariable String accountNo) {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            bankDetBeHistoryList = bankDetBeHistoryRepo.findByAccountNo(accountNo);

            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory FOUND for accountNo [ " + accountNo + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBeHistory FOUND for accountNo [ " + accountNo + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeHistoryByBranchCd/{branchCd}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getBankDetBeHistoryByBranchCd(@PathVariable String branchCd) {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            bankDetBeHistoryList = bankDetBeHistoryRepo.findByBranchCd(branchCd);

            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory FOUND for branchCd [ " + branchCd + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBeHistory FOUND for branchCd [ " + branchCd + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeHistoryByLuAccountTypeCd/{luAccountTypeCd}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getBankDetBeHistoryByLuAccountTypeCd(@PathVariable String luAccountTypeCd) {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            bankDetBeHistoryList = bankDetBeHistoryRepo.findByLuAccountTypeCd(luAccountTypeCd);

            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory FOUND for luAccountTypeCd [ " + luAccountTypeCd + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBeHistory FOUND for luAccountTypeCd [ " + luAccountTypeCd + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeHistoryByName/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getBankDetBeHistoryByName(@PathVariable String name) {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            bankDetBeHistoryList = bankDetBeHistoryRepo.findByName(name);

            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory FOUND for name [ " + name + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBeHistory FOUND for name [ " + name + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeHistoryByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getBankDetBeHistoryByStatChgD(@PathVariable Date statChgD) {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            bankDetBeHistoryList = bankDetBeHistoryRepo.findByStatChgD(statChgD);

            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getBankDetBeHistoryByStatus(@PathVariable String status) {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            bankDetBeHistoryList = bankDetBeHistoryRepo.findByStatus(status);

            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory FOUND for status [ " + status + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBeHistory FOUND for status [ " + status + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBeHistory>> getBankDetBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<BankDetBeHistory> bankDetBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            bankDetBeHistoryList = bankDetBeHistoryRepo.findByRevinfo(revinfo);

            if (bankDetBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + bankDetBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeHistoryList,
                HttpStatus.OK);
    }



}