/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.InvoiceBe;
import co.za.ngwane.billing.repository.InvoiceBeRepo;
import co.za.ngwane.billing.repository.service.InvoiceBeRepoService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/invoiceController/")
public class InvoiceController {

    private static final Logger logger = Logger.getLogger(InvoiceController.class);

    @Autowired
    public InvoiceBeRepoService invoiceBeRepoService;
    private static final String APPLICATION_PDF = "application/pdf";

    @Autowired
    private InvoiceBeRepo invoiceBeRepo;
    
    
    @RequestMapping(value = "invoices/invoiceno/{invoiceNo}", method = RequestMethod.GET)
    public ResponseEntity<InvoiceBe> getInvoiveByNo(@PathVariable String invoiceNo) {
        InvoiceBe invoiceBe = null;
        try {
            invoiceBe = invoiceBeRepoService.getByInvNo(invoiceNo);
            if (invoiceBe == null) {
                logger.info(
                        "===============================================================\n"
                        + "NO INVOICES returned [" + "] INVOICES.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + "Customers ORIGNATING LINES  FOUND  ["  + "] ORIGINATING LINES.\n"
                        + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(invoiceBe,
                HttpStatus.OK);
    }

    @RequestMapping(value = "invoices/{customerId}", method = RequestMethod.GET)
    public ResponseEntity<List<InvoiceBe>> getByCustomer(@PathVariable int customerId) {
        List<InvoiceBe> invoiceBes = new ArrayList<>();
        try {
            invoiceBes = invoiceBeRepoService.findByCustomersId(Long.valueOf(customerId));
            if (invoiceBes.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                        + "NO INVOICES returned [" + invoiceBes.size() + "] INVOICES.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + "Customers ORIGNATING LINES  FOUND  [" + invoiceBes.size() + "] ORIGINATING LINES.\n"
                        + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(invoiceBes,
                HttpStatus.OK);
    }

    @RequestMapping(value = "invoices", method = RequestMethod.GET)
    public ResponseEntity<List<InvoiceBe>> getAll() {
        List<InvoiceBe> invoiceBes = new ArrayList<>();
        try {
            invoiceBes = invoiceBeRepoService.getAll();
            if (invoiceBes.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                        + "NO INVOICES  returned [" + invoiceBes.size() + "] INVOICES.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + " INVOICES  FOUND  [" + invoiceBes.size() + "] INVOICES.\n"
                        + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(invoiceBes,
                HttpStatus.OK);
    }

    //############## this is the working service ##################
    @RequestMapping(value = "invoices/download/{id}", method = RequestMethod.GET, produces = APPLICATION_PDF)
    public @ResponseBody
    void downloadD(HttpServletRequest request, HttpServletResponse response, 
            @PathVariable("id") String id) throws IOException {

        final ServletContext servletContext = request.getSession().getServletContext();
        final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        final String temperotyFilePath = tempDirectory.getAbsolutePath();

        byte[] contents = null;

        logger.info(
                "===============================================================\n "
                + "The fileref is  : [" + id + "]"
                + "\n=============================================================== ");

        contents = invoiceBeRepoService.downloadInvoice(new Long(id));

        String fileName = "INV" + "_" + new Date();

        File fileToWriteTo = new File(temperotyFilePath + "\\" + fileName);

        FileUtils.writeByteArrayToFile(fileToWriteTo, contents);

        InputStream in = new FileInputStream(fileToWriteTo);

        response.setContentType(APPLICATION_PDF);
        response.setHeader("Content-Disposition", "attachment; filename=" + fileToWriteTo.getName());
        response.setHeader("Content-Length", String.valueOf(fileToWriteTo.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }

    @RequestMapping(value = "invoices/invoicesByCdrRefNo/{cdrRefNo}", method = RequestMethod.GET)
    public ResponseEntity<List<InvoiceBe>> getInvoicesByCdrRefNo(@PathVariable String cdrRefNo) {
        List<InvoiceBe> invoicesList = new ArrayList<>();
        try {
            invoicesList = invoiceBeRepo.findByCdrFileRefNo(cdrRefNo);
            if (invoicesList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO INVOICES returned for CDR Ref No [" +cdrRefNo+ "] " +
                                "query returned ["+invoicesList.size()+"] INVOICES.\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + "INVOICES FOUND for CDR Ref No [" +cdrRefNo+ "] " +
                                "query returned ["+invoicesList.size()+"] INVOICES.\n"
                                + "\n===============================================================");
//                BigDecimal totInvAmout = BigDecimal.ZERO;
//                for(InvoiceBe invoiceBe : invoicesList){
//                    totInvAmout = totInvAmout.add(invoiceBe.getInvAmout()) ;
//                }
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(invoicesList,
                HttpStatus.OK);
    }

    
}
