/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.LuBillableItemsBe;
import co.za.ngwane.billing.db.LuIdTypeCdBe;
import co.za.ngwane.billing.db.LuRolesBe;
import co.za.ngwane.billing.repository.LookUpDaoRepo;
import co.za.ngwane.billing.repository.LuBillableItemsRepo;
import co.za.ngwane.billing.repository.LuIdTypeCdRepo;
import co.za.ngwane.billing.repository.LuRolesBeRepo;
import co.za.ngwane.billing.service.BaseLookUpService;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/lookUpMixMasalaController/")
public class LookUpMixMasalaController {

    private static final Logger logger = Logger.getLogger(LookUpMixMasalaController.class);

    @Autowired
    public LuIdTypeCdRepo luIdTypeCdRepo;

    @Autowired
    public LuRolesBeRepo luRolesBeRepo;

    @Autowired
    public LookUpDaoRepo lookUpDAO;

    @Autowired
    private LuBillableItemsRepo luBillableItemsRepo;

    @RequestMapping(value = "/lookUp/{lookup}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<BaseLookUpService>> getLookup(@PathVariable("lookup") String lookup) {

        return new ResponseEntity<List<BaseLookUpService>>(lookUpDAO.getLookup(lookup), new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "billableItems", method = RequestMethod.GET)
    public ResponseEntity<List<LuBillableItemsBe>> getBillItemsType() {
        List<LuBillableItemsBe> luBillableItemsList = new ArrayList<>();
        try {
            luBillableItemsList = luBillableItemsRepo.findAll();
            if (luBillableItemsList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO Billable Item FOUND Query returned [" + luBillableItemsList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + "Billable Items FOUND Query returned [" + luBillableItemsList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luBillableItemsList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "roles", method = RequestMethod.GET)
    public ResponseEntity<List<LuRolesBe>> getAllRoles() {
        List<LuRolesBe> rolesBeList = new ArrayList<>();
        try {
            rolesBeList = luRolesBeRepo.findAll();
            if (rolesBeList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO Roles FOUND Query returned [" + rolesBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + "Roles FOUND Query returned [" + rolesBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(rolesBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "idTypes", method = RequestMethod.GET)
    public ResponseEntity<List<LuIdTypeCdBe>> getAllIdTypes() {
        List<LuIdTypeCdBe> idTypeCdList = new ArrayList<>();
        try {
            idTypeCdList = luIdTypeCdRepo.findAll();
            if (idTypeCdList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO ID Types Codes FOUND Query returned [" + idTypeCdList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + "ID Types Codes FOUND Query returned [" + idTypeCdList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(idTypeCdList,
                HttpStatus.OK);
    }
}
