/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.IcasaRatesBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.IcasaRatesBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/icasaRatesBeHistory")
public class IcasaRatesBeHistoryController {
private static final Logger logger = Logger.getLogger(IcasaRatesBeHistoryController.class);

@Autowired
private IcasaRatesBeHistoryRepo icasaRatesBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<IcasaRatesBeHistory> save(
            @RequestBody IcasaRatesBeHistory icasaRatesBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            icasaRatesBeHistory = icasaRatesBeHistoryRepo.save(icasaRatesBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<IcasaRatesBeHistory> update(
            @RequestBody IcasaRatesBeHistory icasaRatesBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            icasaRatesBeHistory = icasaRatesBeHistoryRepo.save(icasaRatesBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<IcasaRatesBeHistory> delete(
            @RequestBody IcasaRatesBeHistory icasaRatesBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            icasaRatesBeHistory = icasaRatesBeHistoryRepo.save(icasaRatesBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "icasaRatesBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<IcasaRatesBeHistory>> getAllIcasaRatesBeHistory() {
        List<IcasaRatesBeHistory> icasaRatesBeHistoryList = new ArrayList<>();
        try {
            icasaRatesBeHistoryList = icasaRatesBeHistoryRepo.findAll();
            if (icasaRatesBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO IcasaRatesBeHistory List FOUND Query returned [" + icasaRatesBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "IcasaRatesBeHistory List FOUND Query returned [" + icasaRatesBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "icasaRatesBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<IcasaRatesBeHistory>> getIcasaRatesBeHistoryByRevtype(@PathVariable Short revtype) {
        List<IcasaRatesBeHistory> icasaRatesBeHistoryList = new ArrayList<>();
        try {
            icasaRatesBeHistoryList = icasaRatesBeHistoryRepo.findByRevtype(revtype);

            if (icasaRatesBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO IcasaRatesBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "IcasaRatesBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "icasaRatesBeHistoryByBillRates/{billRates}", method = RequestMethod.GET)
    public ResponseEntity<List<IcasaRatesBeHistory>> getIcasaRatesBeHistoryByBillRates(@PathVariable Float billRates) {
        List<IcasaRatesBeHistory> icasaRatesBeHistoryList = new ArrayList<>();
        try {
            icasaRatesBeHistoryList = icasaRatesBeHistoryRepo.findByBillRates(billRates);

            if (icasaRatesBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO IcasaRatesBeHistory FOUND for billRates [ " + billRates + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "IcasaRatesBeHistory FOUND for billRates [ " + billRates + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "icasaRatesBeHistoryByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<IcasaRatesBeHistory>> getIcasaRatesBeHistoryByDescription(@PathVariable String description) {
        List<IcasaRatesBeHistory> icasaRatesBeHistoryList = new ArrayList<>();
        try {
            icasaRatesBeHistoryList = icasaRatesBeHistoryRepo.findByDescription(description);

            if (icasaRatesBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO IcasaRatesBeHistory FOUND for description [ " + description + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "IcasaRatesBeHistory FOUND for description [ " + description + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "icasaRatesBeHistoryByNeoRates/{neoRates}", method = RequestMethod.GET)
    public ResponseEntity<List<IcasaRatesBeHistory>> getIcasaRatesBeHistoryByNeoRates(@PathVariable Float neoRates) {
        List<IcasaRatesBeHistory> icasaRatesBeHistoryList = new ArrayList<>();
        try {
            icasaRatesBeHistoryList = icasaRatesBeHistoryRepo.findByNeoRates(neoRates);

            if (icasaRatesBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO IcasaRatesBeHistory FOUND for neoRates [ " + neoRates + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "IcasaRatesBeHistory FOUND for neoRates [ " + neoRates + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "icasaRatesBeHistoryByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<IcasaRatesBeHistory>> getIcasaRatesBeHistoryByStatChgD(@PathVariable Date statChgD) {
        List<IcasaRatesBeHistory> icasaRatesBeHistoryList = new ArrayList<>();
        try {
            icasaRatesBeHistoryList = icasaRatesBeHistoryRepo.findByStatChgD(statChgD);

            if (icasaRatesBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO IcasaRatesBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "IcasaRatesBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "icasaRatesBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<IcasaRatesBeHistory>> getIcasaRatesBeHistoryByStatus(@PathVariable String status) {
        List<IcasaRatesBeHistory> icasaRatesBeHistoryList = new ArrayList<>();
        try {
            icasaRatesBeHistoryList = icasaRatesBeHistoryRepo.findByStatus(status);

            if (icasaRatesBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO IcasaRatesBeHistory FOUND for status [ " + status + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "IcasaRatesBeHistory FOUND for status [ " + status + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "icasaRatesBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<IcasaRatesBeHistory>> getIcasaRatesBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<IcasaRatesBeHistory> icasaRatesBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            icasaRatesBeHistoryList = icasaRatesBeHistoryRepo.findByRevinfo(revinfo);

            if (icasaRatesBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO IcasaRatesBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "IcasaRatesBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + icasaRatesBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(icasaRatesBeHistoryList,
                HttpStatus.OK);
    }



}