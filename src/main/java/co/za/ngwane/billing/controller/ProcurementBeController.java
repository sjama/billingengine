/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.OrderFormBe;
import co.za.ngwane.billing.db.ProcurementBe;
import co.za.ngwane.billing.repository.OrderFormBeRepo;
import co.za.ngwane.billing.repository.ProcurementBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/procurementBe")
public class ProcurementBeController {
private static final Logger logger = Logger.getLogger(ProcurementBeController.class);

@Autowired
private ProcurementBeRepo procurementBeRepo;
@Autowired
private OrderFormBeRepo orderFormBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ProcurementBe> save(
            @RequestBody ProcurementBe procurementBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementBe = procurementBeRepo.save(procurementBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ProcurementBe> update(
            @RequestBody ProcurementBe procurementBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementBe = procurementBeRepo.save(procurementBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ProcurementBe> delete(
            @RequestBody ProcurementBe procurementBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementBe = procurementBeRepo.save(procurementBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "procurementBe", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getAllProcurementBe() {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findAll();
            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe List FOUND Query returned [" + procurementBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ProcurementBe List FOUND Query returned [" + procurementBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByCreditAvail/{creditAvail}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByCreditAvail(@PathVariable Float creditAvail) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByCreditAvail(creditAvail);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for creditAvail [ " + creditAvail + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for creditAvail [ " + creditAvail + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByCreditLimit/{creditLimit}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByCreditLimit(@PathVariable Float creditLimit) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByCreditLimit(creditLimit);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for creditLimit [ " + creditLimit + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for creditLimit [ " + creditLimit + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByCodSupplier/{codSupplier}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByCodSupplier(@PathVariable String codSupplier) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByCodSupplier(codSupplier);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for codSupplier [ " + codSupplier + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for codSupplier [ " + codSupplier + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByBudget/{budget}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByBudget(@PathVariable Float budget) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByBudget(budget);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for budget [ " + budget + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for budget [ " + budget + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByExpectedTurnAroundTime/{expectedTurnAroundTime}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByExpectedTurnAroundTime(@PathVariable String expectedTurnAroundTime) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByExpectedTurnAroundTime(expectedTurnAroundTime);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for expectedTurnAroundTime [ " + expectedTurnAroundTime + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for expectedTurnAroundTime [ " + expectedTurnAroundTime + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByRefNo(@PathVariable String refNo) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByRefNo(refNo);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for refNo [ " + refNo + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for refNo [ " + refNo + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByStatus(@PathVariable String status) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByStatus(status);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for status [ " + status + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for status [ " + status + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByStatChgDate(@PathVariable Date statChgDate) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByStatChgDate(statChgDate);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByCreatedDate(@PathVariable Date createdDate) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByCreatedDate(createdDate);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for createdDate [ " + createdDate + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for createdDate [ " + createdDate + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByExpectedDeliveryDate/{expectedDeliveryDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByExpectedDeliveryDate(@PathVariable Date expectedDeliveryDate) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByExpectedDeliveryDate(expectedDeliveryDate);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for expectedDeliveryDate [ " + expectedDeliveryDate + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for expectedDeliveryDate [ " + expectedDeliveryDate + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByActualDeliveryDate/{actualDeliveryDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByActualDeliveryDate(@PathVariable Integer actualDeliveryDate) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            procurementBeList = procurementBeRepo.findByActualDeliveryDate(actualDeliveryDate);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for actualDeliveryDate [ " + actualDeliveryDate + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for actualDeliveryDate [ " + actualDeliveryDate + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeByOrderId/{orderId}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBe>> getProcurementBeByOrderId(@PathVariable String orderId) {
        List<ProcurementBe> procurementBeList = new ArrayList<>();
        try {
            OrderFormBe orderFormBe = orderFormBeRepo.findOne(new Long(orderId));
            procurementBeList = procurementBeRepo.findByOrderId(orderFormBe);

            if (procurementBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBe FOUND for orderId [ " + orderId + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBe FOUND for orderId [ " + orderId + " ] query returned [" + procurementBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeList,
                HttpStatus.OK);
    }



}