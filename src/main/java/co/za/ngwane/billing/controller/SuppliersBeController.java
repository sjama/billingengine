/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.SuppliersBe;
import co.za.ngwane.billing.repository.SuppliersBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/suppliersBe")
public class SuppliersBeController {
private static final Logger logger = Logger.getLogger(SuppliersBeController.class);

@Autowired
private SuppliersBeRepo suppliersBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<SuppliersBe> save(
            @RequestBody SuppliersBe suppliersBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            suppliersBe = suppliersBeRepo.save(suppliersBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<SuppliersBe> update(
            @RequestBody SuppliersBe suppliersBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            suppliersBe = suppliersBeRepo.save(suppliersBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<SuppliersBe> delete(
            @RequestBody SuppliersBe suppliersBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            suppliersBe = suppliersBeRepo.save(suppliersBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "suppliersBe", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getAllSuppliersBe() {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findAll();
            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe List FOUND Query returned [" + suppliersBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "SuppliersBe List FOUND Query returned [" + suppliersBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByComName/{comName}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByComName(@PathVariable String comName) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByComName(comName);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for comName [ " + comName + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for comName [ " + comName + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByTradeName/{tradeName}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByTradeName(@PathVariable String tradeName) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByTradeName(tradeName);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for tradeName [ " + tradeName + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for tradeName [ " + tradeName + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeBySupplierNo/{supplierNo}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeBySupplierNo(@PathVariable String supplierNo) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findBySupplierNo(supplierNo);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for supplierNo [ " + supplierNo + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for supplierNo [ " + supplierNo + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByRegNo/{regNo}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByRegNo(@PathVariable String regNo) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByRegNo(regNo);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for regNo [ " + regNo + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for regNo [ " + regNo + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByVatRegNo/{vatRegNo}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByVatRegNo(@PathVariable String vatRegNo) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByVatRegNo(vatRegNo);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for vatRegNo [ " + vatRegNo + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for vatRegNo [ " + vatRegNo + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByRefNo(@PathVariable String refNo) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByRefNo(refNo);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for refNo [ " + refNo + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for refNo [ " + refNo + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByBankDetId/{bankDetId}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByBankDetId(@PathVariable int bankDetId) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByBankDetId(bankDetId);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for bankDetId [ " + bankDetId + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for bankDetId [ " + bankDetId + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByAddressId/{addressId}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByAddressId(@PathVariable long addressId) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByAddressId(addressId);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for addressId [ " + addressId + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for addressId [ " + addressId + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByContactPersonId/{contactPersonId}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByContactPersonId(@PathVariable long contactPersonId) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByContactPersonId(contactPersonId);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for contactPersonId [ " + contactPersonId + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for contactPersonId [ " + contactPersonId + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByStatus(@PathVariable String status) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByStatus(status);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for status [ " + status + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for status [ " + status + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBe>> getSuppliersBeByStatChgD(@PathVariable Date statChgD) {
        List<SuppliersBe> suppliersBeList = new ArrayList<>();
        try {
            suppliersBeList = suppliersBeRepo.findByStatChgD(statChgD);

            if (suppliersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBe FOUND for statChgD [ " + statChgD + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBe FOUND for statChgD [ " + statChgD + " ] query returned [" + suppliersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeList,
                HttpStatus.OK);
    }



}