/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ChosenSupplierBe;
import co.za.ngwane.billing.db.ProcurementBe;
import co.za.ngwane.billing.db.ProcurementSupplierBe;
import co.za.ngwane.billing.db.UsersBe;
import co.za.ngwane.billing.repository.ChosenSupplierBeRepo;
import co.za.ngwane.billing.repository.ProcurementBeRepo;
import co.za.ngwane.billing.repository.ProcurementSupplierBeRepo;
import co.za.ngwane.billing.repository.UserBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/chosenSupplierBe")
public class ChosenSupplierBeController {
private static final Logger logger = Logger.getLogger(ChosenSupplierBeController.class);

@Autowired
private ChosenSupplierBeRepo chosenSupplierBeRepo;
@Autowired
private ProcurementBeRepo procurementBeRepo;
@Autowired
private ProcurementSupplierBeRepo procurementSupplierBeRepo;
@Autowired
private UserBeRepo usersBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ChosenSupplierBe> save(
            @RequestBody ChosenSupplierBe chosenSupplierBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            chosenSupplierBe = chosenSupplierBeRepo.save(chosenSupplierBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ChosenSupplierBe> update(
            @RequestBody ChosenSupplierBe chosenSupplierBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            chosenSupplierBe = chosenSupplierBeRepo.save(chosenSupplierBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ChosenSupplierBe> delete(
            @RequestBody ChosenSupplierBe chosenSupplierBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            chosenSupplierBe = chosenSupplierBeRepo.save(chosenSupplierBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "chosenSupplierBe", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBe>> getAllChosenSupplierBe() {
        List<ChosenSupplierBe> chosenSupplierBeList = new ArrayList<>();
        try {
            chosenSupplierBeList = chosenSupplierBeRepo.findAll();
            if (chosenSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBe List FOUND Query returned [" + chosenSupplierBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ChosenSupplierBe List FOUND Query returned [" + chosenSupplierBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBe>> getChosenSupplierBeByStatus(@PathVariable String status) {
        List<ChosenSupplierBe> chosenSupplierBeList = new ArrayList<>();
        try {
            chosenSupplierBeList = chosenSupplierBeRepo.findByStatus(status);

            if (chosenSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBe FOUND for status [ " + status + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBe FOUND for status [ " + status + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBe>> getChosenSupplierBeByStatChgDate(@PathVariable Date statChgDate) {
        List<ChosenSupplierBe> chosenSupplierBeList = new ArrayList<>();
        try {
            chosenSupplierBeList = chosenSupplierBeRepo.findByStatChgDate(statChgDate);

            if (chosenSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeByApprovedDate/{approvedDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBe>> getChosenSupplierBeByApprovedDate(@PathVariable Date approvedDate) {
        List<ChosenSupplierBe> chosenSupplierBeList = new ArrayList<>();
        try {
            chosenSupplierBeList = chosenSupplierBeRepo.findByApprovedDate(approvedDate);

            if (chosenSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBe FOUND for approvedDate [ " + approvedDate + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBe FOUND for approvedDate [ " + approvedDate + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBe>> getChosenSupplierBeByRefNo(@PathVariable String refNo) {
        List<ChosenSupplierBe> chosenSupplierBeList = new ArrayList<>();
        try {
            chosenSupplierBeList = chosenSupplierBeRepo.findByRefNo(refNo);

            if (chosenSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBe FOUND for refNo [ " + refNo + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBe FOUND for refNo [ " + refNo + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeByProcurementId/{procurementId}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBe>> getChosenSupplierBeByProcurementId(@PathVariable String procurementId) {
        List<ChosenSupplierBe> chosenSupplierBeList = new ArrayList<>();
        try {
            ProcurementBe procurementBe = procurementBeRepo.findOne(new Long(procurementId));
            chosenSupplierBeList = chosenSupplierBeRepo.findByProcurementId(procurementBe);

            if (chosenSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBe FOUND for procurementId [ " + procurementId + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBe FOUND for procurementId [ " + procurementId + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeByProcurementSupplierId/{procurementSupplierId}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBe>> getChosenSupplierBeByProcurementSupplierId(@PathVariable String procurementSupplierId) {
        List<ChosenSupplierBe> chosenSupplierBeList = new ArrayList<>();
        try {
            ProcurementSupplierBe procurementSupplierBe = procurementSupplierBeRepo.findOne(new Long(procurementSupplierId));
            chosenSupplierBeList = chosenSupplierBeRepo.findByProcurementSupplierId(procurementSupplierBe);

            if (chosenSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBe FOUND for procurementSupplierId [ " + procurementSupplierId + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBe FOUND for procurementSupplierId [ " + procurementSupplierId + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeByApprovedByUserId/{approvedByUserId}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBe>> getChosenSupplierBeByApprovedByUserId(@PathVariable String approvedByUserId) {
        List<ChosenSupplierBe> chosenSupplierBeList = new ArrayList<>();
        try {
            UsersBe usersBe = usersBeRepo.findOne(new Integer(approvedByUserId));
            chosenSupplierBeList = chosenSupplierBeRepo.findByApprovedByUserId(usersBe);

            if (chosenSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBe FOUND for approvedByUserId [ " + approvedByUserId + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBe FOUND for approvedByUserId [ " + approvedByUserId + " ] query returned [" + chosenSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeList,
                HttpStatus.OK);
    }



}