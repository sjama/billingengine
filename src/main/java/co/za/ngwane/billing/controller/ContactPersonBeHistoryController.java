/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ContactPersonBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.ContactPersonBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/contactPersonBeHistory")
public class ContactPersonBeHistoryController {
private static final Logger logger = Logger.getLogger(ContactPersonBeHistoryController.class);

@Autowired
private ContactPersonBeHistoryRepo contactPersonBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ContactPersonBeHistory> save(
            @RequestBody ContactPersonBeHistory contactPersonBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            contactPersonBeHistory = contactPersonBeHistoryRepo.save(contactPersonBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ContactPersonBeHistory> update(
            @RequestBody ContactPersonBeHistory contactPersonBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            contactPersonBeHistory = contactPersonBeHistoryRepo.save(contactPersonBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ContactPersonBeHistory> delete(
            @RequestBody ContactPersonBeHistory contactPersonBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            contactPersonBeHistory = contactPersonBeHistoryRepo.save(contactPersonBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "contactPersonBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getAllContactPersonBeHistory() {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findAll();
            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory List FOUND Query returned [" + contactPersonBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ContactPersonBeHistory List FOUND Query returned [" + contactPersonBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getContactPersonBeHistoryByRevtype(@PathVariable Short revtype) {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findByRevtype(revtype);

            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeHistoryByCellNo/{cellNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getContactPersonBeHistoryByCellNo(@PathVariable String cellNo) {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findByCellNo(cellNo);

            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory FOUND for cellNo [ " + cellNo + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBeHistory FOUND for cellNo [ " + cellNo + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeHistoryByEmail/{email}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getContactPersonBeHistoryByEmail(@PathVariable String email) {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findByEmail(email);

            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory FOUND for email [ " + email + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBeHistory FOUND for email [ " + email + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeHistoryByName/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getContactPersonBeHistoryByName(@PathVariable String name) {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findByName(name);

            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory FOUND for name [ " + name + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBeHistory FOUND for name [ " + name + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeHistoryByPhoneNo/{phoneNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getContactPersonBeHistoryByPhoneNo(@PathVariable String phoneNo) {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findByPhoneNo(phoneNo);

            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory FOUND for phoneNo [ " + phoneNo + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBeHistory FOUND for phoneNo [ " + phoneNo + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeHistoryByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getContactPersonBeHistoryByStatChgDate(@PathVariable Date statChgDate) {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findByStatChgDate(statChgDate);

            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getContactPersonBeHistoryByStatus(@PathVariable String status) {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findByStatus(status);

            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory FOUND for status [ " + status + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBeHistory FOUND for status [ " + status + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeHistoryBySurname/{surname}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getContactPersonBeHistoryBySurname(@PathVariable String surname) {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findBySurname(surname);

            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory FOUND for surname [ " + surname + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBeHistory FOUND for surname [ " + surname + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "contactPersonBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<ContactPersonBeHistory>> getContactPersonBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<ContactPersonBeHistory> contactPersonBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            contactPersonBeHistoryList = contactPersonBeHistoryRepo.findByRevinfo(revinfo);

            if (contactPersonBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ContactPersonBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ContactPersonBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + contactPersonBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(contactPersonBeHistoryList,
                HttpStatus.OK);
    }



}