/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ForwardToDeptUsersBe;
import co.za.ngwane.billing.db.LuForwardToDeptBe;
import co.za.ngwane.billing.db.UsersBe;
import co.za.ngwane.billing.repository.ForwardToDeptUsersBeRepo;
import co.za.ngwane.billing.repository.LuForwardToDeptBeRepo;
import co.za.ngwane.billing.repository.UserBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/forwardToDeptUsersBe")
public class ForwardToDeptUsersBeController {
private static final Logger logger = Logger.getLogger(ForwardToDeptUsersBeController.class);

@Autowired
private ForwardToDeptUsersBeRepo forwardToDeptUsersBeRepo;
@Autowired
private LuForwardToDeptBeRepo luForwardToDeptBeRepo;
@Autowired
private UserBeRepo usersBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ForwardToDeptUsersBe> save(
            @RequestBody ForwardToDeptUsersBe forwardToDeptUsersBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            forwardToDeptUsersBe = forwardToDeptUsersBeRepo.save(forwardToDeptUsersBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ForwardToDeptUsersBe> update(
            @RequestBody ForwardToDeptUsersBe forwardToDeptUsersBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            forwardToDeptUsersBe = forwardToDeptUsersBeRepo.save(forwardToDeptUsersBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ForwardToDeptUsersBe> delete(
            @RequestBody ForwardToDeptUsersBe forwardToDeptUsersBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            forwardToDeptUsersBe = forwardToDeptUsersBeRepo.save(forwardToDeptUsersBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "forwardToDeptUsersBe", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBe>> getAllForwardToDeptUsersBe() {
        List<ForwardToDeptUsersBe> forwardToDeptUsersBeList = new ArrayList<>();
        try {
            forwardToDeptUsersBeList = forwardToDeptUsersBeRepo.findAll();
            if (forwardToDeptUsersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBe List FOUND Query returned [" + forwardToDeptUsersBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ForwardToDeptUsersBe List FOUND Query returned [" + forwardToDeptUsersBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBe>> getForwardToDeptUsersBeByStatus(@PathVariable String status) {
        List<ForwardToDeptUsersBe> forwardToDeptUsersBeList = new ArrayList<>();
        try {
            forwardToDeptUsersBeList = forwardToDeptUsersBeRepo.findByStatus(status);

            if (forwardToDeptUsersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBe FOUND for status [ " + status + " ] query returned [" + forwardToDeptUsersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBe FOUND for status [ " + status + " ] query returned [" + forwardToDeptUsersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBe>> getForwardToDeptUsersBeByStatChgDate(@PathVariable Date statChgDate) {
        List<ForwardToDeptUsersBe> forwardToDeptUsersBeList = new ArrayList<>();
        try {
            forwardToDeptUsersBeList = forwardToDeptUsersBeRepo.findByStatChgDate(statChgDate);

            if (forwardToDeptUsersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + forwardToDeptUsersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + forwardToDeptUsersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeByLuForwardToDeptCd/{luForwardToDeptCd}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBe>> getForwardToDeptUsersBeByLuForwardToDeptCd(@PathVariable String luForwardToDeptCd) {
        List<ForwardToDeptUsersBe> forwardToDeptUsersBeList = new ArrayList<>();
        try {
            LuForwardToDeptBe luForwardToDeptBe = luForwardToDeptBeRepo.findOne(new String(luForwardToDeptCd));
            forwardToDeptUsersBeList = forwardToDeptUsersBeRepo.findByLuForwardToDeptCd(luForwardToDeptBe);

            if (forwardToDeptUsersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBe FOUND for luForwardToDeptCd [ " + luForwardToDeptCd + " ] query returned [" + forwardToDeptUsersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBe FOUND for luForwardToDeptCd [ " + luForwardToDeptCd + " ] query returned [" + forwardToDeptUsersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "forwardToDeptUsersBeByUsersId/{usersId}", method = RequestMethod.GET)
    public ResponseEntity<List<ForwardToDeptUsersBe>> getForwardToDeptUsersBeByUsersId(@PathVariable String usersId) {
        List<ForwardToDeptUsersBe> forwardToDeptUsersBeList = new ArrayList<>();
        try {
            UsersBe usersBe = usersBeRepo.findOne(new Integer(usersId));
            forwardToDeptUsersBeList = forwardToDeptUsersBeRepo.findByUsersId(usersBe);

            if (forwardToDeptUsersBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ForwardToDeptUsersBe FOUND for usersId [ " + usersId + " ] query returned [" + forwardToDeptUsersBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ForwardToDeptUsersBe FOUND for usersId [ " + usersId + " ] query returned [" + forwardToDeptUsersBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(forwardToDeptUsersBeList,
                HttpStatus.OK);
    }



}