/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

import co.za.ngwane.billing.db.LuLastNoTypeBe;
import co.za.ngwane.billing.repository.LuLastNoTypeBeRepo;

/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/luLastNoTypeBe")
public class LuLastNoTypeBeController {
private static final Logger logger = Logger.getLogger(LuLastNoTypeBeController.class);

@Autowired
private LuLastNoTypeBeRepo luLastNoTypeBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<LuLastNoTypeBe> save(
            @RequestBody LuLastNoTypeBe luLastNoTypeBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luLastNoTypeBe = luLastNoTypeBeRepo.save(luLastNoTypeBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luLastNoTypeBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<LuLastNoTypeBe> update(
            @RequestBody LuLastNoTypeBe luLastNoTypeBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luLastNoTypeBe = luLastNoTypeBeRepo.save(luLastNoTypeBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luLastNoTypeBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<LuLastNoTypeBe> delete(
            @RequestBody LuLastNoTypeBe luLastNoTypeBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luLastNoTypeBe = luLastNoTypeBeRepo.save(luLastNoTypeBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luLastNoTypeBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "luLastNoTypeBe", method = RequestMethod.GET)
    public ResponseEntity<List<LuLastNoTypeBe>> getAllLuLastNoTypeBe() {
        List<LuLastNoTypeBe> luLastNoTypeBeList = new ArrayList<>();
        try {
            luLastNoTypeBeList = luLastNoTypeBeRepo.findAll();
            if (luLastNoTypeBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuLastNoTypeBe List FOUND Query returned [" + luLastNoTypeBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "LuLastNoTypeBe List FOUND Query returned [" + luLastNoTypeBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luLastNoTypeBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "luLastNoTypeBeByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<LuLastNoTypeBe>> getLuLastNoTypeBeByDescription(@PathVariable String description) {
        List<LuLastNoTypeBe> luLastNoTypeBeList = new ArrayList<>();
        try {
            luLastNoTypeBeList = luLastNoTypeBeRepo.findByDescription(description);

            if (luLastNoTypeBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuLastNoTypeBe FOUND for description [ " + description + " ] query returned [" + luLastNoTypeBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LuLastNoTypeBe FOUND for description [ " + description + " ] query returned [" + luLastNoTypeBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luLastNoTypeBeList,
                HttpStatus.OK);
    }



}