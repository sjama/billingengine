/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.OrderFormBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.OrderFormBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/orderFormBeHistory")
public class OrderFormBeHistoryController {
private static final Logger logger = Logger.getLogger(OrderFormBeHistoryController.class);

@Autowired
private OrderFormBeHistoryRepo orderFormBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<OrderFormBeHistory> save(
            @RequestBody OrderFormBeHistory orderFormBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            orderFormBeHistory = orderFormBeHistoryRepo.save(orderFormBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<OrderFormBeHistory> update(
            @RequestBody OrderFormBeHistory orderFormBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            orderFormBeHistory = orderFormBeHistoryRepo.save(orderFormBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<OrderFormBeHistory> delete(
            @RequestBody OrderFormBeHistory orderFormBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            orderFormBeHistory = orderFormBeHistoryRepo.save(orderFormBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "orderFormBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getAllOrderFormBeHistory() {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findAll();
            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory List FOUND Query returned [" + orderFormBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "OrderFormBeHistory List FOUND Query returned [" + orderFormBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByRevtype(@PathVariable Short revtype) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByRevtype(revtype);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByContactPersonId/{contactPersonId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByContactPersonId(@PathVariable BigInteger contactPersonId) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByContactPersonId(contactPersonId);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for contactPersonId [ " + contactPersonId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for contactPersonId [ " + contactPersonId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByCreatedDate(@PathVariable Date createdDate) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByCreatedDate(createdDate);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByDeliveryDate/{deliveryDate}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByDeliveryDate(@PathVariable Date deliveryDate) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByDeliveryDate(deliveryDate);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for deliveryDate [ " + deliveryDate + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for deliveryDate [ " + deliveryDate + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByNoDaysLeft/{noDaysLeft}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByNoDaysLeft(@PathVariable Integer noDaysLeft) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByNoDaysLeft(noDaysLeft);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for noDaysLeft [ " + noDaysLeft + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for noDaysLeft [ " + noDaysLeft + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByOrderDesc/{orderDesc}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByOrderDesc(@PathVariable String orderDesc) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByOrderDesc(orderDesc);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for orderDesc [ " + orderDesc + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for orderDesc [ " + orderDesc + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByOrderNo/{orderNo}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByOrderNo(@PathVariable String orderNo) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByOrderNo(orderNo);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for orderNo [ " + orderNo + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for orderNo [ " + orderNo + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByProjId/{projId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByProjId(@PathVariable Integer projId) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByProjId(projId);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for projId [ " + projId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for projId [ " + projId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByProjName/{projName}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByProjName(@PathVariable String projName) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByProjName(projName);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for projName [ " + projName + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for projName [ " + projName + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByProjNo/{projNo}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByProjNo(@PathVariable String projNo) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByProjNo(projNo);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for projNo [ " + projNo + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for projNo [ " + projNo + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByRefNo(@PathVariable String refNo) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByRefNo(refNo);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByRequestDate/{requestDate}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByRequestDate(@PathVariable Date requestDate) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByRequestDate(requestDate);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for requestDate [ " + requestDate + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for requestDate [ " + requestDate + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByStatChgDate(@PathVariable Date statChgDate) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByStatChgDate(statChgDate);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByStatus(@PathVariable String status) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByStatus(status);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for status [ " + status + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for status [ " + status + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByCapturedByUsersId/{capturedByUsersId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByCapturedByUsersId(@PathVariable Integer capturedByUsersId) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByCapturedByUsersId(capturedByUsersId);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for capturedByUsersId [ " + capturedByUsersId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for capturedByUsersId [ " + capturedByUsersId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByCustomersId/{customersId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByCustomersId(@PathVariable BigInteger customersId) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByCustomersId(customersId);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for customersId [ " + customersId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for customersId [ " + customersId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByLuReminderPeriodCd/{luReminderPeriodCd}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByLuReminderPeriodCd(@PathVariable String luReminderPeriodCd) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByLuReminderPeriodCd(luReminderPeriodCd);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for luReminderPeriodCd [ " + luReminderPeriodCd + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for luReminderPeriodCd [ " + luReminderPeriodCd + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByRequestedByUsersId/{requestedByUsersId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByRequestedByUsersId(@PathVariable Integer requestedByUsersId) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByRequestedByUsersId(requestedByUsersId);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for requestedByUsersId [ " + requestedByUsersId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for requestedByUsersId [ " + requestedByUsersId + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "orderFormBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBeHistory>> getOrderFormBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<OrderFormBeHistory> orderFormBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            orderFormBeHistoryList = orderFormBeHistoryRepo.findByRevinfo(revinfo);

            if (orderFormBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + orderFormBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeHistoryList,
                HttpStatus.OK);
    }



}