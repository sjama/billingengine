/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ProcurementSupplierBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.ProcurementSupplierBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/procurementSupplierBeHistory")
public class ProcurementSupplierBeHistoryController {
private static final Logger logger = Logger.getLogger(ProcurementSupplierBeHistoryController.class);

@Autowired
private ProcurementSupplierBeHistoryRepo procurementSupplierBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ProcurementSupplierBeHistory> save(
            @RequestBody ProcurementSupplierBeHistory procurementSupplierBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementSupplierBeHistory = procurementSupplierBeHistoryRepo.save(procurementSupplierBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ProcurementSupplierBeHistory> update(
            @RequestBody ProcurementSupplierBeHistory procurementSupplierBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementSupplierBeHistory = procurementSupplierBeHistoryRepo.save(procurementSupplierBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ProcurementSupplierBeHistory> delete(
            @RequestBody ProcurementSupplierBeHistory procurementSupplierBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementSupplierBeHistory = procurementSupplierBeHistoryRepo.save(procurementSupplierBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "procurementSupplierBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getAllProcurementSupplierBeHistory() {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findAll();
            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory List FOUND Query returned [" + procurementSupplierBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ProcurementSupplierBeHistory List FOUND Query returned [" + procurementSupplierBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryByRevtype(@PathVariable Short revtype) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findByRevtype(revtype);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryByCreatedDate(@PathVariable Date createdDate) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findByCreatedDate(createdDate);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryByNoOfDays/{noOfDays}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryByNoOfDays(@PathVariable Integer noOfDays) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findByNoOfDays(noOfDays);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for noOfDays [ " + noOfDays + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for noOfDays [ " + noOfDays + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryByRefNo(@PathVariable String refNo) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findByRefNo(refNo);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryByStatChgDate(@PathVariable Date statChgDate) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findByStatChgDate(statChgDate);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryByStatus(@PathVariable String status) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findByStatus(status);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for status [ " + status + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for status [ " + status + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryBySupplierAmount/{supplierAmount}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryBySupplierAmount(@PathVariable Float supplierAmount) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findBySupplierAmount(supplierAmount);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for supplierAmount [ " + supplierAmount + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for supplierAmount [ " + supplierAmount + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryByProcurementId/{procurementId}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryByProcurementId(@PathVariable BigInteger procurementId) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findByProcurementId(procurementId);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for procurementId [ " + procurementId + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for procurementId [ " + procurementId + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryBySuppliersId/{suppliersId}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryBySuppliersId(@PathVariable BigInteger suppliersId) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findBySuppliersId(suppliersId);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for suppliersId [ " + suppliersId + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for suppliersId [ " + suppliersId + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBeHistory>> getProcurementSupplierBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<ProcurementSupplierBeHistory> procurementSupplierBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            procurementSupplierBeHistoryList = procurementSupplierBeHistoryRepo.findByRevinfo(revinfo);

            if (procurementSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + procurementSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeHistoryList,
                HttpStatus.OK);
    }



}