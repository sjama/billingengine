/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.OriginatingLineBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.OriginatingLineBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/originatingLineBeHistory")
public class OriginatingLineBeHistoryController {
private static final Logger logger = Logger.getLogger(OriginatingLineBeHistoryController.class);

@Autowired
private OriginatingLineBeHistoryRepo originatingLineBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<OriginatingLineBeHistory> save(
            @RequestBody OriginatingLineBeHistory originatingLineBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            originatingLineBeHistory = originatingLineBeHistoryRepo.save(originatingLineBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(originatingLineBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<OriginatingLineBeHistory> update(
            @RequestBody OriginatingLineBeHistory originatingLineBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            originatingLineBeHistory = originatingLineBeHistoryRepo.save(originatingLineBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(originatingLineBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<OriginatingLineBeHistory> delete(
            @RequestBody OriginatingLineBeHistory originatingLineBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            originatingLineBeHistory = originatingLineBeHistoryRepo.save(originatingLineBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(originatingLineBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "originatingLineBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<OriginatingLineBeHistory>> getAllOriginatingLineBeHistory() {
        List<OriginatingLineBeHistory> originatingLineBeHistoryList = new ArrayList<>();
        try {
            originatingLineBeHistoryList = originatingLineBeHistoryRepo.findAll();
            if (originatingLineBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OriginatingLineBeHistory List FOUND Query returned [" + originatingLineBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "OriginatingLineBeHistory List FOUND Query returned [" + originatingLineBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(originatingLineBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "originatingLineBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<OriginatingLineBeHistory>> getOriginatingLineBeHistoryByRevtype(@PathVariable Short revtype) {
        List<OriginatingLineBeHistory> originatingLineBeHistoryList = new ArrayList<>();
        try {
            originatingLineBeHistoryList = originatingLineBeHistoryRepo.findByRevtype(revtype);

            if (originatingLineBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OriginatingLineBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OriginatingLineBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(originatingLineBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "originatingLineBeHistoryByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<OriginatingLineBeHistory>> getOriginatingLineBeHistoryByStatChgD(@PathVariable Date statChgD) {
        List<OriginatingLineBeHistory> originatingLineBeHistoryList = new ArrayList<>();
        try {
            originatingLineBeHistoryList = originatingLineBeHistoryRepo.findByStatChgD(statChgD);

            if (originatingLineBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OriginatingLineBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OriginatingLineBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(originatingLineBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "originatingLineBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<OriginatingLineBeHistory>> getOriginatingLineBeHistoryByStatus(@PathVariable String status) {
        List<OriginatingLineBeHistory> originatingLineBeHistoryList = new ArrayList<>();
        try {
            originatingLineBeHistoryList = originatingLineBeHistoryRepo.findByStatus(status);

            if (originatingLineBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OriginatingLineBeHistory FOUND for status [ " + status + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OriginatingLineBeHistory FOUND for status [ " + status + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(originatingLineBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "originatingLineBeHistoryByCustomersId/{customersId}", method = RequestMethod.GET)
    public ResponseEntity<List<OriginatingLineBeHistory>> getOriginatingLineBeHistoryByCustomersId(@PathVariable BigInteger customersId) {
        List<OriginatingLineBeHistory> originatingLineBeHistoryList = new ArrayList<>();
        try {
            originatingLineBeHistoryList = originatingLineBeHistoryRepo.findByCustomersId(customersId);

            if (originatingLineBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OriginatingLineBeHistory FOUND for customersId [ " + customersId + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OriginatingLineBeHistory FOUND for customersId [ " + customersId + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(originatingLineBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "originatingLineBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<OriginatingLineBeHistory>> getOriginatingLineBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<OriginatingLineBeHistory> originatingLineBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            originatingLineBeHistoryList = originatingLineBeHistoryRepo.findByRevinfo(revinfo);

            if (originatingLineBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OriginatingLineBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OriginatingLineBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + originatingLineBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(originatingLineBeHistoryList,
                HttpStatus.OK);
    }



}