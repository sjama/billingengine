/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

import co.za.ngwane.billing.db.AddressBeHistory;
import co.za.ngwane.billing.repository.AddressBeHistoryRepo;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.RevinfoRepo;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/addressBeHistory")
public class AddressBeHistoryController {
private static final Logger logger = Logger.getLogger(AddressBeHistoryController.class);

@Autowired
private AddressBeHistoryRepo addressBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<AddressBeHistory> save(
            @RequestBody AddressBeHistory addressBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            addressBeHistory = addressBeHistoryRepo.save(addressBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<AddressBeHistory> update(
            @RequestBody AddressBeHistory addressBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            addressBeHistory = addressBeHistoryRepo.save(addressBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<AddressBeHistory> delete(
            @RequestBody AddressBeHistory addressBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            addressBeHistory = addressBeHistoryRepo.save(addressBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "addressBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAllAddressBeHistory() {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findAll();
            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory List FOUND Query returned [" + addressBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "AddressBeHistory List FOUND Query returned [" + addressBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByRevtype(@PathVariable Short revtype) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByRevtype(revtype);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPhyAddrCity/{phyAddrCity}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPhyAddrCity(@PathVariable String phyAddrCity) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPhyAddrCity(phyAddrCity);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for phyAddrCity [ " + phyAddrCity + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for phyAddrCity [ " + phyAddrCity + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPhyAddrCode/{phyAddrCode}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPhyAddrCode(@PathVariable String phyAddrCode) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPhyAddrCode(phyAddrCode);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for phyAddrCode [ " + phyAddrCode + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for phyAddrCode [ " + phyAddrCode + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPhyAddrLine1/{phyAddrLine1}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPhyAddrLine1(@PathVariable String phyAddrLine1) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPhyAddrLine1(phyAddrLine1);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for phyAddrLine1 [ " + phyAddrLine1 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for phyAddrLine1 [ " + phyAddrLine1 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPhyAddrLine2/{phyAddrLine2}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPhyAddrLine2(@PathVariable String phyAddrLine2) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPhyAddrLine2(phyAddrLine2);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for phyAddrLine2 [ " + phyAddrLine2 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for phyAddrLine2 [ " + phyAddrLine2 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPhyAddrLine3/{phyAddrLine3}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPhyAddrLine3(@PathVariable String phyAddrLine3) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPhyAddrLine3(phyAddrLine3);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for phyAddrLine3 [ " + phyAddrLine3 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for phyAddrLine3 [ " + phyAddrLine3 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPhyAddrSuburb/{phyAddrSuburb}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPhyAddrSuburb(@PathVariable String phyAddrSuburb) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPhyAddrSuburb(phyAddrSuburb);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for phyAddrSuburb [ " + phyAddrSuburb + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for phyAddrSuburb [ " + phyAddrSuburb + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPosAddrCity/{posAddrCity}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPosAddrCity(@PathVariable String posAddrCity) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPosAddrCity(posAddrCity);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for posAddrCity [ " + posAddrCity + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for posAddrCity [ " + posAddrCity + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPosAddrCode/{posAddrCode}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPosAddrCode(@PathVariable String posAddrCode) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPosAddrCode(posAddrCode);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for posAddrCode [ " + posAddrCode + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for posAddrCode [ " + posAddrCode + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPosAddrLine1/{posAddrLine1}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPosAddrLine1(@PathVariable String posAddrLine1) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPosAddrLine1(posAddrLine1);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for posAddrLine1 [ " + posAddrLine1 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for posAddrLine1 [ " + posAddrLine1 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPosAddrLine2/{posAddrLine2}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPosAddrLine2(@PathVariable String posAddrLine2) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPosAddrLine2(posAddrLine2);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for posAddrLine2 [ " + posAddrLine2 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for posAddrLine2 [ " + posAddrLine2 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPosAddrLine3/{posAddrLine3}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPosAddrLine3(@PathVariable String posAddrLine3) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPosAddrLine3(posAddrLine3);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for posAddrLine3 [ " + posAddrLine3 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for posAddrLine3 [ " + posAddrLine3 + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByPosAddrSuburb/{posAddrSuburb}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByPosAddrSuburb(@PathVariable String posAddrSuburb) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByPosAddrSuburb(posAddrSuburb);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for posAddrSuburb [ " + posAddrSuburb + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for posAddrSuburb [ " + posAddrSuburb + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByStatChgD(@PathVariable Date statChgD) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByStatChgD(statChgD);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByStatus(@PathVariable String status) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByStatus(status);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for status [ " + status + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for status [ " + status + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByLuAddressTypeCd/{luAddressTypeCd}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByLuAddressTypeCd(@PathVariable String luAddressTypeCd) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            addressBeHistoryList = addressBeHistoryRepo.findByLuAddressTypeCd(luAddressTypeCd);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for luAddressTypeCd [ " + luAddressTypeCd + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for luAddressTypeCd [ " + luAddressTypeCd + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "addressBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<AddressBeHistory>> getAddressBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<AddressBeHistory> addressBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            addressBeHistoryList = addressBeHistoryRepo.findByRevinfo(revinfo);

            if (addressBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO AddressBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "AddressBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + addressBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(addressBeHistoryList,
                HttpStatus.OK);
    }



}