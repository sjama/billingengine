/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.BankDetBe;
import co.za.ngwane.billing.repository.BankDetBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/bankDetBe")
public class BankDetBeController {
private static final Logger logger = Logger.getLogger(BankDetBeController.class);

@Autowired
private BankDetBeRepo bankDetBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<BankDetBe> save(
            @RequestBody BankDetBe bankDetBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            bankDetBe = bankDetBeRepo.save(bankDetBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<BankDetBe> update(
            @RequestBody BankDetBe bankDetBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            bankDetBe = bankDetBeRepo.save(bankDetBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<BankDetBe> delete(
            @RequestBody BankDetBe bankDetBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            bankDetBe = bankDetBeRepo.save(bankDetBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "bankDetBe", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBe>> getAllBankDetBe() {
        List<BankDetBe> bankDetBeList = new ArrayList<>();
        try {
            bankDetBeList = bankDetBeRepo.findAll();
            if (bankDetBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBe List FOUND Query returned [" + bankDetBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "BankDetBe List FOUND Query returned [" + bankDetBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeByName/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBe>> getBankDetBeByName(@PathVariable String name) {
        List<BankDetBe> bankDetBeList = new ArrayList<>();
        try {
            bankDetBeList = bankDetBeRepo.findByName(name);

            if (bankDetBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBe FOUND for name [ " + name + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBe FOUND for name [ " + name + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeByAccountNo/{accountNo}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBe>> getBankDetBeByAccountNo(@PathVariable String accountNo) {
        List<BankDetBe> bankDetBeList = new ArrayList<>();
        try {
            bankDetBeList = bankDetBeRepo.findByAccountNo(accountNo);

            if (bankDetBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBe FOUND for accountNo [ " + accountNo + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBe FOUND for accountNo [ " + accountNo + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeByAccountName/{accountName}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBe>> getBankDetBeByAccountName(@PathVariable String accountName) {
        List<BankDetBe> bankDetBeList = new ArrayList<>();
        try {
            bankDetBeList = bankDetBeRepo.findByAccountName(accountName);

            if (bankDetBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBe FOUND for accountName [ " + accountName + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBe FOUND for accountName [ " + accountName + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeByLuAccountTypeCd/{luAccountTypeCd}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBe>> getBankDetBeByLuAccountTypeCd(@PathVariable String luAccountTypeCd) {
        List<BankDetBe> bankDetBeList = new ArrayList<>();
        try {
            bankDetBeList = bankDetBeRepo.findByLuAccountTypeCd(luAccountTypeCd);

            if (bankDetBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBe FOUND for luAccountTypeCd [ " + luAccountTypeCd + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBe FOUND for luAccountTypeCd [ " + luAccountTypeCd + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeByBranchCd/{branchCd}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBe>> getBankDetBeByBranchCd(@PathVariable String branchCd) {
        List<BankDetBe> bankDetBeList = new ArrayList<>();
        try {
            bankDetBeList = bankDetBeRepo.findByBranchCd(branchCd);

            if (bankDetBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBe FOUND for branchCd [ " + branchCd + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBe FOUND for branchCd [ " + branchCd + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBe>> getBankDetBeByStatus(@PathVariable String status) {
        List<BankDetBe> bankDetBeList = new ArrayList<>();
        try {
            bankDetBeList = bankDetBeRepo.findByStatus(status);

            if (bankDetBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBe FOUND for status [ " + status + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBe FOUND for status [ " + status + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "bankDetBeByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<BankDetBe>> getBankDetBeByStatChgD(@PathVariable Date statChgD) {
        List<BankDetBe> bankDetBeList = new ArrayList<>();
        try {
            bankDetBeList = bankDetBeRepo.findByStatChgD(statChgD);

            if (bankDetBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO BankDetBe FOUND for statChgD [ " + statChgD + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "BankDetBe FOUND for statChgD [ " + statChgD + " ] query returned [" + bankDetBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(bankDetBeList,
                HttpStatus.OK);
    }



}