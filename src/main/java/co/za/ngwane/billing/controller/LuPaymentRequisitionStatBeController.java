/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.LuPaymentRequisitionStatBe;
import co.za.ngwane.billing.repository.LuPaymentRequisitionStatBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/luPaymentRequisitionStatBe")
public class LuPaymentRequisitionStatBeController {
private static final Logger logger = Logger.getLogger(LuPaymentRequisitionStatBeController.class);

@Autowired
private LuPaymentRequisitionStatBeRepo luPaymentRequisitionStatBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<LuPaymentRequisitionStatBe> save(
            @RequestBody LuPaymentRequisitionStatBe luPaymentRequisitionStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luPaymentRequisitionStatBe = luPaymentRequisitionStatBeRepo.save(luPaymentRequisitionStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPaymentRequisitionStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<LuPaymentRequisitionStatBe> update(
            @RequestBody LuPaymentRequisitionStatBe luPaymentRequisitionStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luPaymentRequisitionStatBe = luPaymentRequisitionStatBeRepo.save(luPaymentRequisitionStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPaymentRequisitionStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<LuPaymentRequisitionStatBe> delete(
            @RequestBody LuPaymentRequisitionStatBe luPaymentRequisitionStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luPaymentRequisitionStatBe = luPaymentRequisitionStatBeRepo.save(luPaymentRequisitionStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPaymentRequisitionStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "luPaymentRequisitionStatBe", method = RequestMethod.GET)
    public ResponseEntity<List<LuPaymentRequisitionStatBe>> getAllLuPaymentRequisitionStatBe() {
        List<LuPaymentRequisitionStatBe> luPaymentRequisitionStatBeList = new ArrayList<>();
        try {
            luPaymentRequisitionStatBeList = luPaymentRequisitionStatBeRepo.findAll();
            if (luPaymentRequisitionStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuPaymentRequisitionStatBe List FOUND Query returned [" + luPaymentRequisitionStatBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "LuPaymentRequisitionStatBe List FOUND Query returned [" + luPaymentRequisitionStatBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPaymentRequisitionStatBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "luPaymentRequisitionStatBeByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<LuPaymentRequisitionStatBe>> getLuPaymentRequisitionStatBeByDescription(@PathVariable String description) {
        List<LuPaymentRequisitionStatBe> luPaymentRequisitionStatBeList = new ArrayList<>();
        try {
            luPaymentRequisitionStatBeList = luPaymentRequisitionStatBeRepo.findByDescription(description);

            if (luPaymentRequisitionStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuPaymentRequisitionStatBe FOUND for description [ " + description + " ] query returned [" + luPaymentRequisitionStatBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LuPaymentRequisitionStatBe FOUND for description [ " + description + " ] query returned [" + luPaymentRequisitionStatBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPaymentRequisitionStatBeList,
                HttpStatus.OK);
    }

}