/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.*;
import co.za.ngwane.billing.repository.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/api/orderFormController")
public class OrderFormBeController {
    private static final Logger logger = Logger.getLogger(OrderFormBeController.class);

    @Autowired
    private OrderFormBeRepo orderFormBeRepo;
    @Autowired
    private LuReminderPeriodBeRepo luReminderPeriodBeRepo;
    @Autowired
    private CustomerRepo customersBeRepo;
    @Autowired
    private UserBeRepo usersBeRepo;
    @Autowired
    private ContactPersonBeRepo contactPersonBeRepo;

    @Autowired
    private LuOrderProcessStatBeRepo luOrderProcessStatBeRepo;

    @Autowired
    private LastNoBeRepo lastNoBeRepo;

    @Autowired
    private LuLastNoTypeBeRepo luLastNoTypeBeRepo;

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public ResponseEntity<OrderFormBe> save(
            @RequestBody OrderFormBe orderFormBe,
            UriComponentsBuilder ucBuilder) {
        try {
            ContactPersonBe contactPersonBe = contactPersonBeRepo.save(orderFormBe.getContactPersonId());
            LuReminderPeriodBe luReminderPeriodBe = luReminderPeriodBeRepo.findOne(BillingConstants.LU_REMINDER_DAILY);
            LuOrderProcessStatBe luOrderProcessStatBe = luOrderProcessStatBeRepo.findOne(BillingConstants.LU_ORDER_PROCESS_STAT_PENDING_DIRECTORS);
            orderFormBe.setLuOrderProcessStatCd(luOrderProcessStatBe);
            orderFormBe.setLuReminderPeriodCd(luReminderPeriodBe);

            Date todaysDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(todaysDate);

            switch (orderFormBe.getTagName()) {
                case BillingConstants.TAG_ADD:

                    String year = calendar.get(Calendar.YEAR) + "";
                    String day = calendar.get(Calendar.DATE) + "";
                    String month = calendar.get(Calendar.MONTH) + 1 + "";

                    LuLastNoTypeBe luLastNoTypeBe = luLastNoTypeBeRepo.findOne(BillingConstants.LU_LAST_NO_TYPE_BE_ORDER);
                    LastNoBe lastNoBe = lastNoBeRepo.findByLastNoTypeCd(luLastNoTypeBe);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    System.out.println(dateFormat.format(todaysDate));

                    long counter = 1;
                    String orderNo = "";

                    if (lastNoBe != null) {
                        Date statChangedDate = lastNoBe.getStatChgD();
                        String todayDateString = year + "-" + month + "-" + day;
                        todaysDate = convertStringToDate(todayDateString);
                        if (statChangedDate.compareTo(todaysDate) == 0) {
                            counter = lastNoBe.getNo();
                            lastNoBe.setNo(counter + 1);
                        } else {
                            lastNoBe.setNo(counter);
                        }
                    } else {
                        lastNoBe = new LastNoBe();
                        lastNoBe.setLastNoTypeCd(luLastNoTypeBe);
                        lastNoBe.setNo(counter + 1);
                    }

                    if (counter < 10) {
                        orderNo = "O-" + day + month + year.substring(2, 4) + "-0" + counter;
                    } else {
                        orderNo = "O-" + day + month + year.substring(2, 4) + "-" + counter;
                    }

                    lastNoBe.setStatChgD(todaysDate);
                    lastNoBeRepo.save(lastNoBe);

                    orderFormBe.setOrderNo(orderNo);
                    orderFormBe.setContactPersonId(contactPersonBe);
                    orderFormBe.setStatus(BillingConstants.LU_STAT_ACTIVE);
                    orderFormBe.setStatChgDate(new Date());

                    orderFormBe = orderFormBeRepo.save(orderFormBe);

                    //Send Notifications

                    break;

                case BillingConstants.TAG_UPDATE:
                    break;

                case BillingConstants.TAG_DELETE:
                    orderFormBe.setStatus(BillingConstants.LU_STAT_ACTIVE);
                    orderFormBe.setStatChgDate(new Date());
                    break;

                default:

            }

        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBe, HttpStatus.CREATED);
    }

    private Date convertStringToDate(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        // SimpleDateFormat sdf2 = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        Date parsedDate = null;

        try {
            parsedDate = sdf.parse(dateString);
        } catch (ParseException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return parsedDate;
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<OrderFormBe> update(
            @RequestBody OrderFormBe orderFormBe,
            UriComponentsBuilder ucBuilder) {
        try {
            orderFormBe = orderFormBeRepo.save(orderFormBe);
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBe, HttpStatus.CREATED);
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<OrderFormBe> delete(
            @RequestBody OrderFormBe orderFormBe,
            UriComponentsBuilder ucBuilder) {
        try {
            orderFormBe = orderFormBeRepo.save(orderFormBe);
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBe, HttpStatus.CREATED);
    }

    @RequestMapping(value = "orders", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getAllOrderFormBe() {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findAll();
            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe List FOUND Query returned [" + orderFormBeList.size() + "] AT [ " + new Date() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe List FOUND Query returned [" + orderFormBeList.size() + "] Orders AT [" + new Date() + " ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByOrderNo/{orderNo}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByOrderNo(@PathVariable String orderNo) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByOrderNo(orderNo);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for orderNo [ " + orderNo + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for orderNo [ " + orderNo + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByProjId/{projId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByProjId(@PathVariable int projId) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByProjId(projId);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for projId [ " + projId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for projId [ " + projId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByProjName/{projName}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByProjName(@PathVariable String projName) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByProjName(projName);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for projName [ " + projName + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for projName [ " + projName + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByProjNo/{projNo}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByProjNo(@PathVariable String projNo) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByProjNo(projNo);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for projNo [ " + projNo + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for projNo [ " + projNo + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByOrderDesc/{orderDesc}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByOrderDesc(@PathVariable String orderDesc) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByOrderDesc(orderDesc);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for orderDesc [ " + orderDesc + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for orderDesc [ " + orderDesc + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByRequestDate/{requestDate}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByRequestDate(@PathVariable Date requestDate) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByRequestDate(requestDate);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for requestDate [ " + requestDate + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for requestDate [ " + requestDate + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByDeliveryDate/{deliveryDate}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByDeliveryDate(@PathVariable Date deliveryDate) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByDeliveryDate(deliveryDate);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for deliveryDate [ " + deliveryDate + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for deliveryDate [ " + deliveryDate + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByNoDaysLeft/{noDaysLeft}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByNoDaysLeft(@PathVariable Integer noDaysLeft) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByNoDaysLeft(noDaysLeft);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for noDaysLeft [ " + noDaysLeft + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for noDaysLeft [ " + noDaysLeft + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByRefNo(@PathVariable String refNo) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByRefNo(refNo);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for refNo [ " + refNo + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for refNo [ " + refNo + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByStatus(@PathVariable String status) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByStatus(status);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for status [ " + status + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for status [ " + status + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByStatChgDate(@PathVariable Date statChgDate) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByStatChgDate(statChgDate);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByCreatedDate(@PathVariable Date createdDate) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByCreatedDate(createdDate);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for createdDate [ " + createdDate + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for createdDate [ " + createdDate + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByContactPersonId/{contactPersonId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByContactPersonId(@PathVariable long contactPersonId) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            orderFormBeList = orderFormBeRepo.findByContactPersonId(contactPersonId);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for contactPersonId [ " + contactPersonId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for contactPersonId [ " + contactPersonId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByLuReminderPeriodCd/{luReminderPeriodCd}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByLuReminderPeriodCd(@PathVariable String luReminderPeriodCd) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            LuReminderPeriodBe luReminderPeriodBe = luReminderPeriodBeRepo.findOne(new String(luReminderPeriodCd));
            orderFormBeList = orderFormBeRepo.findByLuReminderPeriodCd(luReminderPeriodBe);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for luReminderPeriodCd [ " + luReminderPeriodCd + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for luReminderPeriodCd [ " + luReminderPeriodCd + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByCustomersId/{customersId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByCustomersId(@PathVariable String customersId) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            CustomersBe customersBe = customersBeRepo.findOne(new Long(customersId));
            orderFormBeList = orderFormBeRepo.findByCustomersId(customersBe);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for customersId [ " + customersId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for customersId [ " + customersId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByCapturedByUsersId/{capturedByUsersId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByCapturedByUsersId(@PathVariable String capturedByUsersId) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            UsersBe usersBe = usersBeRepo.findOne(new Integer(capturedByUsersId));
            orderFormBeList = orderFormBeRepo.findByCapturedByUsersId(usersBe);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for capturedByUsersId [ " + capturedByUsersId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for capturedByUsersId [ " + capturedByUsersId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "orderFormBeByRequestedByUsersId/{requestedByUsersId}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderFormBe>> getOrderFormBeByRequestedByUsersId(@PathVariable String requestedByUsersId) {
        List<OrderFormBe> orderFormBeList = new ArrayList<>();
        try {
            UsersBe usersBe = usersBeRepo.findOne(new Integer(requestedByUsersId));
            orderFormBeList = orderFormBeRepo.findByRequestedByUsersId(usersBe);

            if (orderFormBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO OrderFormBe FOUND for requestedByUsersId [ " + requestedByUsersId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "OrderFormBe FOUND for requestedByUsersId [ " + requestedByUsersId + " ] query returned [" + orderFormBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(orderFormBeList,
                HttpStatus.OK);
    }


}