/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.db.UsersBeHistory;
import co.za.ngwane.billing.repository.RevinfoRepo;
import co.za.ngwane.billing.repository.UsersBeHistoryRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/usersBeHistory")
public class UsersBeHistoryController {
private static final Logger logger = Logger.getLogger(UsersBeHistoryController.class);

@Autowired
private UsersBeHistoryRepo usersBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<UsersBeHistory> save(
            @RequestBody UsersBeHistory usersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            usersBeHistory = usersBeHistoryRepo.save(usersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<UsersBeHistory> update(
            @RequestBody UsersBeHistory usersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            usersBeHistory = usersBeHistoryRepo.save(usersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<UsersBeHistory> delete(
            @RequestBody UsersBeHistory usersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            usersBeHistory = usersBeHistoryRepo.save(usersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "usersBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBeHistory>> getAllUsersBeHistory() {
        List<UsersBeHistory> usersBeHistoryList = new ArrayList<>();
        try {
            usersBeHistoryList = usersBeHistoryRepo.findAll();
            if (usersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO UsersBeHistory List FOUND Query returned [" + usersBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "UsersBeHistory List FOUND Query returned [" + usersBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "usersBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBeHistory>> getUsersBeHistoryByRevtype(@PathVariable Short revtype) {
        List<UsersBeHistory> usersBeHistoryList = new ArrayList<>();
        try {
            usersBeHistoryList = usersBeHistoryRepo.findByRevtype(revtype);

            if (usersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO UsersBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "UsersBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "usersBeHistoryByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBeHistory>> getUsersBeHistoryByStatChgD(@PathVariable Date statChgD) {
        List<UsersBeHistory> usersBeHistoryList = new ArrayList<>();
        try {
            usersBeHistoryList = usersBeHistoryRepo.findByStatChgD(statChgD);

            if (usersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO UsersBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "UsersBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "usersBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBeHistory>> getUsersBeHistoryByStatus(@PathVariable String status) {
        List<UsersBeHistory> usersBeHistoryList = new ArrayList<>();
        try {
            usersBeHistoryList = usersBeHistoryRepo.findByStatus(status);

            if (usersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO UsersBeHistory FOUND for status [ " + status + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "UsersBeHistory FOUND for status [ " + status + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "usersBeHistoryByUserId/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBeHistory>> getUsersBeHistoryByUserId(@PathVariable Integer userId) {
        List<UsersBeHistory> usersBeHistoryList = new ArrayList<>();
        try {
            usersBeHistoryList = usersBeHistoryRepo.findByUserId(userId);

            if (usersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO UsersBeHistory FOUND for userId [ " + userId + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "UsersBeHistory FOUND for userId [ " + userId + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "usersBeHistoryByCustomersId/{customersId}", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBeHistory>> getUsersBeHistoryByCustomersId(@PathVariable BigInteger customersId) {
        List<UsersBeHistory> usersBeHistoryList = new ArrayList<>();
        try {
            usersBeHistoryList = usersBeHistoryRepo.findByCustomersId(customersId);

            if (usersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO UsersBeHistory FOUND for customersId [ " + customersId + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "UsersBeHistory FOUND for customersId [ " + customersId + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "usersBeHistoryByLuRolesCd/{luRolesCd}", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBeHistory>> getUsersBeHistoryByLuRolesCd(@PathVariable String luRolesCd) {
        List<UsersBeHistory> usersBeHistoryList = new ArrayList<>();
        try {
            usersBeHistoryList = usersBeHistoryRepo.findByLuRolesCd(luRolesCd);

            if (usersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO UsersBeHistory FOUND for luRolesCd [ " + luRolesCd + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "UsersBeHistory FOUND for luRolesCd [ " + luRolesCd + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "usersBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBeHistory>> getUsersBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<UsersBeHistory> usersBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            usersBeHistoryList = usersBeHistoryRepo.findByRevinfo(revinfo);

            if (usersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO UsersBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "UsersBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + usersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBeHistoryList,
                HttpStatus.OK);
    }



}