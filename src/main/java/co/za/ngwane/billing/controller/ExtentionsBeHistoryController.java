/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ExtentionsBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.ExtentionsBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/extentionsBeHistory")
public class ExtentionsBeHistoryController {
private static final Logger logger = Logger.getLogger(ExtentionsBeHistoryController.class);

@Autowired
private ExtentionsBeHistoryRepo extentionsBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ExtentionsBeHistory> save(
            @RequestBody ExtentionsBeHistory extentionsBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            extentionsBeHistory = extentionsBeHistoryRepo.save(extentionsBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(extentionsBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ExtentionsBeHistory> update(
            @RequestBody ExtentionsBeHistory extentionsBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            extentionsBeHistory = extentionsBeHistoryRepo.save(extentionsBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(extentionsBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ExtentionsBeHistory> delete(
            @RequestBody ExtentionsBeHistory extentionsBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            extentionsBeHistory = extentionsBeHistoryRepo.save(extentionsBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(extentionsBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "extentionsBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<ExtentionsBeHistory>> getAllExtentionsBeHistory() {
        List<ExtentionsBeHistory> extentionsBeHistoryList = new ArrayList<>();
        try {
            extentionsBeHistoryList = extentionsBeHistoryRepo.findAll();
            if (extentionsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ExtentionsBeHistory List FOUND Query returned [" + extentionsBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ExtentionsBeHistory List FOUND Query returned [" + extentionsBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(extentionsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "extentionsBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<ExtentionsBeHistory>> getExtentionsBeHistoryByRevtype(@PathVariable Short revtype) {
        List<ExtentionsBeHistory> extentionsBeHistoryList = new ArrayList<>();
        try {
            extentionsBeHistoryList = extentionsBeHistoryRepo.findByRevtype(revtype);

            if (extentionsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ExtentionsBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ExtentionsBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(extentionsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "extentionsBeHistoryByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<ExtentionsBeHistory>> getExtentionsBeHistoryByStatChgD(@PathVariable Date statChgD) {
        List<ExtentionsBeHistory> extentionsBeHistoryList = new ArrayList<>();
        try {
            extentionsBeHistoryList = extentionsBeHistoryRepo.findByStatChgD(statChgD);

            if (extentionsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ExtentionsBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ExtentionsBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(extentionsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "extentionsBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ExtentionsBeHistory>> getExtentionsBeHistoryByStatus(@PathVariable String status) {
        List<ExtentionsBeHistory> extentionsBeHistoryList = new ArrayList<>();
        try {
            extentionsBeHistoryList = extentionsBeHistoryRepo.findByStatus(status);

            if (extentionsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ExtentionsBeHistory FOUND for status [ " + status + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ExtentionsBeHistory FOUND for status [ " + status + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(extentionsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "extentionsBeHistoryByBranchCd/{branchCd}", method = RequestMethod.GET)
    public ResponseEntity<List<ExtentionsBeHistory>> getExtentionsBeHistoryByBranchCd(@PathVariable String branchCd) {
        List<ExtentionsBeHistory> extentionsBeHistoryList = new ArrayList<>();
        try {
            extentionsBeHistoryList = extentionsBeHistoryRepo.findByBranchCd(branchCd);

            if (extentionsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ExtentionsBeHistory FOUND for branchCd [ " + branchCd + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ExtentionsBeHistory FOUND for branchCd [ " + branchCd + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(extentionsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "extentionsBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<ExtentionsBeHistory>> getExtentionsBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<ExtentionsBeHistory> extentionsBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            extentionsBeHistoryList = extentionsBeHistoryRepo.findByRevinfo(revinfo);

            if (extentionsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ExtentionsBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ExtentionsBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + extentionsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(extentionsBeHistoryList,
                HttpStatus.OK);
    }



}