/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.PaymentRequisitionBe;
import co.za.ngwane.billing.db.PurchaseOrderBe;
import co.za.ngwane.billing.db.UsersBe;
import co.za.ngwane.billing.repository.PaymentRequisitionBeRepo;
import co.za.ngwane.billing.repository.PurchaseOrderBeRepo;
import co.za.ngwane.billing.repository.UserBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/paymentRequisitionBe")
public class PaymentRequisitionBeController {
private static final Logger logger = Logger.getLogger(PaymentRequisitionBeController.class);

@Autowired
private PaymentRequisitionBeRepo paymentRequisitionBeRepo;
@Autowired
private PurchaseOrderBeRepo purchaseOrderBeRepo;
@Autowired
private UserBeRepo usersBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<PaymentRequisitionBe> save(
            @RequestBody PaymentRequisitionBe paymentRequisitionBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            paymentRequisitionBe = paymentRequisitionBeRepo.save(paymentRequisitionBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<PaymentRequisitionBe> update(
            @RequestBody PaymentRequisitionBe paymentRequisitionBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            paymentRequisitionBe = paymentRequisitionBeRepo.save(paymentRequisitionBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<PaymentRequisitionBe> delete(
            @RequestBody PaymentRequisitionBe paymentRequisitionBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            paymentRequisitionBe = paymentRequisitionBeRepo.save(paymentRequisitionBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "paymentRequisitionBe", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getAllPaymentRequisitionBe() {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            paymentRequisitionBeList = paymentRequisitionBeRepo.findAll();
            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe List FOUND Query returned [" + paymentRequisitionBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "PaymentRequisitionBe List FOUND Query returned [" + paymentRequisitionBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeByLuPayRequisitionProcessStatCd/{luPayRequisitionProcessStatCd}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getPaymentRequisitionBeByLuPayRequisitionProcessStatCd(@PathVariable String luPayRequisitionProcessStatCd) {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            paymentRequisitionBeList = paymentRequisitionBeRepo.findByLuPayRequisitionProcessStatCd(luPayRequisitionProcessStatCd);

            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe FOUND for luPayRequisitionProcessStatCd [ " + luPayRequisitionProcessStatCd + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBe FOUND for luPayRequisitionProcessStatCd [ " + luPayRequisitionProcessStatCd + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getPaymentRequisitionBeByRefNo(@PathVariable String refNo) {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            paymentRequisitionBeList = paymentRequisitionBeRepo.findByRefNo(refNo);

            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe FOUND for refNo [ " + refNo + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBe FOUND for refNo [ " + refNo + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeByAmountRequested/{amountRequested}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getPaymentRequisitionBeByAmountRequested(@PathVariable Float amountRequested) {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            paymentRequisitionBeList = paymentRequisitionBeRepo.findByAmountRequested(amountRequested);

            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe FOUND for amountRequested [ " + amountRequested + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBe FOUND for amountRequested [ " + amountRequested + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeByServiceDesc/{serviceDesc}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getPaymentRequisitionBeByServiceDesc(@PathVariable String serviceDesc) {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            paymentRequisitionBeList = paymentRequisitionBeRepo.findByServiceDesc(serviceDesc);

            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe FOUND for serviceDesc [ " + serviceDesc + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBe FOUND for serviceDesc [ " + serviceDesc + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getPaymentRequisitionBeByCreatedDate(@PathVariable Date createdDate) {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            paymentRequisitionBeList = paymentRequisitionBeRepo.findByCreatedDate(createdDate);

            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe FOUND for createdDate [ " + createdDate + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBe FOUND for createdDate [ " + createdDate + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getPaymentRequisitionBeByStatus(@PathVariable String status) {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            paymentRequisitionBeList = paymentRequisitionBeRepo.findByStatus(status);

            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe FOUND for status [ " + status + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBe FOUND for status [ " + status + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getPaymentRequisitionBeByStatChgDate(@PathVariable Date statChgDate) {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            paymentRequisitionBeList = paymentRequisitionBeRepo.findByStatChgDate(statChgDate);

            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeByPurchaseOrderId/{purchaseOrderId}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getPaymentRequisitionBeByPurchaseOrderId(@PathVariable String purchaseOrderId) {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            PurchaseOrderBe purchaseOrderBe = purchaseOrderBeRepo.findOne(new Long(purchaseOrderId));
            paymentRequisitionBeList = paymentRequisitionBeRepo.findByPurchaseOrderId(purchaseOrderBe);

            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe FOUND for purchaseOrderId [ " + purchaseOrderId + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBe FOUND for purchaseOrderId [ " + purchaseOrderId + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeByCreatedByUserId/{createdByUserId}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBe>> getPaymentRequisitionBeByCreatedByUserId(@PathVariable String createdByUserId) {
        List<PaymentRequisitionBe> paymentRequisitionBeList = new ArrayList<>();
        try {
            UsersBe usersBe = usersBeRepo.findOne(new Integer(createdByUserId));
            paymentRequisitionBeList = paymentRequisitionBeRepo.findByCreatedByUserId(usersBe);

            if (paymentRequisitionBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBe FOUND for createdByUserId [ " + createdByUserId + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBe FOUND for createdByUserId [ " + createdByUserId + " ] query returned [" + paymentRequisitionBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeList,
                HttpStatus.OK);
    }


}