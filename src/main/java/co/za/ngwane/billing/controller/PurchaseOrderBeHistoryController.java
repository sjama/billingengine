/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.PurchaseOrderBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.PurchaseOrderBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/purchaseOrderBeHistory")
public class PurchaseOrderBeHistoryController {
private static final Logger logger = Logger.getLogger(PurchaseOrderBeHistoryController.class);

@Autowired
private PurchaseOrderBeHistoryRepo purchaseOrderBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<PurchaseOrderBeHistory> save(
            @RequestBody PurchaseOrderBeHistory purchaseOrderBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            purchaseOrderBeHistory = purchaseOrderBeHistoryRepo.save(purchaseOrderBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<PurchaseOrderBeHistory> update(
            @RequestBody PurchaseOrderBeHistory purchaseOrderBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            purchaseOrderBeHistory = purchaseOrderBeHistoryRepo.save(purchaseOrderBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<PurchaseOrderBeHistory> delete(
            @RequestBody PurchaseOrderBeHistory purchaseOrderBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            purchaseOrderBeHistory = purchaseOrderBeHistoryRepo.save(purchaseOrderBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "purchaseOrderBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBeHistory>> getAllPurchaseOrderBeHistory() {
        List<PurchaseOrderBeHistory> purchaseOrderBeHistoryList = new ArrayList<>();
        try {
            purchaseOrderBeHistoryList = purchaseOrderBeHistoryRepo.findAll();
            if (purchaseOrderBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBeHistory List FOUND Query returned [" + purchaseOrderBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "PurchaseOrderBeHistory List FOUND Query returned [" + purchaseOrderBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBeHistory>> getPurchaseOrderBeHistoryByRevtype(@PathVariable Short revtype) {
        List<PurchaseOrderBeHistory> purchaseOrderBeHistoryList = new ArrayList<>();
        try {
            purchaseOrderBeHistoryList = purchaseOrderBeHistoryRepo.findByRevtype(revtype);

            if (purchaseOrderBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeHistoryByPoNo/{poNo}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBeHistory>> getPurchaseOrderBeHistoryByPoNo(@PathVariable String poNo) {
        List<PurchaseOrderBeHistory> purchaseOrderBeHistoryList = new ArrayList<>();
        try {
            purchaseOrderBeHistoryList = purchaseOrderBeHistoryRepo.findByPoNo(poNo);

            if (purchaseOrderBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBeHistory FOUND for poNo [ " + poNo + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBeHistory FOUND for poNo [ " + poNo + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeHistoryByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBeHistory>> getPurchaseOrderBeHistoryByRefNo(@PathVariable String refNo) {
        List<PurchaseOrderBeHistory> purchaseOrderBeHistoryList = new ArrayList<>();
        try {
            purchaseOrderBeHistoryList = purchaseOrderBeHistoryRepo.findByRefNo(refNo);

            if (purchaseOrderBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeHistoryByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBeHistory>> getPurchaseOrderBeHistoryByStatChgDate(@PathVariable Date statChgDate) {
        List<PurchaseOrderBeHistory> purchaseOrderBeHistoryList = new ArrayList<>();
        try {
            purchaseOrderBeHistoryList = purchaseOrderBeHistoryRepo.findByStatChgDate(statChgDate);

            if (purchaseOrderBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBeHistory>> getPurchaseOrderBeHistoryByStatus(@PathVariable String status) {
        List<PurchaseOrderBeHistory> purchaseOrderBeHistoryList = new ArrayList<>();
        try {
            purchaseOrderBeHistoryList = purchaseOrderBeHistoryRepo.findByStatus(status);

            if (purchaseOrderBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBeHistory FOUND for status [ " + status + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBeHistory FOUND for status [ " + status + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeHistoryByGeneratedByUserId/{generatedByUserId}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBeHistory>> getPurchaseOrderBeHistoryByGeneratedByUserId(@PathVariable Integer generatedByUserId) {
        List<PurchaseOrderBeHistory> purchaseOrderBeHistoryList = new ArrayList<>();
        try {
            purchaseOrderBeHistoryList = purchaseOrderBeHistoryRepo.findByGeneratedByUserId(generatedByUserId);

            if (purchaseOrderBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBeHistory FOUND for generatedByUserId [ " + generatedByUserId + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBeHistory FOUND for generatedByUserId [ " + generatedByUserId + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeHistoryByProcurementId/{procurementId}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBeHistory>> getPurchaseOrderBeHistoryByProcurementId(@PathVariable BigInteger procurementId) {
        List<PurchaseOrderBeHistory> purchaseOrderBeHistoryList = new ArrayList<>();
        try {
            purchaseOrderBeHistoryList = purchaseOrderBeHistoryRepo.findByProcurementId(procurementId);

            if (purchaseOrderBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBeHistory FOUND for procurementId [ " + procurementId + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBeHistory FOUND for procurementId [ " + procurementId + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBeHistory>> getPurchaseOrderBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<PurchaseOrderBeHistory> purchaseOrderBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            purchaseOrderBeHistoryList = purchaseOrderBeHistoryRepo.findByRevinfo(revinfo);

            if (purchaseOrderBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + purchaseOrderBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeHistoryList,
                HttpStatus.OK);
    }



}