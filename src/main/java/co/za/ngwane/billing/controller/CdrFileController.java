/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.CdrFileBe;
import co.za.ngwane.billing.repository.CdrFileRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author bheki lubisi
 * @Date 12 Aug 2018
 */
@RestController
@RequestMapping("/api/cdrFiles/")
public class CdrFileController {

    @Autowired
    public CdrFileRepo cdrFileRepo;

    private static final Logger logger = LoggerFactory.getLogger(CdrFileController.class);

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<CdrFileBe> save(
            @RequestBody CdrFileBe cdrFileBe,
            UriComponentsBuilder ucBuilder) {
        try {

            switch (cdrFileBe.getTagName()) {
                case BillingConstants.TAG_ADD:

                    logger.info(
                            "\n===============================================================\n "
                                    + "We are doing CDR FILE save for file with Ref number "
                                    + " [" + cdrFileBe.getCdrFileRefNo() + "] AT [" + new Date() + "]"
                                    + "\n=============================================================== ");

                    cdrFileBe.setStatus(BillingConstants.LU_STAT_ACTIVE);
                    cdrFileBe.setStatChgD(new Date());

                    cdrFileBe = cdrFileRepo.save(cdrFileBe);

                    break;

                case BillingConstants.TAG_UPDATE:
                    cdrFileBe.setStatus(BillingConstants.LU_STAT_ACTIVE);
                    cdrFileBe.setStatChgD(new Date());

                    cdrFileBe = cdrFileRepo.save(cdrFileBe);

                    break;

                case BillingConstants.TAG_DELETE:
                    cdrFileBe.setStatus(BillingConstants.LU_STAT_DELETED);
                    cdrFileBe.setStatChgD(new Date());

                    cdrFileBe = cdrFileRepo.save(cdrFileBe);

                    cdrFileBe.setTagFlag(BillingConstants.BOOLEAN_TRUE);
                    cdrFileBe.setTagMsg("CDR File deleted successful.");

                    break;

                default:
            }

        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught : " + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(cdrFileBe, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/files", method = RequestMethod.GET)
    public @ResponseBody
    List<CdrFileBe> getAll() {
        List<CdrFileBe> cdrFilesList = new ArrayList<>();
        try {
            cdrFilesList = cdrFileRepo.findAll();
            if (cdrFilesList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO CDR FILE(s) FOUND Query returned [" + cdrFilesList.size() + "] files.\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + "CDR FILE(s) FOUND Query returned [" + cdrFilesList.size() + "] files.\n"
                                + "\n===============================================================");
                for (CdrFileBe cdrFileBe : cdrFilesList) {
                    logger.info(
                            "===============================================================\n"
                                    + "CDR FILE(s) FOUND Query returned [" + cdrFileBe.getLuBillProStatus().getDescription() + "] files.\n"
                                    + "\n===============================================================");
                }
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return cdrFilesList;
    }
}
