/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.PaymentRequisitionBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.PaymentRequisitionBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/paymentRequisitionBeHistory")
public class PaymentRequisitionBeHistoryController {
private static final Logger logger = Logger.getLogger(PaymentRequisitionBeHistoryController.class);

@Autowired
private PaymentRequisitionBeHistoryRepo paymentRequisitionBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<PaymentRequisitionBeHistory> save(
            @RequestBody PaymentRequisitionBeHistory paymentRequisitionBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            paymentRequisitionBeHistory = paymentRequisitionBeHistoryRepo.save(paymentRequisitionBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<PaymentRequisitionBeHistory> update(
            @RequestBody PaymentRequisitionBeHistory paymentRequisitionBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            paymentRequisitionBeHistory = paymentRequisitionBeHistoryRepo.save(paymentRequisitionBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<PaymentRequisitionBeHistory> delete(
            @RequestBody PaymentRequisitionBeHistory paymentRequisitionBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            paymentRequisitionBeHistory = paymentRequisitionBeHistoryRepo.save(paymentRequisitionBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "paymentRequisitionBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getAllPaymentRequisitionBeHistory() {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findAll();
            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory List FOUND Query returned [" + paymentRequisitionBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "PaymentRequisitionBeHistory List FOUND Query returned [" + paymentRequisitionBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByRevtype(@PathVariable Short revtype) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByRevtype(revtype);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByAmountRequested/{amountRequested}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByAmountRequested(@PathVariable Float amountRequested) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByAmountRequested(amountRequested);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for amountRequested [ " + amountRequested + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for amountRequested [ " + amountRequested + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByCreatedDate(@PathVariable Date createdDate) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByCreatedDate(createdDate);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByLuPayRequisitionProcessStatCd/{luPayRequisitionProcessStatCd}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByLuPayRequisitionProcessStatCd(@PathVariable String luPayRequisitionProcessStatCd) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByLuPayRequisitionProcessStatCd(luPayRequisitionProcessStatCd);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for luPayRequisitionProcessStatCd [ " + luPayRequisitionProcessStatCd + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for luPayRequisitionProcessStatCd [ " + luPayRequisitionProcessStatCd + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByRefNo(@PathVariable String refNo) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByRefNo(refNo);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByServiceDesc/{serviceDesc}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByServiceDesc(@PathVariable String serviceDesc) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByServiceDesc(serviceDesc);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for serviceDesc [ " + serviceDesc + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for serviceDesc [ " + serviceDesc + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByStatChgDate(@PathVariable Date statChgDate) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByStatChgDate(statChgDate);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByStatus(@PathVariable String status) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByStatus(status);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for status [ " + status + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for status [ " + status + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByCreatedByUserId/{createdByUserId}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByCreatedByUserId(@PathVariable Integer createdByUserId) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByCreatedByUserId(createdByUserId);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for createdByUserId [ " + createdByUserId + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for createdByUserId [ " + createdByUserId + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByPurchaseOrderId/{purchaseOrderId}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByPurchaseOrderId(@PathVariable BigInteger purchaseOrderId) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByPurchaseOrderId(purchaseOrderId);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for purchaseOrderId [ " + purchaseOrderId + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for purchaseOrderId [ " + purchaseOrderId + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "paymentRequisitionBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<PaymentRequisitionBeHistory>> getPaymentRequisitionBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<PaymentRequisitionBeHistory> paymentRequisitionBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            paymentRequisitionBeHistoryList = paymentRequisitionBeHistoryRepo.findByRevinfo(revinfo);

            if (paymentRequisitionBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PaymentRequisitionBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PaymentRequisitionBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + paymentRequisitionBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(paymentRequisitionBeHistoryList,
                HttpStatus.OK);
    }



}