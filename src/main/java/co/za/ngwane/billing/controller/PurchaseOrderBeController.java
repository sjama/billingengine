/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ProcurementBe;
import co.za.ngwane.billing.db.PurchaseOrderBe;
import co.za.ngwane.billing.db.UsersBe;
import co.za.ngwane.billing.repository.ProcurementBeRepo;
import co.za.ngwane.billing.repository.PurchaseOrderBeRepo;
import co.za.ngwane.billing.repository.UserBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/purchaseOrderBe")
public class PurchaseOrderBeController {
private static final Logger logger = Logger.getLogger(PurchaseOrderBeController.class);

@Autowired
private PurchaseOrderBeRepo purchaseOrderBeRepo;
@Autowired
private UserBeRepo usersBeRepo;
@Autowired
private ProcurementBeRepo procurementBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<PurchaseOrderBe> save(
            @RequestBody PurchaseOrderBe purchaseOrderBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            purchaseOrderBe = purchaseOrderBeRepo.save(purchaseOrderBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<PurchaseOrderBe> update(
            @RequestBody PurchaseOrderBe purchaseOrderBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            purchaseOrderBe = purchaseOrderBeRepo.save(purchaseOrderBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<PurchaseOrderBe> delete(
            @RequestBody PurchaseOrderBe purchaseOrderBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            purchaseOrderBe = purchaseOrderBeRepo.save(purchaseOrderBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "purchaseOrderBe", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBe>> getAllPurchaseOrderBe() {
        List<PurchaseOrderBe> purchaseOrderBeList = new ArrayList<>();
        try {
            purchaseOrderBeList = purchaseOrderBeRepo.findAll();
            if (purchaseOrderBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBe List FOUND Query returned [" + purchaseOrderBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "PurchaseOrderBe List FOUND Query returned [" + purchaseOrderBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeByPoNo/{poNo}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBe>> getPurchaseOrderBeByPoNo(@PathVariable String poNo) {
        List<PurchaseOrderBe> purchaseOrderBeList = new ArrayList<>();
        try {
            purchaseOrderBeList = purchaseOrderBeRepo.findByPoNo(poNo);

            if (purchaseOrderBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBe FOUND for poNo [ " + poNo + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBe FOUND for poNo [ " + poNo + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBe>> getPurchaseOrderBeByRefNo(@PathVariable String refNo) {
        List<PurchaseOrderBe> purchaseOrderBeList = new ArrayList<>();
        try {
            purchaseOrderBeList = purchaseOrderBeRepo.findByRefNo(refNo);

            if (purchaseOrderBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBe FOUND for refNo [ " + refNo + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBe FOUND for refNo [ " + refNo + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBe>> getPurchaseOrderBeByStatus(@PathVariable String status) {
        List<PurchaseOrderBe> purchaseOrderBeList = new ArrayList<>();
        try {
            purchaseOrderBeList = purchaseOrderBeRepo.findByStatus(status);

            if (purchaseOrderBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBe FOUND for status [ " + status + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBe FOUND for status [ " + status + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBe>> getPurchaseOrderBeByStatChgDate(@PathVariable Date statChgDate) {
        List<PurchaseOrderBe> purchaseOrderBeList = new ArrayList<>();
        try {
            purchaseOrderBeList = purchaseOrderBeRepo.findByStatChgDate(statChgDate);

            if (purchaseOrderBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeByGeneratedByUserId/{generatedByUserId}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBe>> getPurchaseOrderBeByGeneratedByUserId(@PathVariable String generatedByUserId) {
        List<PurchaseOrderBe> purchaseOrderBeList = new ArrayList<>();
        try {
            UsersBe usersBe = usersBeRepo.findOne(new Integer(generatedByUserId));
            purchaseOrderBeList = purchaseOrderBeRepo.findByGeneratedByUserId(usersBe);

            if (purchaseOrderBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBe FOUND for generatedByUserId [ " + generatedByUserId + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBe FOUND for generatedByUserId [ " + generatedByUserId + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "purchaseOrderBeByProcurementId/{procurementId}", method = RequestMethod.GET)
    public ResponseEntity<List<PurchaseOrderBe>> getPurchaseOrderBeByProcurementId(@PathVariable String procurementId) {
        List<PurchaseOrderBe> purchaseOrderBeList = new ArrayList<>();
        try {
            ProcurementBe procurementBe = procurementBeRepo.findOne(new Long(procurementId));
            purchaseOrderBeList = purchaseOrderBeRepo.findByProcurementId(procurementBe);

            if (purchaseOrderBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO PurchaseOrderBe FOUND for procurementId [ " + procurementId + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "PurchaseOrderBe FOUND for procurementId [ " + procurementId + " ] query returned [" + purchaseOrderBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(purchaseOrderBeList,
                HttpStatus.OK);
    }



}