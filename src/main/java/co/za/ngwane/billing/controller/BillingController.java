/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.LuRefNoDescBe;
import co.za.ngwane.billing.repository.LuRefNoDescRepo;
import co.za.ngwane.billing.repository.service.CustomerRepoService;
import co.za.ngwane.billing.service.BillingService;
import co.za.ngwane.billing.service.InvoiceServiceImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

/**
 *
 * @author jamasithole
 */
@RestController
@RequestMapping("/api/billingController/")
public class BillingController {

    @Autowired
    BillingService billingService;

//   @Autowired
//   private  JobLauncher jobLauncher;
//
//   @Autowired
//    private  Job ourJob;

    @Autowired
    public CustomerRepoService customerRepoService;

    @Autowired
    private InvoiceServiceImpl invoiceService;

    @Autowired
    private LuRefNoDescRepo luRefNoDescRepo;
    private static final Logger logger = LoggerFactory.getLogger(BillingController.class);

    @RequestMapping(value = "/billing", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void doBillingForMonth() throws Exception {

        billingService.doBilling();
    }

//    @RequestMapping("/invokejob")
//    public void handle() throws Exception {
//
//        JobParameters jobParameters = new JobParametersBuilder().toJobParameters();
//        System.err.println("Wer are here papa");
//        jobLauncher.run(ourJob, jobParameters);
//
//    }
    @RequestMapping("test")
    public @ResponseBody
    Callable<String> handleTestRequest() {

        logger.info("handler started");
        Callable<String> callable = new Callable<String>() {
            @Override
            public String call() throws Exception {
                logger.info("async task started");
                billingService.doBilling();
                logger.info("async task finished");
                return "async result";
            }
        };

        logger.info("handler finished");
        return callable;
    }

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public @ResponseBody
    List<CustomersBe> getAll() {
        List<CustomersBe> customerList = new ArrayList<>();
        try {
            customerList = customerRepoService.getAll();
            if (customerList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                        + "NO Customer(s) FOUND Get All Customers returned [" + customerList.size() + "] customers.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + "Customers(s) FOUND Get All Customers returned [" + customerList.size() + "] customers.\n"
                        + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return customerList;
    }

    @RequestMapping("testbill")
    public DeferredResult<String> get(@RequestParam String input) {
        DeferredResult<String> defResult = new DeferredResult<>();

        new Thread() {
            public void run() {
                System.out.println("Thread: " + getName() + " running");
                try {
                    billingService.doBilling();
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(BillingController.class.getName()).log(Level.SEVERE, null, ex);
                }
                String apiResponse = "Good";
                defResult.setResult(apiResponse);
            }
        }.start();

        return defResult;
    }

    @RequestMapping(value = "/invoice", method = RequestMethod.GET)
    public @ResponseBody
    void doInvoiceForMonth() {
        //One way or the other we need to pass cdr file ref no here...Jama will provide it.
        LuRefNoDescBe luRefNoDescCd = luRefNoDescRepo.findOne(BillingConstants.LU_REF_NO_DESC_BE_INVOICE);
        invoiceService.processInvoice(11, 2017, "REF-201808", luRefNoDescCd);
    }
}
