/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ProcurementBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.ProcurementBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/procurementBeHistory")
public class ProcurementBeHistoryController {
private static final Logger logger = Logger.getLogger(ProcurementBeHistoryController.class);

@Autowired
private ProcurementBeHistoryRepo procurementBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ProcurementBeHistory> save(
            @RequestBody ProcurementBeHistory procurementBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementBeHistory = procurementBeHistoryRepo.save(procurementBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ProcurementBeHistory> update(
            @RequestBody ProcurementBeHistory procurementBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementBeHistory = procurementBeHistoryRepo.save(procurementBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ProcurementBeHistory> delete(
            @RequestBody ProcurementBeHistory procurementBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementBeHistory = procurementBeHistoryRepo.save(procurementBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "procurementBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getAllProcurementBeHistory() {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findAll();
            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory List FOUND Query returned [" + procurementBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ProcurementBeHistory List FOUND Query returned [" + procurementBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByRevtype(@PathVariable Short revtype) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByRevtype(revtype);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByActualDeliveryDate/{actualDeliveryDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByActualDeliveryDate(@PathVariable Integer actualDeliveryDate) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByActualDeliveryDate(actualDeliveryDate);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for actualDeliveryDate [ " + actualDeliveryDate + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for actualDeliveryDate [ " + actualDeliveryDate + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByBudget/{budget}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByBudget(@PathVariable Float budget) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByBudget(budget);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for budget [ " + budget + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for budget [ " + budget + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByCodSupplier/{codSupplier}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByCodSupplier(@PathVariable String codSupplier) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByCodSupplier(codSupplier);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for codSupplier [ " + codSupplier + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for codSupplier [ " + codSupplier + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByCreatedDate(@PathVariable Date createdDate) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByCreatedDate(createdDate);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByCreditAvail/{creditAvail}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByCreditAvail(@PathVariable Float creditAvail) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByCreditAvail(creditAvail);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for creditAvail [ " + creditAvail + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for creditAvail [ " + creditAvail + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByCreditLimit/{creditLimit}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByCreditLimit(@PathVariable Float creditLimit) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByCreditLimit(creditLimit);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for creditLimit [ " + creditLimit + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for creditLimit [ " + creditLimit + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByExpectedDeliveryDate/{expectedDeliveryDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByExpectedDeliveryDate(@PathVariable Date expectedDeliveryDate) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByExpectedDeliveryDate(expectedDeliveryDate);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for expectedDeliveryDate [ " + expectedDeliveryDate + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for expectedDeliveryDate [ " + expectedDeliveryDate + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByExpectedTurnAroundTime/{expectedTurnAroundTime}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByExpectedTurnAroundTime(@PathVariable String expectedTurnAroundTime) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByExpectedTurnAroundTime(expectedTurnAroundTime);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for expectedTurnAroundTime [ " + expectedTurnAroundTime + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for expectedTurnAroundTime [ " + expectedTurnAroundTime + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByRefNo(@PathVariable String refNo) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByRefNo(refNo);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByStatChgDate(@PathVariable Date statChgDate) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByStatChgDate(statChgDate);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByStatus(@PathVariable String status) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByStatus(status);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for status [ " + status + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for status [ " + status + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByOrderId/{orderId}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByOrderId(@PathVariable BigInteger orderId) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            procurementBeHistoryList = procurementBeHistoryRepo.findByOrderId(orderId);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for orderId [ " + orderId + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for orderId [ " + orderId + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementBeHistory>> getProcurementBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<ProcurementBeHistory> procurementBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            procurementBeHistoryList = procurementBeHistoryRepo.findByRevinfo(revinfo);

            if (procurementBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + procurementBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementBeHistoryList,
                HttpStatus.OK);
    }



}