package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.certificates.util.GUIDKeyGenerator;
import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.UsersBe;
import co.za.ngwane.billing.repository.LuRolesBeRepo;
import co.za.ngwane.billing.repository.service.CustomerRepoService;
import co.za.ngwane.billing.repository.service.UserBeRepoService;
import co.za.ngwane.billing.service.EmailsService;
import co.za.ngwane.billing.vo.EmailVo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author bheki.lubisi
 */
@RestController
@RequestMapping("/api/usersBeController/")
public class UserController {

    private static Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    private EmailsService emailsService;

    @Autowired
    private UserBeRepoService usersBeRepoService;

    @Autowired
    private CustomerRepoService customerRepoService;

    @Autowired
    public LuRolesBeRepo luRolesBeRepo;

    @RequestMapping(value = "/findUser/{username}/{password}", method = RequestMethod.GET)
    public ResponseEntity<UsersBe> doLogin(@PathVariable("username") String username, @PathVariable("password") String password) {
        UsersBe usersBe = new UsersBe();
        try {
            //usersBe = usersBeRepoService.findByUsername(username);
            if (usersBe != null) {

                logger.info(
                        "===============================================================\n "
                        + "User with username [" + username + "] FOUND at [" + new Date() + "]"
                        + "\n=============================================================== ");

            } else {
                logger.info(
                        "===============================================================\n "
                        + "User with username [" + username + "] NOT found at [" + new Date() + "]"
                        + "\n=============================================================== ");

                usersBe = new UsersBe();
                usersBe.setTagFlag(false);
                usersBe.setTagMessage("User not found, please make sure username and password are correct or contact your system administrator.");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBe,
                HttpStatus.OK);
    }

    //save
    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<UsersBe> save(
            @RequestBody UsersBe usersBe,
            UriComponentsBuilder ucBuilder) {
        try {

            switch (usersBe.getTagName()) {
                case BillingConstants.TAG_ADD:
                    //check if email is unique - find employee by email
                    UsersBe usersBeExist = new UsersBe();
                    //UsersBe usersBeExist = usersBeRepoService.findByUsername(usersBe.getUsername());

                    if (usersBeExist == null) {

                        GUIDKeyGenerator myGUID = new GUIDKeyGenerator(true);

                        String tempPass = myGUID.generate('P', 25);
                        usersBe.setPassword(tempPass);

                        logger.info(
                                "===============================================================\n "
                                + "We are doing user ADD [" + usersBe.getName() + "] at [" + new Date() + "]"
                                + "\n=============================================================== ");

                        CustomersBe customersId = customerRepoService.getById(new Long("1"));
                        usersBe.setCustomersId(customersId);

//                        LuRolesBe luRolesBe = luRolesBeRepo.findOne("02");
//                        usersBe.setLuRolesCd(luRolesBe);
                        usersBe.setStatus(BillingConstants.LU_STAT_ACTIVE);
                        usersBe.setStatChgD(new Date());

                        usersBe = usersBeRepoService.save(usersBe);
                        usersBe.setTagFlag(true);
                        usersBe.setTagMessage("User created, temporary password have been emailed to the user.");

                        //email thy password
                        /*####################*/
                        String subject = "Registration Successful";
                        String msg = "Dear " + usersBe.getName() + " " + usersBe.getSurname() + "\n\n"
                                + "You have successful registered to BBE, "
                                + "your username will be the email address you provided."
                                + " Your temporary password is " + usersBe.getPassword() + " please note that this is a "
                                + "system generated password "
                                + "change it to your own. To change password click on the logged in as and select change "
                                + "password. "
                                + "\n\nKind regards,\nBBE Administrator";

                        EmailVo emailVo = new EmailVo();
                        emailVo.setMsg(msg);
                        emailVo.setSubject(subject);
                        emailVo.setTo(usersBe.getEmail());
                        emailVo.setBcc(BillingConstants.EMAIL_NOTIFICATION_BCC);

                        EmailsService.mail(emailVo);

                        //emailsService.sendMail(emailVo);
                        //Notify administrator
                        String subjectAdm = usersBe.getName() + " " + usersBe.getSurname() + " Registered to BBE Application";
                        String msgAdm = "Dear Administrator" + "\n\n"
                                + usersBe.getName() + " " + usersBe.getSurname() + " have just registered to BBE."
                                + "\n\nRegards,\nBBE Application";

                        String toAdm = BillingConstants.EMAIL_TO_ADMIN;

                        EmailVo emailVoAdm = new EmailVo();
                        emailVoAdm.setMsg(msgAdm);
                        emailVoAdm.setSubject(subjectAdm);
                        emailVoAdm.setTo(toAdm);
                        emailVoAdm.setBcc(BillingConstants.EMAIL_NOTIFICATION_BCC);

//                        List<EmailVo> emailsList = new ArrayList<>();
//                        emailsList.add(emailVo);
//                        emailsList.add(emailVoAdm);
                        //emailsService.sendMailList(emailsList);
                        EmailsService.mail(emailVoAdm);

                        /*####################*/
                    } else {
                        usersBe.setTagFlag(false);
                        usersBe.setTagMessage("Failed to create user, username " + usersBe.getUsername() + " is already in used.");
                    }
                    break;

                case BillingConstants.TAG_UPDATE:
                    logger.info(
                            "===============================================================\n "
                            + "We are doing user UPDATE [" + usersBe.getUsername() + "] at [" + new Date() + "]"
                            + "\n=============================================================== ");
                    usersBe = usersBeRepoService.save(usersBe);
                    usersBe.setTagFlag(true);
                    usersBe.setTagMessage("Saved successful.");
                    break;
                case BillingConstants.TAG_DELETE:
                    logger.info(
                            "===============================================================\n "
                            + "We are doing user DELETE for user with surname [" + usersBe.getSurname() + "] at [" + new Date() + "]"
                            + "\n=============================================================== ");

                    usersBe.setStatus(BillingConstants.LU_STAT_DELETED);
                    usersBe.setStatChgD(new Date());
                    usersBe = usersBeRepoService.save(usersBe);

                    usersBe.setTagFlag(true);
                    usersBe.setTagMessage("Deleted successful.");

                    logger.info(
                            "===============================================================\n "
                            + "User with surname [" + usersBe.getSurname() + "] deleted at [" + new Date() + "]"
                            + "\n=============================================================== ");

                    break;
                default:
                    logger.info(
                            "===============================================================\n "
                            + "What are you on about really? No Tag defined at [" + new Date() + "]"
                            + "\n=============================================================== ");
                    break;
            }

        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersBe, HttpStatus.CREATED);
    }

    //save
    @RequestMapping(value = "resetPwd", method = RequestMethod.POST)
    public ResponseEntity<Boolean> resetPwd(
            @RequestBody UsersBe usersBe,
            UriComponentsBuilder ucBuilder) {
        boolean results = false;
        try {
            UsersBe usersBeRecord = new UsersBe();
//            UsersBe usersBeRecord = usersBeRepoService.findByUsername(
//                    usersBe.getUsername());

            if (usersBeRecord == null) {
                logger.info(
                        "===============================================================\n "
                        + "NO user record FOUND for Email : [ " + usersBe.getEmail() + " ],\n"
                        + "Username [" + usersBe.getUsername() + "] \n"
                        + "\n=============================================================== ");
            } else {
                logger.info(
                        "===============================================================\n "
                        + "User record FOUND for Email : [ " + usersBeRecord.getEmail() + " ],\n"
                        + "Username [" + usersBe.getUsername() + "] \n"
                        + "\n=============================================================== ");

                //reset password and email it...
                /*####################*/
                GUIDKeyGenerator myGUID = new GUIDKeyGenerator(true);

                String tempPass = myGUID.generate('P', 25);
                usersBeRecord.setPassword(tempPass);

                usersBeRepoService.save(usersBeRecord);

                String subject = "Password Reset Successful";
                String msg = "Dear " + usersBeRecord.getName() + " " + usersBeRecord.getSurname() + "\n\n"
                        + "Your password have been successful reset."
                        + " The temporary password is " + usersBeRecord.getPassword() + " please note that this is a "
                        + "system generated password "
                        + "change it to your own. "
                        + "\n\n"
                        + "To change password click on the logged in as and select change "
                        + "password. "
                        + "\n\n"
                        + "Kind regards,"
                        + "\nBBE Administrator";

                EmailVo emailVo = new EmailVo();
                emailVo.setMsg(msg);
                emailVo.setSubject(subject);
                emailVo.setTo(usersBe.getEmail());
                emailVo.setBcc(BillingConstants.EMAIL_NOTIFICATION_BCC);

                /*List<EmailVo> emailsList = new ArrayList<>();
                emailsList.add(emailVo);

                emailsService.sendMailList(emailsList);*/
                EmailsService.mail(emailVo);
                results = true;
                /*####################*/
            }

        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(results, HttpStatus.CREATED);
    }

    @RequestMapping(value = "allUsers", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBe>> getAll() {
        List<UsersBe> usersList = new ArrayList<>();
        try {
            usersList = usersBeRepoService.getAll();
            if (usersList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                        + "NO User FOUND Get All Customers returned [" + usersList.size() + "] customers.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + "User FOUND Get All Customers returned [" + usersList.size() + "] customers.\n"
                        + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public ResponseEntity<List<UsersBe>> getAllActive() {
        List<UsersBe> usersList = new ArrayList<>();
        try {
            usersList = usersBeRepoService.getAllActive(BillingConstants.LU_STAT_DELETED);
            if (usersList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                        + "NO User FOUND Get All Customers returned [" + usersList.size() + "] customers.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + "User FOUND Get All Customers returned [" + usersList.size() + "] customers.\n"
                        + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(usersList,
                HttpStatus.OK);
    }
}
