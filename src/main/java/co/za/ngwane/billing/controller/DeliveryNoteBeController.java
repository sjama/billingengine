/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.DeliveryNoteBe;
import co.za.ngwane.billing.db.PurchaseOrderBe;
import co.za.ngwane.billing.db.UsersBe;
import co.za.ngwane.billing.repository.DeliveryNoteBeRepo;
import co.za.ngwane.billing.repository.PurchaseOrderBeRepo;
import co.za.ngwane.billing.repository.UserBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/deliveryNoteBe")
public class DeliveryNoteBeController {
private static final Logger logger = Logger.getLogger(DeliveryNoteBeController.class);

@Autowired
private DeliveryNoteBeRepo deliveryNoteBeRepo;
@Autowired
private UserBeRepo usersBeRepo;
@Autowired
private PurchaseOrderBeRepo purchaseOrderBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<DeliveryNoteBe> save(
            @RequestBody DeliveryNoteBe deliveryNoteBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            deliveryNoteBe = deliveryNoteBeRepo.save(deliveryNoteBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<DeliveryNoteBe> update(
            @RequestBody DeliveryNoteBe deliveryNoteBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            deliveryNoteBe = deliveryNoteBeRepo.save(deliveryNoteBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<DeliveryNoteBe> delete(
            @RequestBody DeliveryNoteBe deliveryNoteBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            deliveryNoteBe = deliveryNoteBeRepo.save(deliveryNoteBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "deliveryNoteBe", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBe>> getAllDeliveryNoteBe() {
        List<DeliveryNoteBe> deliveryNoteBeList = new ArrayList<>();
        try {
            deliveryNoteBeList = deliveryNoteBeRepo.findAll();
            if (deliveryNoteBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBe List FOUND Query returned [" + deliveryNoteBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "DeliveryNoteBe List FOUND Query returned [" + deliveryNoteBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBe>> getDeliveryNoteBeByCreatedDate(@PathVariable Date createdDate) {
        List<DeliveryNoteBe> deliveryNoteBeList = new ArrayList<>();
        try {
            deliveryNoteBeList = deliveryNoteBeRepo.findByCreatedDate(createdDate);

            if (deliveryNoteBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBe FOUND for createdDate [ " + createdDate + " ] query returned [" + deliveryNoteBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBe FOUND for createdDate [ " + createdDate + " ] query returned [" + deliveryNoteBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBe>> getDeliveryNoteBeByRefNo(@PathVariable String refNo) {
        List<DeliveryNoteBe> deliveryNoteBeList = new ArrayList<>();
        try {
            deliveryNoteBeList = deliveryNoteBeRepo.findByRefNo(refNo);

            if (deliveryNoteBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBe FOUND for refNo [ " + refNo + " ] query returned [" + deliveryNoteBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBe FOUND for refNo [ " + refNo + " ] query returned [" + deliveryNoteBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeByCreatedByUserId/{createdByUserId}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBe>> getDeliveryNoteBeByCreatedByUserId(@PathVariable String createdByUserId) {
        List<DeliveryNoteBe> deliveryNoteBeList = new ArrayList<>();
        try {
            UsersBe usersBe = usersBeRepo.findOne(new Integer(createdByUserId));
            deliveryNoteBeList = deliveryNoteBeRepo.findByCreatedByUserId(usersBe);

            if (deliveryNoteBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBe FOUND for createdByUserId [ " + createdByUserId + " ] query returned [" + deliveryNoteBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBe FOUND for createdByUserId [ " + createdByUserId + " ] query returned [" + deliveryNoteBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeByPurchaseOrderId/{purchaseOrderId}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBe>> getDeliveryNoteBeByPurchaseOrderId(@PathVariable String purchaseOrderId) {
        List<DeliveryNoteBe> deliveryNoteBeList = new ArrayList<>();
        try {
            PurchaseOrderBe purchaseOrderBe = purchaseOrderBeRepo.findOne(new Long(purchaseOrderId));
            deliveryNoteBeList = deliveryNoteBeRepo.findByPurchaseOrderId(purchaseOrderBe);

            if (deliveryNoteBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBe FOUND for purchaseOrderId [ " + purchaseOrderId + " ] query returned [" + deliveryNoteBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBe FOUND for purchaseOrderId [ " + purchaseOrderId + " ] query returned [" + deliveryNoteBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeList,
                HttpStatus.OK);
    }


}