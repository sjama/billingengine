/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.LuCustTypeBe;
import co.za.ngwane.billing.repository.LuCustTypeRepo;
import co.za.ngwane.billing.repository.service.CustomerRepoService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api/customerController/")
public class CustomerController {

    private static final Logger logger = Logger.getLogger(CustomerController.class);

    @Autowired
    public CustomerRepoService customerRepoService;

    @Autowired
    public LuCustTypeRepo custTypeRepo;

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<CustomersBe> add(
            @RequestBody CustomersBe customer,
            UriComponentsBuilder ucBuilder) {
        try {

            switch (customer.getTagName()) {
                case BillingConstants.TAG_ADD:
                    LuCustTypeBe luCustTypeBe = custTypeRepo.findOne(BillingConstants.LU_CUST_TYPE_CUSTOMER);

                    if (luCustTypeBe != null) {
                        logger.info(
                                "===============================================================\n"
                                        + "Customer type found, ya eda swesu Customer name  [" + customer.getName() + "] is ready to be added.\n"
                                        + "\n===============================================================");
                        customer.setStatus(BillingConstants.LU_STAT_ACTIVE);
                        customer.setStatChgD(new Date());

                        customer.setLuCustTypeBeCd(luCustTypeBe);

                    } else {
                        logger.info(
                                "===============================================================\n"
                                        + "Can't ADD customer with name [" + customer.getName() + "] Something went wrong.\n"
                                        + "\n===============================================================");
                    }

                    break;

                case BillingConstants.TAG_UPDATE:
                    break;

                case BillingConstants.TAG_DELETE:
                    customer.setStatus(BillingConstants.LU_STAT_DELETED);
                    customer.setStatChgD(new Date());
                    break;

                default:

            }

            customerRepoService.save(customer);

            logger.info(
                    "===============================================================\n"
                            + "Customer with name [" + customer.getName() + "] saved successful.\n"
                            + "\n===============================================================");

        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }

        return new ResponseEntity<>(customer, HttpStatus.CREATED);
    }

    @RequestMapping(value = "customers", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBe>> getAll() {
        List<CustomersBe> customerList = new ArrayList<>();
        try {
            customerList = customerRepoService.getAll();
            if (customerList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO Customer FOUND Get All Customers returned [" + customerList.size() + "] customers.\n"
                                + "\n===============================================================");
            } else {
                //Remove Brilliantel
//                for (CustomersBe customersBe : customerList) {
//                    if (customersBe.getName().equalsIgnoreCase("BRILLIANTEL")) {
//                        customerList.remove(customersBe);
//                        logger.info(
//                                "===============================================================\n"
//                                        + "Customers with name [" + customersBe.getName() + "] removed.\n"
//                                        + "\n===============================================================");
//                    }
//                }
                logger.info(
                        "===============================================================\n"
                                + "Customers FOUND Get All Customers returned [" + customerList.size() + "] customers.\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customerList,
                HttpStatus.OK);
    }
}
