/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.DocReposBe;
import co.za.ngwane.billing.repository.service.DocRepoRepoService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/docRepoController/")
public class DocRepoController {

    private static final Logger logger = Logger.getLogger(DocRepoController.class);

    @Autowired
    public DocRepoRepoService docRepoService;

    private static final String APPLICATION_PDF = "application/pdf";

    @RequestMapping(value = "docs/{refN}", method = RequestMethod.GET)
    public ResponseEntity<List<DocReposBe>> getAllByRefNo(@PathVariable("refN") String refN) {
        List<DocReposBe> docReposBeList = new ArrayList<>();
        try {
            
            docReposBeList = docRepoService.getAllByRefNo(refN);
            
            if (docReposBeList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                        + "NO Document returned [" + docReposBeList.size() + "] documents.\n"
                        + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                        + " Documents FOUND  [" + docReposBeList.size() + "] documents.\n"
                        + "\n===============================================================");
            }
            
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                    + "Exception caught." + e.getMessage()
                    + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        
        return new ResponseEntity<>(docReposBeList,
                HttpStatus.OK);
    }

    //############## this is the working service ##################
    @RequestMapping(value = "download/{id}", method = RequestMethod.GET, produces = APPLICATION_PDF)
    public @ResponseBody
    void downloadD(HttpServletRequest request, HttpServletResponse response,
            @PathVariable("id") String id) throws IOException {

        final ServletContext servletContext = request.getSession().getServletContext();
        final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        final String temperotyFilePath = tempDirectory.getAbsolutePath();

        byte[] contents = null;

        logger.info(
                "===============================================================\n "
                + "The fileref is  : [" + id + "]"
                + "\n=============================================================== ");

        contents = docRepoService.downloadDocument(new Long(id));

        String fileName = "INV" + "_" + new Date();

        File fileToWriteTo = new File(temperotyFilePath + "\\" + fileName);

        FileUtils.writeByteArrayToFile(fileToWriteTo, contents);

        InputStream in = new FileInputStream(fileToWriteTo);

        response.setContentType(APPLICATION_PDF);
        response.setHeader("Content-Disposition", "attachment; filename=" + fileToWriteTo.getName());
        response.setHeader("Content-Length", String.valueOf(fileToWriteTo.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }

}
