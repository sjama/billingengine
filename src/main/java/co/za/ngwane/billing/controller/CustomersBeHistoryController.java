/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.CustomersBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.CustomersBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/customersBeHistory")
public class CustomersBeHistoryController {
private static final Logger logger = Logger.getLogger(CustomersBeHistoryController.class);

@Autowired
private CustomersBeHistoryRepo customersBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<CustomersBeHistory> save(
            @RequestBody CustomersBeHistory customersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            customersBeHistory = customersBeHistoryRepo.save(customersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<CustomersBeHistory> update(
            @RequestBody CustomersBeHistory customersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            customersBeHistory = customersBeHistoryRepo.save(customersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<CustomersBeHistory> delete(
            @RequestBody CustomersBeHistory customersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            customersBeHistory = customersBeHistoryRepo.save(customersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "customersBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getAllCustomersBeHistory() {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findAll();
            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory List FOUND Query returned [" + customersBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "CustomersBeHistory List FOUND Query returned [" + customersBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByRevtype(@PathVariable Short revtype) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByRevtype(revtype);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByAccountNo/{accountNo}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByAccountNo(@PathVariable String accountNo) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByAccountNo(accountNo);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for accountNo [ " + accountNo + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for accountNo [ " + accountNo + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByAgreedRate/{agreedRate}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByAgreedRate(@PathVariable Float agreedRate) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByAgreedRate(agreedRate);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for agreedRate [ " + agreedRate + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for agreedRate [ " + agreedRate + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByCustVatNo/{custVatNo}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByCustVatNo(@PathVariable String custVatNo) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByCustVatNo(custVatNo);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for custVatNo [ " + custVatNo + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for custVatNo [ " + custVatNo + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByLogoRefn/{logoRefn}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByLogoRefn(@PathVariable String logoRefn) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByLogoRefn(logoRefn);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for logoRefn [ " + logoRefn + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for logoRefn [ " + logoRefn + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByName/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByName(@PathVariable String name) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByName(name);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for name [ " + name + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for name [ " + name + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByOriginatingNo/{originatingNo}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByOriginatingNo(@PathVariable String originatingNo) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByOriginatingNo(originatingNo);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for originatingNo [ " + originatingNo + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for originatingNo [ " + originatingNo + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPhyAddrCity/{phyAddrCity}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPhyAddrCity(@PathVariable String phyAddrCity) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPhyAddrCity(phyAddrCity);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for phyAddrCity [ " + phyAddrCity + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for phyAddrCity [ " + phyAddrCity + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPhyAddrCode/{phyAddrCode}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPhyAddrCode(@PathVariable String phyAddrCode) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPhyAddrCode(phyAddrCode);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for phyAddrCode [ " + phyAddrCode + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for phyAddrCode [ " + phyAddrCode + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPhyAddrLine1/{phyAddrLine1}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPhyAddrLine1(@PathVariable String phyAddrLine1) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPhyAddrLine1(phyAddrLine1);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for phyAddrLine1 [ " + phyAddrLine1 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for phyAddrLine1 [ " + phyAddrLine1 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPhyAddrLine2/{phyAddrLine2}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPhyAddrLine2(@PathVariable String phyAddrLine2) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPhyAddrLine2(phyAddrLine2);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for phyAddrLine2 [ " + phyAddrLine2 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for phyAddrLine2 [ " + phyAddrLine2 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPhyAddrLine3/{phyAddrLine3}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPhyAddrLine3(@PathVariable String phyAddrLine3) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPhyAddrLine3(phyAddrLine3);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for phyAddrLine3 [ " + phyAddrLine3 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for phyAddrLine3 [ " + phyAddrLine3 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPhyAddrSuburb/{phyAddrSuburb}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPhyAddrSuburb(@PathVariable String phyAddrSuburb) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPhyAddrSuburb(phyAddrSuburb);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for phyAddrSuburb [ " + phyAddrSuburb + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for phyAddrSuburb [ " + phyAddrSuburb + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPosAddrCity/{posAddrCity}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPosAddrCity(@PathVariable String posAddrCity) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPosAddrCity(posAddrCity);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for posAddrCity [ " + posAddrCity + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for posAddrCity [ " + posAddrCity + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPosAddrCode/{posAddrCode}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPosAddrCode(@PathVariable String posAddrCode) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPosAddrCode(posAddrCode);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for posAddrCode [ " + posAddrCode + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for posAddrCode [ " + posAddrCode + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPosAddrLine1/{posAddrLine1}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPosAddrLine1(@PathVariable String posAddrLine1) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPosAddrLine1(posAddrLine1);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for posAddrLine1 [ " + posAddrLine1 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for posAddrLine1 [ " + posAddrLine1 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPosAddrLine2/{posAddrLine2}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPosAddrLine2(@PathVariable String posAddrLine2) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPosAddrLine2(posAddrLine2);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for posAddrLine2 [ " + posAddrLine2 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for posAddrLine2 [ " + posAddrLine2 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPosAddrLine3/{posAddrLine3}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPosAddrLine3(@PathVariable String posAddrLine3) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPosAddrLine3(posAddrLine3);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for posAddrLine3 [ " + posAddrLine3 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for posAddrLine3 [ " + posAddrLine3 + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByPosAddrSuburb/{posAddrSuburb}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByPosAddrSuburb(@PathVariable String posAddrSuburb) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByPosAddrSuburb(posAddrSuburb);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for posAddrSuburb [ " + posAddrSuburb + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for posAddrSuburb [ " + posAddrSuburb + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByStatChgD(@PathVariable Date statChgD) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByStatChgD(statChgD);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByStatus(@PathVariable String status) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByStatus(status);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for status [ " + status + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for status [ " + status + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByLuCustTypeCd/{luCustTypeCd}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByLuCustTypeCd(@PathVariable String luCustTypeCd) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            customersBeHistoryList = customersBeHistoryRepo.findByLuCustTypeCd(luCustTypeCd);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for luCustTypeCd [ " + luCustTypeCd + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for luCustTypeCd [ " + luCustTypeCd + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "customersBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<CustomersBeHistory>> getCustomersBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<CustomersBeHistory> customersBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            customersBeHistoryList = customersBeHistoryRepo.findByRevinfo(revinfo);

            if (customersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CustomersBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CustomersBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + customersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(customersBeHistoryList,
                HttpStatus.OK);
    }



}