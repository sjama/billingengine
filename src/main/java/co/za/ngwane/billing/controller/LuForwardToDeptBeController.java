/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.LuForwardToDeptBe;
import co.za.ngwane.billing.repository.LuForwardToDeptBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/luForwardToDeptBe")
public class LuForwardToDeptBeController {
private static final Logger logger = Logger.getLogger(LuForwardToDeptBeController.class);

@Autowired
private LuForwardToDeptBeRepo luForwardToDeptBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<LuForwardToDeptBe> save(
            @RequestBody LuForwardToDeptBe luForwardToDeptBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luForwardToDeptBe = luForwardToDeptBeRepo.save(luForwardToDeptBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luForwardToDeptBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<LuForwardToDeptBe> update(
            @RequestBody LuForwardToDeptBe luForwardToDeptBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luForwardToDeptBe = luForwardToDeptBeRepo.save(luForwardToDeptBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luForwardToDeptBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<LuForwardToDeptBe> delete(
            @RequestBody LuForwardToDeptBe luForwardToDeptBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luForwardToDeptBe = luForwardToDeptBeRepo.save(luForwardToDeptBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luForwardToDeptBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "luForwardToDeptBe", method = RequestMethod.GET)
    public ResponseEntity<List<LuForwardToDeptBe>> getAllLuForwardToDeptBe() {
        List<LuForwardToDeptBe> luForwardToDeptBeList = new ArrayList<>();
        try {
            luForwardToDeptBeList = luForwardToDeptBeRepo.findAll();
            if (luForwardToDeptBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuForwardToDeptBe List FOUND Query returned [" + luForwardToDeptBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "LuForwardToDeptBe List FOUND Query returned [" + luForwardToDeptBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luForwardToDeptBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "luForwardToDeptBeByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<LuForwardToDeptBe>> getLuForwardToDeptBeByDescription(@PathVariable String description) {
        List<LuForwardToDeptBe> luForwardToDeptBeList = new ArrayList<>();
        try {
            luForwardToDeptBeList = luForwardToDeptBeRepo.findByDescription(description);

            if (luForwardToDeptBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuForwardToDeptBe FOUND for description [ " + description + " ] query returned [" + luForwardToDeptBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LuForwardToDeptBe FOUND for description [ " + description + " ] query returned [" + luForwardToDeptBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luForwardToDeptBeList,
                HttpStatus.OK);
    }

}