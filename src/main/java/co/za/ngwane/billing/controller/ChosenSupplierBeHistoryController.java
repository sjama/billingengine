/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ChosenSupplierBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.ChosenSupplierBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/chosenSupplierBeHistory")
public class ChosenSupplierBeHistoryController {
private static final Logger logger = Logger.getLogger(ChosenSupplierBeHistoryController.class);

@Autowired
private ChosenSupplierBeHistoryRepo chosenSupplierBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ChosenSupplierBeHistory> save(
            @RequestBody ChosenSupplierBeHistory chosenSupplierBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            chosenSupplierBeHistory = chosenSupplierBeHistoryRepo.save(chosenSupplierBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ChosenSupplierBeHistory> update(
            @RequestBody ChosenSupplierBeHistory chosenSupplierBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            chosenSupplierBeHistory = chosenSupplierBeHistoryRepo.save(chosenSupplierBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ChosenSupplierBeHistory> delete(
            @RequestBody ChosenSupplierBeHistory chosenSupplierBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            chosenSupplierBeHistory = chosenSupplierBeHistoryRepo.save(chosenSupplierBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "chosenSupplierBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getAllChosenSupplierBeHistory() {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findAll();
            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory List FOUND Query returned [" + chosenSupplierBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ChosenSupplierBeHistory List FOUND Query returned [" + chosenSupplierBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getChosenSupplierBeHistoryByRevtype(@PathVariable Short revtype) {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findByRevtype(revtype);

            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeHistoryByApprovedDate/{approvedDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getChosenSupplierBeHistoryByApprovedDate(@PathVariable Date approvedDate) {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findByApprovedDate(approvedDate);

            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory FOUND for approvedDate [ " + approvedDate + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBeHistory FOUND for approvedDate [ " + approvedDate + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeHistoryByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getChosenSupplierBeHistoryByRefNo(@PathVariable String refNo) {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findByRefNo(refNo);

            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeHistoryByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getChosenSupplierBeHistoryByStatChgDate(@PathVariable Date statChgDate) {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findByStatChgDate(statChgDate);

            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBeHistory FOUND for statChgDate [ " + statChgDate + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getChosenSupplierBeHistoryByStatus(@PathVariable String status) {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findByStatus(status);

            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory FOUND for status [ " + status + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBeHistory FOUND for status [ " + status + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeHistoryByApprovedByUserId/{approvedByUserId}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getChosenSupplierBeHistoryByApprovedByUserId(@PathVariable Integer approvedByUserId) {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findByApprovedByUserId(approvedByUserId);

            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory FOUND for approvedByUserId [ " + approvedByUserId + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBeHistory FOUND for approvedByUserId [ " + approvedByUserId + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeHistoryByProcurementId/{procurementId}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getChosenSupplierBeHistoryByProcurementId(@PathVariable BigInteger procurementId) {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findByProcurementId(procurementId);

            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory FOUND for procurementId [ " + procurementId + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBeHistory FOUND for procurementId [ " + procurementId + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeHistoryByProcurementSupplierId/{procurementSupplierId}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getChosenSupplierBeHistoryByProcurementSupplierId(@PathVariable BigInteger procurementSupplierId) {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findByProcurementSupplierId(procurementSupplierId);

            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory FOUND for procurementSupplierId [ " + procurementSupplierId + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBeHistory FOUND for procurementSupplierId [ " + procurementSupplierId + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "chosenSupplierBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<ChosenSupplierBeHistory>> getChosenSupplierBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<ChosenSupplierBeHistory> chosenSupplierBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            chosenSupplierBeHistoryList = chosenSupplierBeHistoryRepo.findByRevinfo(revinfo);

            if (chosenSupplierBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ChosenSupplierBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ChosenSupplierBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + chosenSupplierBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(chosenSupplierBeHistoryList,
                HttpStatus.OK);
    }



}