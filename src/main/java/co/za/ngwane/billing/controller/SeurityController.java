package co.za.ngwane.billing.controller;

//package co.za.rtmc.backoffice.web.controller;
//
//import co.za.rtmc.backoffice.model.User;
//import co.za.rtmc.backoffice.persist.db.CrUser;
//import co.za.rtmc.backoffice.persist.repository.TokenRepo;
//import co.za.rtmc.backoffice.security.SecurityUtils;
//import io.swagger.annotations.Api;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
//@RestController
//@Api(description = "Users management API")
//public class SeurityController {
//
//
//    @Autowired
//    private UserRepo userRepo;
//
//
//    @Autowired
//    private TokenRepo tokenRepo;
//
//    @RequestMapping(value = "/security/account", method = RequestMethod.GET)
//    public @ResponseBody
//    User getUserAccount()  {
//        User user =new  User();
//        user.setLogin(SecurityUtils.getCurrentLogin());
//        user.setPassword(null);
//        return user;
//    }
//
//
//    @PreAuthorize("hasAuthority('admin')")
//    @RequestMapping(value = "/security/tokens", method = RequestMethod.GET)
//    public @ResponseBody
//    List<CrToken> getTokens () {
//        List<CrToken> tokens = tokenRepo.findAll();
//        for(CrToken t:tokens) {
//            t.setSeries(null);
//            t.setValue(null);
//        }
//        return tokens;
//    }
//
//
//
//}
