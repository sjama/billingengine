/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.ProcurementBe;
import co.za.ngwane.billing.db.ProcurementSupplierBe;
import co.za.ngwane.billing.db.SuppliersBe;
import co.za.ngwane.billing.repository.ProcurementBeRepo;
import co.za.ngwane.billing.repository.ProcurementSupplierBeRepo;
import co.za.ngwane.billing.repository.SuppliersBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/procurementSupplierBe")
public class ProcurementSupplierBeController {
private static final Logger logger = Logger.getLogger(ProcurementSupplierBeController.class);

@Autowired
private ProcurementSupplierBeRepo procurementSupplierBeRepo;
@Autowired
private ProcurementBeRepo procurementBeRepo;
@Autowired
private SuppliersBeRepo suppliersBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ProcurementSupplierBe> save(
            @RequestBody ProcurementSupplierBe procurementSupplierBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementSupplierBe = procurementSupplierBeRepo.save(procurementSupplierBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ProcurementSupplierBe> update(
            @RequestBody ProcurementSupplierBe procurementSupplierBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementSupplierBe = procurementSupplierBeRepo.save(procurementSupplierBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ProcurementSupplierBe> delete(
            @RequestBody ProcurementSupplierBe procurementSupplierBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            procurementSupplierBe = procurementSupplierBeRepo.save(procurementSupplierBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "procurementSupplierBe", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBe>> getAllProcurementSupplierBe() {
        List<ProcurementSupplierBe> procurementSupplierBeList = new ArrayList<>();
        try {
            procurementSupplierBeList = procurementSupplierBeRepo.findAll();
            if (procurementSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBe List FOUND Query returned [" + procurementSupplierBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ProcurementSupplierBe List FOUND Query returned [" + procurementSupplierBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeBySupplierAmount/{supplierAmount}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBe>> getProcurementSupplierBeBySupplierAmount(@PathVariable Float supplierAmount) {
        List<ProcurementSupplierBe> procurementSupplierBeList = new ArrayList<>();
        try {
            procurementSupplierBeList = procurementSupplierBeRepo.findBySupplierAmount(supplierAmount);

            if (procurementSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBe FOUND for supplierAmount [ " + supplierAmount + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBe FOUND for supplierAmount [ " + supplierAmount + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeByNoOfDays/{noOfDays}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBe>> getProcurementSupplierBeByNoOfDays(@PathVariable Integer noOfDays) {
        List<ProcurementSupplierBe> procurementSupplierBeList = new ArrayList<>();
        try {
            procurementSupplierBeList = procurementSupplierBeRepo.findByNoOfDays(noOfDays);

            if (procurementSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBe FOUND for noOfDays [ " + noOfDays + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBe FOUND for noOfDays [ " + noOfDays + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBe>> getProcurementSupplierBeByRefNo(@PathVariable String refNo) {
        List<ProcurementSupplierBe> procurementSupplierBeList = new ArrayList<>();
        try {
            procurementSupplierBeList = procurementSupplierBeRepo.findByRefNo(refNo);

            if (procurementSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBe FOUND for refNo [ " + refNo + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBe FOUND for refNo [ " + refNo + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBe>> getProcurementSupplierBeByCreatedDate(@PathVariable Date createdDate) {
        List<ProcurementSupplierBe> procurementSupplierBeList = new ArrayList<>();
        try {
            procurementSupplierBeList = procurementSupplierBeRepo.findByCreatedDate(createdDate);

            if (procurementSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBe FOUND for createdDate [ " + createdDate + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBe FOUND for createdDate [ " + createdDate + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBe>> getProcurementSupplierBeByStatus(@PathVariable String status) {
        List<ProcurementSupplierBe> procurementSupplierBeList = new ArrayList<>();
        try {
            procurementSupplierBeList = procurementSupplierBeRepo.findByStatus(status);

            if (procurementSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBe FOUND for status [ " + status + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBe FOUND for status [ " + status + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeByStatChgDate/{statChgDate}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBe>> getProcurementSupplierBeByStatChgDate(@PathVariable Date statChgDate) {
        List<ProcurementSupplierBe> procurementSupplierBeList = new ArrayList<>();
        try {
            procurementSupplierBeList = procurementSupplierBeRepo.findByStatChgDate(statChgDate);

            if (procurementSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBe FOUND for statChgDate [ " + statChgDate + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeByProcurementId/{procurementId}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBe>> getProcurementSupplierBeByProcurementId(@PathVariable String procurementId) {
        List<ProcurementSupplierBe> procurementSupplierBeList = new ArrayList<>();
        try {
            ProcurementBe procurementBe = procurementBeRepo.findOne(new Long(procurementId));
            procurementSupplierBeList = procurementSupplierBeRepo.findByProcurementId(procurementBe);

            if (procurementSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBe FOUND for procurementId [ " + procurementId + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBe FOUND for procurementId [ " + procurementId + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "procurementSupplierBeBySuppliersId/{suppliersId}", method = RequestMethod.GET)
    public ResponseEntity<List<ProcurementSupplierBe>> getProcurementSupplierBeBySuppliersId(@PathVariable String suppliersId) {
        List<ProcurementSupplierBe> procurementSupplierBeList = new ArrayList<>();
        try {
            SuppliersBe suppliersBe = suppliersBeRepo.findOne(new Long(suppliersId));
            procurementSupplierBeList = procurementSupplierBeRepo.findBySuppliersId(suppliersBe);

            if (procurementSupplierBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ProcurementSupplierBe FOUND for suppliersId [ " + suppliersId + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ProcurementSupplierBe FOUND for suppliersId [ " + suppliersId + " ] query returned [" + procurementSupplierBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(procurementSupplierBeList,
                HttpStatus.OK);
    }



}