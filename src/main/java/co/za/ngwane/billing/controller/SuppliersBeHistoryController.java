/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.db.SuppliersBeHistory;
import co.za.ngwane.billing.repository.RevinfoRepo;
import co.za.ngwane.billing.repository.SuppliersBeHistoryRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/suppliersBeHistory")
public class SuppliersBeHistoryController {
private static final Logger logger = Logger.getLogger(SuppliersBeHistoryController.class);

@Autowired
private SuppliersBeHistoryRepo suppliersBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<SuppliersBeHistory> save(
            @RequestBody SuppliersBeHistory suppliersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            suppliersBeHistory = suppliersBeHistoryRepo.save(suppliersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<SuppliersBeHistory> update(
            @RequestBody SuppliersBeHistory suppliersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            suppliersBeHistory = suppliersBeHistoryRepo.save(suppliersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<SuppliersBeHistory> delete(
            @RequestBody SuppliersBeHistory suppliersBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            suppliersBeHistory = suppliersBeHistoryRepo.save(suppliersBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "suppliersBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getAllSuppliersBeHistory() {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findAll();
            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory List FOUND Query returned [" + suppliersBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "SuppliersBeHistory List FOUND Query returned [" + suppliersBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByRevtype(@PathVariable Short revtype) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByRevtype(revtype);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByAddressId/{addressId}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByAddressId(@PathVariable BigInteger addressId) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByAddressId(addressId);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for addressId [ " + addressId + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for addressId [ " + addressId + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByBankDetId/{bankDetId}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByBankDetId(@PathVariable Integer bankDetId) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByBankDetId(bankDetId);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for bankDetId [ " + bankDetId + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for bankDetId [ " + bankDetId + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByComName/{comName}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByComName(@PathVariable String comName) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByComName(comName);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for comName [ " + comName + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for comName [ " + comName + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByContactPersonId/{contactPersonId}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByContactPersonId(@PathVariable BigInteger contactPersonId) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByContactPersonId(contactPersonId);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for contactPersonId [ " + contactPersonId + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for contactPersonId [ " + contactPersonId + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByRefNo(@PathVariable String refNo) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByRefNo(refNo);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByRegNo/{regNo}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByRegNo(@PathVariable String regNo) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByRegNo(regNo);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for regNo [ " + regNo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for regNo [ " + regNo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByStatChgD(@PathVariable Date statChgD) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByStatChgD(statChgD);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for statChgD [ " + statChgD + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByStatus(@PathVariable String status) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByStatus(status);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for status [ " + status + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for status [ " + status + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryBySupplierNo/{supplierNo}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryBySupplierNo(@PathVariable String supplierNo) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findBySupplierNo(supplierNo);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for supplierNo [ " + supplierNo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for supplierNo [ " + supplierNo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByTradeName/{tradeName}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByTradeName(@PathVariable String tradeName) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByTradeName(tradeName);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for tradeName [ " + tradeName + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for tradeName [ " + tradeName + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByVatRegNo/{vatRegNo}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByVatRegNo(@PathVariable String vatRegNo) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByVatRegNo(vatRegNo);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for vatRegNo [ " + vatRegNo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for vatRegNo [ " + vatRegNo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "suppliersBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<SuppliersBeHistory>> getSuppliersBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<SuppliersBeHistory> suppliersBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            suppliersBeHistoryList = suppliersBeHistoryRepo.findByRevinfo(revinfo);

            if (suppliersBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO SuppliersBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "SuppliersBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + suppliersBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(suppliersBeHistoryList,
                HttpStatus.OK);
    }



}