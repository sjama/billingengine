/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.DeliveryNoteBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.DeliveryNoteBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/deliveryNoteBeHistory")
public class DeliveryNoteBeHistoryController {
private static final Logger logger = Logger.getLogger(DeliveryNoteBeHistoryController.class);

@Autowired
private DeliveryNoteBeHistoryRepo deliveryNoteBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<DeliveryNoteBeHistory> save(
            @RequestBody DeliveryNoteBeHistory deliveryNoteBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            deliveryNoteBeHistory = deliveryNoteBeHistoryRepo.save(deliveryNoteBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<DeliveryNoteBeHistory> update(
            @RequestBody DeliveryNoteBeHistory deliveryNoteBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            deliveryNoteBeHistory = deliveryNoteBeHistoryRepo.save(deliveryNoteBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<DeliveryNoteBeHistory> delete(
            @RequestBody DeliveryNoteBeHistory deliveryNoteBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            deliveryNoteBeHistory = deliveryNoteBeHistoryRepo.save(deliveryNoteBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "deliveryNoteBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBeHistory>> getAllDeliveryNoteBeHistory() {
        List<DeliveryNoteBeHistory> deliveryNoteBeHistoryList = new ArrayList<>();
        try {
            deliveryNoteBeHistoryList = deliveryNoteBeHistoryRepo.findAll();
            if (deliveryNoteBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBeHistory List FOUND Query returned [" + deliveryNoteBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "DeliveryNoteBeHistory List FOUND Query returned [" + deliveryNoteBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBeHistory>> getDeliveryNoteBeHistoryByRevtype(@PathVariable Short revtype) {
        List<DeliveryNoteBeHistory> deliveryNoteBeHistoryList = new ArrayList<>();
        try {
            deliveryNoteBeHistoryList = deliveryNoteBeHistoryRepo.findByRevtype(revtype);

            if (deliveryNoteBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeHistoryByCreatedDate/{createdDate}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBeHistory>> getDeliveryNoteBeHistoryByCreatedDate(@PathVariable Date createdDate) {
        List<DeliveryNoteBeHistory> deliveryNoteBeHistoryList = new ArrayList<>();
        try {
            deliveryNoteBeHistoryList = deliveryNoteBeHistoryRepo.findByCreatedDate(createdDate);

            if (deliveryNoteBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBeHistory FOUND for createdDate [ " + createdDate + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeHistoryByRefNo/{refNo}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBeHistory>> getDeliveryNoteBeHistoryByRefNo(@PathVariable String refNo) {
        List<DeliveryNoteBeHistory> deliveryNoteBeHistoryList = new ArrayList<>();
        try {
            deliveryNoteBeHistoryList = deliveryNoteBeHistoryRepo.findByRefNo(refNo);

            if (deliveryNoteBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBeHistory FOUND for refNo [ " + refNo + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeHistoryByCreatedByUserId/{createdByUserId}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBeHistory>> getDeliveryNoteBeHistoryByCreatedByUserId(@PathVariable Integer createdByUserId) {
        List<DeliveryNoteBeHistory> deliveryNoteBeHistoryList = new ArrayList<>();
        try {
            deliveryNoteBeHistoryList = deliveryNoteBeHistoryRepo.findByCreatedByUserId(createdByUserId);

            if (deliveryNoteBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBeHistory FOUND for createdByUserId [ " + createdByUserId + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBeHistory FOUND for createdByUserId [ " + createdByUserId + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeHistoryByPurchaseOrderId/{purchaseOrderId}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBeHistory>> getDeliveryNoteBeHistoryByPurchaseOrderId(@PathVariable BigInteger purchaseOrderId) {
        List<DeliveryNoteBeHistory> deliveryNoteBeHistoryList = new ArrayList<>();
        try {
            deliveryNoteBeHistoryList = deliveryNoteBeHistoryRepo.findByPurchaseOrderId(purchaseOrderId);

            if (deliveryNoteBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBeHistory FOUND for purchaseOrderId [ " + purchaseOrderId + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBeHistory FOUND for purchaseOrderId [ " + purchaseOrderId + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "deliveryNoteBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<DeliveryNoteBeHistory>> getDeliveryNoteBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<DeliveryNoteBeHistory> deliveryNoteBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            deliveryNoteBeHistoryList = deliveryNoteBeHistoryRepo.findByRevinfo(revinfo);

            if (deliveryNoteBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO DeliveryNoteBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "DeliveryNoteBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + deliveryNoteBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(deliveryNoteBeHistoryList,
                HttpStatus.OK);
    }



}