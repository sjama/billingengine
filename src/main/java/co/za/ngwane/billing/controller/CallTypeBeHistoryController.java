/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.CallTypeBeHistory;
import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.repository.CallTypeBeHistoryRepo;
import co.za.ngwane.billing.repository.RevinfoRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/callTypeBeHistory")
public class CallTypeBeHistoryController {
private static final Logger logger = Logger.getLogger(CallTypeBeHistoryController.class);

@Autowired
private CallTypeBeHistoryRepo callTypeBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<CallTypeBeHistory> save(
            @RequestBody CallTypeBeHistory callTypeBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            callTypeBeHistory = callTypeBeHistoryRepo.save(callTypeBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(callTypeBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<CallTypeBeHistory> update(
            @RequestBody CallTypeBeHistory callTypeBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            callTypeBeHistory = callTypeBeHistoryRepo.save(callTypeBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(callTypeBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<CallTypeBeHistory> delete(
            @RequestBody CallTypeBeHistory callTypeBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            callTypeBeHistory = callTypeBeHistoryRepo.save(callTypeBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(callTypeBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "callTypeBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<CallTypeBeHistory>> getAllCallTypeBeHistory() {
        List<CallTypeBeHistory> callTypeBeHistoryList = new ArrayList<>();
        try {
            callTypeBeHistoryList = callTypeBeHistoryRepo.findAll();
            if (callTypeBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CallTypeBeHistory List FOUND Query returned [" + callTypeBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "CallTypeBeHistory List FOUND Query returned [" + callTypeBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(callTypeBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "callTypeBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<CallTypeBeHistory>> getCallTypeBeHistoryByRevtype(@PathVariable Short revtype) {
        List<CallTypeBeHistory> callTypeBeHistoryList = new ArrayList<>();
        try {
            callTypeBeHistoryList = callTypeBeHistoryRepo.findByRevtype(revtype);

            if (callTypeBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CallTypeBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + callTypeBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CallTypeBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + callTypeBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(callTypeBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "callTypeBeHistoryByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<CallTypeBeHistory>> getCallTypeBeHistoryByDescription(@PathVariable String description) {
        List<CallTypeBeHistory> callTypeBeHistoryList = new ArrayList<>();
        try {
            callTypeBeHistoryList = callTypeBeHistoryRepo.findByDescription(description);

            if (callTypeBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CallTypeBeHistory FOUND for description [ " + description + " ] query returned [" + callTypeBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CallTypeBeHistory FOUND for description [ " + description + " ] query returned [" + callTypeBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(callTypeBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "callTypeBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<CallTypeBeHistory>> getCallTypeBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<CallTypeBeHistory> callTypeBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            callTypeBeHistoryList = callTypeBeHistoryRepo.findByRevinfo(revinfo);

            if (callTypeBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO CallTypeBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + callTypeBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "CallTypeBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + callTypeBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(callTypeBeHistoryList,
                HttpStatus.OK);
    }



}