/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.db.ServiceProviderProductsBeHistory;
import co.za.ngwane.billing.repository.RevinfoRepo;
import co.za.ngwane.billing.repository.ServiceProviderProductsBeHistoryRepo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/serviceProviderProductsBeHistory")
public class ServiceProviderProductsBeHistoryController {
private static final Logger logger = Logger.getLogger(ServiceProviderProductsBeHistoryController.class);

@Autowired
private ServiceProviderProductsBeHistoryRepo serviceProviderProductsBeHistoryRepo;
@Autowired
private RevinfoRepo revinfoRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<ServiceProviderProductsBeHistory> save(
            @RequestBody ServiceProviderProductsBeHistory serviceProviderProductsBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            serviceProviderProductsBeHistory = serviceProviderProductsBeHistoryRepo.save(serviceProviderProductsBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(serviceProviderProductsBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<ServiceProviderProductsBeHistory> update(
            @RequestBody ServiceProviderProductsBeHistory serviceProviderProductsBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            serviceProviderProductsBeHistory = serviceProviderProductsBeHistoryRepo.save(serviceProviderProductsBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(serviceProviderProductsBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<ServiceProviderProductsBeHistory> delete(
            @RequestBody ServiceProviderProductsBeHistory serviceProviderProductsBeHistory ,
            UriComponentsBuilder ucBuilder) {
        try {
            serviceProviderProductsBeHistory = serviceProviderProductsBeHistoryRepo.save(serviceProviderProductsBeHistory);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(serviceProviderProductsBeHistory, HttpStatus.CREATED);
    }

@RequestMapping(value = "serviceProviderProductsBeHistory", method = RequestMethod.GET)
    public ResponseEntity<List<ServiceProviderProductsBeHistory>> getAllServiceProviderProductsBeHistory() {
        List<ServiceProviderProductsBeHistory> serviceProviderProductsBeHistoryList = new ArrayList<>();
        try {
            serviceProviderProductsBeHistoryList = serviceProviderProductsBeHistoryRepo.findAll();
            if (serviceProviderProductsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ServiceProviderProductsBeHistory List FOUND Query returned [" + serviceProviderProductsBeHistoryList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "ServiceProviderProductsBeHistory List FOUND Query returned [" + serviceProviderProductsBeHistoryList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(serviceProviderProductsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "serviceProviderProductsBeHistoryByRevtype/{revtype}", method = RequestMethod.GET)
    public ResponseEntity<List<ServiceProviderProductsBeHistory>> getServiceProviderProductsBeHistoryByRevtype(@PathVariable Short revtype) {
        List<ServiceProviderProductsBeHistory> serviceProviderProductsBeHistoryList = new ArrayList<>();
        try {
            serviceProviderProductsBeHistoryList = serviceProviderProductsBeHistoryRepo.findByRevtype(revtype);

            if (serviceProviderProductsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ServiceProviderProductsBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ServiceProviderProductsBeHistory FOUND for revtype [ " + revtype + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(serviceProviderProductsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "serviceProviderProductsBeHistoryByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<ServiceProviderProductsBeHistory>> getServiceProviderProductsBeHistoryByDescription(@PathVariable String description) {
        List<ServiceProviderProductsBeHistory> serviceProviderProductsBeHistoryList = new ArrayList<>();
        try {
            serviceProviderProductsBeHistoryList = serviceProviderProductsBeHistoryRepo.findByDescription(description);

            if (serviceProviderProductsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ServiceProviderProductsBeHistory FOUND for description [ " + description + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ServiceProviderProductsBeHistory FOUND for description [ " + description + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(serviceProviderProductsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "serviceProviderProductsBeHistoryByPriceAmount/{priceAmount}", method = RequestMethod.GET)
    public ResponseEntity<List<ServiceProviderProductsBeHistory>> getServiceProviderProductsBeHistoryByPriceAmount(@PathVariable Float priceAmount) {
        List<ServiceProviderProductsBeHistory> serviceProviderProductsBeHistoryList = new ArrayList<>();
        try {
            serviceProviderProductsBeHistoryList = serviceProviderProductsBeHistoryRepo.findByPriceAmount(priceAmount);

            if (serviceProviderProductsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ServiceProviderProductsBeHistory FOUND for priceAmount [ " + priceAmount + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ServiceProviderProductsBeHistory FOUND for priceAmount [ " + priceAmount + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(serviceProviderProductsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "serviceProviderProductsBeHistoryByLuServiceProviderBecode/{luServiceProviderBecode}", method = RequestMethod.GET)
    public ResponseEntity<List<ServiceProviderProductsBeHistory>> getServiceProviderProductsBeHistoryByLuServiceProviderBecode(@PathVariable String luServiceProviderBecode) {
        List<ServiceProviderProductsBeHistory> serviceProviderProductsBeHistoryList = new ArrayList<>();
        try {
            serviceProviderProductsBeHistoryList = serviceProviderProductsBeHistoryRepo.findByLuServiceProviderBecode(luServiceProviderBecode);

            if (serviceProviderProductsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ServiceProviderProductsBeHistory FOUND for luServiceProviderBecode [ " + luServiceProviderBecode + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ServiceProviderProductsBeHistory FOUND for luServiceProviderBecode [ " + luServiceProviderBecode + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(serviceProviderProductsBeHistoryList,
                HttpStatus.OK);
    }

@RequestMapping(value = "serviceProviderProductsBeHistoryByRevinfo/{revinfo}", method = RequestMethod.GET)
    public ResponseEntity<List<ServiceProviderProductsBeHistory>> getServiceProviderProductsBeHistoryByRevinfo(@PathVariable String revInfo) {
        List<ServiceProviderProductsBeHistory> serviceProviderProductsBeHistoryList = new ArrayList<>();
        try {
            Revinfo revinfo = revinfoRepo.findOne(new Integer(revInfo));
            serviceProviderProductsBeHistoryList = serviceProviderProductsBeHistoryRepo.findByRevinfo(revinfo);

            if (serviceProviderProductsBeHistoryList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO ServiceProviderProductsBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "ServiceProviderProductsBeHistory FOUND for revinfo [ " + revinfo + " ] query returned [" + serviceProviderProductsBeHistoryList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(serviceProviderProductsBeHistoryList,
                HttpStatus.OK);
    }



}