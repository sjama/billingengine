/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

import co.za.ngwane.billing.db.LastNoBe;
import co.za.ngwane.billing.repository.LastNoBeRepo;
import co.za.ngwane.billing.db.LuLastNoTypeBe;
import co.za.ngwane.billing.repository.LuLastNoTypeBeRepo;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/lastNoBe")
public class LastNoBeController {
    private static final Logger logger = Logger.getLogger(LastNoBeController.class);

    @Autowired
    private LastNoBeRepo lastNoBeRepo;
    @Autowired
    private LuLastNoTypeBeRepo luLastNoTypeBeRepo;

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<LastNoBe> save(
            @RequestBody LastNoBe lastNoBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            lastNoBe = lastNoBeRepo.save(lastNoBe);
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(lastNoBe, HttpStatus.CREATED);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<LastNoBe> update(
            @RequestBody LastNoBe lastNoBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            lastNoBe = lastNoBeRepo.save(lastNoBe);
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(lastNoBe, HttpStatus.CREATED);
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<LastNoBe> delete(
            @RequestBody LastNoBe lastNoBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            lastNoBe = lastNoBeRepo.save(lastNoBe);
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(lastNoBe, HttpStatus.CREATED);
    }

    @RequestMapping(value = "lastNoBe", method = RequestMethod.GET)
    public ResponseEntity<List<LastNoBe>> getAllLastNoBe() {
        List<LastNoBe> lastNoBeList = new ArrayList<>();
        try {
            lastNoBeList = lastNoBeRepo.findAll();
            if (lastNoBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LastNoBe List FOUND Query returned [" + lastNoBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LastNoBe List FOUND Query returned [" + lastNoBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(lastNoBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "lastNoBeByNo/{no}", method = RequestMethod.GET)
    public ResponseEntity<List<LastNoBe>> getLastNoBeByNo(@PathVariable long no) {
        List<LastNoBe> lastNoBeList = new ArrayList<>();
        try {
            lastNoBeList = lastNoBeRepo.findByNo(no);

            if (lastNoBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LastNoBe FOUND for no [ " + no + " ] query returned [" + lastNoBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LastNoBe FOUND for no [ " + no + " ] query returned [" + lastNoBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(lastNoBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "lastNoBeByStatus/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<LastNoBe>> getLastNoBeByStatus(@PathVariable String status) {
        List<LastNoBe> lastNoBeList = new ArrayList<>();
        try {
            lastNoBeList = lastNoBeRepo.findByStatus(status);

            if (lastNoBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LastNoBe FOUND for status [ " + status + " ] query returned [" + lastNoBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LastNoBe FOUND for status [ " + status + " ] query returned [" + lastNoBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(lastNoBeList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "lastNoBeByStatChgD/{statChgD}", method = RequestMethod.GET)
    public ResponseEntity<List<LastNoBe>> getLastNoBeByStatChgD(@PathVariable Date statChgD) {
        List<LastNoBe> lastNoBeList = new ArrayList<>();
        try {
            lastNoBeList = lastNoBeRepo.findByStatChgD(statChgD);

            if (lastNoBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LastNoBe FOUND for statChgD [ " + statChgD + " ] query returned [" + lastNoBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LastNoBe FOUND for statChgD [ " + statChgD + " ] query returned [" + lastNoBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(lastNoBeList,
                HttpStatus.OK);
    }


}