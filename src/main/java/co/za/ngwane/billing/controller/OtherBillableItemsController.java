/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.constant.BillingConstants;
import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.OtherBillItemsBe;
import co.za.ngwane.billing.repository.CustomerRepo;
import co.za.ngwane.billing.repository.OtherBillableItemsRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/otherBillableItemsController/")
public class OtherBillableItemsController {

    private static final Logger logger = Logger.getLogger(OtherBillableItemsController.class);

    @Autowired
    public OtherBillableItemsRepo otherBillableItemsRepo;

    @Autowired
    public CustomerRepo customerRepo;

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<OtherBillItemsBe> add(
            @RequestBody OtherBillItemsBe otherBillItemsBe,
            UriComponentsBuilder ucBuilder) {
        try {
            logger.info(
                    "===============================================================\n"
                            + "Other Billable Items we are in TAG [" + otherBillItemsBe.getTagName() + "] AT [" + new Date() + "].\n"
                            + "\n===============================================================");

            switch (otherBillItemsBe.getTagName()) {
                case BillingConstants.TAG_ADD:
                    otherBillItemsBe.setStatus(BillingConstants.LU_STAT_ACTIVE);
                    otherBillItemsBe.setStatChgD(new Date());
                    break;

                case BillingConstants.TAG_UPDATE:
                    break;

                case BillingConstants.TAG_DELETE:
                    otherBillItemsBe.setStatus(BillingConstants.LU_STAT_DELETED);
                    otherBillItemsBe.setStatChgD(new Date());
                    break;

                default:
                    logger.info(
                            "===============================================================\n"
                                    + "No case specified for Other Billable Item with LuBillableItemCd().getDescription()" +
                                    " [" + otherBillItemsBe.getLuBillableItemCd().getDescription() + "] with "
                                    + "TAG [" + otherBillItemsBe.getTagName() + " ] AT [" + new Date() + " ].\n"
                                    + "\n===============================================================");
            }

            otherBillableItemsRepo.save(otherBillItemsBe);

            logger.info(
                    "===============================================================\n"
                            + "Other Billable Item with LuBillableItemCd().getDescription()" +
                            " [" + otherBillItemsBe.getLuBillableItemCd().getDescription() + "] with "
                            + "TAG [" + otherBillItemsBe.getTagName() + "] saved successful.\n"
                            + "\n===============================================================");

        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }

        return new ResponseEntity<>(otherBillItemsBe, HttpStatus.CREATED);
    }

    @RequestMapping(value = "billItems/{customerId}", method = RequestMethod.GET)
    public ResponseEntity<List<OtherBillItemsBe>> getByCustomer(@PathVariable Long customerId) {
        List<OtherBillItemsBe> otherBillItemsList = new ArrayList<>();
        try {
            CustomersBe customersBe = customerRepo.findOne(customerId);
            otherBillItemsList = otherBillableItemsRepo.findByCustomersId(customersBe);
            if (otherBillItemsList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO Other Billable Items FOUND Get All Customers returned [" + otherBillItemsList.size() + "] ORIGNATING LINES.\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + "Customers Other Billable Items LINES  FOUND  [" + otherBillItemsList.size() + "] ORIGINATING LINES.\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(otherBillItemsList,
                HttpStatus.OK);
    }

    @RequestMapping(value = "billItems", method = RequestMethod.GET)
    public ResponseEntity<List<OtherBillItemsBe>> getAllActive() {
        List<OtherBillItemsBe> otherBillItemsList = new ArrayList<>();
        try {
            otherBillItemsList = otherBillableItemsRepo.findAll();
            if (otherBillItemsList.isEmpty()) {
                logger.info(
                        "===============================================================\n"
                                + "NO Other Billable Items FOUND  returned [" + otherBillItemsList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "===============================================================\n"
                                + " Other Billable Items  FOUND  [" + otherBillItemsList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(otherBillItemsList,
                HttpStatus.OK);
    }
}
