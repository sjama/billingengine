/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.LuOrderProcessStatBe;
import co.za.ngwane.billing.repository.LuOrderProcessStatBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/luOrderProcessStatBe")
public class LuOrderProcessStatBeController {
private static final Logger logger = Logger.getLogger(LuOrderProcessStatBeController.class);

@Autowired
private LuOrderProcessStatBeRepo luOrderProcessStatBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<LuOrderProcessStatBe> save(
            @RequestBody LuOrderProcessStatBe luOrderProcessStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luOrderProcessStatBe = luOrderProcessStatBeRepo.save(luOrderProcessStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luOrderProcessStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<LuOrderProcessStatBe> update(
            @RequestBody LuOrderProcessStatBe luOrderProcessStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luOrderProcessStatBe = luOrderProcessStatBeRepo.save(luOrderProcessStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luOrderProcessStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<LuOrderProcessStatBe> delete(
            @RequestBody LuOrderProcessStatBe luOrderProcessStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luOrderProcessStatBe = luOrderProcessStatBeRepo.save(luOrderProcessStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luOrderProcessStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "luOrderProcessStatBe", method = RequestMethod.GET)
    public ResponseEntity<List<LuOrderProcessStatBe>> getAllLuOrderProcessStatBe() {
        List<LuOrderProcessStatBe> luOrderProcessStatBeList = new ArrayList<>();
        try {
            luOrderProcessStatBeList = luOrderProcessStatBeRepo.findAll();
            if (luOrderProcessStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuOrderProcessStatBe List FOUND Query returned [" + luOrderProcessStatBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "LuOrderProcessStatBe List FOUND Query returned [" + luOrderProcessStatBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luOrderProcessStatBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "luOrderProcessStatBeByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<LuOrderProcessStatBe>> getLuOrderProcessStatBeByDescription(@PathVariable String description) {
        List<LuOrderProcessStatBe> luOrderProcessStatBeList = new ArrayList<>();
        try {
            luOrderProcessStatBeList = luOrderProcessStatBeRepo.findByDescription(description);

            if (luOrderProcessStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuOrderProcessStatBe FOUND for description [ " + description + " ] query returned [" + luOrderProcessStatBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LuOrderProcessStatBe FOUND for description [ " + description + " ] query returned [" + luOrderProcessStatBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luOrderProcessStatBeList,
                HttpStatus.OK);
    }

}