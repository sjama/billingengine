/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.LuPayRequisitionProcessesStatBe;
import co.za.ngwane.billing.repository.LuPayRequisitionProcessesStatBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/luPayRequisitionProcessesStatBe")
public class LuPayRequisitionProcessesStatBeController {
private static final Logger logger = Logger.getLogger(LuPayRequisitionProcessesStatBeController.class);

@Autowired
private LuPayRequisitionProcessesStatBeRepo luPayRequisitionProcessesStatBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<LuPayRequisitionProcessesStatBe> save(
            @RequestBody LuPayRequisitionProcessesStatBe luPayRequisitionProcessesStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luPayRequisitionProcessesStatBe = luPayRequisitionProcessesStatBeRepo.save(luPayRequisitionProcessesStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPayRequisitionProcessesStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<LuPayRequisitionProcessesStatBe> update(
            @RequestBody LuPayRequisitionProcessesStatBe luPayRequisitionProcessesStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luPayRequisitionProcessesStatBe = luPayRequisitionProcessesStatBeRepo.save(luPayRequisitionProcessesStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPayRequisitionProcessesStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<LuPayRequisitionProcessesStatBe> delete(
            @RequestBody LuPayRequisitionProcessesStatBe luPayRequisitionProcessesStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luPayRequisitionProcessesStatBe = luPayRequisitionProcessesStatBeRepo.save(luPayRequisitionProcessesStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPayRequisitionProcessesStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "luPayRequisitionProcessesStatBe", method = RequestMethod.GET)
    public ResponseEntity<List<LuPayRequisitionProcessesStatBe>> getAllLuPayRequisitionProcessesStatBe() {
        List<LuPayRequisitionProcessesStatBe> luPayRequisitionProcessesStatBeList = new ArrayList<>();
        try {
            luPayRequisitionProcessesStatBeList = luPayRequisitionProcessesStatBeRepo.findAll();
            if (luPayRequisitionProcessesStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuPayRequisitionProcessesStatBe List FOUND Query returned [" + luPayRequisitionProcessesStatBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "LuPayRequisitionProcessesStatBe List FOUND Query returned [" + luPayRequisitionProcessesStatBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPayRequisitionProcessesStatBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "luPayRequisitionProcessesStatBeByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<LuPayRequisitionProcessesStatBe>> getLuPayRequisitionProcessesStatBeByDescription(@PathVariable String description) {
        List<LuPayRequisitionProcessesStatBe> luPayRequisitionProcessesStatBeList = new ArrayList<>();
        try {
            luPayRequisitionProcessesStatBeList = luPayRequisitionProcessesStatBeRepo.findByDescription(description);

            if (luPayRequisitionProcessesStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuPayRequisitionProcessesStatBe FOUND for description [ " + description + " ] query returned [" + luPayRequisitionProcessesStatBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LuPayRequisitionProcessesStatBe FOUND for description [ " + description + " ] query returned [" + luPayRequisitionProcessesStatBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPayRequisitionProcessesStatBeList,
                HttpStatus.OK);
    }



}