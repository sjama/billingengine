/*
 * The BhekzinWay of doing things.
 */

package co.za.ngwane.billing.controller;

import co.za.ngwane.billing.db.LuPurchaseOrderStatBe;
import co.za.ngwane.billing.repository.LuPurchaseOrderStatBeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * @author bheki.lubisi
 * @Email bheki.lubisi@gmail.com
 * @Company NgwaneSolutions.com
 */
@RestController
@RequestMapping("/luPurchaseOrderStatBe")
public class LuPurchaseOrderStatBeController {
private static final Logger logger = Logger.getLogger(LuPurchaseOrderStatBeController.class);

@Autowired
private LuPurchaseOrderStatBeRepo luPurchaseOrderStatBeRepo;

@RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<LuPurchaseOrderStatBe> save(
            @RequestBody LuPurchaseOrderStatBe luPurchaseOrderStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luPurchaseOrderStatBe = luPurchaseOrderStatBeRepo.save(luPurchaseOrderStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPurchaseOrderStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<LuPurchaseOrderStatBe> update(
            @RequestBody LuPurchaseOrderStatBe luPurchaseOrderStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luPurchaseOrderStatBe = luPurchaseOrderStatBeRepo.save(luPurchaseOrderStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPurchaseOrderStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<LuPurchaseOrderStatBe> delete(
            @RequestBody LuPurchaseOrderStatBe luPurchaseOrderStatBe ,
            UriComponentsBuilder ucBuilder) {
        try {
            luPurchaseOrderStatBe = luPurchaseOrderStatBeRepo.save(luPurchaseOrderStatBe);
} catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPurchaseOrderStatBe, HttpStatus.CREATED);
    }

@RequestMapping(value = "luPurchaseOrderStatBe", method = RequestMethod.GET)
    public ResponseEntity<List<LuPurchaseOrderStatBe>> getAllLuPurchaseOrderStatBe() {
        List<LuPurchaseOrderStatBe> luPurchaseOrderStatBeList = new ArrayList<>();
        try {
            luPurchaseOrderStatBeList = luPurchaseOrderStatBeRepo.findAll();
            if (luPurchaseOrderStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuPurchaseOrderStatBe List FOUND Query returned [" + luPurchaseOrderStatBeList.size() + "] AT [ " + new Date() +"].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                 + "LuPurchaseOrderStatBe List FOUND Query returned [" + luPurchaseOrderStatBeList.size() + "] AT [" + new Date() +" ].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPurchaseOrderStatBeList,
                HttpStatus.OK);
    }

@RequestMapping(value = "luPurchaseOrderStatBeByDescription/{description}", method = RequestMethod.GET)
    public ResponseEntity<List<LuPurchaseOrderStatBe>> getLuPurchaseOrderStatBeByDescription(@PathVariable String description) {
        List<LuPurchaseOrderStatBe> luPurchaseOrderStatBeList = new ArrayList<>();
        try {
            luPurchaseOrderStatBeList = luPurchaseOrderStatBeRepo.findByDescription(description);

            if (luPurchaseOrderStatBeList.isEmpty()) {
                logger.info(
                        "\n===============================================================\n"
                                + "NO LuPurchaseOrderStatBe FOUND for description [ " + description + " ] query returned [" + luPurchaseOrderStatBeList.size() + "].\n"
                                + "\n===============================================================");
            } else {
                logger.info(
                        "\n===============================================================\n"
                                + "LuPurchaseOrderStatBe FOUND for description [ " + description + " ] query returned [" + luPurchaseOrderStatBeList.size() + "].\n"
                                + "\n===============================================================");
            }
        } catch (Exception e) {
            logger.info(
                    "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n"
                            + "Exception caught." + e.getMessage()
                            + "\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
            e.printStackTrace();
        }
        return new ResponseEntity<>(luPurchaseOrderStatBeList,
                HttpStatus.OK);
    }

}