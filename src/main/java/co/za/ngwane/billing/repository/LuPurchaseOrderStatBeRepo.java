/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.LuPurchaseOrderStatBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface LuPurchaseOrderStatBeRepo extends JpaRepository<LuPurchaseOrderStatBe, String> {

List<LuPurchaseOrderStatBe> findByCode(String code);

List<LuPurchaseOrderStatBe> findByDescription(String description);


}