/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.OriginatingLineBe;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bheki.lubisi
 */
public interface OriginatingLineRepo extends JpaRepository<OriginatingLineBe, String>{
	ArrayList<OriginatingLineBe> findBycustomersId(CustomersBe customersId);
        List<OriginatingLineBe> findByStatusNot(String stat);
    
}
