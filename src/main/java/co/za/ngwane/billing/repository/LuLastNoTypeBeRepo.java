/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import co.za.ngwane.billing.db.LuLastNoTypeBe;

public interface LuLastNoTypeBeRepo extends JpaRepository<LuLastNoTypeBe, String> {

List<LuLastNoTypeBe> findByCode(String code);

List<LuLastNoTypeBe> findByDescription(String description);

}