/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.OriginatingLineBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.OriginatingLineBeHistory;

public interface OriginatingLineBeHistoryRepo extends JpaRepository<OriginatingLineBeHistory, OriginatingLineBeHistoryPK> {

    List<OriginatingLineBeHistory> findByRevtype(Short revtype);

    List<OriginatingLineBeHistory> findByStatChgD(Date statChgD);

    List<OriginatingLineBeHistory> findByStatus(String status);

    List<OriginatingLineBeHistory> findByCustomersId(BigInteger customersId);

    List<OriginatingLineBeHistory> findByRevinfo(Revinfo revinfo);

}