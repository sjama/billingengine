/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository.service;

import co.za.ngwane.billing.db.SysPropBe;
import co.za.ngwane.billing.repository.BoSysPropRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jamasithole
 */
@Service
public class SysPropRepoService {

    @Autowired
    public BoSysPropRepo sysPropRepo;

    public SysPropBe save(SysPropBe boSysPropRepo) {
        return sysPropRepo.save(boSysPropRepo);
    }

}
