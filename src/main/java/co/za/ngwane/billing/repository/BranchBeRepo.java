package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.BranchBe;
import co.za.ngwane.billing.db.CustomersBe;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BranchBeRepo extends JpaRepository<BranchBe, String> {
    List<BranchBe> findByCustomersId(CustomersBe customersId);
    
}
