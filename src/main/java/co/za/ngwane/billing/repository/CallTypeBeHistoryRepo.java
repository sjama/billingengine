/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.CallTypeBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.CallTypeBeHistory;

public interface CallTypeBeHistoryRepo extends JpaRepository<CallTypeBeHistory, CallTypeBeHistoryPK> {

List<CallTypeBeHistory> findByRevtype(Short revtype);

List<CallTypeBeHistory> findByDescription(String description);

List<CallTypeBeHistory> findByRevinfo(Revinfo revinfo);

}