/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.CompanyBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface CompanyBeRepo extends JpaRepository<CompanyBe, Integer> {

List<CompanyBe> findById(Integer id);

List<CompanyBe> findByName(String name);

List<CompanyBe> findByPhyAddrLine1(String phyAddrLine1);

List<CompanyBe> findByPhyAddrLine2(String phyAddrLine2);

List<CompanyBe> findByPhyAddrLine3(String phyAddrLine3);

List<CompanyBe> findByPhyAddrSuburb(String phyAddrSuburb);

List<CompanyBe> findByPhyAddrCity(String phyAddrCity);

List<CompanyBe> findByPhyAddrCode(String phyAddrCode);

List<CompanyBe> findByPosAddrLine1(String posAddrLine1);

List<CompanyBe> findByPosAddrLine2(String posAddrLine2);

List<CompanyBe> findByPosAddrLine3(String posAddrLine3);

List<CompanyBe> findByPosAddrSuburb(String posAddrSuburb);

List<CompanyBe> findByPosAddrCity(String posAddrCity);

List<CompanyBe> findByPosAddrCode(String posAddrCode);

List<CompanyBe> findByTelNo(String telNo);

List<CompanyBe> findByEmail(String email);

List<CompanyBe> findByLogoRefn(String logoRefn);

List<CompanyBe> findByWebUrl(String webUrl);

List<CompanyBe> findByRegNo(String regNo);

List<CompanyBe> findByFaxNo(String faxNo);

List<CompanyBe> findByVatNo(String vatNo);

List<CompanyBe> findByBankAccNo(String bankAccNo);

List<CompanyBe> findByBankName(String bankName);

List<CompanyBe> findByBankBranch(String bankBranch);

List<CompanyBe> findByBankBranchCd(String bankBranchCd);

List<CompanyBe> findByStat(String stat);

List<CompanyBe> findByStatChgD(Date statChgD);

}