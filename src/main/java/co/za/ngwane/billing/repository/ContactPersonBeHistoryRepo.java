/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ContactPersonBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.ContactPersonBeHistory;

public interface ContactPersonBeHistoryRepo extends JpaRepository<ContactPersonBeHistory, ContactPersonBeHistoryPK> {

    List<ContactPersonBeHistory> findByRevtype(Short revtype);

    List<ContactPersonBeHistory> findByCellNo(String cellNo);

    List<ContactPersonBeHistory> findByEmail(String email);

    List<ContactPersonBeHistory> findByName(String name);

    List<ContactPersonBeHistory> findByPhoneNo(String phoneNo);

    List<ContactPersonBeHistory> findByStatChgDate(Date statChgDate);

    List<ContactPersonBeHistory> findByStatus(String status);

    List<ContactPersonBeHistory> findBySurname(String surname);

    List<ContactPersonBeHistory> findByRevinfo(Revinfo revinfo);

}