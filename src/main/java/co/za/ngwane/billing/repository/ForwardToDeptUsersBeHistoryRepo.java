/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ForwardToDeptUsersBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.ForwardToDeptUsersBeHistory;

public interface ForwardToDeptUsersBeHistoryRepo extends JpaRepository<ForwardToDeptUsersBeHistory, ForwardToDeptUsersBeHistoryPK> {

    List<ForwardToDeptUsersBeHistory> findByRevtype(Short revtype);

    List<ForwardToDeptUsersBeHistory> findByStatChgDate(Date statChgDate);

    List<ForwardToDeptUsersBeHistory> findByStatus(String status);

    List<ForwardToDeptUsersBeHistory> findByLuForwardToDeptCd(String luForwardToDeptCd);

    List<ForwardToDeptUsersBeHistory> findByUsersId(Integer usersId);

    List<ForwardToDeptUsersBeHistory> findByRevinfo(Revinfo revinfo);

}