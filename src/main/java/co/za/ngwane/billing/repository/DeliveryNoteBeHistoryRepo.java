/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.DeliveryNoteBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.DeliveryNoteBeHistory;

public interface DeliveryNoteBeHistoryRepo extends JpaRepository<DeliveryNoteBeHistory, DeliveryNoteBeHistoryPK> {

    List<DeliveryNoteBeHistory> findByRevtype(Short revtype);

    List<DeliveryNoteBeHistory> findByCreatedDate(Date createdDate);

    List<DeliveryNoteBeHistory> findByRefNo(String refNo);

    List<DeliveryNoteBeHistory> findByCreatedByUserId(Integer createdByUserId);

    List<DeliveryNoteBeHistory> findByPurchaseOrderId(BigInteger purchaseOrderId);

    List<DeliveryNoteBeHistory> findByRevinfo(Revinfo revinfo);

}