/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ProcurementBe;
import co.za.ngwane.billing.db.PurchaseOrderBe;
import co.za.ngwane.billing.db.UsersBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface PurchaseOrderBeRepo extends JpaRepository<PurchaseOrderBe, Long> {

List<PurchaseOrderBe> findById(Long id);

List<PurchaseOrderBe> findByPoNo(String poNo);

List<PurchaseOrderBe> findByRefNo(String refNo);

List<PurchaseOrderBe> findByStatus(String status);

List<PurchaseOrderBe> findByStatChgDate(Date statChgDate);

List<PurchaseOrderBe> findByGeneratedByUserId(UsersBe generatedByUserId);

List<PurchaseOrderBe> findByProcurementId(ProcurementBe procurementId);

}