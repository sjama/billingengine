/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;


import co.za.ngwane.billing.db.ForwardToDeptUsersBe;
import co.za.ngwane.billing.db.LuForwardToDeptBe;
import co.za.ngwane.billing.db.UsersBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ForwardToDeptUsersBeRepo extends JpaRepository<ForwardToDeptUsersBe, Long> {

List<ForwardToDeptUsersBe> findById(Long id);

List<ForwardToDeptUsersBe> findByStatus(String status);

List<ForwardToDeptUsersBe> findByStatChgDate(Date statChgDate);

List<ForwardToDeptUsersBe> findByLuForwardToDeptCd(LuForwardToDeptBe luForwardToDeptCd);

List<ForwardToDeptUsersBe> findByUsersId(UsersBe usersId);

}