/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.LuPaymentRequisitionStatBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface LuPaymentRequisitionStatBeRepo extends JpaRepository<LuPaymentRequisitionStatBe, String> {

List<LuPaymentRequisitionStatBe> findByCode(String code);

List<LuPaymentRequisitionStatBe> findByDescription(String description);
}