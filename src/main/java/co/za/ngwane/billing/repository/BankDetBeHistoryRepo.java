/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.BankDetBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.BankDetBeHistory;

public interface BankDetBeHistoryRepo extends JpaRepository<BankDetBeHistory, BankDetBeHistoryPK> {

List<BankDetBeHistory> findByRevtype(Short revtype);

List<BankDetBeHistory> findByAccountName(String accountName);

List<BankDetBeHistory> findByAccountNo(String accountNo);

List<BankDetBeHistory> findByBranchCd(String branchCd);

List<BankDetBeHistory> findByLuAccountTypeCd(String luAccountTypeCd);

List<BankDetBeHistory> findByName(String name);

List<BankDetBeHistory> findByStatChgD(Date statChgD);

List<BankDetBeHistory> findByStatus(String status);

List<BankDetBeHistory> findByRevinfo(Revinfo revinfo);

}