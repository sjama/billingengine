/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;


import co.za.ngwane.billing.db.LuDeliveryNoteStatBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface LuDeliveryNoteStatBeRepo extends JpaRepository<LuDeliveryNoteStatBe, String> {

List<LuDeliveryNoteStatBe> findByCode(String code);

List<LuDeliveryNoteStatBe> findByDescription(String description);

}