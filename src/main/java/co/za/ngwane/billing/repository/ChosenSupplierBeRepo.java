/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;


import co.za.ngwane.billing.db.ChosenSupplierBe;
import co.za.ngwane.billing.db.ProcurementBe;
import co.za.ngwane.billing.db.ProcurementSupplierBe;
import co.za.ngwane.billing.db.UsersBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ChosenSupplierBeRepo extends JpaRepository<ChosenSupplierBe, Long> {

List<ChosenSupplierBe> findById(Long id);

List<ChosenSupplierBe> findByStatus(String status);

List<ChosenSupplierBe> findByStatChgDate(Date statChgDate);

List<ChosenSupplierBe> findByApprovedDate(Date approvedDate);

List<ChosenSupplierBe> findByRefNo(String refNo);

List<ChosenSupplierBe> findByProcurementId(ProcurementBe procurementId);

List<ChosenSupplierBe> findByProcurementSupplierId(ProcurementSupplierBe procurementSupplierId);

List<ChosenSupplierBe> findByApprovedByUserId(UsersBe approvedByUserId);

}