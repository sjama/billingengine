/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository.service;

import co.za.ngwane.billing.db.DocReposBe;
import co.za.ngwane.billing.repository.DocRepoRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocRepoRepoService {

    @Autowired
    public DocRepoRepo docRepoRepo;

    public DocReposBe findById(Long id) {
        return docRepoRepo.findOne(id);
    }

    public List<DocReposBe> getAllByRefNo(String refNo) {
        return docRepoRepo.findByRefNo(refNo);
    }

    public List<DocReposBe> getAll(String refNo) {
        return docRepoRepo.findAll();
    }
    
    public byte[] downloadDocument(Long id) {
        return docRepoRepo.findOne(id).getFileData();
    }

}
