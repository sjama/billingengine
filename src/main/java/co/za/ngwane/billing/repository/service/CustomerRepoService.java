/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository.service;

import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.repository.CustomerRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jamasithole
 */
@Service
public class CustomerRepoService {

    @Autowired
    CustomerRepo customerRepo;

    public CustomersBe getById(Long id) {
        return customerRepo.findOne(id);
    }

    public CustomersBe save(CustomersBe customersBe) {
        return customerRepo.save(customersBe);
    }

    public List<CustomersBe> getAll() {
        return customerRepo.findAll();
    }
}
