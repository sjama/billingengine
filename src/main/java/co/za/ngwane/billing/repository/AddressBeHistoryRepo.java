/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.AddressBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.AddressBeHistory;

public interface AddressBeHistoryRepo extends JpaRepository<AddressBeHistory, AddressBeHistoryPK> {

List<AddressBeHistory> findByRevtype(Short revtype);

List<AddressBeHistory> findByPhyAddrCity(String phyAddrCity);

List<AddressBeHistory> findByPhyAddrCode(String phyAddrCode);

List<AddressBeHistory> findByPhyAddrLine1(String phyAddrLine1);

List<AddressBeHistory> findByPhyAddrLine2(String phyAddrLine2);

List<AddressBeHistory> findByPhyAddrLine3(String phyAddrLine3);

List<AddressBeHistory> findByPhyAddrSuburb(String phyAddrSuburb);

List<AddressBeHistory> findByPosAddrCity(String posAddrCity);

List<AddressBeHistory> findByPosAddrCode(String posAddrCode);

List<AddressBeHistory> findByPosAddrLine1(String posAddrLine1);

List<AddressBeHistory> findByPosAddrLine2(String posAddrLine2);

List<AddressBeHistory> findByPosAddrLine3(String posAddrLine3);

List<AddressBeHistory> findByPosAddrSuburb(String posAddrSuburb);

List<AddressBeHistory> findByStatChgD(Date statChgD);

List<AddressBeHistory> findByStatus(String status);

List<AddressBeHistory> findByLuAddressTypeCd(String luAddressTypeCd);

List<AddressBeHistory> findByRevinfo(Revinfo revinfo);

}