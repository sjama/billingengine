/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ExtentionsBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.ExtentionsBeHistory;

public interface ExtentionsBeHistoryRepo extends JpaRepository<ExtentionsBeHistory, ExtentionsBeHistoryPK> {

    List<ExtentionsBeHistory> findByRevtype(Short revtype);

    List<ExtentionsBeHistory> findByStatChgD(Date statChgD);

    List<ExtentionsBeHistory> findByStatus(String status);

    List<ExtentionsBeHistory> findByBranchCd(String branchCd);

    List<ExtentionsBeHistory> findByRevinfo(Revinfo revinfo);

}