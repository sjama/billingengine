/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.InvoiceBe;

/**
 *
 * @author jamasithole
 */
public interface InvoiceBeRepo extends JpaRepository<InvoiceBe, String>{
	//ArrayList<BillDetBe>  findByOriginatingNoAndCallDateBeforeAndCallDaeAfter(String originatingNo, Date fromD, Date toD);
    List<InvoiceBe> findByCustomersId(CustomersBe customersId);
    InvoiceBe findByInvNo(String invNo);

    List<InvoiceBe> findByCdrFileRefNo(String fileRefNo);
}
