package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.UsersBe;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserBeRepo extends JpaRepository<UsersBe, Integer> {
    //UsersBe findByUsername(String username);
    List<UsersBe> findByStatusNot(String stat);
}
