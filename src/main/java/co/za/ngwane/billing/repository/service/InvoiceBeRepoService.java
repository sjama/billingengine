/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository.service;

import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.InvoiceBe;
import co.za.ngwane.billing.repository.DocRepoRepo;
import co.za.ngwane.billing.repository.InvoiceBeRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jamasithole
 */
@Service
public class InvoiceBeRepoService {

    @Autowired
    InvoiceBeRepo invoiceRepo;
     @Autowired
    CustomerRepoService customerRepoService;

    @Autowired
    DocRepoRepo docRepoRepo;

    public InvoiceBe getById(String id) {
        return invoiceRepo.findOne(id);
    }
    public InvoiceBe save(InvoiceBe customersBe) {
        return invoiceRepo.save(customersBe);
    }

    public List<InvoiceBe> getAll() {
        return invoiceRepo.findAll();
    }

    public List<InvoiceBe> findByCustomersId(Long customersId) {
        CustomersBe customersBe = customerRepoService.getById(customersId);
        return invoiceRepo.findByCustomersId(customersBe);
    }

    public byte[] downloadInvoice(Long id) {
        return docRepoRepo.findOne(id).getFileData();
    }
    
    public InvoiceBe getByInvNo(String  invNo) {
        return invoiceRepo.findByInvNo(invNo);
    }
}
