/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.SuppliersBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface SuppliersBeRepo extends JpaRepository<SuppliersBe, Long> {

List<SuppliersBe> findById(Long id);

List<SuppliersBe> findByComName(String comName);

List<SuppliersBe> findByTradeName(String tradeName);

List<SuppliersBe> findBySupplierNo(String supplierNo);

List<SuppliersBe> findByRegNo(String regNo);

List<SuppliersBe> findByVatRegNo(String vatRegNo);

List<SuppliersBe> findByRefNo(String refNo);

List<SuppliersBe> findByBankDetId(int bankDetId);

List<SuppliersBe> findByAddressId(long addressId);

List<SuppliersBe> findByContactPersonId(long contactPersonId);

List<SuppliersBe> findByStatus(String status);

List<SuppliersBe> findByStatChgD(Date statChgD);

}