/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.Revinfo;
import co.za.ngwane.billing.db.UsersBeHistory;
import co.za.ngwane.billing.db.UsersBeHistoryPK;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersBeHistoryRepo extends JpaRepository<UsersBeHistory, UsersBeHistoryPK> {

    List<UsersBeHistory> findByRevtype(Short revtype);

    List<UsersBeHistory> findByStatChgD(Date statChgD);

    List<UsersBeHistory> findByStatus(String status);

    List<UsersBeHistory> findByUserId(Integer userId);

    List<UsersBeHistory> findByCustomersId(BigInteger customersId);

    List<UsersBeHistory> findByLuRolesCd(String luRolesCd);

    List<UsersBeHistory> findByRevinfo(Revinfo revinfo);

}
