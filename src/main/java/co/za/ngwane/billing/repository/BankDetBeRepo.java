/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.BankDetBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface BankDetBeRepo extends JpaRepository<BankDetBe, Integer> {

List<BankDetBe> findById(Integer id);

List<BankDetBe> findByName(String name);

List<BankDetBe> findByAccountNo(String accountNo);

List<BankDetBe> findByAccountName(String accountName);

List<BankDetBe> findByLuAccountTypeCd(String luAccountTypeCd);

List<BankDetBe> findByBranchCd(String branchCd);

List<BankDetBe> findByStatus(String status);

List<BankDetBe> findByStatChgD(Date statChgD);

}