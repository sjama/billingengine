/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.AgreedRatesBe;
import co.za.ngwane.billing.db.AgreedRatesPK;
import co.za.ngwane.billing.db.CustomersBe;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bheki.lubisi
 */
public interface AgreedRatesRepo extends JpaRepository<AgreedRatesBe, AgreedRatesPK> {

    List<AgreedRatesBe> findByCustomersBeAndStatusNot(CustomersBe customersBe, String stat);
    List<AgreedRatesBe> findByStatusNot(String status);
    
}
