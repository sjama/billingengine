/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.PurchaseOrderBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.PurchaseOrderBeHistory;

public interface PurchaseOrderBeHistoryRepo extends JpaRepository<PurchaseOrderBeHistory, PurchaseOrderBeHistoryPK> {

List<PurchaseOrderBeHistory> findByRevtype(Short revtype);

List<PurchaseOrderBeHistory> findByPoNo(String poNo);

List<PurchaseOrderBeHistory> findByRefNo(String refNo);

List<PurchaseOrderBeHistory> findByStatChgDate(Date statChgDate);

List<PurchaseOrderBeHistory> findByStatus(String status);

List<PurchaseOrderBeHistory> findByGeneratedByUserId(Integer generatedByUserId);

List<PurchaseOrderBeHistory> findByProcurementId(BigInteger procurementId);

List<PurchaseOrderBeHistory> findByRevinfo(Revinfo revinfo);

}