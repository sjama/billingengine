/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.LuReminderPeriodBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface LuReminderPeriodBeRepo extends JpaRepository<LuReminderPeriodBe, String> {

List<LuReminderPeriodBe> findByCode(String code);

List<LuReminderPeriodBe> findByName(String name);

List<LuReminderPeriodBe> findByTimeValue(Float timeValue);

}