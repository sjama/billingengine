/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.IcasaRatesBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.IcasaRatesBeHistory;

public interface IcasaRatesBeHistoryRepo extends JpaRepository<IcasaRatesBeHistory, IcasaRatesBeHistoryPK> {

    List<IcasaRatesBeHistory> findByRevtype(Short revtype);

    List<IcasaRatesBeHistory> findByBillRates(Float billRates);

    List<IcasaRatesBeHistory> findByDescription(String description);

    List<IcasaRatesBeHistory> findByNeoRates(Float neoRates);

    List<IcasaRatesBeHistory> findByStatChgD(Date statChgD);

    List<IcasaRatesBeHistory> findByStatus(String status);

    List<IcasaRatesBeHistory> findByRevinfo(Revinfo revinfo);

}