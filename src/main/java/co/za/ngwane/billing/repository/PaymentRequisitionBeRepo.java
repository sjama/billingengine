/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.PaymentRequisitionBe;
import co.za.ngwane.billing.db.PurchaseOrderBe;
import co.za.ngwane.billing.db.UsersBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;


public interface PaymentRequisitionBeRepo extends JpaRepository<PaymentRequisitionBe, Long> {

List<PaymentRequisitionBe> findById(Long id);

List<PaymentRequisitionBe> findByLuPayRequisitionProcessStatCd(String luPayRequisitionProcessStatCd);

List<PaymentRequisitionBe> findByRefNo(String refNo);

List<PaymentRequisitionBe> findByAmountRequested(Float amountRequested);

List<PaymentRequisitionBe> findByServiceDesc(String serviceDesc);

List<PaymentRequisitionBe> findByCreatedDate(Date createdDate);

List<PaymentRequisitionBe> findByStatus(String status);

List<PaymentRequisitionBe> findByStatChgDate(Date statChgDate);

List<PaymentRequisitionBe> findByPurchaseOrderId(PurchaseOrderBe purchaseOrderId);

List<PaymentRequisitionBe> findByCreatedByUserId(UsersBe createdByUserId);
}