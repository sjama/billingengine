/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository.service;

import co.za.ngwane.billing.db.AgreedRatesBe;
import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.repository.AgreedRatesRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AgreedRatesRepoService {

    @Autowired
    AgreedRatesRepo agreedRatesRepo;

    @Autowired
    CustomerRepoService customerRepoService;

    public List<AgreedRatesBe> getAllRates() {
        return agreedRatesRepo.findAll();
    }

    public List<AgreedRatesBe> getAllActive(String stat) {
        return agreedRatesRepo.findByStatusNot(stat);
    }

    public AgreedRatesBe save(AgreedRatesBe agreedRates) {
        return agreedRatesRepo.save(agreedRates);
    }

    public List<AgreedRatesBe> findByCustomersBeAndStatusNot(CustomersBe customersBe, String stat) {
        return agreedRatesRepo.findByCustomersBeAndStatusNot(customersBe, stat);
    }


}
