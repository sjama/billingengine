/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;


import co.za.ngwane.billing.db.LuAddressTypeBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface LuAddressTypeBeRepo extends JpaRepository<LuAddressTypeBe, String> {

List<LuAddressTypeBe> findByCode(String code);

List<LuAddressTypeBe> findByDescription(String description);

}