/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.CustomersBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.CustomersBeHistory;

public interface CustomersBeHistoryRepo extends JpaRepository<CustomersBeHistory, CustomersBeHistoryPK> {

    List<CustomersBeHistory> findByRevtype(Short revtype);

    List<CustomersBeHistory> findByAccountNo(String accountNo);

    List<CustomersBeHistory> findByAgreedRate(Float agreedRate);

    List<CustomersBeHistory> findByCustVatNo(String custVatNo);

    List<CustomersBeHistory> findByLogoRefn(String logoRefn);

    List<CustomersBeHistory> findByName(String name);

    List<CustomersBeHistory> findByOriginatingNo(String originatingNo);

    List<CustomersBeHistory> findByPhyAddrCity(String phyAddrCity);

    List<CustomersBeHistory> findByPhyAddrCode(String phyAddrCode);

    List<CustomersBeHistory> findByPhyAddrLine1(String phyAddrLine1);

    List<CustomersBeHistory> findByPhyAddrLine2(String phyAddrLine2);

    List<CustomersBeHistory> findByPhyAddrLine3(String phyAddrLine3);

    List<CustomersBeHistory> findByPhyAddrSuburb(String phyAddrSuburb);

    List<CustomersBeHistory> findByPosAddrCity(String posAddrCity);

    List<CustomersBeHistory> findByPosAddrCode(String posAddrCode);

    List<CustomersBeHistory> findByPosAddrLine1(String posAddrLine1);

    List<CustomersBeHistory> findByPosAddrLine2(String posAddrLine2);

    List<CustomersBeHistory> findByPosAddrLine3(String posAddrLine3);

    List<CustomersBeHistory> findByPosAddrSuburb(String posAddrSuburb);

    List<CustomersBeHistory> findByStatChgD(Date statChgD);

    List<CustomersBeHistory> findByStatus(String status);

    List<CustomersBeHistory> findByLuCustTypeCd(String luCustTypeCd);

    List<CustomersBeHistory> findByRevinfo(Revinfo revinfo);

}