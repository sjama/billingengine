/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.LuReminderPeriodBe;
import co.za.ngwane.billing.db.OrderFormBe;
import co.za.ngwane.billing.db.UsersBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface OrderFormBeRepo extends JpaRepository<OrderFormBe, Long> {

List<OrderFormBe> findById(Long id);

List<OrderFormBe> findByOrderNo(String orderNo);

List<OrderFormBe> findByProjId(int projId);

List<OrderFormBe> findByProjName(String projName);

List<OrderFormBe> findByProjNo(String projNo);

List<OrderFormBe> findByOrderDesc(String orderDesc);

List<OrderFormBe> findByRequestDate(Date requestDate);

List<OrderFormBe> findByDeliveryDate(Date deliveryDate);

List<OrderFormBe> findByNoDaysLeft(Integer noDaysLeft);

List<OrderFormBe> findByRefNo(String refNo);

List<OrderFormBe> findByStatus(String status);

List<OrderFormBe> findByStatChgDate(Date statChgDate);

List<OrderFormBe> findByCreatedDate(Date createdDate);

List<OrderFormBe> findByContactPersonId(long contactPersonId);

List<OrderFormBe> findByLuReminderPeriodCd(LuReminderPeriodBe luReminderPeriodCd);

List<OrderFormBe> findByCustomersId(CustomersBe customersId);

List<OrderFormBe> findByCapturedByUsersId(UsersBe capturedByUsersId);

List<OrderFormBe> findByRequestedByUsersId(UsersBe requestedByUsersId);

}