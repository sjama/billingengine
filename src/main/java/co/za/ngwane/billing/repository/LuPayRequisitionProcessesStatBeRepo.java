/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.LuPayRequisitionProcessesStatBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LuPayRequisitionProcessesStatBeRepo extends JpaRepository<LuPayRequisitionProcessesStatBe, String> {

List<LuPayRequisitionProcessesStatBe> findByCode(String code);

List<LuPayRequisitionProcessesStatBe> findByDescription(String description);

}