/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;


import co.za.ngwane.billing.db.AddressBe;
import co.za.ngwane.billing.db.LuAddressTypeBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface AddressBeRepo extends JpaRepository<AddressBe, Long> {

List<AddressBe> findById(Long id);

List<AddressBe> findByPhyAddrLine1(String phyAddrLine1);

List<AddressBe> findByPhyAddrLine2(String phyAddrLine2);

List<AddressBe> findByPhyAddrLine3(String phyAddrLine3);

List<AddressBe> findByPhyAddrSuburb(String phyAddrSuburb);

List<AddressBe> findByPhyAddrCity(String phyAddrCity);

List<AddressBe> findByPhyAddrCode(String phyAddrCode);

List<AddressBe> findByPosAddrLine1(String posAddrLine1);

List<AddressBe> findByPosAddrLine2(String posAddrLine2);

List<AddressBe> findByPosAddrLine3(String posAddrLine3);

List<AddressBe> findByPosAddrSuburb(String posAddrSuburb);

List<AddressBe> findByPosAddrCity(String posAddrCity);

List<AddressBe> findByPosAddrCode(String posAddrCode);

List<AddressBe> findByStatus(String status);

List<AddressBe> findByStatChgD(Date statChgD);

List<AddressBe> findByLuAddressTypeCd(LuAddressTypeBe luAddressTypeCd);

}