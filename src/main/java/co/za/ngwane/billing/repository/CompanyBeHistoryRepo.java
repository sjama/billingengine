/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.CompanyBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.CompanyBeHistory;

public interface CompanyBeHistoryRepo extends JpaRepository<CompanyBeHistory, CompanyBeHistoryPK> {

List<CompanyBeHistory> findByRevtype(Short revtype);

List<CompanyBeHistory> findByBankAccNo(String bankAccNo);

List<CompanyBeHistory> findByBankBranch(String bankBranch);

List<CompanyBeHistory> findByBankBranchCd(String bankBranchCd);

List<CompanyBeHistory> findByBankName(String bankName);

List<CompanyBeHistory> findByEmail(String email);

List<CompanyBeHistory> findByFaxNo(String faxNo);

List<CompanyBeHistory> findByLogoRefn(String logoRefn);

List<CompanyBeHistory> findByName(String name);

List<CompanyBeHistory> findByPhyAddrCity(String phyAddrCity);

List<CompanyBeHistory> findByPhyAddrCode(String phyAddrCode);

List<CompanyBeHistory> findByPhyAddrLine1(String phyAddrLine1);

List<CompanyBeHistory> findByPhyAddrLine2(String phyAddrLine2);

List<CompanyBeHistory> findByPhyAddrLine3(String phyAddrLine3);

List<CompanyBeHistory> findByPhyAddrSuburb(String phyAddrSuburb);

List<CompanyBeHistory> findByPosAddrCity(String posAddrCity);

List<CompanyBeHistory> findByPosAddrCode(String posAddrCode);

List<CompanyBeHistory> findByPosAddrLine1(String posAddrLine1);

List<CompanyBeHistory> findByPosAddrLine2(String posAddrLine2);

List<CompanyBeHistory> findByPosAddrLine3(String posAddrLine3);

List<CompanyBeHistory> findByPosAddrSuburb(String posAddrSuburb);

List<CompanyBeHistory> findByRegNo(String regNo);

List<CompanyBeHistory> findByStat(String stat);

List<CompanyBeHistory> findByStatChgD(Date statChgD);

List<CompanyBeHistory> findByTelNo(String telNo);

List<CompanyBeHistory> findByVatNo(String vatNo);

List<CompanyBeHistory> findByWebUrl(String webUrl);

List<CompanyBeHistory> findByRevinfo(Revinfo revinfo);

}