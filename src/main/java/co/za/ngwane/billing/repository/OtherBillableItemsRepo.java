/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.OtherBillItemsBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author bheki.lubisi
 */
public interface OtherBillableItemsRepo extends JpaRepository<OtherBillItemsBe, String> {

    List<OtherBillItemsBe> findByCustomersId(CustomersBe customersBe);

}
