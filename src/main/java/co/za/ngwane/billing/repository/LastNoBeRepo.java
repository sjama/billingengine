/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import co.za.ngwane.billing.db.LuLastNoTypeBe;

import co.za.ngwane.billing.db.LastNoBe;

public interface LastNoBeRepo extends JpaRepository<LastNoBe, Integer> {

    List<LastNoBe> findById(Integer id);

    List<LastNoBe> findByNo(long no);

    List<LastNoBe> findByStatus(String status);

    List<LastNoBe> findByStatChgD(Date statChgD);

    LastNoBe findByLastNoTypeCd(LuLastNoTypeBe lastNoTypeCd);

}