/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ContactPersonBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ContactPersonBeRepo extends JpaRepository<ContactPersonBe, Long> {

List<ContactPersonBe> findById(Long id);

List<ContactPersonBe> findByName(String name);

List<ContactPersonBe> findBySurname(String surname);

List<ContactPersonBe> findByCellNo(String cellNo);

List<ContactPersonBe> findByPhoneNo(String phoneNo);

List<ContactPersonBe> findByEmail(String email);

List<ContactPersonBe> findByStatus(String status);

List<ContactPersonBe> findByStatChgDate(Date statChgDate);

}