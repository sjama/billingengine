/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.CallTypeBe;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bheki.lubisi
 */
public interface CallTypeRepo extends JpaRepository<CallTypeBe, String> {
    
}
