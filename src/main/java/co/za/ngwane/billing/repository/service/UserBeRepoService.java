/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository.service;

import co.za.ngwane.billing.db.UsersBe;
import co.za.ngwane.billing.repository.UserBeRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jamasithole
 */
@Service
public class UserBeRepoService {

    @Autowired
    UserBeRepo userRepo;

    public UsersBe getById(Integer id) {
        return userRepo.findOne(id);
    }

    public UsersBe save(UsersBe usersBe) {
        return userRepo.save(usersBe);
    }

    public List<UsersBe> getAll() {
        return userRepo.findAll();
    }

//    public UsersBe findByUsername(String username) {
//        return userRepo.findByUsername(username);
//    }

    public List<UsersBe> getAllActive(String stat) {
        return userRepo.findByStatusNot(stat);
    }

}
