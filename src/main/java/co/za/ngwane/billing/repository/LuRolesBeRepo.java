/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.LuRolesBe;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bheki.lubisi
 */
public interface LuRolesBeRepo extends JpaRepository<LuRolesBe, String> {
    //public LuCustTypeBe findByCode(String code);
    
}
