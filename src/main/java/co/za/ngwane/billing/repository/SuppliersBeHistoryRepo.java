/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.SuppliersBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.SuppliersBeHistory;

public interface SuppliersBeHistoryRepo extends JpaRepository<SuppliersBeHistory, SuppliersBeHistoryPK> {

List<SuppliersBeHistory> findByRevtype(Short revtype);

List<SuppliersBeHistory> findByAddressId(BigInteger addressId);

List<SuppliersBeHistory> findByBankDetId(Integer bankDetId);

List<SuppliersBeHistory> findByComName(String comName);

List<SuppliersBeHistory> findByContactPersonId(BigInteger contactPersonId);

List<SuppliersBeHistory> findByRefNo(String refNo);

List<SuppliersBeHistory> findByRegNo(String regNo);

List<SuppliersBeHistory> findByStatChgD(Date statChgD);

List<SuppliersBeHistory> findByStatus(String status);

List<SuppliersBeHistory> findBySupplierNo(String supplierNo);

List<SuppliersBeHistory> findByTradeName(String tradeName);

List<SuppliersBeHistory> findByVatRegNo(String vatRegNo);

List<SuppliersBeHistory> findByRevinfo(Revinfo revinfo);

}