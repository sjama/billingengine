/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import co.za.ngwane.billing.db.Revinfo;

public interface RevinfoRepo extends JpaRepository<Revinfo, Integer> {

    List<Revinfo> findByRev(Integer rev);

    List<Revinfo> findByRevtstmp(BigInteger revtstmp);

}