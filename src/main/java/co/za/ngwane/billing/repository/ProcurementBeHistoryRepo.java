/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ProcurementBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.ProcurementBeHistory;

public interface ProcurementBeHistoryRepo extends JpaRepository<ProcurementBeHistory, ProcurementBeHistoryPK> {

    List<ProcurementBeHistory> findByRevtype(Short revtype);

    List<ProcurementBeHistory> findByActualDeliveryDate(Integer actualDeliveryDate);

    List<ProcurementBeHistory> findByBudget(Float budget);

    List<ProcurementBeHistory> findByCodSupplier(String codSupplier);

    List<ProcurementBeHistory> findByCreatedDate(Date createdDate);

    List<ProcurementBeHistory> findByCreditAvail(Float creditAvail);

    List<ProcurementBeHistory> findByCreditLimit(Float creditLimit);

    List<ProcurementBeHistory> findByExpectedDeliveryDate(Date expectedDeliveryDate);

    List<ProcurementBeHistory> findByExpectedTurnAroundTime(String expectedTurnAroundTime);

    List<ProcurementBeHistory> findByRefNo(String refNo);

    List<ProcurementBeHistory> findByStatChgDate(Date statChgDate);

    List<ProcurementBeHistory> findByStatus(String status);

    List<ProcurementBeHistory> findByOrderId(BigInteger orderId);

    List<ProcurementBeHistory> findByRevinfo(Revinfo revinfo);

}