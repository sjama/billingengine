/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ProcurementBe;
import co.za.ngwane.billing.db.ProcurementSupplierBe;
import co.za.ngwane.billing.db.SuppliersBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ProcurementSupplierBeRepo extends JpaRepository<ProcurementSupplierBe, Long> {

List<ProcurementSupplierBe> findById(Long id);

List<ProcurementSupplierBe> findBySupplierAmount(Float supplierAmount);

List<ProcurementSupplierBe> findByNoOfDays(Integer noOfDays);

List<ProcurementSupplierBe> findByRefNo(String refNo);

List<ProcurementSupplierBe> findByCreatedDate(Date createdDate);

List<ProcurementSupplierBe> findByStatus(String status);

List<ProcurementSupplierBe> findByStatChgDate(Date statChgDate);

List<ProcurementSupplierBe> findByProcurementId(ProcurementBe procurementId);

List<ProcurementSupplierBe> findBySuppliersId(SuppliersBe suppliersId);

}