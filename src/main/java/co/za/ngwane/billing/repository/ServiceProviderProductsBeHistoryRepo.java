/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ServiceProviderProductsBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.ServiceProviderProductsBeHistory;

public interface ServiceProviderProductsBeHistoryRepo extends JpaRepository<ServiceProviderProductsBeHistory, ServiceProviderProductsBeHistoryPK> {

    List<ServiceProviderProductsBeHistory> findByRevtype(Short revtype);

    List<ServiceProviderProductsBeHistory> findByDescription(String description);

    List<ServiceProviderProductsBeHistory> findByPriceAmount(Float priceAmount);

    List<ServiceProviderProductsBeHistory> findByLuServiceProviderBecode(String luServiceProviderBecode);

    List<ServiceProviderProductsBeHistory> findByRevinfo(Revinfo revinfo);

}