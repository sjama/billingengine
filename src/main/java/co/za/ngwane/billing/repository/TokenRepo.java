package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.BillingUserToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepo extends JpaRepository<BillingUserToken, String> {
    
     BillingUserToken findByUserLogin(String login);
}
