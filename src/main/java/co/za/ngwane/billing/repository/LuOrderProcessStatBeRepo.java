/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.LuOrderProcessStatBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface LuOrderProcessStatBeRepo extends JpaRepository<LuOrderProcessStatBe, String> {

List<LuOrderProcessStatBe> findByCode(String code);

List<LuOrderProcessStatBe> findByDescription(String description);

}