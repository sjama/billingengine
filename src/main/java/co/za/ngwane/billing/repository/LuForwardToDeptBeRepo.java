/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;


import co.za.ngwane.billing.db.LuForwardToDeptBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LuForwardToDeptBeRepo extends JpaRepository<LuForwardToDeptBe, String> {

    List<LuForwardToDeptBe> findByCode(String code);

    List<LuForwardToDeptBe> findByDescription(String description);
}