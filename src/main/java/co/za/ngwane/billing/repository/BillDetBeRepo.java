/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.za.ngwane.billing.db.BillDetBe;
import co.za.ngwane.billing.db.CustomersBe;

/**
 *
 * @author jamasithole
 */
public interface BillDetBeRepo extends JpaRepository<BillDetBe, Long>{
	ArrayList<BillDetBe>  findByCustomersIdAndCallDateAfterAndCallDateBefore(CustomersBe customersId, Date fromD, Date toD);
	List<BillDetBe> findByCdrFileRefNo(String fileRefNo);
}
