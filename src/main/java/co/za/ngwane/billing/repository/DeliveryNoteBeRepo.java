/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;


import co.za.ngwane.billing.db.DeliveryNoteBe;
import co.za.ngwane.billing.db.PurchaseOrderBe;
import co.za.ngwane.billing.db.UsersBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;


public interface DeliveryNoteBeRepo extends JpaRepository<DeliveryNoteBe, Long> {

List<DeliveryNoteBe> findById(Long id);

List<DeliveryNoteBe> findByCreatedDate(Date createdDate);

List<DeliveryNoteBe> findByRefNo(String refNo);

List<DeliveryNoteBe> findByCreatedByUserId(UsersBe createdByUserId);

List<DeliveryNoteBe> findByPurchaseOrderId(PurchaseOrderBe purchaseOrderId);

}