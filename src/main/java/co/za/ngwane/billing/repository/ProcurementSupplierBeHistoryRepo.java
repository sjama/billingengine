/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ProcurementSupplierBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.ProcurementSupplierBeHistory;

public interface ProcurementSupplierBeHistoryRepo extends JpaRepository<ProcurementSupplierBeHistory, ProcurementSupplierBeHistoryPK> {

    List<ProcurementSupplierBeHistory> findByRevtype(Short revtype);

    List<ProcurementSupplierBeHistory> findByCreatedDate(Date createdDate);

    List<ProcurementSupplierBeHistory> findByNoOfDays(Integer noOfDays);

    List<ProcurementSupplierBeHistory> findByRefNo(String refNo);

    List<ProcurementSupplierBeHistory> findByStatChgDate(Date statChgDate);

    List<ProcurementSupplierBeHistory> findByStatus(String status);

    List<ProcurementSupplierBeHistory> findBySupplierAmount(Float supplierAmount);

    List<ProcurementSupplierBeHistory> findByProcurementId(BigInteger procurementId);

    List<ProcurementSupplierBeHistory> findBySuppliersId(BigInteger suppliersId);

    List<ProcurementSupplierBeHistory> findByRevinfo(Revinfo revinfo);

}