/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.OrderFormBe;
import co.za.ngwane.billing.db.ProcurementBe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ProcurementBeRepo extends JpaRepository<ProcurementBe, Long> {

List<ProcurementBe> findById(Long id);

List<ProcurementBe> findByCreditAvail(Float creditAvail);

List<ProcurementBe> findByCreditLimit(Float creditLimit);

List<ProcurementBe> findByCodSupplier(String codSupplier);

List<ProcurementBe> findByBudget(Float budget);

List<ProcurementBe> findByExpectedTurnAroundTime(String expectedTurnAroundTime);

List<ProcurementBe> findByRefNo(String refNo);

List<ProcurementBe> findByStatus(String status);

List<ProcurementBe> findByStatChgDate(Date statChgDate);

List<ProcurementBe> findByCreatedDate(Date createdDate);

List<ProcurementBe> findByExpectedDeliveryDate(Date expectedDeliveryDate);

List<ProcurementBe> findByActualDeliveryDate(Integer actualDeliveryDate);

List<ProcurementBe> findByOrderId(OrderFormBe orderId);

}