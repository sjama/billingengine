/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository.service;

import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.db.OriginatingLineBe;
import co.za.ngwane.billing.repository.OriginatingLineRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jamasithole
 */
@Service
public class OriginatingLinesRepoService {

    @Autowired
    OriginatingLineRepo lineRepo;

    @Autowired
    CustomerRepoService customerRepoService;

    public List<OriginatingLineBe> getAllLines() {
        return lineRepo.findAll();
    }

    public List<OriginatingLineBe> getAllActive(String stat) {
        return lineRepo.findByStatusNot(stat);
    }

    public OriginatingLineBe save(OriginatingLineBe originatingLine) {
        return lineRepo.save(originatingLine);
    }

    public List<OriginatingLineBe> findByCustomersId(int customersId) {
        CustomersBe customersBe = customerRepoService.getById(Long.valueOf(customersId));
        return lineRepo.findBycustomersId(customersBe);
    }


}
