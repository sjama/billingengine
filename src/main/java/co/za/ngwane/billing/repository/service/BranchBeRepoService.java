/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.ngwane.billing.repository.service;

import co.za.ngwane.billing.db.BranchBe;
import co.za.ngwane.billing.db.CustomersBe;
import co.za.ngwane.billing.repository.BranchBeRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jamasithole
 */
@Service
public class BranchBeRepoService {

    @Autowired
    BranchBeRepo branchRepo;

    public BranchBe getById(String id) {
        return branchRepo.findOne(id);
    }

    public BranchBe save(BranchBe customersBe) {
        return branchRepo.save(customersBe);
    }

    public List<BranchBe> getAll() {
        return branchRepo.findAll();
    }

    public List<BranchBe> findByCustomersId(CustomersBe customersId) {
        return branchRepo.findByCustomersId(customersId);
    }
}
