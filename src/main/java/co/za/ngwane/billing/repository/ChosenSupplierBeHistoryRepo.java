/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.ChosenSupplierBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.ChosenSupplierBeHistory;

public interface ChosenSupplierBeHistoryRepo extends JpaRepository<ChosenSupplierBeHistory, ChosenSupplierBeHistoryPK> {

List<ChosenSupplierBeHistory> findByRevtype(Short revtype);

List<ChosenSupplierBeHistory> findByApprovedDate(Date approvedDate);

List<ChosenSupplierBeHistory> findByRefNo(String refNo);

List<ChosenSupplierBeHistory> findByStatChgDate(Date statChgDate);

List<ChosenSupplierBeHistory> findByStatus(String status);

List<ChosenSupplierBeHistory> findByApprovedByUserId(Integer approvedByUserId);

List<ChosenSupplierBeHistory> findByProcurementId(BigInteger procurementId);

List<ChosenSupplierBeHistory> findByProcurementSupplierId(BigInteger procurementSupplierId);

List<ChosenSupplierBeHistory> findByRevinfo(Revinfo revinfo);

}