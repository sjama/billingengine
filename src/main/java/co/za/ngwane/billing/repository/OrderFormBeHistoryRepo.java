/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.OrderFormBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.OrderFormBeHistory;

public interface OrderFormBeHistoryRepo extends JpaRepository<OrderFormBeHistory, OrderFormBeHistoryPK> {

    List<OrderFormBeHistory> findByRevtype(Short revtype);

    List<OrderFormBeHistory> findByContactPersonId(BigInteger contactPersonId);

    List<OrderFormBeHistory> findByCreatedDate(Date createdDate);

    List<OrderFormBeHistory> findByDeliveryDate(Date deliveryDate);

    List<OrderFormBeHistory> findByNoDaysLeft(Integer noDaysLeft);

    List<OrderFormBeHistory> findByOrderDesc(String orderDesc);

    List<OrderFormBeHistory> findByOrderNo(String orderNo);

    List<OrderFormBeHistory> findByProjId(Integer projId);

    List<OrderFormBeHistory> findByProjName(String projName);

    List<OrderFormBeHistory> findByProjNo(String projNo);

    List<OrderFormBeHistory> findByRefNo(String refNo);

    List<OrderFormBeHistory> findByRequestDate(Date requestDate);

    List<OrderFormBeHistory> findByStatChgDate(Date statChgDate);

    List<OrderFormBeHistory> findByStatus(String status);

    List<OrderFormBeHistory> findByCapturedByUsersId(Integer capturedByUsersId);

    List<OrderFormBeHistory> findByCustomersId(BigInteger customersId);

    List<OrderFormBeHistory> findByLuReminderPeriodCd(String luReminderPeriodCd);

    List<OrderFormBeHistory> findByRequestedByUsersId(Integer requestedByUsersId);

    List<OrderFormBeHistory> findByRevinfo(Revinfo revinfo);

}