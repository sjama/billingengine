/*
 * The BhekzinWay of doing things.
 */
package co.za.ngwane.billing.repository;

import co.za.ngwane.billing.db.PaymentRequisitionBeHistoryPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import co.za.ngwane.billing.db.Revinfo;

import co.za.ngwane.billing.db.PaymentRequisitionBeHistory;

public interface PaymentRequisitionBeHistoryRepo extends JpaRepository<PaymentRequisitionBeHistory, PaymentRequisitionBeHistoryPK> {

    List<PaymentRequisitionBeHistory> findByRevtype(Short revtype);

    List<PaymentRequisitionBeHistory> findByAmountRequested(Float amountRequested);

    List<PaymentRequisitionBeHistory> findByCreatedDate(Date createdDate);

    List<PaymentRequisitionBeHistory> findByLuPayRequisitionProcessStatCd(String luPayRequisitionProcessStatCd);

    List<PaymentRequisitionBeHistory> findByRefNo(String refNo);

    List<PaymentRequisitionBeHistory> findByServiceDesc(String serviceDesc);

    List<PaymentRequisitionBeHistory> findByStatChgDate(Date statChgDate);

    List<PaymentRequisitionBeHistory> findByStatus(String status);

    List<PaymentRequisitionBeHistory> findByCreatedByUserId(Integer createdByUserId);

    List<PaymentRequisitionBeHistory> findByPurchaseOrderId(BigInteger purchaseOrderId);

    List<PaymentRequisitionBeHistory> findByRevinfo(Revinfo revinfo);

}