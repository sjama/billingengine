package co.za.ngwane.billing.rest.provider;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestProvider {

	public static ResponseEntity<String> getRequest(
			String url, HttpEntity<?> requestEntity, Class<String> responseType) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
		return response;

	}
	
	public static ResponseEntity<String> postRequest(
			String url, HttpEntity<?> requestEntity, Class<String> responseType) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
		return response;

	}

}
